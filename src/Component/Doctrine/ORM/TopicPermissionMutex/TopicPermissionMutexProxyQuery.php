<?php

namespace Component\Doctrine\ORM\TopicPermissionMutex;

use AppBundle\Repository\TopicPermissionMutexRepository;
use Component\Doctrine\ORM\SortFixProxyQuery;
use Component\Doctrine\ORM\DoctrineHiddenFieldsCorrector;
use Doctrine\ORM\QueryBuilder;
use Doctrine\ORM\Query;
use Doctrine\ORM\Query\Expr;

/**
 * Class TopicPermissionMutexProxyQuery
 * @package Component\Doctrine\ORM\TopicPermissionMutex
 */
class TopicPermissionMutexProxyQuery extends SortFixProxyQuery
{
    /**
     * @inheritdoc
     */
    protected function getHiddenFixedQueryBuilder(QueryBuilder $queryBuilder)
    {
        $queryBuilderId = clone $queryBuilder;

        // 1. Getting the page data rows ids
        //
        // -------- Doctrine hidden fields correction begins below v:
        // Remember hidden fields of original Query object:
        $corrector = new DoctrineHiddenFieldsCorrector();
        $corrector->remember($queryBuilderId);

        $queryBuilderId->resetDQLPart('select');
        $queryBuilderId->addSelect(
            'DISTINCT ' .
            TopicPermissionMutexRepository::ALIAS_TOPIC_GROUP_PERMISSIONS . '.id AS topicGroupPermissionId, ' .
            TopicPermissionMutexRepository::ALIAS_TOPIC_MODERATORS . '.id AS topicModeratorId'
        );

        // Restore hidden fields of original Query object:
        $corrector->restore();
        unset($corrector);
        // -------- Doctrine hidden fields correction terminates above ^.

        if ($this->getSortBy()) {
            $sortBy = $this->getSortBy();
            if (strpos($sortBy, '.') === false) { // add the current alias
                $sortBy = $queryBuilderId->getRootAlias() . '.' . $sortBy;
            }
            $sortBy .= ' AS __order_by';
            $queryBuilderId->addSelect($sortBy);
        }

        $results = $queryBuilderId->getQuery()->execute(array(), Query::HYDRATE_ARRAY);

        $topicGroupPermissions = [];
        $topicModerators = [];
        foreach ($results as $result) {
            $topicGroupPermissionId = $result['topicGroupPermissionId'];
            $topicModeratorId = $result['topicModeratorId'];
            if (null !== $topicGroupPermissionId) {
                $topicGroupPermissions[] = $topicGroupPermissionId;
            }
            if (null !== $topicModeratorId) {
                $topicModerators[] = $topicModeratorId;
            }
        }

        // 2. Fixing query builder object
        //
        $orX = $queryBuilder->expr()->orX();
        if (!empty($topicGroupPermissions)) {
            $orX->add(
                $queryBuilder->expr()->in(
                    TopicPermissionMutexRepository::ALIAS_TOPIC_GROUP_PERMISSIONS . '.id',
                    ':topicGroupPermissions'
                )
            );
            $queryBuilder->setParameter('topicGroupPermissions', $topicGroupPermissions);
        }
        if (!empty($topicModerators)) {
            $orX->add(
                $queryBuilder->expr()->in(
                    TopicPermissionMutexRepository::ALIAS_TOPIC_MODERATORS . '.id',
                    ':topicModerators'
                )
            );
            $queryBuilder->setParameter('topicModerators', $topicModerators);
        }
        $queryBuilder
            ->andWhere($orX)
            ->setMaxResults(null)
            ->setFirstResult(null);

        return $queryBuilder;
    }
}
