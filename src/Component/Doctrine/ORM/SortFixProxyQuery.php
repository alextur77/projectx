<?php

namespace Component\Doctrine\ORM;

use Doctrine\ORM\QueryBuilder;

/**
 * Class SortFixProxyQuery
 * @package Component\Doctrine\ORM
 */
class SortFixProxyQuery extends HiddenProxyQuery
{
    /**
     * @var string[]
     */
    private $sortFixFields = [];

    /**
     * SortFixProxyQuery constructor.
     *
     * @param QueryBuilder $queryBuilder
     * @param string[]    $sortFixFields
     */
    public function __construct($queryBuilder, $sortFixFields = [])
    {
        parent::__construct($queryBuilder);
        $this->setSortFixFields($sortFixFields);
    }

    /**
     * {@inheritdoc}
     */
    public function setSortBy($parentAssociationMappings, $fieldMapping)
    {
        if (in_array($fieldMapping['fieldName'], $this->sortFixFields)) {
            $fieldMapping['fieldName'] = 'id';
        }
        parent::setSortBy($parentAssociationMappings, $fieldMapping);

        return $this;
    }

    /**
     * @return string[]
     */
    public function getSortFixFields()
    {
        return $this->sortFixFields;
    }

    /**
     * @param string[] $sortFixFields
     *
     * @return SortFixProxyQuery
     */
    public function setSortFixFields($sortFixFields)
    {
        $this->sortFixFields = $sortFixFields;

        return $this;
    }
}
