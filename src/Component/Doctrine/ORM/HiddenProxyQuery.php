<?php

namespace Component\Doctrine\ORM;

use Doctrine\ORM\QueryBuilder;
use Doctrine\ORM\Query;
use Sonata\DoctrineORMAdminBundle\Datagrid\ProxyQuery as BaseProxyQuery;

/**
 * Class HiddenProxyQuery
 * @package AppBundle\Datagrid
 */
class HiddenProxyQuery extends BaseProxyQuery
{
    /**
     * {@inheritdoc}
     *
     * Original code of this function was extracted from Sonata\DoctrineORMAdminBundle\Datagrid\ProxyQuery::execute
     * and modified to call HiddenProxyQuery::getHiddenFixedQueryBuilder()
     * instead of original ProxyQuery::getFixedQueryBuilder().
     */
    public function execute(array $params = array(), $hydrationMode = null)
    {
        // always clone the original queryBuilder
        $queryBuilder = clone $this->queryBuilder;

        // todo : check how doctrine behave, potential SQL injection here ...
        if ($this->getSortBy()) {
            $sortBy = $this->getSortBy();
            if (strpos($sortBy, '.') === false) { // add the current alias
                $sortBy = $queryBuilder->getRootAlias() . '.' . $sortBy;
            }
            $queryBuilder->addOrderBy($sortBy, $this->getSortOrder());
        } else {
            $queryBuilder->resetDQLPart('orderBy');
        }

        return $this->getHiddenFixedQueryBuilder($queryBuilder)->getQuery()->execute($params, $hydrationMode);
    }

    /**
     * This method alters the query to return a clean set of object with a working set of Object.
     *
     * @param QueryBuilder $queryBuilder
     *
     * @return QueryBuilder
     * @throws \Doctrine\Common\Persistence\Mapping\MappingException
     *
     * Original code of this function was extracted from Sonata\DoctrineORMAdminBundle\Datagrid\ProxyQuery::getFixedQueryBuilder
     * and modified to allow doctrine HIDDEN keyword in a custom query in function Admin::createQuery().
     * The function Sonata\DoctrineORMAdminBundle\Datagrid\ProxyQuery::getFixedQueryBuilder is declared as private.
     * So it is impossible to override it. That is why it is necessary to declare a new function getHiddenFixedQueryBuilder
     * instead of Sonata\DoctrineORMAdminBundle\Datagrid\ProxyQuery::getFixedQueryBuilder.
     */
    protected function getHiddenFixedQueryBuilder(QueryBuilder $queryBuilder)
    {
        $queryBuilderId = clone $queryBuilder;

        // step 1 : retrieve the targeted class
        $from = $queryBuilderId->getDQLPart('from');
        $class = $from[0]->getFrom();
        $metadata = $queryBuilderId->getEntityManager()->getMetadataFactory()->getMetadataFor($class);

        // step 2 : retrieve identifier columns
        $idNames = $metadata->getIdentifierFieldNames();

        // step 3 : retrieve the different subjects ids
        $selects = array();
        $idxSelect = '';
        foreach ($idNames as $idName) {
            $select = sprintf('%s.%s', $queryBuilderId->getRootAlias(), $idName);
            // Put the ID select on this array to use it on results QB
            $selects[$idName] = $select;
            // Use IDENTITY if id is a relation too. See: http://doctrine-orm.readthedocs.org/en/latest/reference/dql-doctrine-query-language.html
            // Should work only with doctrine/orm: ~2.2
            $idSelect = $select;
            if ($metadata->hasAssociation($idName)) {
                $idSelect = sprintf('IDENTITY(%s) as %s', $idSelect, $idName);
            }
            $idxSelect .= ($idxSelect !== '' ? ', ' : '') . $idSelect;
        }

        // -------- Doctrine hidden fields correction begins below v:
        // Remember hidden fields of original Query object:
        $corrector = new DoctrineHiddenFieldsCorrector();
        $corrector->remember($queryBuilderId);

        // This was the original code line:
        $queryBuilderId->resetDQLPart('select');
        // This was the original code line:
        $queryBuilderId->add('select', 'DISTINCT ' . $idxSelect);

        // Restore hidden fields of original Query object:
        $corrector->restore();
        unset($corrector);
        // -------- Doctrine hidden fields correction terminates above ^.

        // for SELECT DISTINCT, ORDER BY expressions must appear in idxSelect list
        /* Consider
            SELECT DISTINCT x FROM tab ORDER BY y;
        For any particular x-value in the table there might be many different y
        values.  Which one will you use to sort that x-value in the output?
        */
        // todo : check how doctrine behave, potential SQL injection here ...
        if ($this->getSortBy()) {
            $sortBy = $this->getSortBy();
            if (strpos($sortBy, '.') === false) { // add the current alias
                $sortBy = $queryBuilderId->getRootAlias() . '.' . $sortBy;
            }
            $sortBy .= ' AS __order_by';
            $queryBuilderId->addSelect($sortBy);
        }

        $results = $queryBuilderId->getQuery()->execute(array(), Query::HYDRATE_ARRAY);

        $idxMatrix = array();
        foreach ($results as $id) {
            foreach ($idNames as $idName) {
                $idxMatrix[$idName][] = $id[$idName];
            }
        }

        // step 4 : alter the query to match the targeted ids
        foreach ($idxMatrix as $idName => $idx) {
            if (count($idx) > 0) {
                $idxParamName = sprintf('%s_idx', $idName);
                $idxParamName = preg_replace('/[^\w]+/', '_', $idxParamName);
                $queryBuilder->andWhere(sprintf('%s IN (:%s)', $selects[$idName], $idxParamName));
                $queryBuilder->setParameter($idxParamName, $idx);
                $queryBuilder->setMaxResults(null);
                $queryBuilder->setFirstResult(null);
            }
        }

        return $queryBuilder;
    }
}
