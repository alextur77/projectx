<?php

namespace Component\Doctrine\ORM;

/**
 * Class DoctrineHiddenFieldsCorrector
 * @package Component\Doctrine\ORM
 */
class DoctrineHiddenFieldsCorrector
{
    private $object;
    /** @var array $hiddenFields */
    private $hiddenFields;

    /**
     * Remember hidden fields of the original Query object.
     *
     * @param $object
     */
    public function remember(&$object)
    {
        if (is_object($object)) {
            unset($this->hiddenFields);
            $this->hiddenFields = array();
            $this->object = $object;

            // Remember hidden fields of the original Query object:
            /** @var array $DQLPartsSelect */
            $DQLPartsSelect = $this->object->getDQLPart('select');
            /** @var \Doctrine\ORM\Query\Expr\Select $DQLPart */
            foreach ($DQLPartsSelect as $DQLPart) {
                /** @var array $parts */
                $parts = $DQLPart->getParts();
                $pos = stripos($parts[0], "HIDDEN");
                if (false !== $pos) {
                    $this->hiddenFields[] = $parts[0];
                }
            }
        }
    }

    /**
     * Restore hidden fields of the original Query object.
     */
    public function restore()
    {
        if (is_object($this->object)) {
            // Restore hidden fields of the original Query object:
            foreach ($this->hiddenFields as $hiddenSelect) {
                $this->object->addSelect($hiddenSelect);
            }
        }
    }
}
