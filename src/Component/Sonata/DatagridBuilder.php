<?php

namespace Component\Sonata;

use Component\Doctrine\ORM\HiddenProxyQuery;
use Sonata\AdminBundle\Admin\AdminInterface;
use Sonata\AdminBundle\Datagrid\Datagrid;
use Sonata\DoctrineORMAdminBundle\Builder\DatagridBuilder as BaseDatagridBuilder;
use Sonata\DoctrineORMAdminBundle\Datagrid\ProxyQuery;

/**
 * Class DatagridBuilder
 * @package Component\Sonata
 */
class DatagridBuilder extends BaseDatagridBuilder
{
    /**
     * {@inheritdoc}
     *
     * Original code of this function was extracted from Sonata\DoctrineORMAdminBundle\Builder\DatagridBuilder
     * and modified to allow GROUP BY keyword in a custom query in function Admin::createQuery().
     */
    public function getBaseDatagrid(AdminInterface $admin, array $values = array())
    {
        /** @var \Sonata\AdminBundle\Admin\Admin $admin */
        $pager = $this->getPager($admin->getPagerType());
        $pager->setCountColumn($admin->getModelManager()->getIdentifierFieldNames($admin->getClass()));

        $defaultOptions = array();
        if ($this->csrfTokenEnabled) {
            $defaultOptions['csrf_protection'] = false;
        }

        $formBuilder = $this->formFactory->createNamedBuilder('filter', 'form', array(), $defaultOptions);

        /** @var ProxyQuery $originalProxyQuery */
        $originalProxyQuery = $admin->createQuery();
        if ($originalProxyQuery instanceof HiddenProxyQuery) {
            $proxyQuery = $originalProxyQuery;
        } else {
            $proxyQuery = new HiddenProxyQuery($originalProxyQuery->getQueryBuilder());
        }

        return new Datagrid($proxyQuery, $admin->getList(), $pager, $formBuilder, $values);
    }

    /**
     * @inheritdoc
     */
    protected function getPager($pagerType)
    {
        if (Pager::TYPE_DEFAULT === $pagerType) {
            /** @var \Component\Sonata\Pager $pager - This is important! */
            $pager = new Pager();
        } else {
            $pager = parent::getPager($pagerType);
        }
        return $pager;
    }
}
