<?php

namespace Component\Sonata\TopicPermissionMutex;

use Component\Sonata\DatagridBuilder as BaseDatagridBuilder;

/**
 * Class DatagridBuilder
 * @package Component\Sonata
 */
class DatagridBuilder extends BaseDatagridBuilder
{
    /**
     * @inheritdoc
     */
    protected function getPager($pagerType)
    {
        if (Pager::TYPE_DEFAULT === $pagerType) {
            /** @var \Component\Sonata\TopicPermissionMutex\Pager $pager - This is important! */
            $pager = new Pager();
        } else {
            $pager = parent::getPager($pagerType);
        }
        return $pager;
    }
}
