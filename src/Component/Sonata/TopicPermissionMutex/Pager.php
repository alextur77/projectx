<?php

namespace Component\Sonata\TopicPermissionMutex;

use AppBundle\Repository\TopicPermissionMutexRepository;
use Doctrine\ORM\Query;
use Doctrine\ORM\QueryBuilder;
use Sonata\DoctrineORMAdminBundle\Datagrid\Pager as BasePager;

/**
 * Class Pager
 * @package Component\Sonata
 */
class Pager extends BasePager
{
    /**
     * {@inheritdoc}
     *
     * Original code of this function was extracted from Sonata\DoctrineORMAdminBundle\Datagrid\Pager
     * and modified to allow GROUP BY keyword in a custom query in function Admin::createQuery().
     */
    public function computeNbResult()
    {
        /** @var QueryBuilder $countQuery */
        $countQuery = clone $this->getQuery();

        if (count($this->getParameters()) > 0) {
            $countQuery->setParameters($this->getParameters());
        }

        $countQuery
            ->select(
                sprintf(
                    'count(COALESCE(%s.id, 0)) as cnt',
                    TopicPermissionMutexRepository::ALIAS_GROUPS
                )
            )
            ->resetDQLPart('orderBy')
            ->resetDQLPart('groupBy');

        return $countQuery->getQuery()->getSingleScalarResult();
    }
}
