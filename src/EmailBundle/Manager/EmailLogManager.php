<?php

namespace EmailBundle\Manager;

use EmailBundle\Entity\EmailLog;
use Aristek\Component\ORM\AbstractEntityManager;

/**
 * Class EmailLogManager
 * EmailBundle\Manager
 *
 * @method EmailLog create()
 */
class EmailLogManager extends AbstractEntityManager
{
    /**
     * @param string $recipient
     * @param string $subject
     * @param string $body
     * @param string $status
     * @param string $errMessage
     *
     * @return void
     */
    public function logEmail($recipient, $subject, $body, $status, $errMessage)
    {
        $log = $this->create();
        $log
            ->setRecipient($recipient)
            ->setSubject($subject)
            ->setBody($body)
            ->setDate(new \DateTime())
            ->setStatus($status)
            ->setErrmessage($errMessage);

        $this->save($log);
    }
}
