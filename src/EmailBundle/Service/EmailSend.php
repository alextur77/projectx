<?php

namespace EmailBundle\Service;

use Symfony\Bridge\Monolog\Logger;
use EmailBundle\Manager\EmailLogManager;

/**
 * Class EmailSend
 *
 * EmailBundle\Service
 */
class EmailSend
{
    /**
     * @var
     */
    protected $fromEmailAddress;

    /**
     * @var EmailLogManager
     */
    protected $emailLogManager;

    /**
     * @var Logger
     */
    protected $logger;

    /**
     * @var \Swift_Mailer
     */
    protected $swiftEmail;

    /**
     * EmailSend constructor.
     *
     * @param string $fromEmailAddress
     */
    public function __construct($fromEmailAddress)
    {
        $this->fromEmailAddress = $fromEmailAddress;
    }

    /**
     * @param EmailLogManager $emailLogManager
     *
     * @return $this
     */
    public function setEmailLogManager($emailLogManager)
    {
        $this->emailLogManager = $emailLogManager;

        return $this;
    }

    /**
     * @param Logger $logger
     *
     * @return $this
     */
    public function setLog($logger)
    {
        $this->logger = $logger;

        return $this;
    }

    /**
     * @param \Swift_Mailer $swiftEmail
     *
     * @return $this
     */
    public function setSwiftMailer($swiftEmail)
    {
        $this->swiftEmail = $swiftEmail;

        return $this;
    }

    /**
     * @param string $recipient
     * @param string $subject
     * @param string $body
     *
     * @return void
     */
    public function sendEmail($recipient, $subject, $body)
    {
        $message = \Swift_Message::newInstance()
            ->setSubject($subject)
            ->setFrom($this->fromEmailAddress)
            ->setTo($recipient)
            ->setBody($body);

        $this->swiftEmail->send($message, $error);

        if (empty($error)) {

            $status = 'S';
            $errMessage = 'All is fine';
        } else {

            $status = 'E';
            $errMessage = 'Error during sending to recipient - ' . $recipient . '. Email was not send';
        }

        $this->emailLogManager->logEmail($recipient, $subject, $body, $status, $errMessage);

        $message = 'Recipient: ' . $recipient . ', Subject: ' . $subject . ', Body: ' . $body . ', Status: ' . $status . ',' . 'Error: ' . $errMessage;

        $this->logger->error($message);
    }
}