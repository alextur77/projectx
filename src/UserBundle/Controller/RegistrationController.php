<?php

namespace UserBundle\Controller;

use AppBundle\Entity\RegistrationInvite;
use AppBundle\Entity\Role;
use AppBundle\Entity\User;
use FOS\UserBundle\Controller\RegistrationController as BaseController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Cookie;

/**
 * Class RegistrationController
 * @package UserBundle\Controller
 */
class RegistrationController extends BaseController
{
    /**
     * @return RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function registerAction()
    {
        $response = parent::registerAction();
        if ($response instanceof RedirectResponse) {
            // The registration form is submitted,
            // so remove the token for the registration invite to the organization/groups:
            $request = $this->container->get('request_stack')->getCurrentRequest();
            $cookies = $request->cookies;
            $token = $cookies->has('token') ? $cookies->get('token') : null;
            if ($token) {
                /** @var User $user */
                $user = $this->container->get('fos_user.registration.form')->getData();
                $this->container->get('app.registration_invite_manager')->setUserForToken($token, $user);
                $response->headers->clearCookie('token');
                $inviteGroup = $this->container->get('app.registration_invite_repository')->findOneBy(
                    ['user' => $user]
                );

                if ($inviteGroup->getFlagAdmin() == RegistrationInvite::FLAG_ADMIN_Y) {
                    $this->container->get('app.group_user_permission_manager')->registerUser(
                        $inviteGroup->getGroup(),
                        $user,
                        Role::ROLE_GROUP_ADMIN
                    );
                    $group = $this->container->get('app.group_repository')->findOneBy(
                        ['id' => $inviteGroup->getGroup()]
                    );
                    $organization = $this->container->get('app.organization_repository')->findOneBy(
                        ['id' => $group->getOrganization()]
                    );
                    $this->container->get('app.organization_user_permission_manager')->registerUser(
                        $organization,
                        $user,
                        Role::ROLE_ORGANIZATION_USER
                    );
                }

                $group = $this->container->get('app.group_repository')->findOneBy(['id' => $inviteGroup->getGroup()]);

                $this->container->get('app.user_manager')->updateUserOrganization(
                    $user->getId(),
                    $this->container->get(
                        'app.organization_repository'
                    )->findOneBy(
                        [
                            'id' => $group->getOrganization()
                        ]
                    )->getName()
                );
            }
        }

        return $response;
    }

    /**
     * @param string $token
     *
     * @return RedirectResponse
     */
    public function confirmAction($token)
    {
        $response = parent::confirmAction($token);
        $request = $this->container->get('request_stack')->getCurrentRequest();
        // Set the token to continue to the registration invite to the organization/groups:
        if (null !== $request) {
            $cookieToken = $request->query->get('tkn');
            if ($cookieToken) {
                $response->headers->setCookie(new Cookie('token', $cookieToken, 0, '/', null, false, false));
            }
        }

        return $response;
    }
}
