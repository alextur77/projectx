<?php

namespace UserBundle\Form\Handler;

use FOS\UserBundle\Form\Handler\RegistrationFormHandler as BaseHandler;
use AppBundle\Manager\RegistrationInviteManager;

class RegistrationFormHandler extends BaseHandler
{
    /**
     * @var RegistrationInviteManager
     */
    protected $registrationInviteManager;

    /**
     * @inheritdoc
     */
    public function process($confirmation = false)
    {
        $result = parent::process($confirmation);
        if (!$result) {
            $cookies = $this->request->cookies;
            $token = $cookies->has('token') ? $cookies->get('token') : null;
            if ($token) {
                $email = $this->registrationInviteManager->getCanonicalEmailByToken($token);
                $this->form->get('email')->setData($email);
            }
        }

        return $result;
    }

    /**
     * @param RegistrationInviteManager $registrationInviteManager
     *
     * @return RegistrationFormHandler
     */
    public function setRegistrationInviteManager($registrationInviteManager)
    {
        $this->registrationInviteManager = $registrationInviteManager;

        return $this;
    }
}
