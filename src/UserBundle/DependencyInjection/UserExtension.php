<?php

namespace UserBundle\DependencyInjection;

use Aristek\Component\DependencyInjection\Loader\YamlFileLoader;
use Symfony\Component\HttpKernel\DependencyInjection\Extension;
use Symfony\Component\DependencyInjection\ContainerBuilder;

/**
 * Class UserExtension
 * @package UserBundle\DependencyInjection
 */
class UserExtension extends Extension
{
    /**
     * @inheritdoc
     */
    public function load(array $configs, ContainerBuilder $container)
    {
        $loader = new YamlFileLoader($container, __DIR__ . '/../Resources/config');
        $loader->loadAll();
    }
}
