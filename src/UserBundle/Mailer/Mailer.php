<?php

namespace UserBundle\Mailer;

use FOS\UserBundle\Mailer\Mailer as BaseMailer;
use FOS\UserBundle\Model\UserInterface;
use Symfony\Bundle\FrameworkBundle\Templating\EngineInterface;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * Class Mailer
 * @package UserBundle\Mailer
 */
class Mailer extends BaseMailer
{
    /**
     * @var Request
     */
    protected $request;

    /**
     * @param RequestStack $requestStack
     *
     * @inheritdoc
     */
    public function __construct(
        RequestStack $requestStack,
        $mailer,
        RouterInterface $router,
        EngineInterface $templating,
        array $parameters)
    {
        parent::__construct($mailer, $router, $templating, $parameters);
        $this->request = $requestStack->getCurrentRequest();
    }

    /**
     * @inheritdoc
     */
    public function sendConfirmationEmailMessage(UserInterface $user)
    {
        $template = $this->parameters['confirmation.template'];
        $parameters = ['token' => $user->getConfirmationToken()];
        // Extracting the token for the registration invite to the organization/groups:
        $cookies = $this->request->cookies;
        $cookieToken = $cookies->has('token') ? $cookies->get('token') : null;
        if (null !== $cookieToken) {
            $parameters['tkn'] = $cookieToken;
        }
        $url = $this->router->generate('fos_user_registration_confirm', $parameters, true);
        $rendered = $this->templating->render(
            $template,
            ['user' => $user, 'confirmationUrl' => $url]
        );
        $this->sendEmailMessage($rendered, $this->parameters['from_email']['confirmation'], $user->getEmail());
    }
}
