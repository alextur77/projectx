<?php

namespace SmsBundle\Service;

use Symfony\Bridge\Monolog\Logger;
use SmsBundle\Manager\SmsLogManager;

/**
 * Class SmsSend
 *
 * SmsBundle\Service
 */
class SmsSend
{
    /**
     * @var
     */
    protected $login;

    /**
     * @var
     */
    protected $api;

    /**
     * @var
     */
    protected $sender;

    /**
     * @var SmsLogManager
     */
    protected $smsLogManager;

    /**
     * @var Logger
     */
    protected $logger;

    /**
     * SmsSend constructor.
     *
     * @param string $userLogin
     * @param string $apiKey
     * @param string $smsSender
     */
    public function __construct($userLogin, $apiKey, $smsSender)
    {
        $this->login = $userLogin;
        $this->api = $apiKey;
        $this->sender = $smsSender;
    }

    /**
     *
     */
    const url = 'http://cp.websms.by';

    /**
     *
     */
    const time_out = 15;

    /**
     *
     */
    const func = 'msg_send_bulk';

    /**
     * @param string $recipient
     * @param string $message
     *
     * @return string
     */
    public function sendSms($recipient, $message)
    {
        $smsMass = [
            'recipient' => $recipient,
            'message' => $message,
            'sender' => $this->sender
        ];

        $massiveToSend = [$smsMass];

        $status = 'F';
        $errMessage = 'Not send';

        $ch = curl_init(); //init cURL

        if (!empty($massiveToSend)) {

            $rawData = json_encode($massiveToSend); //making JSON

            //set params CURL
            curl_setopt_array(
                $ch,
                [
                    CURLOPT_URL => self::url,
                    CURLOPT_FAILONERROR => 1,
                    CURLOPT_RETURNTRANSFER => 1,
                    CURLOPT_TIMEOUT => self::time_out,
                    CURLOPT_CONNECTTIMEOUT => 0,
                    CURLOPT_POST => 1,  //send POST
                    CURLOPT_POSTFIELDS =>
                        [
                            'r' => 'api/' . self::func,     //set function to send message
                            'user' => $this->login,  //user login
                            'apikey' => $this->api,        //API-key
                            'messages' => $rawData
                        ]
                ]
            );

            $result = curl_exec($ch);
            curl_close($ch);

            if (empty($result)) {

                $errMessage = 'Http error.';
                $status = 'H';
            } else {

                $result = json_decode($result);

                if ($result->error == 1) {

                    $errMessage = $result->message;
                    $status = 'A';
                } else {

                    $errMessage = 'All is fine';
                    $status = 'S';
                }
            }

            $this->smsLogManager->log($smsMass['recipient'], $smsMass['message'], $status, $errMessage);

            $message = 'Phone number: ' . $smsMass['recipient'] . ', Message: ' . $smsMass['message'] . ', Status: ' . $status . ',' . 'Error: ' . $errMessage;

            $this->logger->error($message);
        } else {

            $message = 'Phone number: ' . $smsMass['recipient'] . ', Message: ' . $smsMass['message'] . ', Status: ' . $status . ',' . 'Error: ' . $errMessage;

            $this->logger->error($message);
        }

        return $status;
    }

    /**
     * @param SmsLogManager $smsLogManager
     *
     * @return SmsSend
     */
    public function setSmsLogManager($smsLogManager)
    {
        $this->smsLogManager = $smsLogManager;

        return $this;
    }

    /**
     * @param Logger $log
     *
     * @return $this
     */
    public function setLog($log)
    {
        $this->logger = $log;

        return $this;
    }
}