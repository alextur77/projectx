<?php

namespace SmsBundle\Manager;

use SmsBundle\Entity\SmsLog;
use Aristek\Component\ORM\AbstractEntityManager;

/**
 * Class SmsLogManager
 * SmsBundle\Manager
 *
 * @method SmsLog create()
 */
class SmsLogManager extends AbstractEntityManager
{
    /**
     * @param string $recipient
     * @param string $message
     * @param string $status
     * @param string $errMessage
     *
     * @return void
     */
    public function log($recipient, $message, $status, $errMessage)
    {
        $log = $this->create();
        $log
            ->setPhone($recipient)
            ->setMessage($message)
            ->setDate(new \DateTime())
            ->setStatus($status)
            ->setErrmessage($errMessage);

        $this->save($log);
    }
}
