<?php

namespace QueueBundle\Service;

use EmailBundle\Service\EmailSend;
use QueueBundle\Repository\QueueRepository;
use QueueBundle\Manager\QueueManager;
use SmsBundle\Service\SmsSend;

/**
 * Class Queue
 *
 * QueueBundle\Service
 */
class Queue
{
    /**
     *
     */
    const TYPE_EMAIL = 'email';

    /**
     *
     */
    const TYPE_PHONE = 'phone';

    /**
     *
     */
    const LIMIT_NUMBER = 100;

    /**
     * @var QueueRepository;
     */
    protected $queueRepository;

    /**
     * @var SmsSend
     */
    protected $sendSms;

    /**
     * @var \Swift_Mailer
     */
    protected $swiftEmail;

    /**
     * @var QueueManager
     */
    protected $queryManager;

    /**
     * @var EmailSend
     */
    protected $emailSend;

    /**
     * @param string $type
     *
     * @return bool
     */
    public function executeSend($type)
    {
        switch ($type) {
            case self::TYPE_PHONE:
                $this->sendSms();
                break;
            case self::TYPE_EMAIL:
                $this->sendEmail();
                break;
            default:
                $this->sendSms();
                $this->sendEmail();
                break;
        }

        return true;
    }

    /**
     * @param QueueRepository $queueRepository
     *
     * @return $this
     */
    public function setQueue($queueRepository)
    {
        $this->queueRepository = $queueRepository;

        return $this;
    }

    /**
     * @param SmsSend $smsSend
     *
     * @return $this
     */
    public function setSmsSend($smsSend)
    {
        $this->sendSms = $smsSend;

        return $this;
    }

    /**
     * @param \Swift_Mailer $swiftEmail
     *
     * @return $this
     */
    public function setSwiftMailer($swiftEmail)
    {
        $this->swiftEmail = $swiftEmail;

        return $this;
    }

    /**
     * @param QueueManager $queueManager
     *
     * @return $this
     */
    public function setQueueManager($queueManager)
    {
        $this->queryManager = $queueManager;

        return $this;
    }

    /**
     * @param EmailSend $emailSend
     *
     * @return $this
     */
    public function setEmailSend($emailSend)
    {
        $this->emailSend = $emailSend;

        return $this;
    }

    /**
     * @return bool
     * @throws \Exception
     */
    public function sendSms()
    {
        $queue = $this->queueRepository->findByType(self::TYPE_PHONE, self::LIMIT_NUMBER);

        foreach ($queue as $key => $value) {

            $recipient = $value->getRecipient();
            $message = $value->getBody();
            $this->sendSms->sendSms($recipient, $message);
        }

        $this->queryManager->remove($queue, true);

        return true;
    }

    /**
     * @return bool
     */
    public function sendEmail()
    {
        $queue = $this->queueRepository->findByType(self::TYPE_EMAIL, self::LIMIT_NUMBER);

        foreach ($queue as $key => $value) {

            $recipient = $value->getRecipient();
            $body = $value->getBody();
            $subject = $value->getSubject();

            $this->emailSend->sendEmail($recipient, $subject, $body);
        }

        $this->queryManager->remove($queue, true);

        return true;
    }
}