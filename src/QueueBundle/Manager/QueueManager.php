<?php

namespace QueueBundle\Manager;

use QueueBundle\Entity\Queue;
use Aristek\Component\ORM\AbstractEntityManager;

/**
 * Class QueueManager
 *
 * QueueBundle\Manager
 *
 * @method Queue create()
 */
class QueueManager extends AbstractEntityManager
{
    /**
     * @param string $recipient
     * @param string $message
     * @param string $subject
     * @param string $type
     *
     * @return void
     */
    public function createQueue($recipient, $message, $subject, $type)
    {
        $queue = $this->create();
        $queue->setRecipient($recipient);
        $queue->setBody($message);
        $queue->setSubject($subject);
        $queue->setType($type);

        $this->save($queue);
    }

}