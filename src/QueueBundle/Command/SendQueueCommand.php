<?php

namespace QueueBundle\Command;

use QueueBundle\Service\Queue;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Input\InputArgument;

/**
 * Class SendQueueCommand
 *
 * QueueBundle\Command
 */
class SendQueueCommand extends ContainerAwareCommand
{
    /**
     * @return void
     */
    protected function configure()
    {
        $this
            ->setName('app:send-queue')
            ->setDescription('Send queue')
            ->setHelp('This command allows you to send queue from database')
            ->addArgument('type', InputArgument::OPTIONAL, 'The type of message');
    }

    /**
     * @param InputInterface  $input
     * @param OutputInterface $output
     *
     * @return void
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $type = $input->getArgument('type');

        $prepareParams = $this->getContainer()->get('queue.queue');

        $prepareParams->executeSend($type);

        if ($type == Queue::TYPE_PHONE) {

            $output->writeln('Your queue were send');
        } elseif ($type == Queue::TYPE_EMAIL) {

            $output->writeln('Your emails were send');
        } elseif (empty($type)) {

            $output->writeln('The full queue was send');
        } else {

            $output->writeln('To send queue you must enter either "phone" - to generate sms, or "email" to generate email or press "enter" to generate full queue from "email" and "phone"');
        }
    }
}