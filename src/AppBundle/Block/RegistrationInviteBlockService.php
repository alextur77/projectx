<?php

namespace AppBundle\Block;

use AppBundle\Entity\User;
use AppBundle\Repository\RegistrationInviteRepository;
use AppBundle\Repository\UserRepository;
use AppBundle\Manager\RegistrationInviteManager;
use AppBundle\Admin\UserRegistrationInviteAdmin;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\Flash\FlashBag;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Symfony\Bundle\FrameworkBundle\Templating\EngineInterface;
use Sonata\BlockBundle\Block\BaseBlockService;
use Sonata\BlockBundle\Block\BlockContextInterface;

/**
 * Class RegistrationInviteBlockService
 */
class RegistrationInviteBlockService extends BaseBlockService
{
    /**
     * @var Request
     */
    protected $request;

    /**
     * @var RegistrationInviteManager
     */
    protected $registrationInviteManager;

    /**
     * @var UserRegistrationInviteAdmin
     */
    protected $admin;

    /**
     * @var UserRepository
     */
    protected $userRepository;

    /**
     * @var RegistrationInviteRepository
     */
    protected $registrationInviteRepository;

    /**
     * RegistrationInviteBlockService constructor.
     *
     * @param string          $name
     * @param EngineInterface $templating
     * @param RequestStack    $requestStack
     */
    public function __construct($name, EngineInterface $templating, RequestStack $requestStack)
    {
        parent::__construct($name, $templating);

        $this->request = $requestStack->getCurrentRequest();
    }

    /**
     * {@inheritdoc}
     */
    public function setDefaultSettings(OptionsResolverInterface $resolver)
    {
        parent::setDefaultSettings($resolver);

        $resolver->setDefaults(
            [
                'title' => 'Registration Invite Confirmation',
                'template' => ':Admin/RegistrationInvite:BlockInvite.html.twig'
            ]
        );
    }

    /**
     * {@inheritdoc}
     */
    public function execute(BlockContextInterface $blockContext, Response $response = null)
    {
        $currUser = $this->getCurrentUser();
        $settings = $blockContext->getSettings();
        $parameters = ['block' => $blockContext->getBlock(), 'settings' => $settings];

        $cookies = $this->request->cookies;
        $token = $cookies->has('token') ? $cookies->get('token') : null;
        if ($token) {
            $user = $this->registrationInviteManager->getUserByToken($token);
            if (null === $user) {
                $email = $this->registrationInviteManager->getCanonicalEmailByToken($token);
                if (null === $email) {
                    $this->sendMessage('sonata_flash_error', 'registration_invite.token.invalid_token');
                } else {
                    $user = $this->userRepository->findUserByCanonicalEmail($email);
                    if (null === $user) {
                        // The registration invite is for an unknown user.
                        $this->sendMessage('sonata_flash_error', 'registration_invite.token.unknown_user');
                    } else {
                        $this->registrationInviteManager->setUserForToken($token, $user);
                    }
                }
            }

            if (null !== $user) {
                // The cookie "token" was possibly set when the user was logged off.
                // It is necessary to insure that the logged on user is that one to whom the invitation email was sent.
                if ($user->getId() === $currUser->getId()) {
                    if ($this->registrationInviteRepository->isConfirmed($token)) {
                        $this->sendMessage('sonata_flash_info', 'registration_invite.token.already_processed');
                    } else {
                        $parameters['fullName'] = $currUser->getFullName();
                        $parameters['admin'] = $this->admin;
                        $parameters['token'] = $token;
                        $parameters['invites'] = $this->registrationInviteRepository->findByTokenAsSortedArray($token);

                        $url = $this->admin->generateUrl('invite', ['token' => $token]);
                        $this->sendMessage(
                            'registration_invite_repeat',
                            "<a href='" . $url . "' id='confirm-repeat'>" .
                            $this->admin->trans('registration_invite.token.repeat') . '</a>'
                        );
                    }
                } else {
                    // The registration invite for another user.
                    $this->sendMessage('sonata_flash_info', 'registration_invite.token.another_user');
                }
            }
        }

        return $this->renderResponse($blockContext->getTemplate(), $parameters, $response);
    }

    /**
     * @param RegistrationInviteManager $registrationInviteManager
     *
     * @return RegistrationInviteBlockService
     */
    public function setRegistrationInviteManager($registrationInviteManager)
    {
        $this->registrationInviteManager = $registrationInviteManager;

        return $this;
    }

    /**
     * @param string $type
     * @param string $code
     */
    private function sendMessage($type, $code)
    {
        /** @noinspection PhpUndefinedMethodInspection */
        /** @var FlashBag $flashBag */
        $flashBag = $this->request->getSession()->getFlashBag();

        $flashBag->add($type, $this->admin->trans($code));
    }

    /**
     * @param UserRepository $userRepository
     *
     * @return RegistrationInviteBlockService
     */
    public function setUserRepository($userRepository)
    {
        $this->userRepository = $userRepository;

        return $this;
    }

    /**
     * @param RegistrationInviteRepository $registrationInviteRepository
     *
     * @return RegistrationInviteBlockService
     */
    public function setRegistrationInviteRepository($registrationInviteRepository)
    {
        $this->registrationInviteRepository = $registrationInviteRepository;

        return $this;
    }

    /**
     * @return User
     */
    private function getCurrentUser()
    {
        $currUser = $this->admin->getCurrentUser();
        if (!is_object($currUser)) {
            // There is no user logged in the application.
            throw new AccessDeniedException();
        }

        return $currUser;
    }

    /**
     * @param UserRegistrationInviteAdmin $userRegistrationInviteAdmin
     *
     * @return RegistrationInviteBlockService
     */
    public function setUserRegistrationInviteAdmin($userRegistrationInviteAdmin)
    {
        $this->admin = $userRegistrationInviteAdmin;

        return $this;
    }
}
