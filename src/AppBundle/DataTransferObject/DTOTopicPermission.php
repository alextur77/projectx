<?php

namespace AppBundle\DataTransferObject;

/**
 * Class DTOTopicPermission
 * @package AppBundle\DataTransferObject
 */
class DTOTopicPermission
{
    const TYPE_GROUP = 'G';
    const TYPE_MODERATOR = 'M';

    /**
     * @var integer
     *
     * Dummy pseudo-id field
     */
    protected $id = 0;

    /** @var integer */
    protected $topicGroupPermissionId;

    /** @var integer */
    protected $topicModeratorId;

    /** @var integer */
    protected $groupId;

    /** @var integer */
    protected $userId;

    /** @var string */
    protected $name;

    /** @var string */
    protected $login;

    /** @var string */
    protected $type;

    /**
     * DTOTopicPermission constructor.
     *
     * @param integer $topicGroupPermissionId
     * @param integer $topicModeratorId
     * @param integer $groupId
     * @param integer $userId
     * @param string  $name
     * @param string  $login
     * @param string  $type
     */
    public function __construct(
        $topicGroupPermissionId,
        $topicModeratorId,
        $groupId,
        $userId,
        $name,
        $login,
        $type)
    {
        $this->topicGroupPermissionId = $topicGroupPermissionId;
        $this->topicModeratorId = $topicModeratorId;
        $this->groupId = $groupId;
        $this->userId = $userId;
        $this->name = $name;
        $this->login = $login;
        $this->type = $type;
    }

    /**
     * @param string $suffix
     *
     * @return array
     */
    public static function getTypesList($suffix = '')
    {
        return [
            self::TYPE_GROUP => 'topic_permission.type.' . self::TYPE_GROUP . $suffix,
            self::TYPE_MODERATOR => 'topic_permission.type.' . self::TYPE_MODERATOR . $suffix
        ];
    }

    /**
     * @return int
     */
    public function getGroupId()
    {
        return $this->groupId;
    }

    /**
     * @return int
     */
    public function getUserId()
    {
        return $this->userId;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return int
     */
    public function getTopicGroupPermissionId()
    {
        return $this->topicGroupPermissionId;
    }

    /**
     * @return int
     */
    public function getTopicModeratorId()
    {
        return $this->topicModeratorId;
    }

    /**
     * @return string
     */
    public function getLogin()
    {
        return $this->login;
    }
}
