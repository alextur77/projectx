<?php

namespace AppBundle\DataTransferObject;

/**
 * Class Group
 * @package AppBundle\DataTransferObject
 */
class DTOGroup
{
    const SHIFT_SIZE = 4;

    /** @var integer */
    public $id = null;

    /** @var string */
    public $name;

    /** @var boolean */
    public $public;

    /** @var boolean */
    public $active;

    /** @var integer */
    public $depth;

    /** @var integer */
    public $userCount = null;

    /**
     * DTOGroup constructor.
     *
     * @param integer  $id
     * @param string   $name
     * @param boolean  $public
     * @param boolean  $active
     * @param int      $depth
     * @param int|null $userCount
     */
    public function __construct($id, $name, $public, $active, $depth, $userCount = null)
    {
        $this->id = $id;
        $this->name = $name;
        $this->public = $public;
        $this->active = $active;
        $this->depth = $depth;
        $this->userCount = (integer) $userCount;
    }

    /**
     * @return string
     */
    public function getShift()
    {
        return str_repeat('-', $this->depth/* * $this::SHIFT_SIZE*/);
    }

    /**
     * @return string
     */
    public function getShiftNBSP()
    {
        return str_repeat('&nbsp;', $this->depth * $this::SHIFT_SIZE);
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @return boolean
     */
    public function isPublic()
    {
        return $this->public;
    }

    /**
     * @return boolean
     */
    public function isActive()
    {
        return $this->active;
    }

    /**
     * @return int
     */
    public function getDepth()
    {
        return $this->depth;
    }

    /**
     * @return int
     */
    public function getUserCount()
    {
        return $this->userCount;
    }
}
