<?php

namespace AppBundle\DataTransferObject;

/**
 * Class DTOTopic
 * @package AppBundle\DataTransferObject
 */
class DTOTopic
{
    /** @var integer */
    protected $id;

    /** @var string */
    protected $name;

    /** @var string */
    protected $description;

    /** @var \DateTime */
    protected $createdAt;

    /** @var boolean */
    protected $opened;

    /** @var boolean */
    protected $active;

    /** @var integer */
    protected $postQuantity;

    /** @var \DateTime */
    protected $lastPostDatetime;

    /**
     * DTOTopic constructor.
     *
     * @param integer   $id
     * @param string    $name
     * @param string    $description
     * @param \DateTime $createdAt
     * @param boolean   $opened
     * @param boolean   $active
     * @param integer   $postQuantity
     * @param \DateTime $lastPostDatetime
     */
    public function __construct(
        $id,
        $name,
        $description,
        $createdAt,
        $opened,
        $active,
        $postQuantity,
        $lastPostDatetime)
    {
        $this->id = $id;
        $this->name = $name;
        $this->description = $description;
        $this->createdAt = $createdAt;
        $this->opened = $opened;
        $this->active = $active;
        $this->postQuantity = $postQuantity;
        $this->lastPostDatetime = $lastPostDatetime;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * @return boolean
     */
    public function isOpened()
    {
        return $this->opened;
    }

    /**
     * @return boolean
     */
    public function isActive()
    {
        return $this->active;
    }

    /**
     * @return int
     */
    public function getPostQuantity()
    {
        return $this->postQuantity;
    }

    /**
     * @return \DateTime
     */
    public function getLastPostDatetime()
    {
        return $this->lastPostDatetime;
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }
}
