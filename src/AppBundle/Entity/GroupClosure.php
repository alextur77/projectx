<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Class GroupClosure
 * @package AppBundle\Entity
 *
 * @ORM\Table(name="group_closure")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\GroupClosureRepository")
 */
class GroupClosure
{
    /**
     * @var integer
     *
     * @ORM\Column(name="ancestor_id", type="integer", nullable=false)
     * @ORM\Id
     */
    private $ancestorId;

    /**
     * @var integer
     *
     * @ORM\Column(name="descendant_id", type="integer", nullable=false)
     * @ORM\Id
     */
    private $descendantId;

    /**
     * @var integer
     *
     * @ORM\Column(name="depth", type="integer", nullable=false)
     */
    private $depth;

    /**
     * @return string
     */
    public function __toString()
    {
        return (string) "ancestorId=" . $this->getAncestorId() . ", descendantId=" . $this->getDescendantId() .
        ", depth=" . $this->getDepth();
    }

    /**
     * @param int $ancestorId
     *
     * @return GroupClosure
     */
    public function setAncestorId($ancestorId)
    {
        $this->ancestorId = $ancestorId;

        return $this;
    }

    /**
     * @return int
     */
    public function getAncestorId()
    {
        return $this->ancestorId;
    }

    /**
     * @param int $descendantId
     *
     * @return GroupClosure
     */
    public function setDescendantId($descendantId)
    {
        $this->descendantId = $descendantId;

        return $this;
    }

    /**
     * @return int
     */
    public function getDescendantId()
    {
        return $this->descendantId;
    }

    /**
     * @param int $depth
     *
     * @return GroupClosure
     */
    public function setDepth($depth)
    {
        $this->depth = $depth;

        return $this;
    }

    /**
     * @return int
     */
    public function getDepth()
    {
        return $this->depth;
    }
}
