<?php

namespace AppBundle\Entity;

use AppBundle\Entity\Traits\LoggingTrait;
use AppBundle\Entity\Traits\StatusTrait;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Class Topic
 * @package AppBundle\Entity
 *
 * @ORM\Entity(repositoryClass="AppBundle\Repository\TopicRepository")
 */
class Topic
{
    use StatusTrait;
    use LoggingTrait;

    /**
     * @var int
     *
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue()
     */
    protected $id;

    /**
     * @var string
     *
     * @ORM\Column(length=1000)
     */
    protected $name;

    /**
     * @var string
     *
     * @ORM\Column(type="text", nullable=true)
     */
    protected $description;

    /**
     * @var bool
     *
     * @ORM\Column(type="boolean")
     */
    protected $opened = true;

    /**
     * @var Organization
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Organization", inversedBy="childrenGroups")
     * @ORM\JoinColumn(nullable=false)
     */
    protected $organization;

    /**
     * @var TopicGroupPermission[]|ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\TopicGroupPermission", mappedBy="topic")
     */
    protected $topicGroupPermissions;

    /**
     * @var TopicModerator[]|ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\TopicModerator", mappedBy="topic")
     */
    protected $topicModerators;

    /**
     * @var Post[]|ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Post", mappedBy="topic")
     */
    protected $posts;

    /**
     * Topic constructor.
     */
    public function __construct()
    {
        $this->topicGroupPermissions = new ArrayCollection();
        $this->topicModerators = new ArrayCollection();
        $this->posts = new ArrayCollection();
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return (string) $this->name;
    }

    /**
     * @return $this
     */
    public function close()
    {
        $this->opened = false;

        return $this;
    }

    /**
     * @return boolean
     */
    public function isOpened()
    {
        return $this->opened;
    }

    /**
     * @param boolean $opened
     *
     * @return Topic
     */
    public function setOpened($opened)
    {
        $this->opened = $opened;

        return $this;
    }

    /**
     * @return bool
     */
    public function getOpened()
    {
        return $this->opened;
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param string $description
     *
     * @return Topic
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * @return Organization
     */
    public function getOrganization()
    {
        return $this->organization;
    }

    /**
     * @param Organization $organization
     *
     * @return Topic
     */
    public function setOrganization($organization)
    {
        $this->organization = $organization;

        return $this;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     *
     * @return Topic
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     *
     * @return Topic
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * @return ArrayCollection|TopicGroupPermission[]
     */
    public function getTopicGroupPermissions()
    {
        return $this->topicGroupPermissions;
    }

    /**
     * @param ArrayCollection|TopicGroupPermission[] $topicGroupPermissions
     *
     * @return Topic
     */
    public function setTopicGroupPermissions($topicGroupPermissions)
    {
        $this->topicGroupPermissions = $topicGroupPermissions;

        return $this;
    }

    /**
     * @param TopicGroupPermission $topicGroupPermission
     *
     * @return Topic
     */
    public function addTopicGroupPermissions($topicGroupPermission)
    {
        $this->topicGroupPermissions->add($topicGroupPermission);

        return $this;
    }

    /**
     * @param TopicGroupPermission $topicGroupPermission
     *
     * @return Topic
     */
    public function removeTopicGroupPermissions($topicGroupPermission)
    {
        $this->topicGroupPermissions->removeElement($topicGroupPermission);

        return $this;
    }

    /**
     * @return TopicModerator[]|ArrayCollection
     */
    public function getTopicModerators()
    {
        return $this->topicModerators;
    }

    /**
     * @param TopicModerator[]|ArrayCollection $topicModerators
     *
     * @return Topic
     */
    public function setTopicModerators($topicModerators)
    {
        $this->topicModerators = $topicModerators;

        return $this;
    }

    /**
     * @param TopicModerator $topicModerator
     *
     * @return Topic
     */
    public function addTopicGroupModerators($topicModerator)
    {
        $this->topicModerators->add($topicModerator);

        return $this;
    }

    /**
     * @param TopicModerator $topicModerator
     *
     * @return Topic
     */
    public function removeTopicModerators($topicModerator)
    {
        $this->topicModerators->removeElement($topicModerator);

        return $this;
    }

    /**
     * @return Post[]|ArrayCollection
     */
    public function getPosts()
    {
        return $this->posts;
    }

    /**
     * @param Post[]|ArrayCollection $posts
     *
     * @return Topic
     */
    public function setPosts($posts)
    {
        $this->posts = $posts;

        return $this;
    }

    /**
     * @param Post $post
     *
     * @return Topic
     */
    public function addPost($post)
    {
        $this->posts->add($post);

        return $this;
    }

    /**
     * @param Post $post
     *
     * @return Topic
     */
    public function removePost($post)
    {
        $this->topicModerators->removeElement($post);

        return $this;
    }
}
