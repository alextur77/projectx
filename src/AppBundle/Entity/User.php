<?php

namespace AppBundle\Entity;

use AppBundle\Entity\Traits\LoggingTrait;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use FOS\UserBundle\Entity\User as BaseUser;
use Symfony\Component\Validator\Context\ExecutionContextInterface;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\Security\Core\Util\SecureRandom;
use Symfony\Component\Translation\Translator;

/**
 * Class User
 * @package AppBundle\Entity
 *
 * @ORM\Entity(repositoryClass="AppBundle\Repository\UserRepository")
 *
 * @ORM\HasLifecycleCallbacks()
 */
class User extends BaseUser
{
    use LoggingTrait;

    /**
     * @var int
     *
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue()
     */
    protected $id;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=1000)
     */
    protected $firstName;

    /**
     * @var Translator
     */
    protected $translator;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=1000)
     */
    protected $lastName;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=1000)
     */
    protected $patronymic;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=255, nullable=true)
     *
     * @Assert\Regex(pattern="/^[0-9]{12}+$/", message="user_edit_form.error.input_phone_number")
     */
    protected $phone;

    /**
     * @var string
     *
     * @ORM\Column(type="boolean", name="send_email")
     */
    protected $sendEmail;

    /**
     * @var string
     *
     * @ORM\Column(type="boolean", name="send_sms")
     */
    protected $sendSms;

    /**
     * @Assert\File(maxSize="2048k")
     * @Assert\Image(mimeTypesMessage="Please upload a valid image.")
     */
    protected $profilePictureFile;

    // for temporary storage
    /**
     * @var
     */
    private $tempProfilePicturePath;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    protected $profilePicturePath;

    /**
     * @var organizationUserPermission[]|ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\OrganizationUserPermission", mappedBy="user")
     */
    protected $organizationUserPermissions;

    /**
     * @var GroupUserPermission[]|ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\GroupUserPermission", mappedBy="user")
     */
    protected $groupUserPermissions;

    /**
     * @var Organization
     *
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    protected $userCurrentOrganization;

    /**
     * Constructor
     */
    public function __construct()
    {
        parent::__construct();

        $this->organizationUserPermissions = new ArrayCollection();
        $this->groupUserPermissions = new ArrayCollection();
    }

    /**
     * Sets the file used for profile picture uploads
     *
     * @param UploadedFile $file
     *
     * @return object
     */
    public function setProfilePictureFile(UploadedFile $file = null)
    {
        // set the value of the holder
        $this->profilePictureFile = $file;
        // check if we have an old image path
        if (isset($this->profilePicturePath)) {
            // store the old name to delete after the update
            $this->tempProfilePicturePath = $this->profilePicturePath;
            $this->profilePicturePath = null;
        } else {
            $this->profilePicturePath = 'initial';
        }

        return $this;
    }

    /**
     * Get the file used for profile picture uploads
     *
     * @return UploadedFile
     */
    public function getProfilePictureFile()
    {

        return $this->profilePictureFile;
    }

    /**
     * @param Translator $translator
     *
     * @return User
     */
    public function setTranslator($translator)
    {
        $this->translator = $translator;

        return $this;
    }

    /**
     * @param string $userCurrentOrganization
     *
     * @return $this
     */
    public function setUserCurrentOrganization($userCurrentOrganization)
    {
        $this->userCurrentOrganization = $userCurrentOrganization;

        return $this;
    }

    /**
     * @return Organization
     */
    public function getUserCurrentOrganization()
    {
        if ($this->userCurrentOrganization == null) {
            $i = 0;
            foreach ($this->organizationUserPermissions as $organizationUserPermission) {
                if ($organizationUserPermission->getRole()->getCode() == Role::ROLE_ORGANIZATION_ADMIN) {
                    if ($i == 0) {
                        $organizations = $organizationUserPermission->getOrganization()->getName();
                        $i++;
                    }
                }
            }
            if (!isset($organizations)) {

                foreach ($this->organizationUserPermissions as $organizationUserPermission) {
                    if ($organizationUserPermission->getRole()->getCode() == Role::ROLE_ORGANIZATION_USER) {
                        if ($i == 0) {
                            $organizations = $organizationUserPermission->getOrganization()->getName();
                            $i++;
                        }
                    }
                }
                if (empty($organizations)) {

                    $organizations = '';
                }
            }
        } else {
            $organizations = $this->userCurrentOrganization;
        }

        return $organizations;
    }

    /**
     * Get profilePicturePath
     *
     * @return string
     */
    public function getProfilePicturePath()
    {
        return $this->profilePicturePath;
    }

    /**
     * Specifies where in the /web directory profile pic uploads are stored
     *
     * @param string $type
     *
     * @return string
     */
    protected function getUploadDir($type = 'profilePicture')
    {
        // the type param is to change these methods at a later date for more file uploads
        // get rid of the __DIR__ so it doesn't screw up
        // when displaying uploaded doc/image in the view.
        return 'files/avatar';
    }

    /**
     * Get the web path for the user
     *
     * @return string
     */
    public function getWebProfilePicturePath()
    {

        return '/' . $this->getUploadDir() . '/' . $this->getProfilePicturePath();
    }

    /**
     * @ORM\PrePersist()
     * @ORM\PreUpdate()
     */
    public function preUploadProfilePicture()
    {
        if (null !== $this->getProfilePictureFile()) {
            // a file was uploaded
            // generate a unique filename
            $filename = $this->generateRandomProfilePictureFilename();
            $this->setProfilePicturePath($filename . '.' . $this->getProfilePictureFile()->guessExtension());
        }
    }

    /**
     * Generates a 32 char long random filename
     *
     * @return string
     */
    public function generateRandomProfilePictureFilename()
    {
        $count = 0;
        do {
            $generator = new SecureRandom();
            $random = $generator->nextBytes(16);
            $randomString = bin2hex($random);
            $count++;
            $guessExtension = $this->getProfilePictureFile()->guessExtension();
        } while (
            file_exists(
                $this->getUploadRootDir() . '/' . $randomString . '.' . $guessExtension
            ) && $count < 50);

        return $randomString;
    }

    /**
     * @ORM\PostPersist()
     * @ORM\PostUpdate()
     *
     * Upload the profile picture
     *
     * @return mixed
     */
    public function uploadProfilePicture()
    {
        // check there is a profile pic to upload
        if ($this->getProfilePictureFile() === null) {
            return;
        }
        // if there is an error when moving the file, an exception will
        // be automatically thrown by move(). This will properly prevent
        // the entity from being persisted to the database on error
        $this->getProfilePictureFile()->move($this->getUploadRootDir(), $this->getProfilePicturePath());

        // check if we have an old image
        if (isset($this->tempProfilePicturePath) &&
            file_exists(
                $this->getUploadRootDir() . '/' . $this->tempProfilePicturePath
            )
        ) {
            // delete the old image
            unlink($this->getUploadRootDir() . '/' . $this->tempProfilePicturePath);
            // clear the temp image path
            $this->tempProfilePicturePath = null;
        }
        $this->profilePictureFile = null;
    }

    /**
     * @ORM\PostRemove()
     */
    public function removeProfilePictureFile()
    {
        if ($file = $this->getProfilePictureAbsolutePath() && file_exists($this->getProfilePictureAbsolutePath())) {
            unlink($file);
        }
    }

    /**
     * Get root directory for file uploads
     *
     * @param string $type
     *
     * @return string
     */
    protected function getUploadRootDir($type = 'profilePicture')
    {
        // the absolute directory path where uploaded
        // documents should be saved
        return $this->getUploadDir($type);
    }

    /**
     * @return null|string
     */
    public function getProfilePictureAbsolutePath()
    {
        return null === $this->profilePicturePath
            ? null
            : $this->getUploadRootDir() . '/' . $this->profilePicturePath;
    }

    /**
     * Set profilePicturePath
     *
     * @param string $profilePicturePath
     *
     * @return User
     */
    public function setProfilePicturePath($profilePicturePath)
    {
        $this->profilePicturePath = $profilePicturePath;

        return $this;
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return (string) $this->username;
    }

    /**
     * @return string
     */
    public function getFullName()
    {
        return $this->lastName . ' ' . $this->firstName . ' ' . $this->patronymic;
    }

    /**
     * @return string
     */
    public function getContactInformation()
    {
        return $this->email . ', ' . $this->phone;
    }

    /**
     * @return array
     */
    public function getRoles()
    {
        $roles = parent::getRoles();

        foreach ($this->organizationUserPermissions as $organizationUserPermission) {
            if ($this->userCurrentOrganization == $organizationUserPermission->getOrganization()->getName()) {
                $roles[] = $organizationUserPermission->getRole()->getCode();
            }
        }

        foreach ($this->groupUserPermissions as $groupUserPermission) {
            $roles[] = $groupUserPermission->getRole()->getCode();
        }

        $roles = array_unique($roles);

        return $roles;
    }

    /**
     * @return Organization[]
     */
    public function getAdminOrganizations()
    {
        $organizations = [];
        foreach ($this->organizationUserPermissions as $organizationUserPermission) {
            if ($organizationUserPermission->getRole()->getCode() == Role::ROLE_ORGANIZATION_ADMIN || $organizationUserPermission->getRole()->getCode() == Role::ROLE_ORGANIZATION_USER) {
                $organizations[] = $organizationUserPermission->getOrganization();
            }
        }

        return $organizations;
    }

    /**
     * @return Organization[]
     */
    public function getAdminOrganizationsName()
    {
        $organizations = [];
        foreach ($this->organizationUserPermissions as $organizationUserPermission) {
            if ($organizationUserPermission->getRole()->getCode() == Role::ROLE_ORGANIZATION_ADMIN || $organizationUserPermission->getRole()->getCode() == Role::ROLE_ORGANIZATION_USER) {
                $organizations[] = $organizationUserPermission->getOrganization()->getName();
            }
        }

        return $organizations;
    }

    /**
     * @return Organization[]
     */
    public function getUserOrganizationsName()
    {
        $organizations = [];
        foreach ($this->organizationUserPermissions as $organizationUserPermission) {

            $organizations[] = $organizationUserPermission->getOrganization()->getName();
        }

        return $organizations;
    }

    /**
     * @return Group[]
     */
    public function getAdminGroups()
    {
        $organizations = $this->getAdminOrganizations();
        $groups = [];
        foreach ($organizations as $organization) {
            foreach ($organization->getChildrenGroups() as $group) {
                $groups[] = $group;
                $this->getChildrenGroups($group, $groups);
            }
        }

        foreach ($this->groupUserPermissions as $groupUserPermission) {
            if ($groupUserPermission->getRole()->getCode() == Role::ROLE_GROUP_ADMIN) {
                $groups[] = $groupUserPermission->getGroup();
                $this->getChildrenGroups($groupUserPermission->getGroup(), $groups);
            }
        }

        return $groups;
    }

    /**
     * @param Group $group
     * @param array $groups
     */
    private function getChildrenGroups($group, &$groups)
    {
        if (null !== $group->getChildrenGroups()) {
            foreach ($group->getChildrenGroups() as $child) {
                $groups[] = $child;
                $this->getChildrenGroups($child, $groups);
            }
        }
    }

    /**
     * @return array
     */
    public function getUserGroups()
    {
        $groups = [];
        foreach ($this->groupUserPermissions as $groupUserPermission) {
            if ($groupUserPermission->getRole()->getCode() == Role::ROLE_GROUP_USER) {
                $groups[] = $groupUserPermission->getGroup();
                $this->getChildrenGroups($groupUserPermission->getGroup(), $groups);
            }
        }

        return $groups;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     *
     * @return User
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * @return string
     */
    public function getFirstName()
    {
        return $this->firstName;
    }

    /**
     * @param string $phone
     *
     *
     * @return User
     */
    public function setPhone($phone)
    {
        $this->phone = $phone;

        return $this;
    }

    /**
     * @return string
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * @param string $patronymic
     *
     *
     * @return User
     */
    public function setPatronymic($patronymic)
    {
        $this->patronymic = $patronymic;

        return $this;
    }

    /**
     * @return string
     */
    public function getPatronymic()
    {
        return $this->patronymic;
    }

    /**
     * @param string $firstName
     *
     * @return User
     */
    public function setFirstName($firstName)
    {
        $this->firstName = $firstName;

        return $this;
    }

    /**
     * @return string
     */
    public function getLastName()
    {
        return $this->lastName;
    }

    /**
     * @param string $lastName
     *
     * @return User
     */
    public function setLastName($lastName)
    {
        $this->lastName = $lastName;

        return $this;
    }

    /**
     * @return string
     */
    public function getSendEmail()
    {
        return $this->sendEmail;
    }

    /**
     * @param string $sendEmail
     *
     * @return User
     */
    public function setSendEmail($sendEmail)
    {
        $this->sendEmail = $sendEmail;

        return $this;
    }

    /**
     * @return string
     */
    public function getSendSms()
    {
        return $this->sendSms;
    }

    /**
     * @param string $sendSms
     *
     * @return User
     */
    public function setSendSms($sendSms)
    {
        $this->sendSms = $sendSms;

        return $this;
    }

    /**
     * @return organizationUserPermission[]
     */
    public function getOrganizationUserPermissions()
    {
        return $this->organizationUserPermissions;
    }

    /**
     * @param organizationUserPermission[] $organizationUserPermissions
     *
     * @return User
     */
    public function setOrganizationUserPermissions($organizationUserPermissions)
    {
        $this->organizationUserPermissions = $organizationUserPermissions;

        return $this;
    }

    /**
     * @param organizationUserPermission $organizationUserPermission
     *
     * @return User
     */
    public function addOrganizationUserPermission($organizationUserPermission)
    {
        $this->organizationUserPermissions->add($organizationUserPermission);

        return $this;
    }

    /**
     * @param organizationUserPermission $organizationUserPermission
     *
     * @return User
     */
    public function removeOrganizationUserPermission($organizationUserPermission)
    {
        $this->organizationUserPermissions->removeElement($organizationUserPermission);

        return $this;
    }

    /**
     * @return GroupUserPermission[]
     */
    public function getGroupUserPermissions()
    {
        return $this->groupUserPermissions;
    }

    /**
     * @param GroupUserPermission[] $groupUserPermissions
     *
     * @return User
     */
    public function setGroupUserPermissions($groupUserPermissions)
    {
        $this->groupUserPermissions = $groupUserPermissions;

        return $this;
    }

    /**
     * @param GroupUserPermission $groupUserPermission
     *
     * @return User
     */
    public function addGroupUserPermission($groupUserPermission)
    {
        $this->groupUserPermissions->add($groupUserPermission);

        return $this;
    }

    /**
     * @param GroupUserPermission $groupUserPermission
     *
     * @return User
     */
    public function removeGroupUserPermission($groupUserPermission)
    {
        $this->groupUserPermissions->removeElement($groupUserPermission);

        return $this;
    }

    /**
     * @Assert\True(message="user_edit_form.error.phone_number", payload="messages.ru.yml")
     *
     * @return bool
     */
    public function isValidate()
    {
        if ($this->getSendSms() == true && empty($this->getPhone())) {

            return false;
        }
    }
}
