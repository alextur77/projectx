<?php

namespace AppBundle\Entity;

use AppBundle\Entity\Traits\LoggingTrait;
use Doctrine\ORM\Mapping as ORM;

/**
 * Class RegistrationRequest
 * @package AppBundle\Entity
 *
 * @ORM\Entity(repositoryClass="AppBundle\Repository\RegistrationRequestRepository")
 * @ORM\EntityListeners(value={"AppBundle\EventListener\Security\RegistrationRequestListener"})
 */
class RegistrationRequest
{
    use LoggingTrait;

    const STATUS_NEW = 'N';
    const STATUS_APPROVED = 'A';
    const STATUS_DECLINED = 'D';

    /**
     * @var int
     *
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue()
     */
    protected $id;

    /**
     * @var string
     *
     * @ORM\Column(name="status", type="string", length=1, nullable=false)
     */
    protected $status;

    /**
     * @var Organization
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Organization", inversedBy="registrationRequests")
     * @ORM\JoinColumn(nullable=true)
     */
    protected $organization;

    /**
     * @var Group
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Group", inversedBy="registrationRequests")
     * @ORM\JoinColumn(nullable=true)
     */
    protected $group;

    /**
     * @var User
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\User")
     * @ORM\JoinColumn(nullable=false)
     */
    protected $user;

    /**
     * @var string
     *
     * @ORM\Column(type="text", nullable=true)
     */
    protected $comment;

    /**
     * @return array
     */
    public static function getStatusesList()
    {
        return [
            self::STATUS_NEW => 'registration_request.status.' . self::STATUS_NEW,
            self::STATUS_APPROVED => 'registration_request.status.' . self::STATUS_APPROVED,
            self::STATUS_DECLINED => 'registration_request.status.' . self::STATUS_DECLINED
        ];
    }

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->status = self::STATUS_NEW;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     *
     * @return RegistrationRequest
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * @return Organization
     */
    public function getOrganization()
    {
        return $this->organization;
    }

    /**
     * @param Organization $organization
     *
     * @return RegistrationRequest
     */
    public function setOrganization($organization)
    {
        $this->organization = $organization;

        return $this;
    }

    /**
     * @return Group
     */
    public function getGroup()
    {
        return $this->group;
    }

    /**
     * @param Group $group
     *
     * @return RegistrationRequest
     */
    public function setGroup($group)
    {
        $this->group = $group;

        return $this;
    }

    /**
     * @return User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param User $user
     *
     * @return RegistrationRequest
     */
    public function setUser($user)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * @return string
     */
    public function getComment()
    {
        return $this->comment;
    }

    /**
     * @param string $comment
     *
     * @return RegistrationRequest
     */
    public function setComment($comment)
    {
        $this->comment = $comment;

        return $this;
    }

    /**
     * @return string
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param string $status
     *
     * @return RegistrationRequest
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return (string) $this->user->getFullName();
    }
}
