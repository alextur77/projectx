<?php

namespace AppBundle\Entity;

use AppBundle\Entity\Traits\LoggingTrait;
use Doctrine\ORM\Mapping as ORM;

/**
 * Class RegistrationInvite
 * @package AppBundle\Entity
 *
 * @ORM\Entity(repositoryClass="AppBundle\Repository\RegistrationInviteRepository")
 * @ORM\Table(indexes={@ORM\Index(name="token_idx", columns={"token"})})
 */
class RegistrationInvite
{
    use LoggingTrait;

    const STATUS_NEW = 'N';
    const STATUS_ACCEPTED = 'A';
    const STATUS_DECLINED = 'D';

    const TYPE_ORGANIZATION = 'O';
    const TYPE_GROUP = 'G';

    const FLAG_ADMIN_Y = 'Y';
    const FLAG_ADMIN_N = 'N';

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string $email
     *
     * @ORM\Column(type="string", nullable=false)
     */
    private $email;

    /**
     * @var Organization
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Organization")
     * @ORM\JoinColumn(nullable=true)
     */
    private $organization;

    /**
     * @var Group
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Group")
     * @ORM\JoinColumn(nullable=true)
     */
    private $group;

    /**
     * @var User
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\User")
     * @ORM\JoinColumn(nullable=true)
     */
    private $user;

    /**
     * @var string
     *
     * @ORM\Column(name="status", type="string", length=1, nullable=false)
     */
    private $status;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=32, nullable=false)
     */
    private $token;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=1, nullable=true)
     */
    private $flagAdmin = RegistrationInvite::FLAG_ADMIN_N;

    /**
     * @return array
     */
    public static function getStatusesList()
    {
        return [
            self::STATUS_NEW => 'registration_invite.status.' . self::STATUS_NEW,
            self::STATUS_ACCEPTED => 'registration_invite.status.' . self::STATUS_ACCEPTED,
            self::STATUS_DECLINED => 'registration_invite.status.' . self::STATUS_DECLINED
        ];
    }

    /**
     * @return array
     */
    public static function getTypesList()
    {
        return [
            self::TYPE_ORGANIZATION => 'registration_invite.type.' . self::TYPE_ORGANIZATION,
            self::TYPE_GROUP => 'registration_invite.type.' . self::TYPE_GROUP,
        ];
    }

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->status = self::STATUS_NEW;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set status
     *
     * @param string $status
     *
     * @return RegistrationInvite
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Set flagAdmin
     *
     * @param string $flagAdmin
     *
     * @return RegistrationInvite
     */
    public function setFlagAdmin($flagAdmin)
    {
        $this->flagAdmin = $flagAdmin;

        return $this;
    }

    /**
     * Get flagAdmin
     *
     * @return string
     */
    public function getFlagAdmin()
    {
        return $this->flagAdmin;
    }

    /**
     * Get status
     *
     * @return string
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set organization
     *
     * @param Organization $organization
     *
     * @return RegistrationInvite
     */
    public function setOrganization(Organization $organization = null)
    {
        $this->organization = $organization;

        return $this;
    }

    /**
     * Get organization
     *
     * @return Organization
     */
    public function getOrganization()
    {
        return $this->organization;
    }

    /**
     * Set group
     *
     * @param Group $group
     *
     * @return RegistrationInvite
     */
    public function setGroup(Group $group = null)
    {
        $this->group = $group;

        return $this;
    }

    /**
     * Get group
     *
     * @return Group
     */
    public function getGroup()
    {
        return $this->group;
    }

    /**
     * Set user
     *
     * @param User $user
     *
     * @return RegistrationInvite
     */
    public function setUser(User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set email
     *
     * @param string $email
     *
     * @return RegistrationInvite
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email
     *
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set token
     *
     * @param string $token
     *
     * @return RegistrationInvite
     */
    public function setToken($token)
    {
        $this->token = $token;

        return $this;
    }

    /**
     * Get token
     *
     * @return string
     */
    public function getToken()
    {
        return $this->token;
    }

    /**
     * @return string
     */
    public function getOrganizationGroupName()
    {
        if (null === $this->organization) {
            $result = $this->group->getName();
        } else {
            $result = $this->organization->getName();
        }

        return $result;
    }

    /**
     * @return bool
     */
    public function getActive()
    {
        if (null === $this->organization) {
            $result = $this->group->getActive();
        } else {
            $result = $this->organization->getActive();
        }

        return $result;
    }

    /**
     * @return string
     */
    public function getOrganizationGroupType()
    {
        if (null === $this->organization) {
            $result = self::TYPE_GROUP;
        } else {
            $result = self::TYPE_ORGANIZATION;
        }

        return $result;
    }
}
