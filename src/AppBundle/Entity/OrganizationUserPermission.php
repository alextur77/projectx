<?php

namespace AppBundle\Entity;

use AppBundle\Entity\Traits\LoggingTrait;
use Doctrine\ORM\Mapping as ORM;

/**
 * Class OrganizationUserPermission
 * @package AppBundle\Entity
 *
 * @ORM\Entity(repositoryClass="AppBundle\Repository\OrganizationUserPermissionRepository")
 * @ORM\Table(uniqueConstraints={@ORM\UniqueConstraint(name="organization_user", columns={"organization_id", "user_id"})})
 */
class OrganizationUserPermission
{
    use LoggingTrait;

    /**
     * @var int
     *
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue()
     */
    protected $id;

    /**
     * @var Organization
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Organization", inversedBy="organizationUserPermissions")
     * @ORM\JoinColumn(nullable=false)
     */
    protected $organization;

    /**
     * @var User
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\User", inversedBy="organizationUserPermissions")
     * @ORM\JoinColumn(nullable=false)
     */
    protected $user;

    /**
     * @var Role
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Role")
     * @ORM\JoinColumn(nullable=false)
     */
    protected $role;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     *
     * @return OrganizationUserPermission
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * @return Organization
     */
    public function getOrganization()
    {
        return $this->organization;
    }

    /**
     * @param Organization $organization
     *
     * @return OrganizationUserPermission
     */
    public function setOrganization($organization)
    {
        $this->organization = $organization;

        return $this;
    }

    /**
     * @return User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param User $user
     *
     * @return OrganizationUserPermission
     */
    public function setUser($user)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * @return Role
     */
    public function getRole()
    {
        return $this->role;
    }

    /**
     * @param Role $role
     *
     * @return OrganizationUserPermission
     */
    public function setRole($role)
    {
        $this->role = $role;

        return $this;
    }
}

