<?php

namespace AppBundle\Entity;

use AppBundle\Entity\Traits\LoggingTrait;
use AppBundle\Entity\Traits\StatusTrait;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Class Organization
 * @package AppBundle\Entity
 *
 * @ORM\Entity(repositoryClass="AppBundle\Repository\OrganizationRepository")
 */
class Organization
{
    use StatusTrait;
    use LoggingTrait;

    /**
     * @var int
     *
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue()
     */
    protected $id;

    /**
     * @var string
     *
     * @ORM\Column(length=1000)
     */
    protected $name;

    /**
     * @var Group[]
     *
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Group", mappedBy="organization")
     */
    protected $childrenGroups;

    /**
     * @var string
     *
     * @ORM\Column(length=1000)
     */
    protected $email;

    /**
     * @var string
     *
     * @ORM\Column(length=1000)
     */
    protected $phoneNumber;

    /**
     * @var string
     *
     * @ORM\Column(length=1000)
     */
    protected $websiteURL;

    /**
     * @var string
     *
     * @ORM\Column(length=1000)
     */
    protected $address;

    /**
     * @var string
     *
     * @ORM\Column(length=1000)
     */
    protected $city;

    /**
     * @var string
     *
     * @ORM\Column(length=1000)
     */
    protected $state;

    /**
     * @var string
     *
     * @ORM\Column(length=1000)
     */
    protected $zip;

    /**
     * @var bool
     *
     * @ORM\Column(type="boolean")
     */
    protected $public;

    /**
     * @var bool
     *
     * @ORM\Column(type="boolean")
     */
    protected $allowSingUp;

    /**
     * @var OrganizationUserPermission[]
     *
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\OrganizationUserPermission", mappedBy="organization", cascade={"persist"})
     */
    protected $organizationUserPermissions;

    /**
     * @var EventRelation[]
     *
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\EventRelation", mappedBy="organization", cascade={"persist"})
     */
    protected $eventRelations;

    /**
     * @var RegistrationRequest[]
     *
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\RegistrationRequest", mappedBy="organization", cascade={"persist"})
     */
    protected $registrationRequests;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->organizationUserPermissions = new ArrayCollection();
        $this->registrationRequests = new ArrayCollection();
        $this->public = false;
        $this->allowSingUp = false;
    }

    /**
     * @return EventRelation[]
     */
    public function getEventRelations()
    {
        return $this->eventRelations;
    }

    /**
     * @param EventRelation[] $eventRelations
     *
     * @return Organization
     */
    public function setEventRelations($eventRelations)
    {
        $this->eventRelations = $eventRelations;

        return $this;
    }

    /**
     * @return RegistrationRequest[]
     */
    public function getRegistrationRequests()
    {
        return $this->registrationRequests;
    }

    /**
     * @param RegistrationRequest[] $registrationRequests
     *
     * @return Organization
     */
    public function setRegistrationRequests($registrationRequests)
    {
        foreach ($registrationRequests as $registrationRequest) {
            $registrationRequest->setOrganization($this);
        }
        $this->registrationRequests = $registrationRequests;

        return $this;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     *
     * @return Organization
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     *
     * @return Organization
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param string $email
     *
     * @return Organization
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * @return string
     */
    public function getPhoneNumber()
    {
        return $this->phoneNumber;
    }

    /**
     * @param string $phoneNumber
     *
     * @return Organization
     */
    public function setPhoneNumber($phoneNumber)
    {
        $this->phoneNumber = $phoneNumber;

        return $this;
    }

    /**
     * @return string
     */
    public function getWebsiteURL()
    {
        return $this->websiteURL;
    }

    /**
     * @param string $websiteURL
     *
     * @return Organization
     */
    public function setWebsiteURL($websiteURL)
    {
        $this->websiteURL = $websiteURL;

        return $this;
    }

    /**
     * @return string
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * @param string $address
     *
     * @return Organization
     */
    public function setAddress($address)
    {
        $this->address = $address;

        return $this;
    }

    /**
     * @return string
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * @param string $city
     *
     * @return Organization
     */
    public function setCity($city)
    {
        $this->city = $city;

        return $this;
    }

    /**
     * @return string
     */
    public function getState()
    {
        return $this->state;
    }

    /**
     * @param string $state
     *
     * @return Organization
     */
    public function setState($state)
    {
        $this->state = $state;

        return $this;
    }

    /**
     * @return string
     */
    public function getZip()
    {
        return $this->zip;
    }

    /**
     * @param string $zip
     *
     * @return Organization
     */
    public function setZip($zip)
    {
        $this->zip = $zip;

        return $this;
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return (string) $this->name;
    }

    /**
     * @return OrganizationUserPermission[]
     */
    public function getOrganizationUserPermissions()
    {
        return $this->organizationUserPermissions;
    }

    /**
     * @param OrganizationUserPermission $organizationUserPermission
     *
     * @return Organization
     */
    public function addOrganizationUserPermissions(OrganizationUserPermission $organizationUserPermission)
    {
        $organizationUserPermission->setOrganization($this);

        $this->organizationUserPermissions->add($organizationUserPermission);

        return $this;
    }

    /**
     * @param OrganizationUserPermission $organizationUserPermission
     *
     * @return Organization
     */
    public function removeOrganizationUserPermissions($organizationUserPermission)
    {
        $this->organizationUserPermissions->removeElement($organizationUserPermission);

        return $this;
    }

    /**
     * @param OrganizationUserPermission[] $organizationUserPermissions
     *
     * @return Organization
     */
    public function setOrganizationUserPermissions($organizationUserPermissions)
    {
        foreach ($organizationUserPermissions as $organizationUserPermission) {
            $organizationUserPermission->setOrganization($this);
        }

        $this->organizationUserPermissions = $organizationUserPermissions;

        return $this;
    }

    /**
     * @return boolean
     */
    public function getPublic()
    {
        return $this->public;
    }

    /**
     * @return boolean
     */
    public function isPublic()
    {
        return $this->public;
    }

    /**
     * @param boolean $public
     *
     * @return Organization
     */
    public function setPublic($public)
    {
        $this->public = $public;

        return $this;
    }

    /**
     * @return boolean
     */
    public function isAllowSingUp()
    {
        return $this->allowSingUp;
    }

    /**
     * @param boolean $allowSingUp
     *
     * @return Organization
     */
    public function setAllowSingUp($allowSingUp)
    {
        $this->allowSingUp = $allowSingUp;

        return $this;
    }

    /**
     * @return Group[]|ArrayCollection
     */
    public function getChildrenGroups()
    {
        return $this->childrenGroups;
    }

    /**
     * @param Group[] $childrenGroups
     *
     * @return Group
     */
    public function setChildrenGroups($childrenGroups)
    {
        $this->childrenGroups = $childrenGroups;

        return $this;
    }

    /**
     * @param User $user
     *
     * @return string
     */
    public function getUserPermission($user)
    {
        foreach ($this->organizationUserPermissions as $perm) {
            if ($perm->getUser() == $user) {
                return $perm->getRole()->getCode();
            }
        }

        return null;
    }
}
