<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Class EventRelation
 * @package AppBundle\Entity
 *
 * @ORM\Table(uniqueConstraints={@ORM\UniqueConstraint(name="event_group", columns={"event_id", "group_id"}),
 * @ORM\UniqueConstraint(name="event_organization", columns={"event_id", "organization_id"}),
 * @ORM\UniqueConstraint(name="event_user", columns={"event_id", "user_id"})})
 * @ORM\Entity()
 */
class EventRelation
{
    const TYPE_ORGANIZATION = 'O';
    const TYPE_GROUP = 'G';
    const TYPE_USER = 'U';

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var Event
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Event", inversedBy="eventRelations")
     * @ORM\JoinColumn(nullable=false)
     */
    protected $event;

    /**
     * @var string
     *
     * @ORM\Column(name="type", type="string", length=1)
     */
    protected $type;

    /**
     * @var Organization
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Organization", inversedBy="eventRelations")
     * @ORM\JoinColumn(nullable=true)
     */
    protected $organization = null;

    /**
     * @var Group
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Group")
     * @ORM\JoinColumn(nullable=true)
     */
    protected $group = null;

    /**
     * @var User
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\User")
     * @ORM\JoinColumn(nullable=true)
     */
    protected $user = null;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param string $type
     *
     * @return EventRelation
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param \AppBundle\Entity\Event $event
     *
     * @return EventRelation
     */
    public function setEvent($event)
    {
        $this->event = $event;

        return $this;
    }

    /**
     * @return Event
     */
    public function getEvent()
    {
        return $this->event;
    }

    /**
     * @param \AppBundle\Entity\Organization $organization
     *
     * @return EventRelation
     */
    public function setOrganization($organization)
    {
        $this->organization = $organization;

        return $this;
    }

    /**
     * @return Organization
     */
    public function getOrganization()
    {
        return $this->organization;
    }

    /**
     * @param \AppBundle\Entity\Group $group
     *
     * @return EventRelation
     */
    public function setGroup($group)
    {
        $this->group = $group;

        return $this;
    }

    /**
     * @return Group
     */
    public function getGroup()
    {
        return $this->group;
    }

    /**
     * @param \AppBundle\Entity\User $user
     *
     * @return EventRelation
     */
    public function setUser($user)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * @return User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @return string
     */
    public function getParticipant()
    {
        switch ($this->getType()) {
            case self::TYPE_USER:
                return $this->getUser();
            case self::TYPE_GROUP:
                return $this->getGroup();
            case self::TYPE_ORGANIZATION:
                return $this->getOrganization();
            default:
                return null;
        }
    }

    /**
     * @param Organization|Group|User $participant
     *
     * @return EventRelation
     */
    public function setParticipant($participant)
    {
        $this->setType($this->getParticipantType($participant));
        switch ($this->type) {
            case self::TYPE_USER:
                $this->setUser($participant);
                break;
            case self::TYPE_GROUP:
                $this->setGroup($participant);
                break;
            case self::TYPE_ORGANIZATION:
                $this->setOrganization($participant);
                break;
        }

        return $this;
    }

    /**
     * @param Organization|Group|User $participant
     *
     * @return string
     */
    static public function getParticipantType($participant)
    {
        switch (get_class($participant)) {
            case 'User':
                return self::TYPE_USER;
            case 'Group':
                return self::TYPE_GROUP;
            case 'Organization':
                return self::TYPE_ORGANIZATION;
            default:
                return null;
        }
    }

    /**
     * @param string $type
     *
     * @return string
     */
    static public function typeToClassName($type)
    {
        switch ($type) {
            case self::TYPE_USER:
                return 'User';
            case self::TYPE_GROUP:
                return 'Group';
            case self::TYPE_ORGANIZATION:
                return 'Organization';
            default:
                return null;
        }
    }
}
