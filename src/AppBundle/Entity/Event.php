<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Class Event
 * @package AppBundle\Entity
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="AppBundle\Repository\EventRepository")
 */
class Event
{
    /**
     *
     */
    const TYPE_SCHOOL_EVENT = 'E';
    /**
     *
     */
    const TYPE_SPORT_EVENT = 'S';
    /**
     *
     */
    const TYPE_CLASS_EVENT = 'C';
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    protected $name;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="string", length=255)
     */
    protected $description;

    /**
     * @var User
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\User")
     * @ORM\JoinColumn(nullable=false)
     */
    protected $owner;

    /**
     * @var string
     *
     * @ORM\Column(name="type", type="string", length=2)
     */
    protected $type;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="time_start", type="datetime")
     */
    protected $timeStart;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="time_end", type="datetime")
     */
    protected $timeEnd;

    /**
     * @var EventRelation[]
     *
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\EventRelation", mappedBy="event")
     */
    protected $eventRelations;

    /**
     * @param int $id
     *
     * @return Event
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param string $name
     *
     * @return Event
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $description
     *
     * @return Event
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param User $owner
     *
     * @return Event
     */
    public function setOwner($owner)
    {
        $this->owner = $owner;

        return $this;
    }

    /**
     * @return User
     */
    public function getOwner()
    {
        return $this->owner;
    }

    /**
     * @param string $type
     *
     * @return Event
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param \DateTime $timeStart
     *
     * @return Event
     */
    public function setTimeStart($timeStart)
    {
        $this->timeStart = $timeStart;

        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getTimeStart()
    {
        return $this->timeStart;
    }

    /**
     * @param \DateTime $timeEnd
     *
     * @return Event
     */
    public function setTimeEnd($timeEnd)
    {
        $this->timeEnd = $timeEnd;

        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getTimeEnd()
    {
        return $this->timeEnd;
    }

    /**
     * @return EventRelation[]
     */
    public function getEventRelations()
    {
        return $this->eventRelations;
    }

    /**
     * @param EventRelation[] $eventRelations
     *
     * @return Event
     */
    public function setEventRelations($eventRelations)
    {
        foreach ($eventRelations as $eventRelation) {
            $eventRelation->setEvent($this);
        }

        $this->eventRelations = $eventRelations;

        return $this;
    }

    /**
     * @param User $user
     *
     * @return string
     */
    public function getUserPermission($user = null)
    {
        if ($this->owner == $user) {
            return ROLE::ROLE_EVENT_OWNER;
        }

        foreach ($this->eventRelations as $eventRelation) {
            switch ($eventRelation->getType()) {
                case 'O':
                    switch ($eventRelation->getOrganization()->getUserPermission($user)) {
                        case ROLE::ROLE_ORGANIZATION_ADMIN:
                            return ROLE::ROLE_EVENT_ADMIN;
                        case ROLE::ROLE_ORGANIZATION_USER:
                            return ROLE::ROLE_EVENT_USER;
                    }
                    break;
                case 'G':
                    $userPermission = $eventRelation->getGroup()->getOrganization()->getUserPermission($user);
                    if ($userPermission == ROLE::ROLE_ORGANIZATION_ADMIN) {
                        return ROLE::ROLE_EVENT_ADMIN;
                    }
                    if ($eventRelation->getGroup()->getUserPermission($user) !== null) {
                        return ROLE::ROLE_EVENT_USER;
                    }
                    break;
                case 'U':
                    if ($eventRelation->getUser() == $user) {
                        return ROLE::ROLE_EVENT_USER;
                    }
            }
        }

        return null;
    }

    /**
     * @param string $type
     *
     * @return Organization[]|Group[]|User[]
     */
    public function getParticipantsOfType($type)
    {
        $participants = [];
        foreach ($this->getEventRelations() as $eventRelation) {
            if ($eventRelation->getType() == $type) {
                $participants[] = $eventRelation->getParticipant();
            }
        }

        return $participants;
    }

    /**
     * @return Organization
     */
    public function getOrganizationParticipants()
    {
        return $this->getParticipantsOfType(EventRelation::TYPE_ORGANIZATION);
    }

    /**
     * @return User[]
     */
    public function getUserParticipants()
    {
        return $this->getParticipantsOfType(EventRelation::TYPE_USER);
    }

    /**
     * @return Group[]
     */
    public function getGroupParticipants()
    {
        return $this->getParticipantsOfType(EventRelation::TYPE_GROUP);
    }

    /**
     * @return mixed
     */
    public static function getEventConst()
    {
        $const[self::TYPE_CLASS_EVENT] = 'Классное событие';
        $const[self::TYPE_SCHOOL_EVENT] = 'Школьное событие';
        $const[self::TYPE_SPORT_EVENT] = 'Спортивное событие';

        return $const;
    }

    /**
     * @return mixed
     */
    public static function getEventListConst()
    {
        $const[] = self::TYPE_CLASS_EVENT;
        $const[] = self::TYPE_SCHOOL_EVENT;
        $const[] = self::TYPE_SPORT_EVENT;

        return $const;
    }

    /**
     * @return string
     */
    public function getFullNameOwner()
    {
        return $this->owner->getFullName();
    }
}
