<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * This pseudo-entity class is used as a mutex-table to simulate UNION keyword in DQL select query in order to
 * display the group and user topic permissions in the same admin list view.
 *
 * The content of this table must be:
 * INSERT INTO `topic_permission_mutex` (`topic_group_permission_id`, `topic_moderator_id`, `id`)
 * VALUES (0, 0, 0), (1, 1, 0);
 *
 * Class TopicPermissionMutex
 * @package AppBundle\Entity
 *
 * @ORM\Table(name="topic_permission_mutex")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\TopicPermissionMutexRepository")
 */
class TopicPermissionMutex
{
    /**
     * @var integer
     *
     * @ORM\Id
     * @ORM\Column(type="integer")
     *
     * WARNING: Don't use autogeneration like this!: ORM\GeneratedValue()
     * WARNING: Don't create setter for this field!
     */
    protected $topicGroupPermissionId;

    /**
     * @var integer
     *
     * @ORM\Id
     * @ORM\Column(type="integer")
     *
     * WARNING: Don't use autogeneration like this!: ORM\GeneratedValue()
     * WARNING: Don't create setter for this field!
     */
    protected $topicModeratorId;

    /**
     * @var integer
     *
     * @ORM\Column(type="integer")
     *
     * This is a dummy field to emulate sorting.
     * WARNING: This is not the primary key!
     * WARNING: Don't create setter for this field!
     */
    protected $id;

    /**
     * @return integer
     */
    public function getTopicGroupPermissionId()
    {
        return $this->topicGroupPermissionId;
    }

    /**
     * @return integer
     */
    public function getTopicModeratorId()
    {
        return $this->topicModeratorId;
    }

    /**
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }
}
