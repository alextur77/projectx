<?php

namespace AppBundle\Entity;

use AppBundle\Entity\Traits\LoggingTrait;
use AppBundle\Entity\Traits\StatusTrait;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Class Group
 * @package AppBundle\Entity
 *
 * @ORM\Entity(repositoryClass="AppBundle\Repository\GroupRepository")
 */
class Group
{
    use StatusTrait;
    use LoggingTrait;

    /**
     *
     */
    const TYPE_CLASS_NUMBER = 11;

    /**
     * @var integer
     *
     * @ORM\Id
     * @ORM\Column(type="integer", nullable=false)
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string
     *
     * @ORM\Column(length=1000)
     */
    protected $name;

    /**
     * @var string
     *
     * @ORM\Column(type="text", nullable=true)
     */
    protected $description;

    /**
     * @var Group
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Group", inversedBy="childrenGroups")
     */
    protected $parentGroup;

    /**
     * @var Group[]|ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Group", mappedBy="parentGroup")
     */
    protected $childrenGroups;

    /**
     * @var Organization
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Organization", inversedBy="childrenGroups")
     * @ORM\JoinColumn(nullable=false)
     */
    protected $organization;

    /**
     * @var GroupUserPermission[]|ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\GroupUserPermission", mappedBy="group")
     */
    protected $groupUserPermissions;

    /**
     * @var bool
     *
     * @ORM\Column(type="boolean")
     */
    protected $public;

    /**
     * @var RegistrationRequest[]|ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\RegistrationRequest", mappedBy="group", cascade={"persist"})
     */
    protected $registrationRequests;

    /**
     * Group constructor.
     */
    public function __construct()
    {
        $this->groupUserPermissions = new ArrayCollection();
        $this->registrationRequests = new ArrayCollection();
        $this->childrenGroups = new ArrayCollection();
        $this->public = false;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     *
     * @return Group
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return Group
     */
    public function getParentGroup()
    {
        return $this->parentGroup;
    }

    /**
     * @param Group $parentGroup
     *
     * @return Group
     */
    public function setParentGroup($parentGroup)
    {
        $this->parentGroup = $parentGroup;

        return $this;
    }

    /**
     * @return Organization
     */
    public function getOrganization()
    {
        return $this->organization;
    }

    /**
     * @param Organization $organization
     *
     * @return Group
     */
    public function setOrganization($organization)
    {
        $this->organization = $organization;

        return $this;
    }

    /**
     * @return GroupUserPermission[]|ArrayCollection
     */
    public function getGroupUserPermissions()
    {
        return $this->groupUserPermissions;
    }

    /**
     * @param GroupUserPermission[]|ArrayCollection $groupUserPermissions
     *
     * @return Group
     */
    public function setGroupUserPermissions($groupUserPermissions)
    {
        $this->groupUserPermissions = $groupUserPermissions;

        return $this;
    }

    /**
     * @param GroupUserPermission $groupUserPermission
     *
     * @return Group
     */
    public function addGroupUserPermissions($groupUserPermission)
    {
        $this->groupUserPermissions->add($groupUserPermission);

        return $this;
    }

    /**
     * @param GroupUserPermission $groupUserPermission
     *
     * @return Group
     */
    public function removeGroupUserPermissions($groupUserPermission)
    {
        $this->groupUserPermissions->removeElement($groupUserPermission);

        return $this;
    }

    /**
     * @return boolean
     */
    public function getPublic()
    {
        return $this->public;
    }

    /**
     * @return boolean
     */
    public function isPublic()
    {
        return $this->public;
    }

    /**
     * @param boolean $public
     *
     * @return Group
     */
    public function setPublic($public)
    {
        $this->public = $public;

        return $this;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     *
     * @return Group
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return (string) $this->name;
    }

    /**
     * @return Group[]|ArrayCollection
     */
    public function getChildrenGroups()
    {
        return $this->childrenGroups;
    }

    /**
     * @param Group[] $childrenGroups
     *
     * @return Group
     */
    public function setChildrenGroups($childrenGroups)
    {
        $this->childrenGroups = $childrenGroups;

        return $this;
    }

    /**
     * @param Group $childrenGroup
     *
     * @return Group
     */
    public function addChildrenGroups($childrenGroup)
    {
        $this->childrenGroups->add($childrenGroup);

        return $this;
    }

    /**
     * @param Group $childrenGroup
     *
     * @return Group
     */
    public function removeChildrenGroups($childrenGroup)
    {
        $this->childrenGroups->removeElement($childrenGroup);

        return $this;
    }

    /**
     * @param User $user
     *
     * @return string
     */
    private function getUserRelations($user)
    {
        foreach ($this->getGroupUserPermissions() as $perm) {
            if ($perm->getUser() == $user) {
                return $perm->getRole()->getCode();
            }
        }

        return null;
    }

    /**
     * @param User $user
     *
     * @return string
     */
    public function getUserPermission($user)
    {
        $permission = null;

        switch ($this->organization->getUserPermission($user)) {
            case Role::ROLE_ORGANIZATION_ADMIN:
                return Role::ROLE_GROUP_ADMIN;
            case Role::ROLE_ORGANIZATION_USER:
                $permission = Role::ROLE_GROUP_USER;
        }

        switch ($this->getUserRelations($user)) {
            case Role::ROLE_GROUP_ADMIN:
                return Role::ROLE_GROUP_ADMIN;
            case Role::ROLE_GROUP_USER:
                $permission = Role::ROLE_GROUP_USER;
        }

        $currentGroup = $this->getParentGroup();

        while ($currentGroup !== null) {
            switch ($this->getUserRelations($user)) {
                case Role::ROLE_GROUP_ADMIN:
                    return Role::ROLE_GROUP_ADMIN;
                case Role::ROLE_GROUP_USER:
                    $permission = Role::ROLE_GROUP_USER;
            }
            $currentGroup = $currentGroup->getParentGroup();
        }

        return $permission;
    }

    /**
     * @return RegistrationRequest[]
     */
    public function getRegistrationRequests()
    {
        return $this->registrationRequests;
    }

    /**
     * @param RegistrationRequest[] $registrationRequests
     *
     * @return Group
     */
    public function setRegistrationRequests($registrationRequests)
    {
        foreach ($registrationRequests as $registrationRequest) {
            $registrationRequest->setGroup($this);
        }
        $this->registrationRequests = $registrationRequests;

        return $this;
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param string $description
     *
     * @return Group
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }
}
