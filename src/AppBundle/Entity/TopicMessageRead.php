<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Class TopicMessageRead
 * @package AppBundle\Entity
 *
 * @ORM\Entity(repositoryClass="AppBundle\Repository\TopicMessageReadRepository")
 */
class TopicMessageRead
{
    /**
     * @var int
     *
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue()
     */
    protected $id;

    /**
     * @var Topic
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Topic")
     * @ORM\JoinColumn(nullable=false)
     */
    protected $topic;

    /**
     * @var Post
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Post")
     * @ORM\JoinColumn(nullable=false)
     */
    protected $post;

    /**
     * @var User
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\User")
     * @ORM\JoinColumn(nullable=true)
     */
    protected $recipient;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     *
     * @return TopicMessageRead
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * @return TopicMessageRead
     */
    public function getTopic()
    {
        return $this->topic;
    }

    /**
     * @param Topic $topic
     *
     * @return TopicMessageRead
     */
    public function setTopic($topic)
    {
        $this->topic = $topic;

        return $this;
    }

    /**
     * @return TopicMessageRead
     */
    public function getPost()
    {
        return $this->post;
    }

    /**
     * @param Post $post
     *
     * @return TopicMessageRead
     */
    public function setPost($post)
    {
        $this->post = $post;

        return $this;
    }

    /**
     * @param User $recipient
     *
     * @return TopicMessageRead
     */
    public function setRecipient($recipient)
    {
        $this->recipient = $recipient;

        return $this;
    }

    /**
     * @return User
     */
    public function getRecipient()
    {
        return $this->recipient;
    }
}