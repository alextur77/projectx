<?php

namespace AppBundle\Entity\Traits;

use Doctrine\ORM\Mapping as ORM;

/**
 * Trait StatusTrait
 */
trait StatusTrait
{
    /**
     * @var bool
     *
     * @ORM\Column(type="boolean")
     */
    protected $active = true;

    /**
     * @return bool
     */
    public function isActive()
    {
        return $this->active;
    }

    /**
     * get active status
     *
     * @return bool
     */
    public function getActive()
    {
        return $this->active;
    }

    /**
     * @param boolean $active
     *
     * @return StatusTrait
     */
    public function setActive($active = true)
    {
        $this->active = $active;

        return $this;
    }

    /**
     * Set inactive status
     */
    public function setInactive()
    {
        $this->active = false;
    }
}

