<?php

namespace AppBundle\Entity;

use AppBundle\Entity\Traits\LoggingTrait;
use AppBundle\Entity\Traits\StatusTrait;
use Doctrine\ORM\Mapping as ORM;

/**
 * Class Role
 * @package AppBundle\Entity
 *
 * @ORM\Entity(repositoryClass="AppBundle\Repository\RoleRepository")
 * @ORM\Table(indexes={@ORM\Index(name="code_idx", columns={"code"})})
 */
class Role
{
    use StatusTrait;
    use LoggingTrait;

    const TYPE_ORGANIZATION = 'O';
    const TYPE_GROUP = 'G';
    const TYPE_SYSTEM = 'S';
    const TYPE_EVENT = 'E';
    const TYPE_TOPIC = 'T';

    const ROLE_ORGANIZATION_ADMIN = 'ROLE_ORGANIZATION_ADMIN';
    const ROLE_ORGANIZATION_USER = 'ROLE_ORGANIZATION_USER';
    const ROLE_GROUP_ADMIN = 'ROLE_GROUP_ADMIN';
    const ROLE_GROUP_USER = 'ROLE_GROUP_USER';
    const ROLE_EVENT_ADMIN = 'ROLE_EVENT_ADMIN';
    const ROLE_EVENT_OWNER = 'ROLE_EVENT_OWNER';
    const ROLE_EVENT_USER = 'ROLE_EVENT_USER';
    const ROLE_TOPIC_ADMIN = 'ROLE_TOPIC_ADMIN';
    const ROLE_TOPIC_USER = 'ROLE_TOPIC_USER';

    /**
     * @var int
     *
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue()
     */
    protected $id;

    /**
     * @var string
     *
     * @ORM\Column(length=1000)
     */
    protected $name;

    /**
     * @var string
     *
     * @ORM\Column(length=1)
     */
    protected $type;

    /**
     * @var string
     *
     * @ORM\Column()
     */
    protected $code;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     *
     * @return Role
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     *
     * @return Role
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param string $type
     *
     * @return Role
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * @return string
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * @param string $code
     *
     * @return Role
     */
    public function setCode($code)
    {
        $this->code = $code;

        return $this;
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return (string) $this->name;
    }
}
