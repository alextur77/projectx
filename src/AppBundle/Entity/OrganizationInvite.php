<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * OrganizationInvite
 *
 * @ORM\Table(name="organization_invite")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\OrganizationInviteRepository")
 */
class OrganizationInvite
{
    /**
     *
     */
    const TYPE_SEND = 'S';
    /**
     *
     */
    const TYPE_VISIT = 'V';

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="email", type="string", length=255)
     */
    private $email;

    /**
     * @var string
     *
     * @ORM\Column(name="url", type="string", length=255)
     */
    private $url;

    /**
     * @var string
     *
     * @ORM\Column(name="was_send", type="string", length=255)
     */
    private $wasSend = 'S';

    /**
     * @var string
     *
     * @ORM\Column(name="hash", type="string", length=255)
     */
    private $hash;

    /**
     * @var
     */
    protected $invite;

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set email
     *
     * @param string $email
     *
     * @return OrganizationInvite
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email
     *
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set hash
     *
     * @param string $hash
     *
     * @return OrganizationInvite
     */
    public function setHash($hash)
    {
        $this->hash = hash('md5', $this->getEmail());

        return $this;
    }

    /**
     * Get hash
     *
     * @return string
     */
    public function getHash()
    {
        return $this->hash;
    }

    /**
     * Set url
     *
     * @param string $url
     *
     * @return OrganizationInvite
     */
    public function setUrl($url)
    {
        $this->url = $url;

        return $this;
    }

    /**
     * Get url
     *
     * @return string
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * Set wasSend
     *
     * @param string $wasSend
     *
     * @return OrganizationInvite
     */
    public function setWasSend($wasSend)
    {
        $this->wasSend = $wasSend;

        return $this;
    }

    /**
     * Get wasSend
     *
     * @return string
     */
    public function getWasSend()
    {
        return $this->wasSend;
    }

    /**
     * @return string
     */
    public function getUrlInvite()
    {
        return 'http://projectx.lh/organization-invite?hash=' . hash('md5', $this->getEmail());
    }

    /**
     * @param string $invite
     *
     * @return void
     */
    public function setUrlInvite($invite)
    {
        $this->url = 'http://projectx.lh/organization-invite?hash=' . hash('md5', $this->getEmail());
    }

    /**
     * @return string
     */
    public function getUrlHash()
    {
        return hash('md5', $this->getEmail());
    }

    /**
     * @param string $invite
     *
     * @return void
     */
    public function setUrlHash($invite)
    {
        $this->hash = hash('md5', $this->getEmail());
    }

    /**
     * @return string
     */
    public function getStatusName()
    {
        switch ($this->getWasSend()) {
            case self::TYPE_SEND:
                return 'Отправлено';
            case self::TYPE_VISIT:
                return 'Переход по ссылке';
        }
    }
}