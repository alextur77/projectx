<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Class EventNotification
 * @package AppBundle\Entity
 *
 * @ORM\Table()
 * @ORM\Entity
 */
class EventNotification
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var User
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Event", inversedBy="eventNotifications")
     * @ORM\JoinColumn(nullable=false)
     */
    protected $event;

    /**
     * @var integer
     *
     * @ORM\Column(name="days_offset", type="integer")
     */
    protected $daysOffset;

    /**
     * @var integer
     *
     * @ORM\Column(name="hours_offset", type="integer")
     */
    protected $hoursOffset;

    /**
     * @var integer
     *
     * @ORM\Column(name="minutes_offset", type="integer")
     */
    protected $minutesOffset;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="string", length=255)
     */
    protected $description;

    /**
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     *
     * @return EventNotification
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param string $description
     *
     * @return EventNotification
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * @return int
     */
    public function getDaysOffset()
    {
        return $this->daysOffset;
    }

    /**
     * @param int $daysOffset
     *
     * @return EventNotification
     */
    public function setDaysOffset($daysOffset)
    {
        $this->daysOffset = $daysOffset;

        return $this;
    }

    /**
     * @return int
     */
    public function getHoursOffset()
    {
        return $this->hoursOffset;
    }

    /**
     * @param int $hoursOffset
     *
     * @return EventNotification
     */
    public function setHoursOffset($hoursOffset)
    {
        $this->hoursOffset = $hoursOffset;

        return $this;
    }

    /**
     * @return int
     */
    public function getMinutesOffset()
    {
        return $this->minutesOffset;
    }

    /**
     * @param int $minutesOffset
     *
     * @return EventNotification
     */
    public function setMinutesOffset($minutesOffset)
    {
        $this->minutesOffset = $minutesOffset;

        return $this;
    }

}
