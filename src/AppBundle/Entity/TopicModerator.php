<?php

namespace AppBundle\Entity;

use AppBundle\Entity\Traits\LoggingTrait;
use Doctrine\ORM\Mapping as ORM;

/**
 * Class TopicModerator
 * @package AppBundle\Entity
 *
 * @ORM\Entity(repositoryClass="AppBundle\Repository\TopicModeratorRepository")
 * @ORM\Table(uniqueConstraints={@ORM\UniqueConstraint(name="topic_moderator", columns={"topic_id", "user_id"})})
 */
class TopicModerator
{
    use LoggingTrait;

    /**
     * @var int
     *
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue()
     */
    protected $id;

    /**
     * @var Topic
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Topic", inversedBy="topicModerators")
     * @ORM\JoinColumn(nullable=false)
     */
    protected $topic;

    /**
     * @var User
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\User")
     * @ORM\JoinColumn(nullable=false)
     */
    protected $user;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     *
     * @return TopicModerator
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * @return Topic
     */
    public function getTopic()
    {
        return $this->topic;
    }

    /**
     * @param Topic $topic
     *
     * @return TopicModerator
     */
    public function setTopic($topic)
    {
        $this->topic = $topic;

        return $this;
    }

    /**
     * @return User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param User $user
     *
     * @return TopicModerator
     */
    public function setUser($user)
    {
        $this->user = $user;

        return $this;
    }
}
