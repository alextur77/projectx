<?php

namespace AppBundle\Security;

use FOS\UserBundle\Security\InteractiveLoginListener as BaseInteractiveLoginListener;
use Symfony\Component\Security\Http\Event\InteractiveLoginEvent;

/**
 * Class InteractiveLoginListener
 */
class InteractiveLoginListener extends BaseInteractiveLoginListener
{
    /**
     * @param InteractiveLoginEvent $event
     */
    public function onSecurityInteractiveLogin(InteractiveLoginEvent $event)
    {
    }
}
