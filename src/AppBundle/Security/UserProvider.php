<?php

namespace AppBundle\Security;

use AppBundle\Repository\UserRepository;
use FOS\UserBundle\Security\UserProvider as BaseUserProvider;

/**
 * Class UserProvider
 */
class UserProvider extends BaseUserProvider
{
    /**
     * @var UserRepository
     */
    protected $userRepository;

    /**
     * @param UserRepository $userRepository
     *
     * @return UserProvider
     */
    public function setUserRepository($userRepository)
    {
        $this->userRepository = $userRepository;

        return $this;
    }

    /**
     * @param string $username
     *
     * @return \FOS\UserBundle\Model\UserInterface|null
     */
    protected function findUser($username)
    {
        return $this->userRepository->findUserWithRoles($username);
    }
}
