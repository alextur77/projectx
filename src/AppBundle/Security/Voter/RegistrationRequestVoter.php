<?php

namespace AppBundle\Security\Voter;

use AppBundle\Entity\RegistrationRequest;
use AppBundle\Entity\User;
use AppBundle\Repository\RegistrationRequestRepository;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\VoterInterface;

/**
 * Class RegistrationRequestVoter
 */
class RegistrationRequestVoter implements VoterInterface
{
    /**
     * @var RegistrationRequestRepository
     */
    protected $registrationRequestRepository;

    /**
     * @param string $attribute
     *
     * @return bool
     */
    public function supportsAttribute($attribute)
    {
        return true;
    }

    /**
     * @param string $class
     *
     * @return bool
     */
    public function supportsClass($class)
    {
        $supportedClass = 'AppBundle\Entity\RegistrationRequest';

        return $supportedClass === $class || is_subclass_of($class, $supportedClass);
    }

    /**
     * @param TokenInterface      $token
     * @param RegistrationRequest $registrationRequest
     * @param array               $attributes
     *
     * @return int
     */
    public function vote(TokenInterface $token, $registrationRequest, array $attributes)
    {
        if (!$this->supportsClass(get_class($registrationRequest))) {
            return VoterInterface::ACCESS_ABSTAIN;
        }

        $user = $token->getUser();

        // make sure there is a user object (i.e. that the user is logged in)
        if (!$user instanceof User) {
            return VoterInterface::ACCESS_DENIED;
        }

        if ($this->registrationRequestRepository->checkAccess($registrationRequest, $user)) {
            return VoterInterface::ACCESS_GRANTED;
        }

        return VoterInterface::ACCESS_DENIED;
    }

    /**
     * @param RegistrationRequestRepository $registrationRequestRepository
     *
     * @return RegistrationRequestVoter
     */
    public function setRegistrationRequestRepository($registrationRequestRepository)
    {
        $this->registrationRequestRepository = $registrationRequestRepository;

        return $this;
    }
}
