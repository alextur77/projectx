<?php

namespace AppBundle\Manager;

use AppBundle\Entity\Event;
use AppBundle\Entity\EventRelation;
use AppBundle\Entity\Group;
use AppBundle\Entity\Organization;
use AppBundle\Entity\User;
use AppBundle\Repository\EventRelationRepository;
use Aristek\Component\ORM\AbstractEntityManager;

/**
 * Class EventManager
 *
 * @method EventRelation create()
 */
class EventRelationManager extends AbstractEntityManager
{
    /**
     * @var EventRelationRepository
     */
    protected $repository;

    /**
     * @param Event                   $event
     * @param User|Organization|Group $organization
     * @param string                  $type
     * @param Group                   $group
     */
    public function add($event, $organization, $type, $group)
    {
        $eventRelation = $this->create();
        $eventRelation->setEvent($event);
        $eventRelation->setOrganization($organization);
        $eventRelation->setType($type);
        $eventRelation->setGroup($group);
        $this->save($eventRelation);
    }

    /**
     * @param Event  $event
     * @param string $type
     * @param Group  $group
     *
     * @return void
     */
    public function update($event, $type, $group)
    {
        /** @var EventRelation $eventRelation */
        $eventRelation = $this->repository->findOneBy(['event' => $event]);
        $eventRelation->setType($type);
        $eventRelation->setGroup($group);
        $this->save($eventRelation);
    }
}
