<?php

namespace AppBundle\Manager;

use AppBundle\Entity\Organization;
use AppBundle\Entity\OrganizationUserPermission;
use AppBundle\Entity\Role;
use AppBundle\Entity\User;
use AppBundle\Repository\RoleRepository;
use Aristek\Component\ORM\AbstractEntityManager;

/**
 * Class OrganizationUserPermissionManager
 *
 * @method OrganizationUserPermission create()
 */
class OrganizationUserPermissionManager extends AbstractEntityManager
{
    /**
     * @var RoleRepository
     */
    protected $roleRepository;

    /**
     * @param Organization $organization
     * @param User         $user
     * @param string       $role
     */
    public function registerUser($organization, $user, $role = Role::ROLE_ORGANIZATION_USER)
    {
        $role = $this->roleRepository->findByTypeAndCode(Role::TYPE_ORGANIZATION, $role);

        $organizationUserPermission = $this->create();
        $organizationUserPermission->setOrganization($organization);
        $organizationUserPermission->setUser($user);
        $organizationUserPermission->setRole($role);
        $this->save($organizationUserPermission);
    }

    /**
     * @param RoleRepository $roleRepository
     *
     * @return OrganizationUserPermissionManager
     */
    public function setRoleRepository($roleRepository)
    {
        $this->roleRepository = $roleRepository;

        return $this;
    }
}
