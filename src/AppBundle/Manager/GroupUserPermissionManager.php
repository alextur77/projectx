<?php

namespace AppBundle\Manager;

use AppBundle\Entity\Group;
use AppBundle\Entity\GroupUserPermission;
use AppBundle\Entity\Role;
use AppBundle\Entity\User;
use AppBundle\Repository\RoleRepository;
use AppBundle\Repository\GroupUserPermissionRepository;
use Aristek\Component\ORM\AbstractEntityManager;

/**
 * Class GroupUserPermissionManager
 * @package AppBundle\Manager
 *
 * @method GroupUserPermission create()
 */
class GroupUserPermissionManager extends AbstractEntityManager
{
    /**
     * @var string
     */
    private $entityName = 'AppBundle\Entity\GroupUserPermission';

    /**
     * @var GroupUserPermissionRepository
     */
    protected $repository;

    /**
     * @var RoleRepository
     */
    protected $roleRepository;

    /**
     * @param integer|GroupUserPermission $groupUserPermission - groupUserPermissionId or GroupUserPermission
     * @param integer|Role                $role                - roleId or Role
     *
     * @return bool - true if the user role was really changed
     */
    public function changeUserRole($groupUserPermission, $role)
    {
        $isChanged = $groupUserPermission instanceof GroupUserPermission;
        if (!$isChanged) {
            $isChanged = null !== $groupUserPermission;
            if ($isChanged) {
                $groupUserPermission = $this->repository->find($groupUserPermission);
                $isChanged = null !== $groupUserPermission;
            }
        }
        if ($isChanged) {
            $isChanged = $role instanceof Role;
            if (!$isChanged) {
                $isChanged = null !== $role;
                if ($isChanged) {
                    $role = $this->roleRepository->find($role);
                    $isChanged = null !== $role;
                }
            }
        }
        if ($isChanged) {
            $isChanged = $groupUserPermission->getRole()->getId() != $role->getId();
        }
        if ($isChanged) {
            $groupUserPermission->setRole($role);
            $this->save($groupUserPermission);
        }

        return $isChanged;
    }

    /**
     * @param Group       $group
     * @param User        $user
     * @param string|Role $role
     */
    public function registerUser(Group $group, User $user, $role = Role::ROLE_GROUP_USER)
    {
        if (!$role instanceof Role) {
            $role = $this->roleRepository->findByTypeAndCode(Role::TYPE_GROUP, $role);
        }
        $groupUserPermission = $this->repository->findOneBy(['group' => $group, 'user' => $user]);
        if (null === $groupUserPermission) {
            $groupUserPermission = $this->create();
            $groupUserPermission->setGroup($group);
            $groupUserPermission->setUser($user);
            $groupUserPermission->setRole($role);
            $this->save($groupUserPermission);
        } elseif ($groupUserPermission->getRole()->getId() != $role->getId()) {
            $groupUserPermission->setRole($role);
            $this->save($groupUserPermission);
        }
    }

    /**
     * @param integer|Group|integer[]|Group[] $groups
     * @param User                            $user
     */
    public function removeUser($groups, User $user)
    {
        if (!is_array($groups)) {
            $groups = [$groups];
        }
        $alias = 'Groups';
        $qb = $this->em->createQueryBuilder();
        $qb
            ->delete()
            ->from($this->entityName, $alias)
            ->andWhere($alias . '.user = :user')
            ->andWhere($qb->expr()->in($alias . '.group', ':groups'))
            ->setParameter('user', $user)
            ->setParameter('groups', $groups);

        $qb->getQuery()->execute();
    }

    /**
     * @param RoleRepository $roleRepository
     *
     * @return GroupUserPermissionManager
     */
    public function setRoleRepository($roleRepository)
    {
        $this->roleRepository = $roleRepository;

        return $this;
    }
}
