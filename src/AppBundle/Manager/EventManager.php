<?php

namespace AppBundle\Manager;

use AppBundle\Entity\Event;
use AppBundle\Entity\User;
use Aristek\Component\ORM\AbstractEntityManager;

/**
 * Class EventManager
 *
 * @method EventManager create()
 */
class EventManager extends AbstractEntityManager
{

    /**
     * @param string    $title
     * @param User      $owner
     * @param string    $description
     * @param \DateTime $dateStart
     * @param \DateTime $dateFinish
     * @param string    $type
     *
     * @return void
     */
    public function add($title, $owner, $description, $dateStart, $dateFinish, $type)
    {
        /** @var Event $event */
        $event = $this->create();
        $event->setName($title);
        $event->setOwner($owner);
        $event->setDescription($description);
        $event->setTimeStart($dateStart);
        $event->setTimeEnd($dateFinish);
        $event->setType($type);
        $this->save($event);
    }

    /**
     * @return mixed
     */
    public function getEventListConst()
    {
        /** @var Event $event */
        $event = $this->create();

        return $event->getEventListConst();
    }

    /**
     * @param integer   $id
     * @param string    $title
     * @param User      $owner
     * @param string    $description
     * @param \DateTime $dateStart
     * @param \DateTime $dateFinish
     * @param string    $type
     *
     * @return void
     */
    public function update($id, $title, $owner, $description, $dateStart, $dateFinish, $type)
    {
        $event = $this->repository->find($id);
        $event->setName($title);
        $event->setOwner($owner);
        $event->setDescription($description);
        $event->setTimeStart($dateStart);
        $event->setTimeEnd($dateFinish);
        $event->setType($type);
        $this->save($event);
    }
}
