<?php
namespace AppBundle\Manager;

use AppBundle\Entity\Organization;
use AppBundle\Entity\PersonalMessage;
use AppBundle\Entity\User;
use Aristek\Component\ORM\AbstractEntityManager;

/**
 * Class PersonalMessageManager
 *
 * @method PersonalMessageManager create()
 */
class PersonalMessageManager extends AbstractEntityManager
{
    /**
     * @param string       $subject
     * @param string       $message
     * @param User         $sender
     * @param User         $recipient
     * @param Organization $organization
     *
     * @return void
     */
    public function add($subject, $message, $sender, $recipient, $organization)
    {
        /** @var PersonalMessage $read */
        $read = $this->create();
        $read->setBody($message);
        $read->setSubject($subject);
        $read->setOrganization($organization);
        $read->setSender($sender);
        $read->setRecipient($recipient);
        $this->save($read);
    }
}
