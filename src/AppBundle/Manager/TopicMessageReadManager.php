<?php
namespace AppBundle\Manager;

use AppBundle\Entity\Post;
use AppBundle\Entity\Topic;
use AppBundle\Entity\TopicMessageRead;
use AppBundle\Entity\User;
use Aristek\Component\ORM\AbstractEntityManager;

/**
 * Class TopicMessageReadManager
 *
 * @method TopicMessageReadManager create()
 */
class TopicMessageReadManager extends AbstractEntityManager
{
    /**
     * @param Topic $topic
     * @param Post  $post
     * @param User  $recipient
     *
     * @return void
     */
    public function add($topic, $post, $recipient)
    {
        /** @var TopicMessageRead $postRead */
        $postRead = $this->create();
        $postRead->setTopic($topic);
        $postRead->setPost($post);
        $postRead->setRecipient($recipient);
        $this->save($postRead);
    }
}
