<?php
namespace AppBundle\Manager;

use AppBundle\Entity\GroupClosure;
use AppBundle\Entity\Role;
use AppBundle\Repository\RoleRepository;
use Aristek\Component\ORM\AbstractEntityManager;
use ReflectionClass;

/**
 * Class RoleManager
 *
 * @method RoleManager create()
 */
class RoleManager extends AbstractEntityManager
{
    /**
     * @var RoleRepository
     */
    protected $repository;

    /**
     * @param string $name
     * @param string $type
     * @param string $code
     *
     * @return void
     */
    public function add($name, $type, $code)
    {
        /** @var Role $role */
        $role = $this->create();
        $role->setName($name);
        $role->setType($type);
        $role->setCode($code);
        $role->setActive(true);
        $this->save($role);
    }

    /**
     * @param string $code
     *
     * @return Role
     */
    public function checkIfNotExistRole($code)
    {
        return $this->repository->findOneBy(['code' => $code]);
    }
}
