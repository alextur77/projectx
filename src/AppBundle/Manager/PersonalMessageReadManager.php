<?php
namespace AppBundle\Manager;

use AppBundle\Entity\PersonalMessage;
use AppBundle\Entity\PersonalMessageRead;
use AppBundle\Entity\User;
use Aristek\Component\ORM\AbstractEntityManager;

/**
 * Class PersonalMessageReadManager
 *
 * @method PersonalMessageReadManager create()
 */
class PersonalMessageReadManager extends AbstractEntityManager
{
    /**
     * @param PersonalMessage $message
     * @param User            $user
     *
     * @return void
     */
    public function add($message, $user)
    {
        /** @var PersonalMessageRead $read */
        $read = $this->create();
        $read->setMessage($message);
        $read->setRecipient($user);
        $this->save($read);
    }
}
