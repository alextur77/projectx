<?php

namespace AppBundle\Manager;

use Aristek\Component\ORM\AbstractEntityManager;
use AppBundle\Entity\Topic;
use AppBundle\Entity\TopicModerator;
use AppBundle\Repository\UserRepository;
use AppBundle\Repository\TopicModeratorRepository;

/**
 * Class TopicModeratorManager
 * @package AppBundle\Manager
 *
 * @method TopicModerator create()
 */
class TopicModeratorManager extends AbstractEntityManager
{
    /**
     * @var TopicModeratorRepository
     */
    protected $repository;

    /**
     * @var UserRepository
     */
    protected $userRepository;

    /**
     * @param TopicModerator|TopicModerator[]|integer|integer[] $topicModerators
     */
    public function delete($topicModerators)
    {
        if (!is_array($topicModerators)) {
            $topicModerators = [$topicModerators];
        }
        $qb = $this->repository->createDeleteQueryBuilder();
        $qb
            ->where($qb->expr()->in(TopicModeratorRepository::ALIAS . '.id', ':topicModerators'))
            ->setParameter('topicModerators', $topicModerators);
        $qb->getQuery()->execute();
    }

    /**
     * @param Topic|Topic[]|integer|integer[] $topics - Topic objects or the topic ids
     */
    public function deleteByTopic($topics)
    {
        if (!is_array($topics)) {
            $topics = [$topics];
        }
        $qb = $this->repository->createDeleteQueryBuilder();
        $qb
            ->where($qb->expr()->in(TopicModeratorRepository::ALIAS . '.topic', ':topics'))
            ->setParameter('topics', $topics);
        $qb->getQuery()->execute();
    }

    /**
     * @param Topic|integer $topic - Topic object or the topic id
     */
    public function deleteProhibited($topic)
    {
        $qb = $this->repository->createProhibitedQueryBuilder($topic);
        $qb->select($qb->getRootAliases()[0] . '.id');
        $ids = $qb->getQuery()->getResult();
        $ids = array_column($ids, 'id');

        $qbDelete = $this->repository->createDeleteQueryBuilder();
        $qbDelete
            ->where($qb->expr()->in($qbDelete->getRootAliases()[0] . '.id', ':ids'))
            ->setParameter('ids', $ids);
        $qbDelete->getQuery()->execute();
    }

    /**
     * @param Topic                         $topic
     * @param User|User[]|integer|integer[] $moderators
     */
    public function add(Topic $topic, $moderators)
    {
        if (!is_array($moderators)) {
            $moderators = [$moderators];
        }
        if (!reset($moderators) instanceof User) {
            $moderators = $this->userRepository->findBy(['id' => $moderators]);
        }
        foreach ($moderators as $user) {
            $topicModerator = $this->repository->findOneBy(['topic' => $topic, 'user' => $user]);
            if (null === $topicModerator) {
                $topicModerator = $this->create();
                $topicModerator->setTopic($topic);
                $topicModerator->setUser($user);
                $this->save($topicModerator);
            }
        }
    }

    /**
     * @param Topic|integer|null                 $topic
     * @param User|User[]|integer|integer[]|null $users
     *
     * @return integer
     */
    public function getQuantity(Topic $topic = null, $users = null)
    {
        $qb = $this->repository->em()->createQueryBuilder();
        $qb
            ->select($qb->expr()->count(TopicModeratorRepository::ALIAS . '.id'))
            ->from($this->repository->getClassName(), TopicModeratorRepository::ALIAS);
        if (null !== $topic) {
            $qb
                ->where(TopicModeratorRepository::ALIAS . '.topic = :topic')
                ->setParameter('topic', $topic);
        }
        if (null !== $users) {
            $qb
                ->andWhere($qb->expr()->in(TopicModeratorRepository::ALIAS . '.user', ':users'))
                ->setParameter('users', $users);
        }

        return $qb->getQuery()->getSingleScalarResult();
    }

    /**
     * @param UserRepository $userRepository
     *
     * @return TopicModeratorManager
     */
    public function setUserRepository($userRepository)
    {
        $this->userRepository = $userRepository;

        return $this;
    }
}
