<?php

namespace AppBundle\Manager;

use AppBundle\Entity\RegistrationInvite;
use Aristek\Component\ORM\AbstractEntityManager;
use AppBundle\Repository\RegistrationInviteRepository;
use AppBundle\Repository\GroupUserPermissionRepository;
use AppBundle\Repository\OrganizationUserPermissionRepository;
use AppBundle\Entity\Organization;
use AppBundle\Entity\User;
use AppBundle\Entity\Group;
use AppBundle\Service\User\CurrentUserService;
use FOS\UserBundle\Util\Canonicalizer;

/**
 * Class RegistrationInviteManager
 * @package AppBundle\Manager
 *
 * @method RegistrationInvite create()
 */
class RegistrationInviteManager extends AbstractEntityManager
{
    /**
     * @var Canonicalizer
     */
    protected $canonicalizer;

    /**
     * @var RegistrationInviteRepository
     */
    protected $repository;

    /**
     * @var CurrentUserService
     */
    protected $currentUserService;

    /**
     * @var OrganizationUserPermissionManager
     */
    protected $organizationUserPermissionManager;

    /**
     * @var OrganizationUserPermissionRepository
     */
    protected $organizationUserPermissionRepository;

    /**
     * @var GroupUserPermissionManager
     */
    protected $groupUserPermissionManager;

    /**
     * @var GroupUserPermissionRepository
     */
    protected $groupUserPermissionRepository;

    /**
     * Stores in the database a single record for pair of $email/[$group|$organization].
     *
     * @param string       $email
     * @param string       $token
     * @param Group        $group
     * @param Organization $organization
     *
     * @return RegistrationInvite
     */
    public function addNew($email, $token, Group $group = null, Organization $organization = null)
    {
        $invite = $this->create();
        $invite->setEmail($email);
        $invite->setOrganization($organization);
        $invite->setGroup($group);
        $invite->setToken($token);
        $this->save($invite);

        return $invite;
    }

    /**
     * @param int $id
     *
     * @return bool $accepted
     */
    public function acceptById($id)
    {
        $invite = $this->repository->find($id);
        return $this->accept($invite);
    }

    /**
     * Confirms $invites for the user currently logged on.
     *
     * @param RegistrationInvite|RegistrationInvite[] $invites
     *
     * @return bool $accepted
     */
    public function accept($invites)
    {
        $accepted = false;
        $currUser = $this->currentUserService->getCurrentUser();
        if (!is_array($invites)) {
            $invites = [$invites];
        }

        $processedOrganizations = [];
        $processedGroups = [];
        /** @var RegistrationInvite $invite */
        foreach ($invites as $invite) {
            if ($invite->getStatus() === RegistrationInvite::STATUS_NEW) {
                $qb = $this->repository->createUpdateQueryBuilder($currUser, RegistrationInvite::STATUS_ACCEPTED);
                $needExecute = false;

                $organization = $invite->getOrganization();
                if (null !== $organization) {
                    $id = $organization->getId();
                    if (!array_key_exists($id, $processedOrganizations)) {
                        $processedOrganizations[$id] = $id;
                        if (!$this->organizationUserPermissionRepository->checkExists($organization, $currUser)) {
                            $this->organizationUserPermissionManager->registerUser($organization, $currUser);
                            $accepted = true;
                        }
                        $qb
                            ->andWhere($qb->expr()->eq($qb->getRootAliases()[0] . '.organization', ':organization'))
                            ->setParameter('organization', $organization);
                        $needExecute = true;
                    }
                } else {
                    $group = $invite->getGroup();
                    if (null !== $group) {
                        $id = $group->getId();
                        if (!array_key_exists($id, $processedGroups)) {
                            $processedGroups[$id] = $id;
                            if (!$this->groupUserPermissionRepository->checkExists($group, $currUser)) {
                                $this->groupUserPermissionManager->registerUser($group, $currUser);
                                $accepted = true;
                            }
                            $qb
                                ->andWhere($qb->expr()->eq($qb->getRootAliases()[0] . '.group', ':group'))
                                ->setParameter('group', $group);
                            $needExecute = true;
                        }
                    }
                }

                if ($needExecute) {
                    $qb->getQuery()->execute();
                }
            }
        }

        return $accepted;
    }

    /**
     * @param int $id
     */
    public function declineById($id)
    {
        $invite = $this->repository->find($id);
        $this->decline($invite);
    }

    /**
     * Declines $invites for the user currently logged on.
     *
     * @param RegistrationInvite|RegistrationInvite[] $invites
     */
    public function decline($invites)
    {
        $currUser = $this->currentUserService->getCurrentUser();
        if (!is_array($invites)) {
            $invites = [$invites];
        }

        $processedOrganizations = [];
        $processedGroups = [];
        /** @var RegistrationInvite $invite */
        foreach ($invites as $invite) {
            if ($invite->getStatus() === RegistrationInvite::STATUS_NEW) {
                $qb = $this->repository->createUpdateQueryBuilder($currUser, RegistrationInvite::STATUS_DECLINED);
                $needExecute = false;

                $organization = $invite->getOrganization();
                if (null !== $organization) {
                    $id = $organization->getId();
                    if (!array_key_exists($id, $processedOrganizations)) {
                        $processedOrganizations[$id] = $id;
                        $qb
                            ->andWhere($qb->expr()->eq($qb->getRootAliases()[0] . '.organization', ':organization'))
                            ->setParameter('organization', $organization);
                        $needExecute = true;
                    }
                } else {
                    $group = $invite->getGroup();
                    if (null !== $group) {
                        $id = $group->getId();
                        if (!array_key_exists($id, $processedGroups)) {
                            $processedGroups[$id] = $id;
                            $qb
                                ->andWhere($qb->expr()->eq($qb->getRootAliases()[0] . '.group', ':group'))
                                ->setParameter('group', $group);
                            $needExecute = true;
                        }
                    }
                }

                if ($needExecute) {
                    $qb->getQuery()->execute();
                }
            }
        }
    }

    /**
     * @param string $token
     * @param User   $user
     */
    public function setUserForToken($token, User $user)
    {
        $invites = $this->repository->findByToken($token);
        foreach ($invites as $invite) {
            $invite->setUser($user);
            $this->save($invite);
        }
    }

    /**
     * @param string $token
     *
     * @return string|null
     */
    public function getCanonicalEmailByToken($token)
    {
        $email = null;
        $registrationInvite = $this->repository->findOneByToken($token);
        if (!is_null($registrationInvite)) {
            $email = $registrationInvite->getEmail();
        }
        if (!is_null($email)) {
            $email = $this->canonicalizer->canonicalize($email);
        }

        return $email;
    }

    /**
     * @param string $token
     *
     * @return User|null
     */
    public function getUserByToken($token)
    {
        $user = null;
        $registrationInvite = $this->repository->findOneByToken($token);
        if (!is_null($registrationInvite)) {
            $user = $registrationInvite->getUser();
        }

        return $user;
    }

    /**
     * @param integer $inviteID
     *
     * @return void
     */
    public function setRegistrationInvite($inviteID)
    {
        $registrationInvite = $this->repository->find($inviteID);
        $registrationInvite->setFlagAdmin(RegistrationInvite::FLAG_ADMIN_Y);
        $this->save($registrationInvite);
    }

    /**
     * @param Canonicalizer $canonicalizer
     *
     * @return RegistrationInviteManager
     */
    public function setCanonicalizer($canonicalizer)
    {
        $this->canonicalizer = $canonicalizer;

        return $this;
    }

    /**
     * @param CurrentUserService $currentUserService
     *
     * @return RegistrationInviteManager
     */
    public function setCurrentUserService(CurrentUserService $currentUserService)
    {
        $this->currentUserService = $currentUserService;

        return $this;
    }

    /**
     * @param OrganizationUserPermissionManager $organizationUserPermissionManager
     *
     * @return RegistrationInviteManager
     */
    public function setOrganizationUserPermissionManager($organizationUserPermissionManager)
    {
        $this->organizationUserPermissionManager = $organizationUserPermissionManager;

        return $this;
    }

    /**
     * @param OrganizationUserPermissionRepository $organizationUserPermissionRepository
     *
     * @return RegistrationInviteManager
     */
    public function setOrganizationUserPermissionRepository($organizationUserPermissionRepository)
    {
        $this->organizationUserPermissionRepository = $organizationUserPermissionRepository;

        return $this;
    }

    /**
     * @param GroupUserPermissionManager $groupUserPermissionManager
     *
     * @return RegistrationInviteManager
     */
    public function setGroupUserPermissionManager($groupUserPermissionManager)
    {
        $this->groupUserPermissionManager = $groupUserPermissionManager;

        return $this;
    }

    /**
     * @param GroupUserPermissionRepository $groupUserPermissionRepository
     *
     * @return RegistrationInviteManager
     */
    public function setGroupUserPermissionRepository($groupUserPermissionRepository)
    {
        $this->groupUserPermissionRepository = $groupUserPermissionRepository;

        return $this;
    }
}
