<?php

namespace AppBundle\Manager;

use AppBundle\Entity\OrganizationInvite;

use AppBundle\Repository\OrganizationInviteRepository;
use Aristek\Component\ORM\AbstractEntityManager;



/**
 * Class OrganizationInviteManager
 *
 * @method OrganizationInvite update()
 */
class OrganizationInviteManager extends AbstractEntityManager
{
    /**
     * @var OrganizationInviteRepository
     */
    protected $repository;

    /**
     * @param integer $userID
     *
     * @return void
     */
    public function updateOrganizationInvite($userID)
    {
        $invite = $this->repository->find($userID);
        $invite->setWasSend(OrganizationInvite::TYPE_VISIT);
        $this->save($invite);
    }
}
