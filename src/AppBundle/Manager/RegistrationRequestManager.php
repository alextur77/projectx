<?php

namespace AppBundle\Manager;

use Aristek\Component\ORM\AbstractEntityManager;
use AppBundle\Entity\Group;
use AppBundle\Entity\Organization;
use AppBundle\Entity\RegistrationRequest;
use AppBundle\Entity\User;
use AppBundle\Exception\UserLevelException;
use AppBundle\Repository\GroupUserPermissionRepository;
use AppBundle\Repository\OrganizationUserPermissionRepository;
use AppBundle\Repository\RegistrationRequestRepository;
use Symfony\Component\Translation\Translator;

/**
 * Class RegistrationRequestManager
 *
 * @method RegistrationRequest create()
 */
class RegistrationRequestManager extends AbstractEntityManager
{
    /**
     * @var RegistrationRequestRepository
     */
    protected $repository;

    /**
     * @var Translator
     */
    protected $translator;

    /**
     * @var OrganizationUserPermissionManager
     */
    protected $organizationUserPermissionManager;

    /**
     * @var OrganizationUserPermissionRepository
     */
    protected $organizationUserPermissionRepository;

    /**
     * @var GroupUserPermissionManager
     */
    protected $groupUserPermissionManager;

    /**
     * @var GroupUserPermissionRepository
     */
    protected $groupUserPermissionRepository;

    /**
     * @param User         $user
     * @param Organization $organization
     *
     * @return RegistrationRequest
     */
    public function createOrganizationRequest(User $user, Organization $organization)
    {
        $this->checkAbilityToRegister($user, $organization, null);

        $registrationRequest = $this->create();
        $registrationRequest->setOrganization($organization);
        $registrationRequest->setUser($user);
        $registrationRequest->setGroup(null);
        $this->save($registrationRequest);

        return $registrationRequest;
    }

    /**
     * @param User    $user
     * @param Group[] $groups
     */
    public function createGroupsRequest(User $user, $groups)
    {
        if (!is_array($groups)) {
            $groups = [$groups];
        }
        // First check all groups...
        foreach ($groups as $group) {
            $this->checkAbilityToRegister($user, null, $group);
        }
        // ...and only then write to DB!
        foreach ($groups as $group) {
            $registrationRequest = $this->create();
            $registrationRequest->setOrganization($group->getOrganization());
            $registrationRequest->setUser($user);
            $registrationRequest->setGroup($group);
            $this->save($registrationRequest);
        }
    }

    /**
     * @param int $id - ID of RegistrationRequest
     */
    public function approveById($id)
    {
        $registrationRequest = $this->repository->find($id);
        $this->approve($registrationRequest);
    }

    /**
     * @param RegistrationRequest $registrationRequest
     */
    public function approve(RegistrationRequest $registrationRequest)
    {
        $user = $registrationRequest->getUser();
        $organization = $registrationRequest->getOrganization();
        $group = $registrationRequest->getGroup();

        if (null === $group) {
            if (null === $organization) {
                throw new \LogicException(
                    __METHOD__ . ': Registration request with ID=' . $id . ' has no organization nor group!'
                );
            }
            if (!$this->organizationUserPermissionRepository->checkExists($organization, $user)) {
                $this->organizationUserPermissionManager->registerUser($organization, $user);
            }
        } else {
            if (null === $organization) {
                $organization = $group->getOrganization();
            }
            if (!$this->groupUserPermissionRepository->checkExists($group, $user)) {
                $this->groupUserPermissionManager->registerUser($group, $user);
            }
        }

        $qb = $this->repository->createUpdateQueryBuilder(
            RegistrationRequest::STATUS_APPROVED,
            $user,
            $organization,
            $group
        );
        $qb->getQuery()->execute();
    }

    /**
     * @param int    $id      - ID of RegistrationRequest
     * @param string $comment - Comment for the declination
     */
    public function declineById($id, $comment = null)
    {
        $registrationRequest = $this->repository->find($id);
        $this->decline($registrationRequest, $comment);
    }

    /**
     * @param RegistrationRequest $registrationRequest
     * @param string|null         $comment - Comment for the declination
     */
    public function decline(RegistrationRequest $registrationRequest, $comment = null)
    {
        $user = $registrationRequest->getUser();
        $organization = $registrationRequest->getOrganization();
        $group = $registrationRequest->getGroup();

        $qb = $this->repository->createUpdateQueryBuilder(
            RegistrationRequest::STATUS_DECLINED,
            $user,
            $organization,
            $group
        );
        $qb->set($qb->getRootAliases()[0] . '.comment', '\'' . $comment . '\'');
        $qb->getQuery()->execute();
    }

    /**
     * @param OrganizationUserPermissionManager $organizationUserPermissionManager
     *
     * @return RegistrationRequestManager
     */
    public function setOrganizationUserPermissionManager($organizationUserPermissionManager)
    {
        $this->organizationUserPermissionManager = $organizationUserPermissionManager;

        return $this;
    }

    /**
     * @param GroupUserPermissionManager $groupUserPermissionManager
     *
     * @return RegistrationRequestManager
     */
    public function setGroupUserPermissionManager($groupUserPermissionManager)
    {
        $this->groupUserPermissionManager = $groupUserPermissionManager;

        return $this;
    }

    /**
     * @param User              $user
     * @param Organization|null $organization
     * @param Group|null        $group
     *
     * @throws UserLevelException
     */
    private function checkAbilityToRegister(User $user, Organization $organization = null, Group $group = null)
    {
        if (null !== $group) {
            if ($this->repository->checkExists($user, null, $group)) {
                throw new UserLevelException(
                    $this->translator->trans(
                        'registration_request.error.group_request_already_exists',
                        ['%group%' => $group->getName()]
                    )
                );
            }

            if ($this->groupUserPermissionRepository->checkExists($group, $user)) {
                throw new UserLevelException(
                    $this->translator->trans(
                        'registration_request.error.user_already_exists_in_group',
                        ['%group%' => $group->getName()]
                    )
                );
            }
        } else {
            if (null !== $organization) {
                if ($this->repository->checkExists($user, $organization, null)) {
                    throw new UserLevelException(
                        $this->translator->trans(
                            'registration_request.error.organization_request_already_exists',
                            ['%organization%' => $organization->getName()]
                        )
                    );
                }

                if ($this->organizationUserPermissionRepository->checkExists($organization, $user)) {
                    throw new UserLevelException(
                        $this->translator->trans(
                            'registration_request.error.user_already_exists_in_organization',
                            ['%organization%' => $organization->getName()]
                        )
                    );
                }

                if (!$organization->isAllowSingUp()) {
                    throw new UserLevelException(
                        $this->translator->trans(
                            'registration_request.error.no_registration_in_organization',
                            ['%organization%' => $organization->getName()]
                        )
                    );
                }
            }
        }
    }

    /**
     * @param Translator $translator
     *
     * @return RegistrationRequestManager
     */
    public function setTranslator($translator)
    {
        $this->translator = $translator;

        return $this;
    }

    /**
     * @param OrganizationUserPermissionRepository $organizationUserPermissionRepository
     *
     * @return RegistrationRequestManager
     */
    public function setOrganizationUserPermissionRepository($organizationUserPermissionRepository)
    {
        $this->organizationUserPermissionRepository = $organizationUserPermissionRepository;

        return $this;
    }

    /**
     * @param GroupUserPermissionRepository $groupUserPermissionRepository
     *
     * @return RegistrationRequestManager
     */
    public function setGroupUserPermissionRepository($groupUserPermissionRepository)
    {
        $this->groupUserPermissionRepository = $groupUserPermissionRepository;

        return $this;
    }
}
