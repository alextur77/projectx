<?php

namespace AppBundle\Manager;

use Aristek\Component\ORM\AbstractEntityManager;
use AppBundle\DataTransferObject\DTOTopic;
use AppBundle\Entity\Group;
use AppBundle\Entity\Organization;
use AppBundle\Entity\Topic;
use AppBundle\Entity\User;
use AppBundle\Repository\PostRepository;
use AppBundle\Repository\TopicRepository;
use AppBundle\Repository\UserRepository;
use Doctrine\DBAL\Exception\ForeignKeyConstraintViolationException;

/**
 * Class TopicManager
 * @package AppBundle\Manager
 *
 * @method Topic create()
 */
class TopicManager extends AbstractEntityManager
{
    /**
     * @var TopicRepository
     */
    protected $repository;

    /**
     * @var PostRepository
     */
    protected $postRepository;

    /**
     * @var TopicGroupPermissionManager
     */
    protected $topicGroupPermissionManager;

    /**
     * @var TopicModeratorManager
     */
    protected $topicModeratorManager;

    /**
     * @param Topic                                $topic
     * @param integer|integer[]|Group|Group[]|null $groups
     * @param integer|integer[]|User|User[]|null   $moderators
     *
     * @throws \Doctrine\DBAL\ConnectionException
     * @throws \Exception
     */
    public function add(Topic $topic, $groups = null, $moderators = null)
    {
        $connection = $this->em->getConnection();
        $connection->beginTransaction();
        try {
            $this->save($topic);
            if (null !== $groups) {
                if (!is_array($groups)) {
                    $groups = [$groups];
                }
                $this->topicGroupPermissionManager->add($topic, $groups);
            }
            if (null !== $moderators) {
                if (!is_array($moderators)) {
                    $moderators = [$moderators];
                }
                $this->topicModeratorManager->add($topic, $moderators);
            }
            $connection->commit();
        } catch (\Exception $e) {
            $connection->rollBack();
            throw $e;
        }
    }

    /**
     * @param Topic|integer $topicId
     *
     * @return bool - Returns true if the topic was really deleted but false otherwise.
     * @throws \Doctrine\DBAL\ConnectionException
     * @throws \Exception
     */
    public function delete($topicId)
    {
        $result = false;
        $topic = null;
        if ($topicId instanceof Topic) {
            $topic = $topicId;
            $topicId = $topicId->getId();
        }

        $connection = $this->em->getConnection();
        $connection->beginTransaction();
        try {
            $this->topicGroupPermissionManager->deleteByTopic($topicId);
            $this->topicModeratorManager->deleteByTopic($topicId);

            $qb = $this->repository->createDeleteQueryBuilder();
            $qb
                ->where(TopicRepository::ALIAS . '.id = :id')
                ->setParameter('id', $topicId);
            $qb->getQuery()->execute();

            $connection->commit();
            $result = true;
        } catch (ForeignKeyConstraintViolationException $e) {
            $connection->rollBack();
            $connection->beginTransaction();
            try {
                if (null === $topic) {
                    $topic = $this->repository->find($topicId);
                }
                $topic->setActive(false);
                $this->save($topic);

                $connection->commit();
            } catch (\Exception $e) {
                $connection->rollBack();
                throw $e;
            }
        } catch (\Exception $e) {
            $connection->rollBack();
            throw $e;
        }

        return $result;
    }

    /**
     * @param Organization                 $organization
     * @param DTOTopic[]|Topic[]|integer[] $topics
     *
     * @return array
     */
    public function getLastPostInfo(Organization $organization, $topics)
    {
        $results = [];
        if (!reset($topics) instanceof DTOTopic) {
            $qb = $this->em->createQueryBuilder();
            $qb
                ->select(TopicRepository::ALIAS . '.id')
                ->addSelect($qb->expr()->max(PostRepository::ALIAS . '.dateTime') . ' AS dateTime')
                ->from($this->repository->getClassName(), TopicRepository::ALIAS)
                ->leftJoin(TopicRepository::ALIAS . '.posts', PostRepository::ALIAS)
                ->where($qb->expr()->in(TopicRepository::ALIAS . '.id', ':topics'))
                ->andWhere(TopicRepository::ALIAS . '.organization = :organization')
                ->groupBy(TopicRepository::ALIAS . '.id')
                ->setParameter('topics', $topics)
                ->setParameter('organization', $organization);
            $results = $qb->getQuery()->getResult();
            $results = array_column($results, null, 'id');
        } else {
            /**
             * @var DTOTopic $topic
             */
            foreach ($topics as $index => $topic) {
                $id = $topic->getId();
                $results[$id] = ['id' => $id, 'dateTime' => $topic->getLastPostDatetime()];
            }
        }

        $qb = $this->postRepository->createQueryBuilder();
        $qb
            ->select('IDENTITY(' . PostRepository::ALIAS . '.topic) AS topicId')
            ->addSelect(UserRepository::ALIAS . '.username')
            ->innerJoin(PostRepository::ALIAS . '.user', UserRepository::ALIAS);
        $expr = $qb->expr();
        $i = 0;
        foreach ($results as $index => &$result) {
            if (null !== $result['dateTime']) {
                $i++;
                $topicIdParam = 'topic_id' . $i;
                $dateTimeParam = 'dateTime' . $i;
                $qb
                    ->orWhere(
                        $expr->andX(
                            PostRepository::ALIAS . '.topic = :' . $topicIdParam,
                            PostRepository::ALIAS . '.dateTime = :' . $dateTimeParam
                        )
                    )
                    ->setParameter($topicIdParam, $result['id'])
                    ->setParameter($dateTimeParam, $result['dateTime']);
            }
            $result['username'] = null;
            $result['lastUrl'] = '/user-topics/'.$result['id'].'/post/list';

        }
        $posts = $qb->getQuery()->getResult();

        foreach ($posts as $post) {
            $topicId = $post['topicId'];
            if (array_key_exists($topicId, $results)) {
                $results[$topicId]['username'] = $post['username'];
            }
        }

        return $results;
    }

    /**
     * @param TopicModeratorManager $topicModeratorManager
     *
     * @return TopicManager
     */
    public function setTopicModeratorManager($topicModeratorManager)
    {
        $this->topicModeratorManager = $topicModeratorManager;

        return $this;
    }

    /**
     * @param TopicGroupPermissionManager $topicGroupPermissionManager
     *
     * @return TopicManager
     */
    public function setTopicGroupPermissionManager($topicGroupPermissionManager)
    {
        $this->topicGroupPermissionManager = $topicGroupPermissionManager;

        return $this;
    }

    /**
     * @param PostRepository $postRepository
     *
     * @return TopicManager
     */
    public function setPostRepository($postRepository)
    {
        $this->postRepository = $postRepository;

        return $this;
    }
}
