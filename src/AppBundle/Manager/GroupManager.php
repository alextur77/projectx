<?php

namespace AppBundle\Manager;

use AppBundle\DataTransferObject\DTOGroup;
use AppBundle\Entity\Organization;
use Aristek\Component\ORM\AbstractEntityManager;
use AppBundle\Repository\GroupRepository;
use AppBundle\Repository\GroupClosureRepository;
use AppBundle\Entity\User;
use AppBundle\Entity\Group;
use AppBundle\Entity\Role;
use AppBundle\Entity\GroupClosure;
use Doctrine\ORM\Query\Expr;
use Doctrine\ORM\Query\Expr\Join;
use Doctrine\DBAL\Exception\ForeignKeyConstraintViolationException;

/**
 * Class GroupManager
 * @package AppBundle\Manager
 *
 * @method Group create()
 */
class GroupManager extends AbstractEntityManager
{
    const ENTITY_NAME = 'AppBundle\Entity\Group';
    const PERMISSION_NAME = 'AppBundle\Entity\GroupUserPermission';
    const CLOSURE_NAME = 'AppBundle\Entity\GroupClosure';

    /**
     * @var GroupRepository
     */
    protected $repository;

    /**
     * @var GroupClosureRepository
     */
    protected $groupClosureRepository;

    /**
     * @param Group[]|DTOGroup[] $groups
     *
     * @return Group[]|DTOGroup[]
     */
    public function eliminateAncestorDescendantRelations(array $groups)
    {
        if (count($groups) > 1) {
            // Delete all ancestor<---->descendant links:
            foreach ($groups as $outIndex => $outGroup) {
                $descendantIds = $this->groupClosureRepository->findDescendantIds($outGroup);
                if (!empty($descendantIds)) {
                    foreach ($groups as $inIndex => $inGroup) {
                        if ($outIndex != $inIndex) {
                            if (in_array($inGroup->getId(), $descendantIds)) {
                                unset($groups[$inIndex]);
                            }
                        }
                    }
                }
            }
        }

        return $groups;
    }

    /**
     * @param integer $groupId
     * @param string  $fieldName
     * @param boolean $boolValue
     * @param boolean $withDescendants
     *
     * @return array - The group ids witch boolean status was changed
     */
    private function changeBoolean($groupId, $fieldName, $boolValue, $withDescendants)
    {
        $descendantIds = [];
        if ($withDescendants) {
            $descendantIds = $this->groupClosureRepository->findDescendantIds($groupId);
        }
        $descendantIds[] = $groupId;

        $qb = $this->em->createQueryBuilder();
        $qb
            ->update($this::ENTITY_NAME, 'g')
            ->set('g.' . $fieldName, ':' . $fieldName)
            ->andWhere($qb->expr()->in('g.id', ':ids'))
            ->setParameter('ids', $descendantIds)
            ->setParameter($fieldName, $boolValue);
        $qb->getQuery()->execute();

        return $descendantIds;
    }

    /**
     * @param integer $groupId
     * @param boolean $active
     * @param boolean $withDescendants
     *
     * @return array - The group ids witch active status was changed
     */
    public function changeActive($groupId, $active, $withDescendants)
    {
        return $this->changeBoolean($groupId, 'active', $active, $withDescendants);
    }

    /**
     * @param integer $groupId
     * @param boolean $public
     * @param boolean $withDescendants
     *
     * @return array - The group ids witch active status was changed
     */
    public function changePublic($groupId, $public, $withDescendants)
    {
        return $this->changeBoolean($groupId, 'public', $public, $withDescendants);
    }

    /**
     * @param Group         $group
     * @param integer|Group $parentGroupId
     *
     * @return Group
     * @throws \Doctrine\DBAL\ConnectionException
     * @throws \Exception
     */
    public function addNew(Group $group, $parentGroupId)
    {
        // DQL implementation of the following SQL query:
        //
        //    INSERT INTO group_closure (ancestor_id, descendant_id, depth)
        //        SELECT ancestor_id, :new_id, depth + 1
        //        FROM group_closure
        //        WHERE descendant_id = :parentGroupId
        //        UNION ALL SELECT :new_id, :new_id, 0;
        //
        if ($parentGroupId instanceof Group) {
            $parentGroupId = $parentGroupId->getId();
        }

        $connection = $this->em->getConnection();
        $connection->beginTransaction();
        try {
            $this->em->persist($group);
            $this->em->flush($group);

            if (null !== $parentGroupId) {
                $ancestors = $this->groupClosureRepository->findAncestors($parentGroupId, true);
                foreach ($ancestors as $ancestor) {
                    $closure = new GroupClosure();
                    $closure->setAncestorId($ancestor->getAncestorId());
                    $closure->setDescendantId($group->getId());
                    $closure->setDepth($ancestor->getDepth() + 1);
                    $this->em->persist($closure);
                    $this->em->flush($closure);
                }
            }

            $closure = new GroupClosure();
            $closure->setAncestorId($group->getId());
            $closure->setDescendantId($group->getId());
            $closure->setDepth(0);
            $this->em->persist($closure);
            $this->em->flush($closure);

            $connection->commit();
        } catch (\Exception $e) {
            $connection->rollBack();
            throw $e;
        }

        return $group;
    }

    /**
     *
     *
     * @param string       $name
     * @param Organization $orgId
     * @param integer      $active
     * @param integer      $public
     *
     * @return void
     */
    public function add($name, $orgId, $active, $public)
    {
        $group = $this->create();
        $group->setName($name);
        $group->setOrganization($orgId);
        $group->setActive($active);
        $group->setPublic($public);
        $this->save($group);
    }

    /**
     * @param Group         $group
     * @param integer|Group $parentGroupId
     *
     * @return Group
     * @throws \Doctrine\DBAL\ConnectionException
     * @throws \Exception
     */
    public function update(Group $group, $parentGroupId)
    {
        if ($parentGroupId instanceof Group) {
            $parentGroupId = $parentGroupId->getId();
        }

        $connection = $this->em->getConnection();
        $connection->beginTransaction();
        try {
            // DQL implementation of the following SQL query:
            //
            //    DELETE subTree
            //    FROM group_closure AS subTree
            //        JOIN group_closure AS superTree
            //            ON subTree.descendant_id = superTree.descendant_id
            //        LEFT JOIN group_closure AS links
            //            ON links.ancestor_id = superTree.ancestor_id AND links.descendant_id = subTree.ancestor_id
            //    WHERE superTree.ancestor_id = $group->getId() AND links.ancestor_id IS NULL;
            //
            $aliasSubTree = 'subTree';
            $aliasSuperTree = 'superTree';
            $aliasLinks = 'links';
            $qb = $this->em->createQueryBuilder();
            $qb
                ->select($aliasSubTree)
                ->from($this::CLOSURE_NAME, $aliasSubTree)
                ->innerJoin(
                    $this::CLOSURE_NAME,
                    $aliasSuperTree,
                    Join::WITH,
                    $aliasSubTree . '.descendantId = ' . $aliasSuperTree . '.descendantId'
                )
                ->leftJoin(
                    $this::CLOSURE_NAME,
                    $aliasLinks,
                    Join::WITH,
                    $qb->expr()->andX(
                        $aliasLinks . '.ancestorId = ' . $aliasSuperTree . '.ancestorId',
                        $aliasLinks . '.descendantId = ' . $aliasSubTree . '.ancestorId'
                    )
                )
                ->andWhere($aliasSuperTree . '.ancestorId = :subTreeRoot')
                ->andWhere($qb->expr()->isNull($aliasLinks . '.ancestorId'))
                ->setParameter('subTreeRoot', $group->getId());

            $closures = $qb->getQuery()->getResult();
            /** @var GroupClosure $closure */
            foreach ($closures as $closure) {
                $this->em->remove($closure);
                $this->em->flush($closure);
            }

            // DQL implementation of the following SQL query:
            //
            //    INSERT INTO group_closure (ancestor_id, descendant_id, depth)
            //        SELECT superTree.ancestor_id, subTree.descendant_id, superTree.depth + subTree.depth + 1
            //        FROM group_closure AS superTree
            //            JOIN group_closure AS subTree
            //        WHERE subTree.ancestor_id = $group->getId() AND superTree.descendant_id = $parentGroupId;
            //
            $qb->resetDQLParts();
            $qb
                ->select($aliasSuperTree . '.ancestorId')
                ->addSelect($aliasSubTree . '.descendantId')
                ->addSelect($aliasSuperTree . '.depth + ' . $aliasSubTree . '.depth + 1 AS depth')
                ->from($this::CLOSURE_NAME, $aliasSuperTree)
                ->innerJoin($this::CLOSURE_NAME, $aliasSubTree)
                ->andWhere($aliasSubTree . '.ancestorId = :subTreeRoot')
                ->andWhere($aliasSuperTree . '.descendantId = :destination')
                ->setParameter('subTreeRoot', $group->getId())
                ->setParameter('destination', $parentGroupId);

            $closures = $qb->getQuery()->getResult();

            foreach ($closures as $closureData) {
                $closure = new GroupClosure();
                $closure->setAncestorId($closureData['ancestorId']);
                $closure->setDescendantId($closureData['descendantId']);
                $closure->setDepth($closureData['depth']);
                $this->em->persist($closure);
                $this->em->flush($closure);
            }

            $this->em->persist($group);
            $this->em->flush($group);

            $connection->commit();
        } catch (\Exception $e) {
            $connection->rollBack();
            throw $e;
        }

        return $group;
    }

    /**
     * @param Group|DTOGroup|integer $groupId
     *
     * @throws \Doctrine\DBAL\ConnectionException
     * @throws \Exception
     */
    public function delete($groupId)
    {
        $group = null;
        if ($groupId instanceof Group) {
            $group = $groupId;
        }
        if (($groupId instanceof Group) or ($groupId instanceof DTOGroup)) {
            $groupId = $groupId->getId();
        }

        $connection = $this->em->getConnection();
        $connection->beginTransaction();
        try {
            // DQL implementation of the following SQL query:
            //
            //    UPDATE group_closure gc1
            //        INNER JOIN group_closure gc2
            //            ON gc1.descendant_id = gc2.descendant_id
            //    SET gc1.depth = gc1.depth - 1
            //    WHERE gc2.ancestor_id = $groupId AND
            //          gc1.descendant_id <> $groupId AND gc1.depth > gc2.depth;
            //
            $qb = $this->em->createQueryBuilder();
            $qb
                ->select('gc1')
                ->from($this::CLOSURE_NAME, 'gc1')
                ->innerJoin($this::CLOSURE_NAME, 'gc2', Join::WITH, 'gc1.descendantId = gc2.descendantId')
                ->andWhere('gc2.ancestorId = :id')
                ->andWhere('gc2.descendantId <> :id')
                ->andWhere('gc1.depth > gc2.depth')
                ->setParameter('id', $groupId);
            $closures = $qb->getQuery()->getResult();

            /** @var GroupClosure $closure */
            foreach ($closures as $closure) {
                $closure->setDepth($closure->getDepth() - 1);
                $this->em->persist($closure);
                $this->em->flush($closure);
            }

            // DQL implementation of the following SQL query:
            //
            //    DELETE FROM group_closure
            //    WHERE ancestor_id = $groupId OR descendant_id = $groupId;
            //
            $qb->resetDQLParts();
            $qb
                ->delete($this::CLOSURE_NAME, 'gc')
                ->andWhere($qb->expr()->orX('gc.ancestorId = :id', 'gc.descendantId = :id'))
                ->setParameter('id', $groupId);
            $qb->getQuery()->execute();

            $qb->resetDQLParts();
            $qb
                ->delete($this::ENTITY_NAME, 'g')
                ->andWhere('g.id = :id')
                ->setParameter('id', $groupId);
            $qb->getQuery()->execute();

            $connection->commit();
        } catch (ForeignKeyConstraintViolationException $e) {
            $connection->rollBack();
            $connection->beginTransaction();
            try {
                if (null === $group) {
                    $group = $this->repository->find($groupId);
                }
                $group->setActive(false);
                $this->em->persist($group);
                $this->em->flush($group);

                $connection->commit();
            } catch (\Exception $e) {
                $connection->rollBack();
                throw $e;
            }
        } catch (\Exception $e) {
            $connection->rollBack();
            throw $e;
        }
    }

    /**
     * @param Group|DTOGroup|integer $groupId
     *
     * @throws \Doctrine\DBAL\ConnectionException
     * @throws \Exception
     */
    public function deleteSubtree($groupId)
    {
        if (($groupId instanceof Group) or ($groupId instanceof DTOGroup)) {
            $groupId = $groupId->getId();
        }

        $descendantIds = $this->groupClosureRepository->findDescendantIds($groupId);
        $descendantIds[] = $groupId;
        $qb = $this->em->createQueryBuilder();
        $expr = $qb->expr();

        $connection = $this->em->getConnection();
        $connection->beginTransaction();
        try {
            $qb
                ->delete($this::CLOSURE_NAME, 'gc')
                ->andWhere(
                    $expr->orX
                    (
                        $expr->in('gc.ancestorId', ':ids'),
                        $expr->in('gc.descendantId', ':ids')
                    )
                )
                ->setParameter('ids', $descendantIds);
            $qb->getQuery()->execute();

            $qb->resetDQLParts();
            $qb
                ->delete($this::ENTITY_NAME, 'g')
                ->andWhere($expr->in('g.id', ':ids'))
                ->setParameter('ids', $descendantIds);
            $qb->getQuery()->execute();

            $connection->commit();
        } catch (ForeignKeyConstraintViolationException $e) {
            $connection->rollBack();
            $connection->beginTransaction();
            try {
                $qb->resetDQLParts();
                $qb
                    ->update($this::ENTITY_NAME, 'g')
                    ->set('g.active', ':active')
                    ->andWhere($expr->in('g.id', ':ids'))
                    ->setParameter('ids', $descendantIds)
                    ->setParameter('active', 0);
                $qb->getQuery()->execute();

                $connection->commit();
            } catch (\Exception $e) {
                $connection->rollBack();
                throw $e;
            }
        } catch (\Exception $e) {
            $connection->rollBack();
            throw $e;
        }
    }

    /**
     * @param User      $user
     * @param integer[] $groups
     */
    public function getUserGroupAdditionalData(User $user, &$groups)
    {
        $qb = $this->em->createQueryBuilder();
        $alias = 'Groups';
        $aliasGUP = 'GroupUserPermissions';
        $aliasR = 'Roles';
        $aliasGC = 'Closures';
        $aliasGUPGC = 'GroupUserPermissionsGC';
        $aliasRGC = 'RolesGC';
        $qb
            ->select($alias . '.id')
            ->addSelect($aliasR . '.name')
            ->addSelect($aliasR . '.code')
            ->addSelect(
                'SUM(CASE WHEN (' . $aliasRGC . '.code = \'' .
                Role::ROLE_GROUP_ADMIN . '\') THEN 1 ELSE 0 END) AS adminQuantity'
            )
            ->from($this::ENTITY_NAME, $alias)
            ->innerJoin($alias . '.groupUserPermissions', $aliasGUP)
            ->innerJoin($aliasGUP . '.role', $aliasR)
            ->innerJoin(
                $this::CLOSURE_NAME,
                $aliasGC,
                Join::WITH,
                $aliasGC . '.descendantId = ' . $alias . '.id'
            )
            ->innerJoin(
                $this::PERMISSION_NAME,
                $aliasGUPGC,
                Join::WITH,
                $aliasGC . '.ancestorId = ' . $aliasGUPGC . '.group'
            )
            ->innerJoin($aliasGUPGC . '.role', $aliasRGC)
            ->andWhere($qb->expr()->in($alias . '.id', ':groups'))
            ->andWhere($aliasGUP . '.user = :user')
            ->groupBy($alias . '.id')
            ->setParameter('user', $user)
            ->setParameter('groups', $groups);
        $results = $qb->getQuery()->getArrayResult();

        $groups = array_flip($groups);
        foreach ($results as $result) {
            $groups[$result['id']] = [
                'name' => $result['name'],
                'code' => $result['code'],
                'adminQuantity' => $result['adminQuantity']
            ];
        }
    }

    /**
     * @param GroupClosureRepository $groupClosureRepository
     *
     * @return GroupManager
     */
    public function setGroupClosureRepository($groupClosureRepository)
    {
        $this->groupClosureRepository = $groupClosureRepository;

        return $this;
    }
}
