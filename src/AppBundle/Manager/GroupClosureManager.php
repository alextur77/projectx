<?php
namespace AppBundle\Manager;

use AppBundle\Entity\GroupClosure;
use Aristek\Component\ORM\AbstractEntityManager;

/**
 * Class GroupClosureManager
 *
 * @method GroupClosureManager create()
 */
class GroupClosureManager extends AbstractEntityManager
{
    /**
     * @param int $id
     */
    public function add($id)
    {
        /** @var GroupClosure $groupClosure */
        $groupClosure = $this->create();
        $groupClosure->setAncestorId($id);
        $groupClosure->setDescendantId($id);
        $groupClosure->setDepth(0);
        $this->save($groupClosure);
    }
}
