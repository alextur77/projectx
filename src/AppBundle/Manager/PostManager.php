<?php

namespace AppBundle\Manager;

use Aristek\Component\ORM\AbstractEntityManager;
use AppBundle\Entity\Post;
use AppBundle\Repository\PostRepository;
use Doctrine\ORM\Query\Expr;

/**
 * Class PostManager
 * @package AppBundle\Manager
 *
 * @method Post create()
 */
class PostManager extends AbstractEntityManager
{
    /**
     * @var PostRepository
     */
    protected $repository;

    /**
     * @param Post|integer $post
     * @param boolean      $active
     * @param boolean      $withChildren
     */
    public function changeActive($post, $active, $withChildren)
    {
        if ($post instanceof Post) {
            $id = $post->getId();
        } else {
            $id = $post;
        }
        $alias = PostRepository::ALIAS;
        $qb = $this->em->createQueryBuilder();
        $orX = $qb->expr()->orX($alias . '.id = :id');
        if ($withChildren) {
            $orX->add($alias . '.parentPost = :id');
        }
        $qb
            ->update($this->repository->getClassName(), $alias)
            ->set($alias . '.active', $active ? 1 : 0)
            ->where($orX)
            ->setParameter('id', $id);
        $qb->getQuery()->execute();
    }
}
