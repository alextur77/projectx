<?php

namespace AppBundle\Manager;

use Aristek\Component\ORM\AbstractEntityManager;
use AppBundle\Entity\Organization;

/**
 * Class OrganizationManager
 * @package AppBundle\Manager
 */
class OrganizationManager extends AbstractEntityManager
{
    /**
     * @param Organization $name
     * @param Organization $address
     * @param Organization $city
     * @param Organization $state
     * @param Organization $zip
     * @param Organization $active
     * @param Organization $public
     * @param Organization $allowsignup
     * @param Organization $email
     * @param Organization $phone
     * @param Organization $website
     */
    public function add($name, $address, $city, $state, $zip, $active, $public, $allowsignup, $email, $phone, $website)
    {
        $eventRelation = $this->create();
        $eventRelation->setName($name);
        $eventRelation->setAddress($address);
        $eventRelation->setCity($city);
        $eventRelation->setState($state);
        $eventRelation->setZip($zip);
        $eventRelation->setActive($active);
        $eventRelation->setPublic($public);
        $eventRelation->setAllowSingUp($allowsignup);
        $eventRelation->setEmail($email);
        $eventRelation->setPhoneNumber($phone);
        $eventRelation->setWebsiteURL($website);
        $this->save($eventRelation);
    }
}
