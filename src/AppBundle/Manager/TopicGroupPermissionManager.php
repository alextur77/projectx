<?php

namespace AppBundle\Manager;

use Aristek\Component\ORM\AbstractEntityManager;
use AppBundle\Repository\GroupRepository;
use AppBundle\Entity\Group;
use AppBundle\Entity\Role;
use AppBundle\Entity\Topic;
use AppBundle\Entity\User;
use AppBundle\Entity\TopicGroupPermission;
use AppBundle\Repository\TopicGroupPermissionRepository;
use Doctrine\ORM\Query\Expr\Join;

/**
 * Class TopicGroupPermissionManager
 * @package AppBundle\Manager
 *
 * @method TopicGroupPermission create()
 */
class TopicGroupPermissionManager extends AbstractEntityManager
{
    /**
     * @var TopicGroupPermissionRepository
     */
    protected $repository;

    /**
     * @var GroupRepository
     */
    protected $groupRepository;

    /**
     * @param TopicGroupPermission|TopicGroupPermission[]|integer|integer[] $topicGroupPermissions
     */
    public function delete($topicGroupPermissions)
    {
        if (!is_array($topicGroupPermissions)) {
            $topicGroupPermissions = [$topicGroupPermissions];
        }
        $qb = $this->repository->createDeleteQueryBuilder();
        $qb
            ->where($qb->expr()->in(TopicGroupPermissionRepository::ALIAS . '.id', ':topicGroupPermissions'))
            ->setParameter('topicGroupPermissions', $topicGroupPermissions);
        $qb->getQuery()->execute();
    }

    /**
     * @param Topic|Topic[]|integer|integer[] $topics - Topic objects or the topic ids
     */
    public function deleteByTopic($topics)
    {
        if (!is_array($topics)) {
            $topics = [$topics];
        }
        $qb = $this->repository->createDeleteQueryBuilder();
        $qb
            ->where($qb->expr()->in(TopicGroupPermissionRepository::ALIAS . '.topic', ':topics'))
            ->setParameter('topics', $topics);
        $qb->getQuery()->execute();
    }

    /**
     * @param Topic                           $topic
     * @param Group|Group[]|integer|integer[] $groups
     */
    public function add(Topic $topic, $groups)
    {
        if (!is_array($groups)) {
            $groups = [$groups];
        }
        if (!reset($groups) instanceof Group) {
            $groups = $this->groupRepository->findBy(['id' => $groups]);
        }
        foreach ($groups as $group) {
            $topicGroupPermission = $this->repository->findOneBy(['topic' => $topic, 'group' => $group]);
            if (null === $topicGroupPermission) {
                $topicGroupPermission = $this->create();
                $topicGroupPermission->setTopic($topic);
                $topicGroupPermission->setGroup($group);
                $this->save($topicGroupPermission);
            }
        }
    }

    /**
     * @param Topic|integer|null                   $topic
     * @param Group|Group[]|integer|integer[]|null $groups
     *
     * @return integer
     */
    public function getGroupQuantity($topic = null, $groups = null)
    {
        $qb = $this->repository->em()->createQueryBuilder();
        $qb
            ->select($qb->expr()->count(TopicGroupPermissionRepository::ALIAS . '.id'))
            ->from($this->repository->getClassName(), TopicGroupPermissionRepository::ALIAS);
        if (null !== $topic) {
            $qb
                ->where(TopicGroupPermissionRepository::ALIAS . '.topic = :topic')
                ->setParameter('topic', $topic);
        }
        if (null !== $groups) {
            $qb
                ->andWhere($qb->expr()->in(TopicGroupPermissionRepository::ALIAS . '.group', ':groups'))
                ->setParameter('groups', $groups);
        }

        return $qb->getQuery()->getSingleScalarResult();
    }

    /**
     * @param Group|Group[]|integer|integer[]|null $groups
     *
     * @return integer[] - data format: [group_id => topic_quantity]
     */
    public function getAllTopicQuantity($groups)
    {
        $qb = $this->repository->createQueryBuilder();
        $qb
            ->select('IDENTITY(' . TopicGroupPermissionRepository::ALIAS . '.group) AS group_id')
            ->addSelect($qb->expr()->count(TopicGroupPermissionRepository::ALIAS . '.id') . 'AS topic_quantity')
            ->where($qb->expr()->in(TopicGroupPermissionRepository::ALIAS . '.group', ':groups'))
            ->groupBy(TopicGroupPermissionRepository::ALIAS . '.group')
            ->setParameter('groups', $groups);
        $result = $qb->getQuery()->getResult();
        $result = array_column($result, 'topic_quantity', 'group_id');

        return $result;
    }

    /**
     * @param Group|Group[]|integer|integer[]|null $groups
     * @param User                                 $user
     *
     * @return integer[] - data format: [group_id => topic_quantity]
     */
    public function getTopicQuantityForUser($groups, User $user)
    {
        // DQL implementation of the following SQL query:
        //
        //    SELECT
        //        tgp.group_id,
        //        COUNT(DISTINCT tgp.id)
        //    FROM
        //        topic_group_permissions tgp
        //        INNER JOIN topics t ON t.id = tgp.topic_id
        //        INNER JOIN group_closure gc ON gc.descendant_id = tgp.group_id
        //        INNER JOIN group_user_permissions gup ON gup.group_id = gc.ancestor_id
        //        INNER JOIN roles r ON r.id = gup.role_id
        //    WHERE
        //        tgp.group_id IN (:groupIds) AND
        //        gup.user_id = :user AND
        //        ((t.active = 1 AND gc.ancestor_id = gc.descendant_id) OR
        //         (r.code = 'ROLE_GROUP_ADMIN'))
        //    GROUP BY
        //       tgp.group_id
        //
        $aliasT = 'topics';
        $aliasR = 'roles';
        $qb = $this->repository->createQueryBuilder();
        $qb
            ->select('IDENTITY(' . TopicGroupPermissionRepository::ALIAS . '.group) AS group_id')
            ->addSelect($qb->expr()->countDistinct(TopicGroupPermissionRepository::ALIAS . '.id') . 'AS topic_quantity')
            ->innerJoin(TopicGroupPermissionRepository::ALIAS . '.topic', $aliasT)
            ->innerJoin(
                TopicGroupPermissionRepository::ENTITY_GROUP_CLOSURE,
                TopicGroupPermissionRepository::ALIAS_GROUP_CLOSURE,
                Join::WITH,
                TopicGroupPermissionRepository::ALIAS_GROUP_CLOSURE . '.descendantId = ' .
                TopicGroupPermissionRepository::ALIAS . '.group'
            )
            ->innerJoin(
                TopicGroupPermissionRepository::ENTITY_GROUP_PERMISSION,
                TopicGroupPermissionRepository::ALIAS_GROUP_PERMISSION,
                Join::WITH,
                TopicGroupPermissionRepository::ALIAS_GROUP_PERMISSION . '.group = ' .
                TopicGroupPermissionRepository::ALIAS_GROUP_CLOSURE . '.ancestorId'
            )
            ->innerJoin(TopicGroupPermissionRepository::ALIAS_GROUP_PERMISSION . '.role', $aliasR)
            ->where($qb->expr()->in(TopicGroupPermissionRepository::ALIAS . '.group', ':groups'))
            ->andWhere(TopicGroupPermissionRepository::ALIAS_GROUP_PERMISSION . '.user = :user')
            ->andWhere(
                $qb->expr()->orX(
                    $qb->expr()->andX(
                        $aliasT . '.active = :active',
                        TopicGroupPermissionRepository::ALIAS_GROUP_CLOSURE . '.descendantId = ' .
                        TopicGroupPermissionRepository::ALIAS_GROUP_CLOSURE . '.ancestorId'
                    ),
                    $aliasR . '.code = :code'
                )
            )
            ->groupBy(TopicGroupPermissionRepository::ALIAS . '.group')
            ->setParameter('groups', $groups)
            ->setParameter('user', $user)
            ->setParameter('code', Role::ROLE_GROUP_ADMIN)
            ->setParameter('active', 1);

        $result = $qb->getQuery()->getResult();
        $result = array_column($result, 'topic_quantity', 'group_id');

        return $result;
    }

    /**
     * @param GroupRepository $groupRepository
     *
     * @return TopicGroupPermissionManager
     */
    public function setGroupRepository($groupRepository)
    {
        $this->groupRepository = $groupRepository;

        return $this;
    }
}
