<?php

namespace AppBundle\Manager;

use Aristek\Component\ORM\AbstractEntityManager;
use AppBundle\DataTransferObject\DTOGroup;
use AppBundle\Entity\Topic;
use AppBundle\Entity\Role;
use AppBundle\Repository\TopicRepository;
use AppBundle\Repository\TopicGroupPermissionRepository;
use AppBundle\Repository\TopicModeratorRepository;
use AppBundle\Repository\GroupUserPermissionRepository;
use AppBundle\Repository\GroupRepository;
use AppBundle\Service\User\CurrentUserService;
use AppBundle\Repository\TopicPermissionMutexRepository;

/**
 * Class TopicPermissionMutexManager
 * @package AppBundle\Manager
 */
class TopicPermissionMutexManager extends AbstractEntityManager
{
    /**
     * @var TopicPermissionMutexRepository
     */
    protected $repository;

    /**
     * @var CurrentUserService
     */
    protected $currentUserService;

    /**
     * @var GroupRepository
     */
    protected $groupRepository;

    /**
     * @var GroupUserPermissionRepository
     */
    protected $groupUserPermissionRepository;

    /**
     * @var TopicGroupPermissionRepository
     */
    protected $topicGroupPermissionRepository;

    /**
     * @var TopicModeratorRepository
     */
    protected $topicModeratorRepository;

    /**
     * @var TopicRepository
     */
    protected $topicRepository;

    /**
     * @param Topic|integer|null $topic                          - Topic or the topic id
     * @param integer|integer[]  $selectedGroupIds
     * @param array|null         &$allPossibleModeratorChoices   - When a new topic is being created it is necessary
     *                                                           to assign all possible user ids for the moderator's
     *                                                           choice control in order to pass the sonata's built-in
     *                                                           choice validator when submitting the creation form.
     *                                                           $allPossibleModeratorChoices will return
     *                                                           an array of format [$userId  => $userFullName]
     *
     * @return array - array($groupChoices, $moderatorChoices)
     *     $groupChoices     is an array of format [$groupId => $groupName]
     *     $moderatorChoices is an array of format [$userId  => $userFullName]
     */
    public function getParticipantChoices($topic = null, $selectedGroupIds = [], &$allPossibleModeratorChoices = null)
    {
        $selectedGroupIds = (array) $selectedGroupIds;
        $currentUser = $this->currentUserService->getCurrentUser();
        $organization = $this->currentUserService->getCurrentOrganization();
        $role = $this->currentUserService->getCurrentUserRoleInOrganization();

        // Finding the groups which the current user has GROUP_ADMIN access to.
        if ($role === Role::ROLE_ORGANIZATION_ADMIN) {
            $ownGroupDTOs = $this->groupRepository->findActiveByOrganization($organization);
        } elseif (($role === Role::ROLE_ORGANIZATION_USER) or (null === $role)) {
            // $user has Role::ROLE_ORGANIZATION_USER role in $organization or have not any role at all:
            $ownGroupDTOs = $this->groupRepository->findByGroupAdmin($currentUser, $organization);
        } else {
            throw new \LogicException(__METHOD__ . ': Unknown organization user role: "' . $role . '""!');
        }
        $groupChoices = [];
        $ownGroupIds = [];
        /** @var DTOGroup $group */
        foreach ($ownGroupDTOs as $group) {
            $groupId = $group->id;
            $groupChoices[$groupId] = $group->getShift() . $group->name;
            $ownGroupIds[] = $groupId;
        }

        if (null == $allPossibleModeratorChoices) {
            $allPossibleModeratorChoices = $this->groupUserPermissionRepository->getUserFullNameByGroups($ownGroupIds);
            $allPossibleModeratorChoices = array_column($allPossibleModeratorChoices, 'text', 'id');
        }

        $topicAssignedGroupIds = [];
        if (null !== $topic) {
            // Excluding from the $groupChoices array the groups that already have permissions to this topic:
            $topicGroupPermissions = $this->topicGroupPermissionRepository->findBy(['topic' => $topic]);
            foreach ($topicGroupPermissions as $permission) {
                $groupId = $permission->getGroup()->getId();
                if (array_key_exists($groupId, $groupChoices)) {
                    unset($groupChoices[$groupId]);
                    $topicAssignedGroupIds[] = $groupId;
                }
            }
        }
        $topicAssignedGroupIds = array_merge($topicAssignedGroupIds, $selectedGroupIds);

        $moderatorChoices = [];
        if (!empty($topicAssignedGroupIds)) {
            $moderatorChoices = $this->groupUserPermissionRepository->getUserFullNameByGroups($topicAssignedGroupIds);
            $moderatorChoices = array_column($moderatorChoices, 'text', 'id');
            if (null !== $topic) {
                // Excluding from the $moderatorChoices array the users that already have permissions to this topic:
                $topicModerators = $this->topicModeratorRepository->findBy(['topic' => $topic]);
                foreach ($topicModerators as $moderator) {
                    $userId = $moderator->getUser()->getId();
                    if (array_key_exists($userId, $moderatorChoices)) {
                        unset($moderatorChoices[$userId]);
                    }
                }
            }
        }

        return array($groupChoices, $moderatorChoices);
    }

    /**
     * @param Topic|Topic[]|integer|integer[] $topics
     *
     * @return array
     */
    public function getParticipantsQuantity($topics)
    {
        // DQL implementation of the following SQL query:
        /*
            SELECT
                COUNT(DISTINCT tgp.group_id),
                COUNT(DISTINCT tm.user_id)
            FROM topics t
                LEFT JOIN topic_group_permissions tgp ON (t.id = tgp.topic_id)
                LEFT JOIN topic_moderators tm ON (t.id = tm.topic_id)
            WHERE t.id IN (:topics)
            GROUP BY t.id
         */
        if (!is_array($topics)) {
            $topics = [$topics];
        }
        $qb = $this->topicRepository->createQueryBuilder();
        $alias = $qb->getRootAliases()[0];
        $aliasTGP = 'topicGroupPermission';
        $aliasTM = 'topicModerator';
        $qb
            // If remove id from this select then function $qb->indexBy() does not work properly.
            ->select($alias . '.id')
            ->addSelect($qb->expr()->countDistinct($aliasTGP . '.group') . ' AS groupQuantity')
            ->addSelect($qb->expr()->countDistinct($aliasTM . '.user') . ' AS moderatorQuantity')
            ->leftJoin($alias . '.topicGroupPermissions', $aliasTGP)
            ->leftJoin($alias . '.topicModerators', $aliasTM)
            ->indexBy($alias, $alias . '.id')
            ->where($qb->expr()->in($alias . '.id', ':topics'))
            ->setParameter('topics', $topics)
            ->groupBy($alias . '.id');

        return $qb->getQuery()->getResult();
    }

    /**
     * @param GroupRepository $groupRepository
     *
     * @return TopicPermissionMutexManager
     */
    public function setGroupRepository($groupRepository)
    {
        $this->groupRepository = $groupRepository;

        return $this;
    }

    /**
     * @param CurrentUserService $currentUserService
     *
     * @return TopicPermissionMutexManager
     */
    public function setCurrentUserService($currentUserService)
    {
        $this->currentUserService = $currentUserService;

        return $this;
    }

    /**
     * @param GroupUserPermissionRepository $groupUserPermissionRepository
     *
     * @return TopicPermissionMutexManager
     */
    public function setGroupUserPermissionRepository($groupUserPermissionRepository)
    {
        $this->groupUserPermissionRepository = $groupUserPermissionRepository;

        return $this;
    }

    /**
     * @param TopicModeratorRepository $topicModeratorRepository
     *
     * @return TopicPermissionMutexManager
     */
    public function setTopicModeratorRepository($topicModeratorRepository)
    {
        $this->topicModeratorRepository = $topicModeratorRepository;

        return $this;
    }

    /**
     * @param TopicGroupPermissionRepository $topicGroupPermissionRepository
     *
     * @return TopicPermissionMutexManager
     */
    public function setTopicGroupPermissionRepository($topicGroupPermissionRepository)
    {
        $this->topicGroupPermissionRepository = $topicGroupPermissionRepository;

        return $this;
    }

    /**
     * @param TopicRepository $topicRepository
     */
    public function setTopicRepository($topicRepository)
    {
        $this->topicRepository = $topicRepository;
    }
}
