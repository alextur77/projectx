<?php

namespace AppBundle\Manager;

use AppBundle\Entity\User;
use Aristek\Component\ORM\AbstractEntityManager;
use AppBundle\Repository\UserRepository;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\Validator\Constraints\Collection;

/**
 * Class UserManager
 *
 * @method User update()
 */
class UserManager extends AbstractEntityManager
{
    /**
     * @var UserRepository
     */
    protected $repository;

    /**
     * @param integer $userID
     * @param string  $organization
     *
     * @return void
     */
    public function updateUserOrganization($userID, $organization)
    {
        $user = $this->repository->find($userID);
        $user->setUserCurrentOrganization($organization);
        $this->save($user);
    }

    /**
     * @param string $userName
     * @param string $email
     * @param string $password
     * @param string $firstName
     * @param string $lastName
     * @param string $phone
     * @param string $sendEmail
     * @param string $sendSms
     * @param string $patronymic
     * @param string $token
     * @param File   $file
     * @param array  $role
     *
     * @return void
     */
    public function add($userName, $email, $password, $firstName, $lastName, $phone, $sendEmail, $sendSms, $patronymic, $token, $file, $role = [])
    {
        $user = $this->create();
        $user->setUsername($userName);
        $user->setUsernameCanonical(strtolower($userName));
        $user->setEmail($email);
        $user->setEmailCanonical(strtolower($email));
        $user->setEnabled(1);
        $user->getSalt();
        $user->setPassword($password);
        $user->setLocked(false);
        $user->setExpired(false);
        $user->setRoles($role);
        $user->setFirstName($firstName);
        $user->setLastName($lastName);
        $user->setPhone($phone);
        $user->setSendEmail($sendEmail);
        $user->setSendSms($sendSms);
        $user->setPatronymic($patronymic);
        $user->setConfirmationToken($token);
        $user->setProfilePictureFile($file);

        $this->save($user);
    }

    /**
     * @param string $userID
     * @param string $password
     *
     * @return void
     */
    public function updateUserPassword($userID, $password)
    {
        $user = $this->repository->find($userID);
        $user->setPassword($password);
        $this->save($user);
    }
}