<?php

namespace AppBundle\Admin;

use Aristek\Bundle\AdminBundle\Admin\Admin;
use Component\Doctrine\ORM\SortFixProxyQuery;
use AppBundle\Entity\User;
use AppBundle\Entity\RegistrationInvite;
use AppBundle\Service\DateService;
use AppBundle\Repository\RegistrationInviteRepository;
use Doctrine\ORM\QueryBuilder;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Route\RouteCollection;
use Sonata\AdminBundle\Datagrid\DatagridMapper;

/**
 * Class UserRegistrationInviteAdmin
 * @package AppBundle\Admin
 *
 * @method User getCurrentUser()
 */
class UserRegistrationInviteAdmin extends Admin
{
    private $filterParametersCache = null;

    /**
     * @var integer
     *
     * This value is calculated once in $this->getNewRegistrationInviteCount() and
     * is used as a parameter for the list template rendering.
     */
    private $newRegistrationInviteCount = null;

    /**
     * {@inheritdoc}
     */
    protected $baseRouteName = 'user_registration_invites';

    /**
     * {@inheritdoc}
     */
    protected $baseRoutePattern = 'user-registration-invites';

    /**
     * @var DateService
     */
    protected $dateService;

    /**
     * @var RegistrationInviteRepository
     */
    protected $registrationInviteRepository;

    /**
     * {@inheritdoc}
     */
    protected $datagridValues = array(
        '_page' => 1,
        '_per_page' => 32,
        'status' => ['value' => RegistrationInvite::STATUS_NEW],
        '_sort_order' => 'DESC',
        '_sort_by' => 'createdAt'
    );

    /**
     * {@inheritdoc}
     */
    public function getFilterParameters()
    {
        if (null === $this->filterParametersCache) {
            $this->filterParametersCache = parent::getFilterParameters();
        }

        return $this->filterParametersCache;
    }

    /**
     * {@inheritdoc}
     */
    public function createQuery($context = 'list')
    {
        if ($context === 'list') {
            /** @var QueryBuilder $qb */
            $qb = new SortFixProxyQuery(
                $this->registrationInviteRepository->createSelectByUserQueryBuilder($this->getCurrentUser()),
                ['status', 'organization_group_name', 'organization_group_type']
            );

            $filterParameters = $this->getFilterParameters();
            if (isset($filterParameters['_sort_by'])) {
                $this->registrationInviteRepository->correctQueryBuilderSorting(
                    $qb,
                    $filterParameters['_sort_by'],
                    $filterParameters['_sort_order']
                );
            }

            return $qb;
        } else {
            return parent::createQuery($context);
        }
    }

    /**
     * @param DatagridMapper $filter
     */
    protected function configureDatagridFilters(DatagridMapper $filter)
    {
        $filter
            ->add('status', null, [], 'choice', ['choices' => $this->getStatusesList()])
            ->add(
                'createdAt',
                'doctrine_orm_date_range',
                [],
                null,
                null,
                ['label' => 'registration_invite.label.invitation_datetime']
            );
    }

    /**
     * {@inheritdoc}
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->add(
                'status',
                'string',
                [
                    'label' => 'registration_invite.label.invitation_status',
                    'template' => '::Admin/RegistrationInvite/list__status.html.twig'
                ]
            )
            ->add(
                'organization_group_type',
                null,
                [
                    'label' => 'registration_invite.label.invitation_type',
                    'template' => '::Admin/RegistrationInvite/list__type.html.twig',
                    'sortable' => true,
                    'sort_field_mapping' => ['fieldName' => 'organization_group_type'],
                    'sort_parent_association_mappings' => []
                ]
            )
            ->add(
                'organization_group_name',
                null,
                [
                    'label' => 'registration_invite.label.invitation_name',
                    'sortable' => true,
                    'sort_field_mapping' => ['fieldName' => 'organization_group_name'],
                    'sort_parent_association_mappings' => []
                ]
            )
            ->add(
                'createdAt',
                null,
                [
                    'label' => 'registration_invite.label.invitation_datetime',
                    'template' => '::Admin/RegistrationInvite/list_datetime.html.twig',
                    'format' => $this->dateService->getDateTimeFormat()
                ]
            );

        if ($this->getNewRegistrationInviteCount() > 0) {
            $listMapper
                ->add(
                    '_action',
                    'actions',
                    [
                        'actions' => [
                            'confirm' => ['template' => '::Admin/RegistrationInvite/list__action_accept.html.twig'],
                            'decline' => ['template' => '::Admin/RegistrationInvite/list__action_decline.html.twig']
                        ],
                        'label' => 'registration_invite.label.invitation_actions',
                    ]
                );
        } else {

            //  $this->sendMessage('sonata_flash_info', 'registration_invite.list.no_new_invitations');
        }
    }

    /**
     * {@inheritdoc}
     */
    protected function configureRoutes(RouteCollection $collection)
    {
        $collection
            ->clearExcept(['list', 'batch'])
            ->add('invite', 'invite/{token}')
            ->add('accept-all', 'accept-all/{token}')
            ->add('decline-all', 'decline-all/{token}')
            ->add('accept', $this->getRouterIdParameter() . '/accept')
            ->add('decline', $this->getRouterIdParameter() . '/decline');
    }

    /**
     * @return array
     */
    public function getTypesList()
    {
        return RegistrationInvite::getTypesList();
    }

    /**
     * @return array
     */
    public function getStatusesList()
    {
        return RegistrationInvite::getStatusesList();
    }

    /**
     * @param RegistrationInviteRepository $registrationInviteRepository
     *
     * @return UserRegistrationInviteAdmin
     */
    public function setRegistrationInviteRepository(RegistrationInviteRepository $registrationInviteRepository)
    {
        $this->registrationInviteRepository = $registrationInviteRepository;

        return $this;
    }

    /**
     * @param DateService $dateService
     *
     * @return UserRegistrationInviteAdmin
     */
    public function setDateService(DateService $dateService)
    {
        $this->dateService = $dateService;

        return $this;
    }

    /**
     * @inheritdoc
     *
     * @return array
     */
    public function getBatchActions()
    {
        $actions = parent::getBatchActions();

        if ($this->getNewRegistrationInviteCount() > 0) {
            if ($this->hasRoute('accept') && $this->isGranted('LIST')) {
                $actions['accept'] = [
                    'label' => $this->trans('registration_invite.action.accept'),
                    'translation_domain' => 'AppBundle',
                    'ask_confirmation' => true
                ];
            }

            if ($this->hasRoute('decline') && $this->isGranted('LIST')) {
                $actions['decline'] = [
                    'label' => $this->trans('registration_invite.action.decline'),
                    'translation_domain' => 'AppBundle',
                    'ask_confirmation' => true
                ];
            }
        }

        return $actions;
    }

    /**
     * @return int
     */
    public function getNewRegistrationInviteCount()
    {
        if (null === $this->newRegistrationInviteCount) {
            $this->newRegistrationInviteCount = $this->registrationInviteRepository->getUserInvitesCount(
                $this->getCurrentUser(),
                RegistrationInvite::STATUS_NEW
            );
        }

        return $this->newRegistrationInviteCount;
    }
}
