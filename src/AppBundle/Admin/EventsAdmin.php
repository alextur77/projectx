<?php

namespace AppBundle\Admin;

use AppBundle\Entity\Event;
use AppBundle\Repository\EventRepository;
use AppBundle\Service\User\CurrentUserService;
use Aristek\Bundle\AdminBundle\Admin\Admin;
use Component\Doctrine\ORM\SortFixProxyQuery;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;
use Sonata\AdminBundle\Route\RouteCollection;
use AppBundle\Form\Type\DateTimePickerType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Tree\Fixture\User;

/**
 * Class EventsAdmin.
 */
class EventsAdmin extends Admin
{
    /**
     * @var string
     */
    protected $baseRouteName = 'my_events';

    /**
     * @var string
     */
    protected $baseRoutePattern = 'my_events';

    /**
     * @var CurrentUserService
     */
    protected $currentUserService;

    /**
     * @var EventRepository
     */
    protected $eventRepository;

    /**
     * {@inheritdoc}
     */
    public function createQuery($context = 'list')
    {
        if ($context === 'list') {

            $organization = $this->currentUserService->getCurrentOrganization();

            $qb = new SortFixProxyQuery(
                $this->eventRepository->createOrganizationAdminQueryBuilder($organization)
            );

            return $qb;
        } else {
            return parent::createQuery($context);
        }
    }

    /**
     * {@inheritdoc}
     */
    protected function configureListFields(ListMapper $list)
    {
        $this->setTemplate('event_list_outer_rows_list', 'AppBundle:Events:event_list_outer_rows_list.html.twig');
        $this->setTemplate('event_list_outer_rows_mosaic', 'AppBundle:Events:event_list_outer_rows_mosaic.html.twig');
        $this->setTemplate('list', 'AppBundle:Events:event_list.html.twig');

        $list
            ->addIdentifier(
                'name',
                null,
                [
                    'route' => [
                        'name' => 'go',
                    ],
                ]
            )
            ->add('description')
            ->add(
                'timeStart',
                null,
                array(
                    'format' => 'd.m.Y H:i'
                )
            )
            ->add(
                'timeEnd',
                null,
                array(
                    'format' => 'd.m.Y H:i'
                )
            );
    }

    /**
     * {@inheritdoc}
     */
    protected function configureShowFields(ShowMapper $show)
    {
        $this->setTemplate('show', 'AppBundle:Events:event_show.html.twig');

        $show
            ->with('General')
            ->add('name')
            ->add('description')
            ->add('type', 'choice', ['choices' => Event::getEventConst()])
            ->add('time_start', 'date', ['format' => 'd.m.Y H:i'])
            ->add('time_end', 'date', ['format' => 'd.m.Y H:i'])
            ->add('FullNameOwner', null, ['admin_code' => 'app.user_admin'])
            ->end();
    }

    /**
     * @return mixed
     */
    public function getNewInstance()
    {
        $instance = parent::getNewInstance();
        $owner = $this->getCurrentUser();
        $instance->setOwner($owner);

        return $instance;
    }

    /**
     * {@inheritdoc}
     */
    protected function configureFormFields(FormMapper $form)
    {

        $form
            ->with('General')
            ->add('name')
            ->add('description')
            ->add('type', ChoiceType::class, ['choices' => Event::getEventConst()])
            ->add('time_start', DateTimePickerType::class)
            ->add('time_end', DateTimePickerType::class)
            ->add('owner', TextType::class, ['disabled' => true], ['admin_code' => 'app.user_admin'])
            ->end();
    }

    /**
     * {@inheritdoc}
     */
    protected function configureRoutes(RouteCollection $collection)
    {
        $collection->add('go', $this->getRouterIdParameter() . '/go');
    }

    /**
     * @param mixed $currentUserService
     *
     * @return BaseGroupAdmin
     */
    public function setCurrentUserService($currentUserService)
    {
        $this->currentUserService = $currentUserService;

        return $this;
    }

    /**
     * @param EventRepository $eventRepository
     *
     * @return EventsAdmin
     */
    public function setEventRepository($eventRepository)
    {
        $this->eventRepository = $eventRepository;

        return $this;
    }
}

