<?php

namespace AppBundle\Admin;

use AppBundle\Repository\EventRepository;
use Aristek\Bundle\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Route\RouteCollection;
use Sonata\DoctrineORMAdminBundle\Datagrid\ProxyQuery;

/**
 * Class EventOrganizationsAdmin.
 */
class EventOrganizationsAdmin extends Admin
{
    /**
     * @var EventRepository
     */
    protected $eventRepository;

    /** @var string  */
    protected $baseRouteName = 'event_organizations';

    /**
     * @var string
     */
    protected $baseRoutePattern = 'event_organizations';

    /**
     * {@inheritdoc}
     */
    protected function configureListFields(ListMapper $list)
    {
        $this->setTemplate('inline_list', 'AppBundle:CRUD:inline_list.html.twig');

        $list
            ->addIdentifier('name')
            ->add('address')
            ->add('city')
            ->add('public')
            ->add('allowSingUp')
            ->add('active');
    }

    /**
     * {@inheritdoc}
     */
    protected function configureRoutes(RouteCollection $collection)
    {
        $collection->clear();
        $collection->add('list', 'list');
    }

    /**
     * {@inheritdoc}
     */
    public function createQuery($context = 'list')
    {
        $event = $this->request->cookies->get('event_id');
        $query = new ProxyQuery($this->eventRepository->createOrganizationQueryBuilder($event));

         return $query;
    }

    /**
     * @param /AppBundle/Repository/EventRepository $eventRepository
     *
     * @return EventOrganizationsAdmin
     */
    public function setEventRepository($eventRepository)
    {
        $this->eventRepository = $eventRepository;

        return $this;
    }
}
