<?php

namespace AppBundle\Admin;

use AppBundle\Entity\RegistrationRequest;
use AppBundle\Repository\RegistrationRequestRepository;
use Aristek\Bundle\AdminBundle\Admin\Admin;
use Doctrine\ORM\QueryBuilder;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\ProxyQueryInterface;
use Sonata\DoctrineORMAdminBundle\Datagrid\ProxyQuery;

/**
 * Class UserRegistrationRequestAdmin
 */
class UserRegistrationRequestAdmin extends Admin
{
    /**
     * @inheritdoc
     */
    protected $baseRouteName = 'user_registration_requests';

    /**
     * @inheritdoc
     */
    protected $baseRoutePattern = 'user-registration-requests';

    /**
     * @var RegistrationRequestRepository
     */
    protected $registrationRequestRepository;

    /**
     * Default values to the datagrid
     *
     * @var array
     */
    protected $datagridValues = array(
        '_page'       => 1,
        '_per_page'   => 32,
        'status' => ['value' => RegistrationRequest::STATUS_NEW]
    );

    /**
     * @param string $context
     *
     * @return ProxyQueryInterface
     */
    public function createQuery($context = 'list')
    {
        /** @var QueryBuilder $qb */
        $qb = new ProxyQuery($this->registrationRequestRepository->createUserQueryBuilder($this->getCurrentUser()));
        $qb
            ->addOrderBy('RegistrationRequest.createdAt', 'DESC');

        return $qb;
    }

    /**
     * @param RegistrationRequestRepository $registrationRequestRepository
     *
     * @return UserRegistrationRequestAdmin
     */
    public function setRegistrationRequestRepository($registrationRequestRepository)
    {
        $this->registrationRequestRepository = $registrationRequestRepository;

        return $this;
    }

    /**
     * @param DatagridMapper $filter
     */
    protected function configureDatagridFilters(DatagridMapper $filter)
    {
        $filter
            ->add('status', null, [], 'choice', ['choices' => $this->getStatusesList()]);
    }

    /**
     * @param ListMapper $list
     */
    protected function configureListFields(ListMapper $list)
    {
        $list
            ->add('organization.name')
            ->add('group.name')
            ->add('status', 'string', ['template' => '::Admin/RegistrationRequest/list__status.html.twig'])
            ->add('createdAt');
    }

    /**
     * @return array
     */
    public function getStatusesList()
    {
        return RegistrationRequest::getStatusesList();
    }
}
