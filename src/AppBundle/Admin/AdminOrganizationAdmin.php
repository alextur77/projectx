<?php

namespace AppBundle\Admin;

use Aristek\Bundle\AdminBundle\Admin\Admin;
use Knp\Menu\ItemInterface as MenuItemInterface;
use Knp\Menu\ItemInterface;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Route\RouteCollection;

/**
 * Class AdminOrganizationsAdmin
 */
class AdminOrganizationAdmin extends Admin
{
    /**
     * @var string
     */
    protected $baseRouteName = 'admin_organization';

    /**
     * @var string
     */
    protected $baseRoutePattern = 'admin-organization';

    /**
     * {@inheritdoc}
     */
    protected function configureFormFields(FormMapper $form)
    {
        $form
            ->add('name')
            ->add('address')
            ->add('city')
            ->add('state')
            ->add('zip')
            ->add('email');
    }

    /**
     * {@inheritdoc}
     */
    protected function configureRoutes(RouteCollection $collection)
    {

        $collection
            ->clearExcept(['edit', 'list']);
    }

    /**
     * {@inheritdoc}
     */
    public function getTemplate($name)
    {
        switch ($name) {
            case 'edit':
                return '::Admin\CRUD\edit_as_list.html.twig';
            default:
                return parent::getTemplate($name);
        }
    }

    /**
     * Generates the breadcrumbs array.
     *
     * Note: the method will be called by the top admin instance (parent => child)
     *
     * @param string             $action
     * @param ItemInterface|null $menu
     *
     * @return array
     */
    public function buildBreadcrumbs($action, MenuItemInterface $menu = null)
    {
        $items = parent::buildBreadcrumbs($action, $menu);
        $items->setParent($items->getParent()->getParent());

        return $items;
    }
}
