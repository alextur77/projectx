<?php

namespace AppBundle\Admin;

use Aristek\Bundle\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;
use Doctrine\ORM\QueryBuilder;

/**
 * Class UserAdmin
 */
class UserAdmin extends Admin
{
    /**
     * @var string
     */
    protected $baseRouteName = 'user_admin';

    /**
     * @var string
     */
    protected $baseRoutePattern = 'user-admin';

    /**
     * {@inheritdoc}
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->addIdentifier('username')
            ->add('fullName')
            ->add('contactInformation')
            ->add('enabled', null, array('editable' => true))
            ->add('locked', null, array('editable' => true))
            ->add('createdAt');
    }

    /**
     * {@inheritdoc}
     */
    protected function configureDatagridFilters(DatagridMapper $filterMapper)
    {
        $filterMapper
            ->add(
                'fullName',
                'doctrine_orm_callback',
                [
                    'callback' => [$this, 'getFullTextFilter'],
                    'field_type' => 'text'
                ]
            )
            ->add('phone')
            ->add('username')
            ->add('email');
    }

    /**
     * @param QueryBuilder $queryBuilder
     * @param string       $alias
     * @param string       $field
     * @param array        $value
     *
     * @return bool|void
     */
    public function getFullTextFilter($queryBuilder, $alias, $field, $value)
    {
        if (!$value['value']) {

            return true;
        }

        $queryBuilder->andWhere(
            $queryBuilder->expr()->orX(
                $queryBuilder->expr()->like(
                    $alias . '.firstName',
                    $queryBuilder->expr()->literal('%' . $value['value'] . '%')
                ),
                $queryBuilder->expr()->like(
                    $alias . '.lastName',
                    $queryBuilder->expr()->literal('%' . $value['value'] . '%')
                ),
                $queryBuilder->expr()->like(
                    $alias . '.patronymic',
                    $queryBuilder->expr()->literal('%' . $value['value'] . '%')
                )
            )
        );

        return true;
    }

    /**
     * {@inheritdoc}
     */
    protected function configureShowFields(ShowMapper $showMapper)
    {
        $showMapper
            ->with('General')
            ->add('username')
            ->add('email')
            ->end()
            ->with('Profile')
            ->add('firstName')
            ->add('lastName')
            ->end();
    }

    /**
     * {@inheritdoc}
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        // define group zoning
        $formMapper
            ->tab('User')
            ->with('General', array('class' => 'col-md-6'))->end()
            ->with('Profile', array('class' => 'col-md-6'))->end()
            ->end()
            ->tab('Security')
            ->with('Status', array('class' => 'col-md-4'))->end()
            ->end();

        $formMapper
            ->tab('User')
            ->with('General')
            ->add('username')
            ->add('email')
            ->add(
                'plainPassword',
                'text',
                [
                    'required' => (!$this->getSubject() || is_null($this->getSubject()->getId()))
                ]
            )
            ->add('enabled')
            ->end()
            ->with('Profile')
            ->add('firstName', null, array('required' => false))
            ->add('lastName', null, array('required' => false))
            ->end()
            ->end();

        $formMapper
            ->tab('Security')
            ->with('Status')
            ->add('locked', null, array('required' => false))
            ->add('expired', null, array('required' => false))
            ->add('enabled', null, array('required' => false))
            ->add('credentialsExpired', null, array('required' => false))
            ->end()
            ->end();
    }
}
