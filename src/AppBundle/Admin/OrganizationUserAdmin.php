<?php

namespace AppBundle\Admin;

use AppBundle\Entity\User;
use AppBundle\Repository\UserRepository;
use AppBundle\Service\User\CurrentUserService;
use AppBundle\Service\User\OrganizationUserService;
use Aristek\Bundle\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Route\RouteCollection;
use Sonata\DoctrineORMAdminBundle\Datagrid\ProxyQuery;

/**
 * Class OrganizationUserAdmin
 *
 * @method User getSubject()
 */
class OrganizationUserAdmin extends Admin
{
    /**
     * @var string
     */
    protected $baseRouteName = 'organization_user';

    /**
     * @var string
     */
    protected $baseRoutePattern = 'organization-user';

    /**
     * @var UserRepository
     */
    protected $userRepository;

    /**
     * @var CurrentUserService
     */
    protected $currentUserService;

    /**
     * @var OrganizationUserService
     */
    protected $organizationUserService;

    /**
     * {@inheritdoc}
     */
    public function createQuery($context = 'list')
    {
        $org = $this->currentUserService->getCurrentOrganization();
        $params = $this->getFilterParameters();
        $filters = [];
        if (!empty($params['group']['value'])) {
            $filters['group'] = $params['group'];
        }

        $queryBuilder = $this->userRepository->createOrganizationUsersQueryBuilder($org, $filters);

        return new ProxyQuery($queryBuilder);
    }

    /**
     * {@inheritdoc}
     */
    protected function configureRoutes(RouteCollection $collection)
    {
        $collection
            ->clearExcept(['list'])
            ->add('remove', $this->getRouterIdParameter() . '/remove')
            ->add('group-remove', $this->getRouterIdParameter() . '/group-remove/$groupId');
    }

    /**
     * {@inheritdoc}
     */
    protected function configureDatagridFilters(DatagridMapper $filter)
    {
        $filter
            ->add('lastName', null, ['show_filter' => true])
            ->add('firstName', null, ['show_filter' => true])
            ->add('group', 'doctrine_orm_string', ['show_filter' => true], null, ['mapped' => false]);
    }

    /**
     * {@inheritdoc}
     */
    protected function configureListFields(ListMapper $list)
    {
        $list
            ->add('lastName')
            ->add('firstName')
            ->add('groups', 'string', ['template' => '::Admin/OrganizationUser/groups.html.twig'])
            ->add(
                '_action',
                'actions',
                [
                    'actions' => [
                        'remove' => [
                            'template' => '::Admin/OrganizationUser/list__action_remove.html.twig'
                        ]
                    ]
                ]
            );
    }

    /**
     * {@inheritdoc}
     */
    public function getTemplate($name)
    {
        switch ($name) {
            case 'list':
                return '::/Admin/OrganizationUser/list.html.twig';
            default:
                return parent::getTemplate($name);
        }
    }

    /**
     * @param User $user
     *
     * @return array
     */
    public function getUserGroups($user)
    {
        $groupNames = [];
        $groups = $this->organizationUserService->getGroups($user);
        foreach ($groups as $group) {
            $groupNames[$group->getId()] = $group->getName();
        }

        return $groupNames;
    }

    /**
     * @param User $user
     *
     * @return integer
     */
    public function getUserCountGroups($user)
    {
        return count($this->organizationUserService->getGroups($user));
    }

    /**
     * @return int
     */
    public function generateRandomNumber()
    {
        return rand(0, 9999999999999);
    }

    /**
     * @param UserRepository $userRepository
     *
     * @return OrganizationUserAdmin
     */
    public function setUserRepository($userRepository)
    {
        $this->userRepository = $userRepository;

        return $this;
    }

    /**
     * @param CurrentUserService $currentUserService
     *
     * @return OrganizationUserAdmin
     */
    public function setCurrentUserService($currentUserService)
    {
        $this->currentUserService = $currentUserService;

        return $this;
    }

    /**
     * @param OrganizationUserService $organizationUserService
     *
     * @return OrganizationUserAdmin
     */
    public function setOrganizationUserService($organizationUserService)
    {
        $this->organizationUserService = $organizationUserService;

        return $this;
    }
}
