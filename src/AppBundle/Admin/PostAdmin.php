<?php

namespace AppBundle\Admin;

use Aristek\Bundle\AdminBundle\Admin\Admin;
use AppBundle\DataTransferObject\DTOTopic;
use AppBundle\Entity\Post;
use AppBundle\Entity\Role;
use AppBundle\Entity\Topic;
use AppBundle\Entity\User;
use AppBundle\Repository\PostRepository;
use AppBundle\Repository\TopicPermissionMutexRepository;
use AppBundle\Service\User\CurrentUserService;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Route\RouteCollection;
use Sonata\DoctrineORMAdminBundle\Model\ModelManager;

/**
 * Class PostAdmin
 * @package AppBundle\Admin
 *
 * @method User getCurrentUser()
 * @method Post getSubject()
 */
class PostAdmin extends Admin
{
    const PARENT_POST_ID = 'parent_post_id';

    /**
     * @var boolean|null
     */
    private $currentUserModeratorRights = null;

    /**
     * @var CurrentUserService
     */
    protected $currentUserService;

    /**
     * @var PostRepository
     */
    protected $postRepository;

    /**
     * @var TopicPermissionMutexRepository
     */
    protected $topicPermissionMutexRepository;

    /**
     * {@inheritdoc}
     */
    protected $parentAssociationMapping = 'topic';

    /**
     * {@inheritdoc}
     */
    protected $datagridValues = array(
        '_page' => 1,
        '_per_page' => 32,
        '_sort_order' => 'ASC',
        '_sort_by' => 'dateTime'
    );

    /**
     * {@inheritdoc}
     */
    protected function configureDatagridFilters(DatagridMapper $filter)
    {
        $filter
            ->add(
                'createdAt',
                'doctrine_orm_datetime_range',
                [],
                null,
                null,
                ['label' => 'post.label.date_time']
            )
            ->add('user.username', null, ['label' => 'post.label.user'])
            ->add('body', null, ['label' => 'post.label.body']);
    }

    /**
     * @return boolean
     */
    public function hasCurrentUserModeratorRights()
    {
        if (null === $this->currentUserModeratorRights) {
            $role = $this->currentUserService->getCurrentUserRoleInOrganization();
            $this->currentUserModeratorRights = $role === Role::ROLE_ORGANIZATION_ADMIN;
            if (!$this->currentUserModeratorRights) {
                $user = $this->currentUserService->getCurrentUser();
                /** @var Topic $topic */
                $topic = $this->getParent()->getSubject();
                $rights = $this->topicPermissionMutexRepository->hasUserTopicModeratorRights(
                    $user,
                    $topic
                );
                $this->currentUserModeratorRights = $rights[$topic->getId()];
            }
        }

        return $this->currentUserModeratorRights;
    }

    /**
     * {@inheritdoc}
     */
    protected function configureRoutes(RouteCollection $collection)
    {
        $collection
            ->clearExcept(['list', 'create', 'edit'])
            ->add('quote', $this->getRouterIdParameter() . '/quote')
            ->add('activate', $this->getRouterIdParameter() . '/activate')
            ->add('activate-all', $this->getRouterIdParameter() . '/activate-all')
            ->add('inactivate', $this->getRouterIdParameter() . '/inactivate')
            ->add('inactivate-all', $this->getRouterIdParameter() . '/inactivate-all');
    }

    /**
     * {@inheritdoc}
     */
    protected function configureListFields(ListMapper $list)
    {
        $list
            ->add('user.username', null, ['label' => 'post.label.user', 'sortable' => false])
            // Showing Post::body field caption but sorting by Post::dateTime
            ->add('dateTime', null, ['label' => 'post.label.body']);
    }

    /**
     * {@inheritdoc}
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->with($this->trans('post.label.body'))
            ->add('body', null, ['required' => true, 'label' => 'post.label.body']);

        $parentPostId = $this->getRequest()->query->get($this::PARENT_POST_ID);
        $formMapper->add($this::PARENT_POST_ID, 'hidden', ['mapped' => false, 'data' => $parentPostId]);
        $formMapper->end();
    }

    /**
     * {@inheritdoc}
     *
     * @param Post $object
     *
     * @return Post
     */
    public function prePersist($object)
    {
        $object->setDateTime(new \DateTime('NOW'));
        $object->setUser($this->currentUserService->getCurrentUser());

        $parentPostId = $this->getForm()->get($this::PARENT_POST_ID)->getData();
        if (null !== $parentPostId) {
            // This easy trick allows to use the entity id instead of the entity object
            // in order to avoid an additional SQL SELECT like this:
            // $parentPost = PostRepository::find($parentPostId);
            /** @var ModelManager $modelManager */
            $modelManager = $this->modelManager;
            $className = $this->getClass();
            $entityManager = $modelManager->getEntityManager($className);
            /** @var Post $parentPost */
            $parentPost = $entityManager->getReference($className, $parentPostId);
            $object->setParentPost($parentPost);
        }

        return $object;
    }

    /**
     * {@inheritdoc}
     */
    public function getUrlsafeIdentifier($entity)
    {
        return $entity instanceof DTOTopic ?
            $entity->getId() :
            parent::getUrlsafeIdentifier($entity);
    }

    /**
     * {@inheritdoc}
     */
    public function getNormalizedIdentifier($entity)
    {
        return $entity instanceof DTOTopic ?
            $entity->getId() :
            parent::getNormalizedIdentifier($entity);
    }

    /**
     * @param mixed $currentUserService
     *
     * @return PostAdmin
     */
    public function setCurrentUserService($currentUserService)
    {
        $this->currentUserService = $currentUserService;

        return $this;
    }

    /**
     * @param PostRepository $postRepository
     *
     * @return PostAdmin
     */
    public function setPostRepository($postRepository)
    {
        $this->postRepository = $postRepository;

        return $this;
    }

    /**
     * @param TopicPermissionMutexRepository $topicPermissionMutexRepository
     *
     * @return PostAdmin
     */
    public function setTopicPermissionMutexRepository($topicPermissionMutexRepository)
    {
        $this->topicPermissionMutexRepository = $topicPermissionMutexRepository;

        return $this;
    }
}
