<?php

namespace AppBundle\Admin;

use Aristek\Bundle\AdminBundle\Admin\Admin;
use AppBundle\Entity\Role;
use AppBundle\Entity\Topic;
use AppBundle\Entity\User;
use AppBundle\Manager\TopicManager;
use AppBundle\Manager\TopicPermissionMutexManager;
use AppBundle\Repository\TopicRepository;
use AppBundle\Service\User\CurrentUserService;
use Component\Doctrine\ORM\HiddenProxyQuery;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Route\RouteCollection;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\CoreBundle\Validator\ErrorElement;

/**
 * Class TopicAdmin
 * @package AppBundle\Admin
 *
 * @method User getCurrentUser()
 * @method Topic getSubject()
 */
class TopicAdmin extends Admin
{
    /**
     * @var CurrentUserService
     */
    protected $currentUserService;

    /**
     * @var TopicManager
     */
    protected $topicManager;

    /**
     * @var TopicPermissionMutexManager
     */
    protected $topicPermissionMutexManager;

    /**
     * @var TopicRepository
     */
    protected $topicRepository;

    /**
     * @var integer[]
     */
    private $participantsQuantity = null;

    /**
     * {@inheritdoc}
     */
    protected $datagridValues = array(
        '_page' => 1,
        '_per_page' => 32,
        '_sort_order' => 'ASC',
        '_sort_by' => 'name'
    );

    /**
     * {@inheritdoc}
     */
    protected function configureDatagridFilters(DatagridMapper $filter)
    {
        $filter
            ->add('name', null, ['label' => 'topic.label.name'])
            ->add('description', null, ['label' => 'topic.label.description'])
            ->add('opened', null, ['label' => 'topic.label.opened'])
            ->add('active', null, ['label' => 'topic.label.active'])
            ->add(
                'createdAt',
                'doctrine_orm_datetime_range',
                [],
                null,
                null,
                ['label' => 'topic.label.created_at']
            );
    }

    /**
     * {@inheritdoc}
     */
    public function createQuery($context = 'list')
    {
        if ($context === 'list') {
            $user = $this->currentUserService->getCurrentUser();
            $organization = $this->currentUserService->getCurrentOrganization();
            $role = $this->currentUserService->getCurrentUserRoleInOrganization();
            if ($role === Role::ROLE_ORGANIZATION_ADMIN) {
                $qb = $this->topicRepository->createQueryBuilderForOrganizationAdmin($organization, false);
            } else {
                if ($this->currentUserService->hasCurrentUserAnyGroupAdminAccess()) {
                    $qb = $this->topicRepository->createQueryBuilderForGroupAdmin(
                        $organization,
                        $user,
                        false,
                        true,
                        null
                    );
                } else {
                    throw new \LogicException(
                        __METHOD__ . ': Current user has no admin rights to administrate topics!'
                    );
                }
            }

            return new HiddenProxyQuery($qb);
        } else {
            return parent::createQuery($context);
        }
    }

    /**
     * {@inheritdoc}
     */
    protected function configureRoutes(RouteCollection $collection)
    {
        $collection
            ->remove('export')
            ->remove('show')
            ->add('participants', 'participants/{topicId}')
            ->add('update-moderators');
    }

    /**
     * {@inheritdoc}
     */
    protected function configureListFields(ListMapper $list)
    {
        $list
            ->addIdentifier('name', null, ['label' => 'topic.label.name'])
            ->add('createdAt', null, ['label' => 'topic.label.created_at'])
            ->add(
                'opened',
                null,
                [
                    'editable' => true,
                    'row_align' => 'center',
                    'label' => 'topic.label.opened'
                ]
            )
            ->add(
                'active',
                null,
                [
                    'editable' => true,
                    'row_align' => 'center',
                    'label' => 'topic.label.active'
                ]
            )
            ->add(
                '_action',
                'actions',
                [
                    'actions' => [
                        'participants' => ['template' => '::Admin/Topic/list__action_participants.html.twig'],
                        'delete' => ['template' => '::Admin/Topic/list__action_delete.html.twig']
                    ],
                    'label' => 'topic.label.actions',
                ]
            );
    }

    /**
     * {@inheritdoc}
     *
     * @throws \LogicException
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        $subject = $this->getSubject();
        $creating = !is_object($subject);
        if (!$creating) {
            $creating = null === $subject->getId();
        }

        $formMapper
            ->with($this->trans('topic.label.description'))
            ->add('name', null, ['required' => true, 'label' => 'topic.label.name'])
            ->add('opened', 'sonata_type_boolean', ['transform' => true, 'label' => 'topic.label.opened'])
            ->add('active', 'sonata_type_boolean', ['transform' => true, 'label' => 'topic.label.active'])
            ->add('description', null, ['required' => false, 'label' => 'topic.label.description'])
            ->end();

        if ($creating) {
            $allPossibleModeratorChoices = null;
            list($groupChoices) =
                $this->topicPermissionMutexManager->getParticipantChoices(null, [], $allPossibleModeratorChoices);
            $formMapper
                ->with($this->trans('topic.label.participants'))
                ->add(
                    'groups',
                    'choice',
                    [
                        'choices' => $groupChoices,
                        'required' => false,
                        'multiple' => true,
                        'mapped' => false,
                        'data' => [],
                        'attr' => ['placeholder' => $this->trans('topic.admin.groups_placeholder')],
                        'label' => 'topic.label.groups'
                    ]
                )
                ->add(
                    'moderators',
                    'choice',
                    [
                        'choices' => $allPossibleModeratorChoices,
                        'required' => false,
                        'multiple' => true,
                        'mapped' => false,
                        'data' => [],
                        'attr' => ['placeholder' => $this->trans('topic.admin.moderators_placeholder')],
                        'label' => 'topic.label.moderators'
                    ]
                )
                ->end();
        }
    }

    /**
     * {@inheritdoc}
     *
     * @param Topic $object
     */
    public function create($object)
    {
        $this->prePersist($object);
        foreach ($this->extensions as $extension) {
            $extension->prePersist($this, $object);
        }

        /** @var array $groupIds */
        $groupIds = $this->getForm()->get('groups')->getData();
        if ((null !== $groupIds) and empty($groupIds)) {
            $groupIds = null;
        }
        /** @var array $moderatorIds */
        $moderatorIds = $this->getForm()->get('moderators')->getData();
        if ((null !== $moderatorIds) and empty($moderatorIds)) {
            $moderatorIds = null;
        }
        $this->topicManager->add($object, $groupIds, $moderatorIds);

        $this->postPersist($object);
        foreach ($this->extensions as $extension) {
            $extension->postPersist($this, $object);
        }

        $this->createObjectSecurity($object);

        return $object;
    }

    /**
     * {@inheritdoc}
     *
     * @param Topic $object
     */
    public function delete($object)
    {
        $this->preRemove($object);
        foreach ($this->extensions as $extension) {
            $extension->preRemove($this, $object);
        }

        $this->getSecurityHandler()->deleteObjectSecurity($this, $object);
        $this->topicManager->delete($object);

        $this->postRemove($object);
        foreach ($this->extensions as $extension) {
            $extension->postRemove($this, $object);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function validate(ErrorElement $errorElement, $object)
    {
        $form = $this->getForm();
        $controlName = 'groups';
        if ($form->has($controlName)) {
            $groups = $form->get($controlName)->getData();
            if (empty($groups)) {
                $errorElement->addViolation($this->trans('topic.validator.no_groups'));
            }
        }
    }

    /**
     * {@inheritdoc}
     *
     * @param Topic $object
     *
     * @return Topic
     */
    public function prePersist($object)
    {
        return $object->setOrganization($this->currentUserService->getCurrentOrganization());
    }

    /**
     * @return integer[]
     */
    public function getParticipantsQuantity()
    {
        if (null === $this->participantsQuantity) {
            /** @var Topic[] $topics */
            $topics = $this->datagrid->getResults();
            $this->participantsQuantity = $this->topicPermissionMutexManager->getParticipantsQuantity($topics);
        }

        return $this->participantsQuantity;
    }

    /**
     * @param CurrentUserService $currentUserService
     *
     * @return TopicAdmin
     */
    public function setCurrentUserService(CurrentUserService $currentUserService)
    {
        $this->currentUserService = $currentUserService;

        return $this;
    }

    /**
     * @param TopicManager $topicManager
     *
     * @return TopicAdmin
     */
    public function setTopicManager($topicManager)
    {
        $this->topicManager = $topicManager;

        return $this;
    }

    /**
     * @param TopicPermissionMutexManager $topicPermissionMutexManager
     *
     * @return TopicAdmin
     */
    public function setTopicPermissionMutexManager($topicPermissionMutexManager)
    {
        $this->topicPermissionMutexManager = $topicPermissionMutexManager;

        return $this;
    }

    /**
     * @param TopicRepository $topicRepository
     *
     * @return TopicAdmin
     */
    public function setTopicRepository($topicRepository)
    {
        $this->topicRepository = $topicRepository;

        return $this;
    }
}
