<?php

namespace AppBundle\Admin;

use AppBundle\Entity\Organization;
use Aristek\Bundle\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;

/**
 * Class OrganizationAdmin
 */
class OrganizationAdmin extends Admin
{
    /**
     * @inheritdoc
     */
    protected function configureListFields(ListMapper $list)
    {
        $list
            ->addIdentifier('name')
            ->add('address')
            ->add('city')
            ->add('public')
            ->add('allowSingUp')
            ->add('active')
            ->add('_action', 'actions', array(
                'actions' => array(
                    'edit' => array(),
                    'delete' => array(),
                )
            ));
    }

    /**
     * @inheritdoc
     */
    protected function configureFormFields(FormMapper $form)
    {
        $form
            ->add('name')
            ->add('email')
            ->add('phoneNumber')
            ->add('websiteURL')
            ->add('address')
            ->add('city')
            ->add('state')
            ->add('zip')
            ->add('public')
            ->add('allowSingUp')
            ->add('active')
            ->add(
                'organizationUserPermissions',
                'sonata_type_collection',
                ['by_reference' => true],
                ['edit' => 'inline', 'inline' => 'table', 'admin_code' => 'app.organization_user_permission_admin']
            );
    }

    /**
     * @param Organization $object
     *
     * @return Organization
     */
    public function preUpdate($object)
    {
        $object->setOrganizationUserPermissions($object->getOrganizationUserPermissions());

        return $object;
    }

    /**
     * @param Organization $object
     *
     * @return Organization
     */
    public function prePersist($object)
    {
        $object->setOrganizationUserPermissions($object->getOrganizationUserPermissions());

        return $object;
    }
}
