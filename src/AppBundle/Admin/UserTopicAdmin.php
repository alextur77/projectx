<?php

namespace AppBundle\Admin;

use AppBundle\Entity\Post;
use AppBundle\Repository\PostRepository;
use AppBundle\Repository\TopicGroupPermissionRepository;
use AppBundle\Repository\TopicMessageReadRepository;
use Aristek\Bundle\AdminBundle\Admin\Admin;
use AppBundle\DataTransferObject\DTOGroup;
use AppBundle\DataTransferObject\DTOTopic;
use AppBundle\Entity\Role;
use AppBundle\Entity\Topic;
use AppBundle\Entity\User;
use AppBundle\Manager\TopicManager;
use AppBundle\Repository\GroupRepository;
use AppBundle\Repository\TopicPermissionMutexRepository;
use AppBundle\Repository\TopicRepository;
use AppBundle\Service\User\CurrentUserService;
use Component\Doctrine\ORM\SortFixProxyQuery;
use Doctrine\ORM\QueryBuilder;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Route\RouteCollection;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class UserTopicAdmin
 * @package AppBundle\Admin
 *
 * @method User getCurrentUser()
 * @method Topic getSubject()
 */
class UserTopicAdmin extends Admin
{
    private $filterParametersCache = null;

    /**
     * This array is indexed by the topic ids.
     * Don't use this field directly. Use getter function instead.
     *
     * @var array|null
     */
    private $currentUserTopicRights = null;

    /**
     * Don't use this field directly. Use getter function instead.
     *
     * @var DTOTopic[]|null
     */
    private $datagridResults = null;

    /**
     * Don't use this field directly. Use getter function instead.
     *
     * @var array|null
     */
    private $lastPostInfo = null;

    /**
     * {@inheritdoc}
     */
    protected $baseRouteName = 'user_topics';

    /**
     * {@inheritdoc}
     */
    protected $baseRoutePattern = 'user-topics';

    /**
     * @var CurrentUserService
     */
    protected $currentUserService;

    /**
     * @var TopicPermissionMutexRepository
     */
    protected $topicPermissionMutexRepository;

    /**
     * @var TopicGroupPermissionRepository
     */
    protected $topicGroupPermissionRepository;

    /**
     * @var TopicManager
     */
    protected $topicManager;

    /**
     * @var TopicRepository
     */
    protected $topicRepository;

    /**
     * @var TopicMessageReadRepository
     */
    protected $topicMessageReadRepository;

    /**
     * @var PostRepository
     */
    protected $postRepository;

    /**
     * @var GroupRepository
     */
    protected $groupRepository;

    /**
     * {@inheritdoc}
     */
    protected $datagridValues = array(
        '_page' => 1,
        '_per_page' => 32,
        '_sort_order' => 'ASC',
        '_sort_by' => 'name'
    );

    /**
     * {@inheritdoc}
     */
    public function getFilterParameters()
    {
        if (null === $this->filterParametersCache) {
            $this->datagridValues = array_merge(
                [
                    'read' => [
                        'type' => '',
                        'value' => 'yes'
                    ]
                ],
                $this->datagridValues

            );
            $this->filterParametersCache = parent::getFilterParameters();
        }

        return $this->filterParametersCache;
    }

    /**
     * @param Request $request
     * @param integer|integer[] $groups
     *
     * @return string
     */
    public function generateListURLFilteredByGroups(Request $request, $groups)
    {
        if (!is_array($groups)) {
            $groups = [$groups];
        }
        // It is necessary to read the all current filter state before changing its groups filter.
        // This code is based on public function \Sonata\AdminBundle\Admin\Admin::getFilterParameters():
        $parameters = [];
        if (null !== $request) {
            $storedFilters = [];
            if ($this->persistFilters) {
                $storedFilters = $request->getSession()->get($this->getCode() . '.filter.parameters', []);
            }

            $parameters = array_merge(
                $this->getModelManager()->getDefaultSortValues($this->getClass()),
                $this->datagridValues,
                $storedFilters
            );

            if (!$this->determinedPerPageValue($parameters['_per_page'])) {
                $parameters['_per_page'] = $this->maxPerPage;
            }

            // always force the parent value
            if ($this->isChild() && $this->getParentAssociationMapping()) {
                $name = str_replace('.', '__', $this->getParentAssociationMapping());
                $parameters[$name] = array('value' => $request->get($this->getParent()->getIdParameter()));
            }
        }
        $parameters['groups'] = ['value' => $groups];

        return $this->generateUrl('list', ['filter' => $parameters]);
    }

    /**
     * {@inheritdoc}
     */
    protected function configureDatagridFilters(DatagridMapper $filter)
    {
        $user = $this->currentUserService->getCurrentUser();
        $organization = $this->currentUserService->getCurrentOrganization();
        $role = $this->currentUserService->getCurrentUserRoleInOrganization();
        $preferredChoices = [];
        $choices = [];
        if ($role === Role::ROLE_ORGANIZATION_ADMIN) {
            $adminGroups = $this->groupRepository->findActiveByOrganization($organization);
            foreach ($adminGroups as $group) {
                $choices[$group->getId()] = $group->getShift() . $group->getName();
            }
        } elseif (($role === Role::ROLE_ORGANIZATION_USER) or (null === $role)) {
            // $user has Role::ROLE_ORGANIZATION_USER role in $organization or have not any role at all:
            $adminGroups = $this->groupRepository->findByGroupAdmin($user, $organization);
            $userGroups = $this->groupRepository->findByOnlyUserAccess($user, $organization);
            /** @var DTOGroup $group */
            foreach ($adminGroups as $group) {
                $id = $group->getId();
                $choices[$id] = $group->getShift() . $group->getName();
                $preferredChoices[] = $id;
            }
            foreach ($userGroups as $group) {
                $choices[$group->getId()] = $group->getName();
            }
        } else {
            throw new \LogicException(__METHOD__ . ': Unknown organization user role: "' . $role . '""!');
        }

        $filter
            ->add('name', null, ['label' => 'topic.label.name'])
            ->add('description', null, ['label' => 'topic.label.description'])
            ->add('opened', null, ['label' => 'topic.label.opened'])
            ->add(
                'createdAt',
                'doctrine_orm_datetime_range',
                [],
                null,
                null,
                ['label' => 'topic.label.created_at']
            )
            ->add(
                'groups',
                'doctrine_orm_callback',
                [
                    'label' => 'topic.label.groups',
                    'callback' => function ($qb, $alias, $field, $value) {
                        if (!$value['value']) {
                            return false;
                        }
                        $this->topicRepository->correctQueryBuilderGroupsFilter($qb, $value['value']);

                        return true;
                    }
                ],
                'choice',
                [
                    'choices' => $choices,
                    'preferred_choices' => $preferredChoices,
                    'required' => false,
                    'multiple' => true
                ]
            )
            ->add(
                'read',
                'doctrine_orm_callback',
                [
                    'callback' => [$this, 'getReadFilter'],
                    'field_type' => 'choice',
                    'field_options' => ['choices' => ['yes' => 'yes', 'no' => 'no']]
                ],
                null,
                null,
                ['label' => 'label.read_topic']
            );

    }

    /**
     * @param QueryBuilder $queryBuilder
     * @param string       $alias
     * @param string       $field
     * @param array        $value
     *
     * @return bool|void
     */
    public function getReadFilter($queryBuilder, $alias, $field, $value)
    {
        if (!$value['value']) {

            return true;
        }

        if ($value['value'] == 'yes') {

            $role = $this->currentUserService->getCurrentUserRoleInOrganization();

            if ($role == Role::ROLE_ORGANIZATION_ADMIN) {

                $topic = $this->topicRepository->findBy(
                    ['organization' => $this->currentUserService->getCurrentOrganization()]
                );

                $topicMassId = '';

                foreach ($topic as $item) {
                    $topicMassId .= $item->getId() . ',';
                }

                $topicIdFromGroups = substr($topicMassId, 0, -1);

            } else {

                $groups = $this->groupRepository->findUserGroupsByOrganization(
                    $this->getCurrentUser(),
                    $this->currentUserService->getCurrentOrganization()

                );

                $topicIdFromGroups = '';

                foreach ($groups as $item) {
                    $topicFromGroups = $this->topicGroupPermissionRepository->findBy(['group' => $item->getId()]);

                    if (!empty($topicFromGroups)) {

                        foreach ($topicFromGroups as $item1) {
                            $topicIdFromGroups .= $item1->getTopic()->getId() . ',';
                        }

                    }
                }

                $topicIdFromGroups = substr($topicIdFromGroups, 0, -1);

            }

            if (!empty($topicIdFromGroups)) {

                $readTopicMessage = $this->topicMessageReadRepository->getTopicReadMessage(
                    $topicIdFromGroups,
                    $this->getCurrentUser()
                );

                if (empty($readTopicMessage)) {

                    $queryBuilder
                        ->andWhere($alias . '.id IN (' . $topicIdFromGroups . ')');
                } else {

                    $readPost = '';
                    foreach ($readTopicMessage as $value) {

                        $readPost .= $this->topicMessageReadRepository->findOneBy(['id' => $value['id']])->getPost(
                            )->getId() . ',';
                    }

                    $readPost = substr($readPost, 0, -1);

                    $queryBuilder->leftJoin('AppBundle:Post', 'r', 'WITH', $alias . '.id = r.topic')
                        ->andWhere($alias . '.id IN (' . $topicIdFromGroups . ')')
                        ->andWhere('r.id NOT IN (' . $readPost . ')');
                }
            }

        }

        return true;
    }

    /**
     * {@inheritdoc}
     */
    public function createQuery($context = 'list')
    {
        if ($context === 'list') {
            $organization = $this->currentUserService->getCurrentOrganization();
            $user = $this->currentUserService->getCurrentUser();
            $role = $this->currentUserService->getCurrentUserRoleInOrganization();
            if ($role === Role::ROLE_ORGANIZATION_ADMIN) {
                /** @var QueryBuilder $qb */
                $qb = new SortFixProxyQuery(
                    $this->topicRepository->createQueryBuilderForOrganizationAdmin($organization, true),
                    ['post_quantity', 'last_post']
                );
            } else {
                /** @var QueryBuilder $qb */
                $qb = new SortFixProxyQuery(
                    $this->topicRepository->createQueryBuilderForGroupAdmin(
                        $organization,
                        $user,
                        true,
                        false
                    ),
                    ['post_quantity', 'last_post']
                );
            }

            $filterParameters = $this->getFilterParameters();
            if (isset($filterParameters['_sort_by'])) {
                $this->topicRepository->correctQueryBuilderSorting(
                    $qb,
                    $filterParameters['_sort_by'],
                    $filterParameters['_sort_order']
                );
            }

            return $qb;
        } else {
            return parent::createQuery($context);
        }
    }

    /**
     * {@inheritdoc}
     */
    protected function configureRoutes(RouteCollection $collection)
    {
        $collection
            ->remove('create')
            ->remove('edit')
            ->remove('delete')
            ->remove('batch')
            ->remove('export')
            ->remove('show');
        // The following line eliminates the access to the child admin routes:
        // $collection->clearExcept(['list']);
    }

    /**
     * {@inheritdoc}
     */
    protected function configureListFields(ListMapper $list)
    {
        $list
            ->add(
                'name',
                null,
                [
                    'label' => 'topic.label.name',
                    'template' => '::Admin/Topic/user_list__name.html.twig'
                ]
            )
            ->add('createdAt', null, ['label' => 'topic.label.created_at', 'format' => 'd.m.Y H:i'])
            ->add('opened', null, ['row_align' => 'center', 'label' => 'topic.label.opened'])
            ->add(
                'post_quantity',
                null,
                [
                    'label' => 'topic.label.post_quantity',
                    'template' => '::Admin/Topic/user_list__post_quantity.html.twig',
                    'sortable' => true,
                    'sort_field_mapping' => ['fieldName' => 'post_quantity'],
                    'sort_parent_association_mappings' => []
                ]
            )
            ->add(
                'last_post',
                null,
                [
                    'label' => 'topic.label.last_post',
                    'template' => '::Admin/Topic/user_list__last_post.html.twig',
                    'sortable' => true,
                    'sort_field_mapping' => ['fieldName' => 'last_post'],
                    'sort_parent_association_mappings' => []
                ]
            )
            ->add(
                'access',
                null,
                [
                    'row_align' => 'center',
                    'label' => 'topic.label.access',
                    'template' => '::Admin/Topic/user_list__access.html.twig'
                ]
            );
    }

    /**
     * @return DTOTopic[]|null
     */
    public function getDatagridResults()
    {
        if (null === $this->datagridResults) {
            $this->datagridResults = $this->datagrid->getResults();
        }

        return $this->datagridResults;
    }

    /**
     * @return array|null
     */
    public function getCurrentUserTopicRights()
    {
        if (null === $this->currentUserTopicRights) {
            $role = $this->currentUserService->getCurrentUserRoleInOrganization();
            if ($role === Role::ROLE_ORGANIZATION_ADMIN) {
                foreach ($this->getDatagridResults() as $topic) {
                    $this->currentUserTopicRights[$topic->getId()] = Role::ROLE_GROUP_ADMIN;
                }
            } else {
                $user = $this->currentUserService->getCurrentUser();
                $this->currentUserTopicRights = $this->topicPermissionMutexRepository->getUserRights(
                    $user,
                    $this->getDatagridResults()
                );
            }
        }

        return $this->currentUserTopicRights;
    }

    /**
     * @return array
     */
    public function getNumberOfNewMessageFromTopic()
    {
        $numberOfNewMessage = [];
        foreach ($this->getDatagridResults() as $topic) {

            $lastUrl = '/user-topics/' . $topic->getId() . '/post/list';

            $postRead = $this->topicMessageReadRepository->findBy(
                [
                    'topic' => $this->topicRepository->findOneBy(['id' => $topic->getId()]),
                    'recipient' => $this->getCurrentUser()
                ]
            );

            $postWrite = $this->postRepository->findBy(
                ['topic' => $this->topicRepository->findOneBy(['id' => $topic->getId()])]
            );

            $numberOfNewMessage[$topic->getId()] = [
                'count' => count($postWrite) - count($postRead),
                'lastUrl' => $lastUrl
            ];
        }

        return $numberOfNewMessage;

    }

    /**
     * {@inheritdoc}
     */
    public function getUrlsafeIdentifier($entity)
    {
        return $entity instanceof DTOTopic ?
            $entity->getId() :
            parent::getUrlsafeIdentifier($entity);
    }

    /**
     * {@inheritdoc}
     */
    public function getNormalizedIdentifier($entity)
    {
        return $entity instanceof DTOTopic ?
            $entity->getId() :
            parent::getNormalizedIdentifier($entity);
    }

    /**
     * @return array|null
     */
    public function getLastPostInfo()
    {
        if (null === $this->lastPostInfo) {
            $organization = $this->currentUserService->getCurrentOrganization();
            $this->lastPostInfo = $this->topicManager->getLastPostInfo($organization, $this->getDatagridResults());

        }

        return $this->lastPostInfo;
    }

    /**
     * @param CurrentUserService $currentUserService
     *
     * @return UserTopicAdmin
     */
    public function setCurrentUserService($currentUserService)
    {
        $this->currentUserService = $currentUserService;

        return $this;
    }

    /**
     * @param TopicRepository $topicRepository
     *
     * @return UserTopicAdmin
     */
    public function setTopicRepository($topicRepository)
    {
        $this->topicRepository = $topicRepository;

        return $this;
    }

    /**
     * @param TopicManager $topicManager
     *
     * @return UserTopicAdmin
     */
    public function setTopicManager($topicManager)
    {
        $this->topicManager = $topicManager;

        return $this;
    }

    /**
     * @param TopicPermissionMutexRepository $topicPermissionMutexRepository
     *
     * @return UserTopicAdmin
     */
    public function setTopicPermissionMutexRepository($topicPermissionMutexRepository)
    {
        $this->topicPermissionMutexRepository = $topicPermissionMutexRepository;

        return $this;
    }

    /**
     * @param GroupRepository $groupRepository
     *
     * @return UserTopicAdmin
     */
    public function setGroupRepository($groupRepository)
    {
        $this->groupRepository = $groupRepository;

        return $this;
    }

    /**
     * @param PostRepository $postRepository
     *
     * @return UserTopicAdmin
     */
    public function setPostRepository($postRepository)
    {
        $this->postRepository = $postRepository;

        return $this;
    }

    /**
     * @param  TopicGroupPermissionRepository $topicGroupPermissionRepository
     *
     * @return UserTopicAdmin
     */
    public function setTopicGroupPermissionRepository($topicGroupPermissionRepository)
    {
        $this->topicGroupPermissionRepository = $topicGroupPermissionRepository;

        return $this;
    }

    /**
     * @param  TopicMessageReadRepository $topicMessageReadRepository
     *
     * @return UserTopicAdmin
     */
    public function setTopicMessageReadRepository($topicMessageReadRepository)
    {
        $this->topicMessageReadRepository = $topicMessageReadRepository;

        return $this;
    }
}
