<?php

namespace AppBundle\Admin;

use Aristek\Bundle\AdminBundle\Admin\Admin;
use Component\Doctrine\ORM\TopicPermissionMutex\TopicPermissionMutexProxyQuery;
use AppBundle\DataTransferObject\DTOGroup;
use AppBundle\DataTransferObject\DTOTopicPermission;
use AppBundle\Entity\Topic;
use AppBundle\Entity\Role;
use AppBundle\Entity\TopicPermissionMutex;
use AppBundle\Manager\TopicGroupPermissionManager;
use AppBundle\Manager\TopicPermissionMutexManager;
use AppBundle\Manager\TopicModeratorManager;
use AppBundle\Repository\GroupRepository;
use AppBundle\Repository\GroupUserPermissionRepository;
use AppBundle\Repository\TopicRepository;
use AppBundle\Repository\TopicPermissionMutexRepository;
use AppBundle\Service\User\CurrentUserService;
use Doctrine\ORM\Query;
use Knp\Menu\ItemInterface as MenuItemInterface;
use Sonata\AdminBundle\Admin\AdminInterface;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Route\RouteCollection;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\CoreBundle\Validator\ErrorElement;
use Sonata\DoctrineORMAdminBundle\Model\ModelManager;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

/**
 * Class TopicPermissionAdmin
 * @package AppBundle\Admin
 *
 * @method User getCurrentUser()
 * @method TopicPermissionMutex getSubject();
 */
class TopicPermissionAdmin extends Admin
{
    private $filterParametersCache = null;

    /**
     * @var Topic|null
     *
     * WARNING: Don't use this variable directly! Use $this->getTopic() instead.
     */
    private $topic = null;

    /**
     * @var boolean|null
     */
    private $currentIsUserOrganizationAdmin = null;

    /**
     * @var array|null
     */
    private $ownGroupIds = null;

    /**
     * @var array|null
     */
    private $ownGroupUserIds = null;

    /**
     * @var integer|null
     */
    private $ownGroupPermissionsQuantity = null;

    /**
     * @var integer|null
     */
    private $totalGroupPermissionsQuantity = null;

    /**
     * @var integer|null
     */
    private $ownModeratorsQuantity = null;

    /**
     * @var integer|null
     */
    private $ownParticipantsQuantity = null;

    /**
     * @var CurrentUserService
     */
    protected $currentUserService;

    /**
     * @var TopicPermissionMutexRepository
     */
    protected $topicPermissionMutexRepository;

    /**
     * @var TopicGroupPermissionManager
     */
    protected $topicGroupPermissionManager;

    /**
     * @var TopicModeratorManager
     */
    protected $topicModeratorManager;

    /**
     * @var TopicRepository
     */
    protected $topicRepository;

    /**
     * @var GroupRepository
     */
    protected $groupRepository;

    /**
     * @var GroupUserPermissionRepository
     */
    protected $groupUserPermissionRepository;

    /**
     * @var TopicAdmin
     */
    protected $topicAdmin;

    /**
     * @var TopicPermissionMutexManager
     */
    protected $topicPermissionMutexManager;

    /**
     * {@inheritdoc}
     */
    protected $datagridValues = array(
        '_page' => 1,
        '_per_page' => 32,
        '_sort_order' => 'ASC',
        '_sort_by' => 'name'
    );

    /**
     * @return boolean|null
     */
    public function isCurrentUserOrganizationAdmin()
    {
        if (null === $this->currentIsUserOrganizationAdmin) {
            $role = $this->currentUserService->getCurrentUserRoleInOrganization();
            if ($role === Role::ROLE_ORGANIZATION_ADMIN) {
                $this->currentIsUserOrganizationAdmin = true;
            } elseif (($role === Role::ROLE_ORGANIZATION_USER) or (null === $role)) {
                // $user has Role::ROLE_ORGANIZATION_USER role in $organization or have not any role at all:
                $this->currentIsUserOrganizationAdmin = false;
            } else {
                throw new \LogicException(__METHOD__ . ': Unknown organization user role: "' . $role . '""!');
            }
        }

        return $this->currentIsUserOrganizationAdmin;
    }

    /**
     * @return array|null
     */
    public function getOwnGroupIds()
    {
        if ((null === $this->ownGroupIds) and !$this->isCurrentUserOrganizationAdmin()) {
            $currentUser = $this->currentUserService->getCurrentUser();
            $organization = $this->currentUserService->getCurrentOrganization();

            // Finding the groups which the current user has GROUP_ADMIN access to.
            /** @var DTOGroup[] $ownGroupDTOs */
            $ownGroupDTOs = $this->groupRepository->findByGroupAdmin($currentUser, $organization);
            foreach ($ownGroupDTOs as $ownGroupDTO) {
                $id = $ownGroupDTO->getId();
                $this->ownGroupIds[$id] = $id;
            }
        }

        return $this->ownGroupIds;
    }

    /**
     * @return array|null
     */
    public function getOwnGroupUserIds()
    {
        if ((null === $this->ownGroupUserIds) and !$this->isCurrentUserOrganizationAdmin()) {
            $ownGroupIds = $this->getOwnGroupIds();
            $this->ownGroupUserIds = $this->groupUserPermissionRepository->getUserIdsByGroups($ownGroupIds);
        }

        return $this->ownGroupUserIds;
    }

    /**
     * @return int|null
     */
    public function getOwnGroupPermissionsQuantity()
    {
        if (null === $this->ownGroupPermissionsQuantity) {
            if ($this->isCurrentUserOrganizationAdmin()) {
                $this->ownGroupPermissionsQuantity = $this->getTotalGroupPermissionsQuantity();
            } else {
                $this->ownGroupPermissionsQuantity = $this->topicGroupPermissionManager->getGroupQuantity(
                    $this->getTopic(),
                    $this->getOwnGroupIds()
                );
            }
        }

        return $this->ownGroupPermissionsQuantity;
    }

    /**
     * @return int|null
     */
    public function getTotalGroupPermissionsQuantity()
    {
        if (null === $this->totalGroupPermissionsQuantity) {
            $this->totalGroupPermissionsQuantity = $this->topicGroupPermissionManager->getGroupQuantity(
                $this->getTopic()
            );
        }

        return $this->totalGroupPermissionsQuantity;
    }

    /**
     * @return int|null
     */
    public function getOwnModeratorsQuantity()
    {
        if ((null === $this->ownModeratorsQuantity) and !$this->isCurrentUserOrganizationAdmin()) {
            $this->ownModeratorsQuantity = $this->topicModeratorManager->getQuantity(
                $this->getTopic(),
                $this->getOwnGroupUserIds()
            );
        }

        return $this->ownModeratorsQuantity;
    }

    /**
     * @return int|null
     */
    public function getOwnParticipantsQuantity()
    {
        if ((null === $this->ownParticipantsQuantity) and !$this->isCurrentUserOrganizationAdmin()) {
            $this->ownParticipantsQuantity =
                $this->getOwnModeratorsQuantity() + $this->getOwnGroupPermissionsQuantity();
        }

        return $this->ownParticipantsQuantity;
    }

    /**
     * @param integer $groupId
     *
     * @return bool
     */
    public function isOwnGroupId($groupId)
    {
        if ($this->isCurrentUserOrganizationAdmin()) {
            return true;
        } else {
            $ownGroupIds = $this->getOwnGroupIds();

            return array_key_exists($groupId, $ownGroupIds);
        }
    }

    /**
     * @param integer $userId
     *
     * @return bool
     */
    public function isOwnGroupUserId($userId)
    {
        if ($this->isCurrentUserOrganizationAdmin()) {
            return true;
        } else {
            $ownGroupUserIds = $this->getOwnGroupUserIds();

            return array_key_exists($userId, $ownGroupUserIds);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function getFilterParameters()
    {
        if (null === $this->filterParametersCache) {
            $this->filterParametersCache = parent::getFilterParameters();
        }

        return $this->filterParametersCache;
    }

    /**
     * {@inheritdoc}
     */
    protected function configureRoutes(RouteCollection $collection)
    {
        // The topic permission cannot be edited! It can be created or deleted only.
        $collection
            ->clearExcept(['list', 'create', 'batch'])
            ->add('exclude', 'exclude/{topicId}/{type}/{id}')
            ->add('update-moderators')
            ->add('group-members', 'group-members/{groupId}');
    }

    /**
     * @inheritdoc
     */
    public function getBatchActions()
    {
        $actions = parent::getBatchActions();

        if ($this->isCurrentUserOrganizationAdmin() or $this->getOwnParticipantsQuantity() > 0) {
            if ($this->hasRoute('exclude') && $this->isGranted('DELETE')) {
                $actions['exclude'] = [
                    'label' => $this->trans('topic_permission.action.exclude'),
                    'translation_domain' => 'AppBundle',
                    'ask_confirmation' => true
                ];
            }
        }

        return $actions;
    }

    /**
     * {@inheritdoc}
     */
    public function createQuery($context = 'list')
    {
        if ($context === 'list') {
            $topic = $this->getTopic();
            $organization = $this->currentUserService->getCurrentOrganization();

            $qb = new TopicPermissionMutexProxyQuery(
                $this->topicPermissionMutexRepository->createAdminQueryBuilder($organization, $topic),
                ['login', 'name', 'type']
            );

            $filterParameters = $this->getFilterParameters();
            if (isset($filterParameters['_sort_by'])) {
                $this->topicPermissionMutexRepository->correctAdminQueryBuilderSorting(
                    $qb,
                    $filterParameters['_sort_by'],
                    $filterParameters['_sort_order']
                );
            }

            return $qb;
        } else {
            return parent::createQuery($context);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function generateUrl($name, array $parameters = array(), $absolute = UrlGeneratorInterface::ABSOLUTE_PATH)
    {
        if ((($name == 'list') or ($name == 'batch') or ($name == 'create') or ($name == 'update-moderators')) and
            !array_key_exists('topicId', $parameters)
        ) {
            $parameters['topicId'] = $this->getTopic()->getId();
        }

        return parent::generateUrl($name, $parameters, $absolute);
    }

    /**
     * {@inheritdoc}
     *
     * @throws \Exception
     * @throws \InvalidArgumentException
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        $subject = $this->getSubject();
        $creating = !is_object($subject);
        if (!$creating) {
            $creating = null === $subject->getId();
        }
        if (!$creating) {
            throw new \Exception(__METHOD__ . ': The topic permission cannot be edited!');
        }

        $allPossibleModeratorChoices = null;
        list($groupChoices, $moderatorChoices) = $this->topicPermissionMutexManager->getParticipantChoices(
            $this->getTopic(),
            [],
            $allPossibleModeratorChoices
        );
        $moderatorChoices = array_keys($moderatorChoices);

        $formMapper
            ->with($this->trans('topic_permission.label.participants'))
            ->add(
                'groups',
                'choice',
                [
                    'choices' => $groupChoices,
                    'required' => false,
                    'multiple' => true,
                    'mapped' => false,
                    'data' => [],
                    'attr' => ['placeholder' => $this->trans('topic_permission.admin.groups_placeholder')],
                    'label' => 'topic_permission.label.groups'
                ]
            )
            ->add(
                'moderators',
                'choice',
                [
                    'choices' => $allPossibleModeratorChoices,
                    'required' => false,
                    'multiple' => true,
                    'mapped' => false,
                    'data' => $moderatorChoices,
                    'attr' => ['placeholder' => $this->trans('topic_permission.admin.moderators_placeholder')],
                    'label' => 'topic_permission.label.moderators'
                ]
            )
            ->end();
    }

    /**
     * {@inheritdoc}
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->add(
                'type',
                null,
                [
                    'label' => 'topic_permission.label.type',
                    'template' => '::Admin/TopicPermission/list__type.html.twig',
                    'sortable' => true,
                    'sort_field_mapping' => ['fieldName' => 'type'],
                    'sort_parent_association_mappings' => []
                ]
            )
            ->add(
                'login',
                null,
                [
                    'label' => 'topic_permission.label.login',
                    'sortable' => true,
                    'sort_field_mapping' => ['fieldName' => 'login'],
                    'sort_parent_association_mappings' => []
                ]
            )
            ->add(
                'name',
                null,
                [
                    'label' => 'topic_permission.label.name',
                    'template' => '::Admin/TopicPermission/list__name.html.twig',
                    'sortable' => true,
                    'sort_field_mapping' => ['fieldName' => 'name'],
                    'sort_parent_association_mappings' => []
                ]
            );

        if ($this->isCurrentUserOrganizationAdmin() or $this->getOwnParticipantsQuantity() > 0) {
            $listMapper
                ->add(
                    '_action',
                    'actions',
                    [
                        'actions' => [
                            'exclude' => ['template' => '::Admin/TopicPermission/list__action_exclude.html.twig']
                        ],
                        'label' => 'topic_permission.label.actions',
                    ]
                );
        }

    }

    /**
     * {@inheritdoc}
     */
    public function create($object)
    {
        $topic = $this->getTopic();

        /** @var array $groupIds */
        $groupIds = $this->getForm()->get('groups')->getData();
        if (!empty($groupIds)) {
            $this->topicGroupPermissionManager->add($topic, $groupIds);
        }

        /** @var array $moderatorIds */
        $moderatorIds = $this->getForm()->get('moderators')->getData();
        if (!empty($moderatorIds)) {
            $this->topicModeratorManager->add($topic, $moderatorIds);
        }

        //  This is original return value.
        //      return $object;
        //  The null value must be returned because the $object entity was not persisted to the database.
        //  See how this return value is used in CRUDController::createAction().
        return null;
    }

    /**
     * {@inheritdoc}
     */
    public function getUrlsafeIdentifier($entity)
    {
        if ($entity instanceof DTOTopicPermission) {
            /** @var DTOTopicPermission $entity */
            return $entity->getTopicGroupPermissionId() . ModelManager::ID_SEPARATOR . $entity->getTopicModeratorId();
        } else {
            return parent::getUrlsafeIdentifier($entity);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function getNormalizedIdentifier($entity)
    {
        if ($entity instanceof DTOTopicPermission) {
            /** @var DTOTopicPermission $entity */
            return $entity->getTopicGroupPermissionId() . ModelManager::ID_SEPARATOR . $entity->getTopicModeratorId();
        } else {
            return parent::getNormalizedIdentifier($entity);
        }
    }

    /**
     * @return Topic|null
     * @throws \InvalidArgumentException
     */
    public function getTopic()
    {
        if (null === $this->topic) {
            $topicId = $this->getRequest()->query->get('topicId');
            if (null !== $topicId) {
                $this->topic = $this->topicRepository->find($topicId);
            }
        }
        if (null === $this->topic) {
            throw new \InvalidArgumentException(
                __METHOD__ . ': The topicId GET parameter value is invalid or not specified!'
            );
        }

        return $this->topic;
    }

    /**
     * @inheritdoc
     */
    public function buildBreadcrumbs($action, MenuItemInterface $menu = null)
    {
        if (isset($this->breadcrumbs[$action])) {
            return $this->breadcrumbs[$action];
        }

        if ('list' == $action) {
            $menu = $this->topicAdmin->buildBreadcrumbs($action, $menu);
            $topic = $this->getTopic();
            $menu = $menu->addChild(
                $topic->getName(),
                [
                    'uri' => $this->topicAdmin->hasRoute('edit') &&
                    $this->topicAdmin->isGranted('EDIT') ?
                        $this->topicAdmin->generateUrl('edit', ['id' => $topic->getId()]) : null
                ]
            );
            $menu = $menu->addChild(
                $this->trans(
                    $this->getLabelTranslatorStrategy()->getLabel(
                        sprintf('%s_list', $this->getClassnameLabel()),
                        'breadcrumb',
                        'link'
                    )
                )
            );

            return $this->breadcrumbs[$action] = $menu;
        } else {
            return parent::buildBreadcrumbs($action, $menu);
        }
    }

    /**
     * @inheritdoc
     */
    protected function configureTabMenu(MenuItemInterface $menu, $action, AdminInterface $childAdmin = null)
    {
        $topic = $this->getTopic();
        if ((null === $childAdmin) and ('list' == $action)) {
            $childMenu = $menu->addChild(
                'topic_permission.label.add',
                ['uri' => $this->generateUrl('create', ['topicId' => $topic->getId()])]
            );
            $childMenu->setLinkAttribute('style', 'font-weight: bold; color: green;');
        } else {
            parent::configureTabMenu($menu, $action, $childAdmin);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function validate(ErrorElement $errorElement, $object)
    {
        /** @var array $groupIds */
        $groupIds = $this->getForm()->get('groups')->getData();
        /** @var array $moderatorIds */
        $moderatorIds = $this->getForm()->get('moderators')->getData();

        if (empty($groupIds) and empty($moderatorIds)) {
            $errorElement
                ->addViolation(
                    $this->trans('topic_permission.validator')
                );
        }
    }

    /**
     * @param CurrentUserService $currentUserService
     *
     * @return TopicPermissionAdmin
     */
    public function setCurrentUserService(CurrentUserService $currentUserService)
    {
        $this->currentUserService = $currentUserService;

        return $this;
    }

    /**
     * @param TopicPermissionMutexRepository $topicPermissionMutexRepository
     *
     * @return TopicPermissionAdmin
     */
    public function setTopicPermissionMutexRepository($topicPermissionMutexRepository)
    {
        $this->topicPermissionMutexRepository = $topicPermissionMutexRepository;

        return $this;
    }

    /**
     * @param TopicRepository $topicRepository
     *
     * @return TopicPermissionAdmin
     */
    public function setTopicRepository($topicRepository)
    {
        $this->topicRepository = $topicRepository;

        return $this;
    }

    /**
     * @param TopicAdmin $topicAdmin
     *
     * @return TopicPermissionAdmin
     */
    public function setTopicAdmin($topicAdmin)
    {
        $this->topicAdmin = $topicAdmin;

        return $this;
    }

    /**
     * @param TopicGroupPermissionManager $topicGroupPermissionManager
     *
     * @return TopicPermissionAdmin
     */
    public function setTopicGroupPermissionManager($topicGroupPermissionManager)
    {
        $this->topicGroupPermissionManager = $topicGroupPermissionManager;

        return $this;
    }

    /**
     * @param TopicModeratorManager $topicModeratorManager
     *
     * @return TopicPermissionAdmin
     */
    public function setTopicModeratorManager($topicModeratorManager)
    {
        $this->topicModeratorManager = $topicModeratorManager;

        return $this;
    }

    /**
     * @param TopicPermissionMutexManager $topicPermissionMutexManager
     *
     * @return TopicPermissionAdmin
     */
    public function setTopicPermissionMutexManager($topicPermissionMutexManager)
    {
        $this->topicPermissionMutexManager = $topicPermissionMutexManager;

        return $this;
    }

    /**
     * @param GroupRepository $groupRepository
     *
     * @return TopicPermissionAdmin
     */
    public function setGroupRepository($groupRepository)
    {
        $this->groupRepository = $groupRepository;

        return $this;
    }

    /**
     * @param GroupUserPermissionRepository $groupUserPermissionRepository
     *
     * @return TopicPermissionAdmin
     */
    public function setGroupUserPermissionRepository($groupUserPermissionRepository)
    {
        $this->groupUserPermissionRepository = $groupUserPermissionRepository;

        return $this;
    }
}
