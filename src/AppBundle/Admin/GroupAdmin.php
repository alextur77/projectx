<?php

namespace AppBundle\Admin;

use AppBundle\Repository\OrganizationRepository;
use Component\Doctrine\ORM\SortFixProxyQuery;
use AppBundle\DataTransferObject\DTOGroup;
use AppBundle\Entity\Group;
use AppBundle\Entity\Role;
use AppBundle\Manager\GroupManager;
use AppBundle\Repository\GroupRepository;
use AppBundle\Repository\GroupClosureRepository;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Route\RouteCollection;
use Sonata\AdminBundle\Admin\AdminInterface;
use Knp\Menu\ItemInterface as MenuItemInterface;

/**
 * Class GroupAdmin
 * @package AppBundle\Admin
 */
class GroupAdmin extends BaseGroupAdmin
{
    const DELETE_SUB_TREE = '_delete_sub_tree';

    private $filterParametersCache = null;

    /**
     * @var RegistrationInviteAdmin
     */
    protected $registrationInviteAdmin;

    /**
     * @var GroupRepository
     */
    protected $groupRepository;

    /**
     * @var OrganizationRepository
     */
    protected $organizationRepository;

    /**
     * @var GroupClosureRepository
     */
    protected $groupClosureRepository;

    /**
     * @var GroupManager
     */
    protected $groupManager;

    /**
     * @var UserTopicAdmin
     */
    protected $userTopicAdmin;

    /**
     * {@inheritdoc}
     */
    protected $datagridValues = array(
        '_page' => 1,
        '_per_page' => 32,
        '_sort_order' => 'ASC',
        '_sort_by' => 'name'
    );

    /**
     * {@inheritdoc}
     */
    public function getFilterParameters()
    {
        if (null === $this->filterParametersCache) {
            $role = $this->currentUserService->getCurrentUserRoleInGroup();
            if ($role === Role::ROLE_GROUP_ADMIN) {
                $this->datagridValues = array_merge(['active' => ['value' => 1]], $this->datagridValues);
            }
            $this->filterParametersCache = parent::getFilterParameters();
        }

        return $this->filterParametersCache;
    }

    /**
     * {@inheritdoc}
     */
    public function createQuery($context = 'list')
    {
        if ($context === 'list') {
            $user = $this->getCurrentUser();

            $organization = $this->currentUserService->getCurrentOrganization();

            $role = $this->currentUserService->getCurrentUserRoleInGroup();

            $roleOrganization = $this->currentUserService->getCurrentUserRoleInOrganization();

            if ($roleOrganization == Role::ROLE_ORGANIZATION_ADMIN) {
                $qb = new SortFixProxyQuery(
                    $this->groupRepository
                        ->createOrganizationAdminQueryBuilder($organization), ['name']
                );

            } elseif ($role === Role::ROLE_GROUP_ADMIN) {
                $qb = new SortFixProxyQuery(
                    $this->groupRepository
                        ->createGroupAdminIDQueryBuilder($organization, $user->getId()), ['name']
                );

            } elseif (($role === Role::ROLE_GROUP_USER) or (null === $role) or $roleOrganization === Role::ROLE_ORGANIZATION_USER) {

                // $user has Role::ROLE_GROUP_USER role in $group or have not any role at all:
                $qb = new SortFixProxyQuery(
                    $this->groupRepository
                        ->createGroupAdminQueryBuilder($user, $organization), ['name']
                );

            } else {
                throw new \LogicException(__METHOD__ . ': Unknown organization user role: "' . $role . '""!');
            }
            $this->groupRepository->correctTreeQueryBuilderForUserCount($qb);

            $filterParameters = $this->getFilterParameters();
            if (isset($filterParameters['_sort_by'])) {
                $this->groupRepository->correctAdminQueryBuilderSorting(
                    $qb,
                    $filterParameters['_sort_by'],
                    $filterParameters['_sort_order']
                );
            }

            return $qb;
        } else {
            return parent::createQuery($context);
        }
    }

    /**
     * {@inheritdoc}
     */
    protected function configureRoutes(RouteCollection $collection)
    {
        $collection
            ->remove('export')
            ->remove('show')
            ->add('change-active', $this->getRouterIdParameter() . '/change-active')
            ->add('change-public', $this->getRouterIdParameter() . '/change-public');
    }

    /**
     * {@inheritdoc}
     */
    protected function configureDatagridFilters(DatagridMapper $filter)
    {
        $filter
            ->add('name')
            ->add('public');
        $role = $this->currentUserService->getCurrentUserRoleInGroup();
        $roleOrganization = $this->currentUserService->getCurrentUserRoleInOrganization();
        if (!$role === Role::ROLE_GROUP_ADMIN or !$roleOrganization === Role::ROLE_ORGANIZATION_ADMIN) {
            $filter->add('active');
        }
    }

    /**
     * {@inheritdoc}
     */
    protected function configureListFields(ListMapper $list)
    {
        $role = $this->currentUserService->getCurrentUserRoleInGroup();
        $roleOrganization = $this->currentUserService->getCurrentUserRoleInOrganization();
        $actions = [];
        if ($role === Role::ROLE_GROUP_ADMIN or $roleOrganization === Role::ROLE_ORGANIZATION_ADMIN) {
            $list->addIdentifier(
                'name',
                null,
                [
                    'label' => 'group.label.name',
                    'template' => '::Admin/Group/list__name.html.twig'
                ]
            );
        } else {
            $list->add(
                'name',
                null,
                [
                    'label' => 'group.label.name',
                    'template' => '::Admin/Group/list__name.html.twig'
                ]
            );
        }
        $list->add(
            'public',
            null,
            [
                'editable' => true,
                'row_align' => 'center',
                'label' => 'group.label.public',
                'template' => '::Admin/Group/list__public.html.twig'
            ]
        );
        $actions['topics'] = ['template' => '::Admin/Group/list__action_topics.html.twig'];
        $actions['users'] = ['template' => '::Admin/Group/list__action_users.html.twig'];
        if ($role === Role::ROLE_GROUP_ADMIN or $roleOrganization === Role::ROLE_ORGANIZATION_ADMIN) {
            $list->add(
                'active',
                null,
                [
                    'editable' => true,
                    'row_align' => 'center',
                    'label' => 'group.label.active',
                    'template' => '::Admin/Group/list__active.html.twig'
                ]
            );
            $actions['delete'] = ['template' => '::Admin/Group/list__action_delete.html.twig'];
        }
        $list->add('_action', 'actions', ['actions' => $actions, 'label' => 'group.label.group_actions']);
    }

    /**
     * {@inheritdoc}
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        $role = $this->currentUserService->getCurrentUserRoleInGroup();
        $roleOrganization = $this->currentUserService->getCurrentUserRoleInOrganization();
        if ($role === Role::ROLE_GROUP_ADMIN or $roleOrganization === Role::ROLE_ORGANIZATION_ADMIN) {
            $formMapper
                ->add('name', null, ['required' => true]);

            // Warning! Function $this->getSubject() may return boolean value in case of editing
            // x-editable boolean field right directly from the list view.
            $currentGroup = $this->getSubject();
            if ($currentGroup instanceof Group) {
                $parentGroupId = null;
                if (null !== $currentGroup->getId()) {
                    $parentGroupId = $this->groupClosureRepository->findParentId($currentGroup);
                }
                $organization = $this->currentUserService->getCurrentOrganization();
                $groups = $this->groupRepository->findByOrganization($organization);
                $groupChoices = [];
                /** @var DTOGroup $group */
                foreach ($groups as $group) {
                    $needAdd = $currentGroup->getId() != $group->id;
                    if ($needAdd) {
                        // Even if the parent group is non-active
                        // it must be presented in the parent group list or the group being edited.
                        $needAdd = $group->active;
                        if (!$needAdd) {
                            $needAdd = $group->id == $parentGroupId;
                        }
                    }
                    if ($needAdd) {
                        $groupChoices[$group->id] = $group->getShift() . $group->name;
                    }
                }

                $formMapper
                    ->add(
                        'initialParentGroup',
                        'hidden',
                        ['mapped' => false, 'data' => $parentGroupId]
                    )
                    ->add(
                        'editableParentGroup',
                        'choice',
                        [
                            'choices' => $groupChoices,
                            'required' => false,
                            'mapped' => false,
                            'data' => $parentGroupId,
                            'empty_data' => null,
                            'empty_value' => 'group.label.no_parent_group',
                            'label' => 'group.label.parent_group'
                        ]
                    );
            }

            $formMapper
                ->add('public', 'sonata_type_boolean', ['transform' => true])
                ->add('active', 'sonata_type_boolean', ['transform' => true]);
        } else {
            parent::configureFormFields($formMapper);
        }
    }

    /**
     * @inheritdoc
     *
     * @return array
     */
    public function getBatchActions()
    {
        $actions = parent::getBatchActions();
        if ($this->hasRoute('delete') && $this->isGranted('DELETE')) {
            $actions['delete_subtree'] = [
                'label' => $this->trans('group.action.delete_subtree'),
                'translation_domain' => 'AppBundle',
                'ask_confirmation' => true
            ];
        }

        if ($this->isGranted('LIST')) {
            $actions['send_invite'] = [
                'label' => $this->trans('group.action.send_invite'),
                'translation_domain' => 'AppBundle',
                'ask_confirmation' => false
            ];

            $actions['show_topics'] = [
                'label' => $this->trans('group.action.show_topics'),
                'translation_domain' => 'AppBundle',
                'ask_confirmation' => false
            ];
        }

        return $actions;
    }

    /**
     * {@inheritdoc}
     *
     * @param Group $object
     */
    public function create($object)
    {
        $this->prePersist($object);
        foreach ($this->extensions as $extension) {
            $extension->prePersist($this, $object);
        }

        /** @var integer $parentGroupId */
        $parentGroupId = $this->getForm()->get('editableParentGroup')->getData();
        $object->setOrganization($this->currentUserService->getCurrentOrganization());
        $this->groupManager->addNew($object, $parentGroupId);

        $this->postPersist($object);
        foreach ($this->extensions as $extension) {
            $extension->postPersist($this, $object);
        }

        $this->createObjectSecurity($object);

        return $object;
    }

    /**
     * {@inheritdoc}
     *
     * @param Group $object
     */
    public function update($object)
    {
        $this->preUpdate($object);
        foreach ($this->extensions as $extension) {
            $extension->preUpdate($this, $object);
        }

        $doStandardUpdate = !$this->getForm()->has('initialParentGroup');
        if (!$doStandardUpdate) {
            /** @var integer $initialParentGroupId */
            $initialParentGroupId = $this->getForm()->get('initialParentGroup')->getData();
            /** @var integer $editableParentGroupId */
            $editableParentGroupId = $this->getForm()->get('editableParentGroup')->getData();

            $doStandardUpdate = $initialParentGroupId === $editableParentGroupId;
            if (!$doStandardUpdate) {
                $this->groupManager->update($object, $editableParentGroupId);
            }
        }

        if ($doStandardUpdate) {
            $result = $this->getModelManager()->update($object);
            // BC compatibility
            if (null !== $result) {
                $object = $result;
            }
        }

        $this->postUpdate($object);
        foreach ($this->extensions as $extension) {
            $extension->postUpdate($this, $object);
        }

        return $object;
    }

    /**
     * {@inheritdoc}
     *
     * @param Group $object
     */
    public function delete($object)
    {
        $this->preRemove($object);
        foreach ($this->extensions as $extension) {
            $extension->preRemove($this, $object);
        }

        $this->getSecurityHandler()->deleteObjectSecurity($this, $object);
        if ($this->getRequest()->request->get($this::DELETE_SUB_TREE)) {
            $this->groupManager->deleteSubtree($object);
        } else {
            $this->groupManager->delete($object);
        }

        $this->postRemove($object);
        foreach ($this->extensions as $extension) {
            $extension->postRemove($this, $object);
        }
    }

    /**
     * @inheritdoc
     */
    protected function configureTabMenu(MenuItemInterface $menu, $action, AdminInterface $childAdmin = null)
    {
        if (!$childAdmin or ('list' != $action)) {
            return;
        }
        $admin = $this->isChild() ? $this->getParent() : $this;
        $request = $admin->getRequest();
        $id = $request->get('id');
        $childMenu = $menu->addChild(
            $admin->trans('group.action.send_invite'),
            ['uri' => $this->registrationInviteAdmin->generateUrl('create', ['groups' => $id])]
        );
        $childMenu->setLinkAttribute('style', 'font-weight: bold; color: green;');

        $childMenu = $menu->addChild(
            $admin->trans('group.action.show_topics'),
            ['uri' => $this->userTopicAdmin->generateListURLFilteredByGroups($request, $id)]
        );
        $childMenu->setLinkAttribute('style', 'font-weight: bold; color: green;');
    }

    /**
     * @param RegistrationInviteAdmin $registrationInviteAdmin
     *
     * @return GroupAdmin
     */
    public function setRegistrationInviteAdmin($registrationInviteAdmin)
    {
        $this->registrationInviteAdmin = $registrationInviteAdmin;

        return $this;
    }

    /**
     * @param GroupRepository $groupRepository
     *
     * @return GroupAdmin
     */
    public function setGroupRepository($groupRepository)
    {
        $this->groupRepository = $groupRepository;

        return $this;
    }

    /**
     * @param OrganizationRepository $organizationRepository
     *
     * @return GroupAdmin
     */
    public function setOrganizationRepository($organizationRepository)
    {
        $this->organizationRepository = $organizationRepository;

        return $this;
    }

    /**
     * @param mixed $groupManager
     *
     * @return GroupAdmin
     */
    public function setGroupManager($groupManager)
    {
        $this->groupManager = $groupManager;

        return $this;
    }

    /**
     * @param GroupClosureRepository $groupClosureRepository
     *
     * @return GroupAdmin
     */
    public function setGroupClosureRepository($groupClosureRepository)
    {
        $this->groupClosureRepository = $groupClosureRepository;

        return $this;
    }

    /**
     * This getter is used in the action template
     *
     * @return UserTopicAdmin
     */
    public function getUserTopicAdmin()
    {
        return $this->userTopicAdmin;
    }

    /**
     * @param UserTopicAdmin $userTopicAdmin
     *
     * @return GroupAdmin
     */
    public function setUserTopicAdmin($userTopicAdmin)
    {
        $this->userTopicAdmin = $userTopicAdmin;

        return $this;
    }
}