<?php

namespace AppBundle\Admin;

use Aristek\Bundle\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;
use AppBundle\Service\SendOrganizationInviteService;

/**
 * Class OrganizationCreationInviteAdmin
 */
class OrganizationCreationInviteAdmin extends Admin
{
    /**
     * @var string
     */
    protected $baseRouteName = 'org_invite';

    /**
     * @var string
     */
    protected $baseRoutePattern = 'org_invite';

    /**
     * @var SendOrganizationInviteService
     */
    protected $sendOrganizationInviteService;

    /**
     * @param mixed $object
     *
     * @return void
     */
    public function postPersist($object)
    {
        $this->sendOrganizationInviteService->sendEmailInviteAction($object->getEmail(), $object->getUrl());
    }

    /**
     * {@inheritdoc}
     */
    protected function configureListFields(ListMapper $list)
    {
        $list
            ->addIdentifier('email')
            ->add('urlInvite')
            ->add('statusName');
    }

    /**
     * {@inheritdoc}
     */
    protected function configureFormFields(FormMapper $form)
    {
        $form
            ->with('General')
            ->add('email')
            ->add('urlInvite', 'hidden')
            ->add('was_send', 'hidden')
            ->add('urlHash', 'hidden')
            ->end();
    }

    /**
     * @param SendOrganizationInviteService $sendOrganizationInviteService
     *
     * @return $this
     */
    public function setSendOrganizationInviteService($sendOrganizationInviteService)
    {
        $this->sendOrganizationInviteService = $sendOrganizationInviteService;

        return $this;
    }
}
