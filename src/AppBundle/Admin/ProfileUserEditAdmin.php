<?php

namespace AppBundle\Admin;

use Aristek\Bundle\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;
use FOS\UserBundle\Model\UserManager;
use FOS\UserBundle\Model\UserInterface;

/**
 * Class ProfileUserEditAdmin
 */
class ProfileUserEditAdmin extends Admin
{
    /**
     * The base route name used to generate the routing information.
     *
     * @var string
     */
    protected $baseRouteName = 'profile';

    /**
     * The base route pattern used to generate the routing information.
     *
     * @var string
     */
    protected $baseRoutePattern = 'profile';

    /**
     * @var UserManager
     */
    protected $userManager;

    /**
     * @var UserInterface
     */
    protected $user;

    /**
     * {@inheritdoc}
     */
    protected function configureShowFields(ShowMapper $showMapper)
    {
        $showMapper
            ->with('general')
            ->add('username')
            ->add('email')
            ->add('profilePictureFile')
            ->end()
            ->with('additional')
            ->add('firstName')
            ->add('lastName')
            ->add('patronymic')
            ->add('sendEmail')
            ->add('sendSms')
            ->add('phone')
            ->end();
    }

    /**
     * {@inheritdoc}
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        // define group zoning
        $formMapper
            ->tab('user')
            ->with('mainInfo', ['class' => 'col-md-6'])->end()
            ->with('additionalInfo', ['class' => 'col-md-6'])->end()
            ->end();

        $formMapper
            ->tab('user')
            ->with('mainInfo')
            ->add('username', null, ['disabled' => true])
            ->add('email')
            ->add(
                'plainPassword',
                'repeated',
                [
                    'type' => 'password',
                    'options' => ['translation_domain' => 'FOSUserBundle'],
                    'first_options' => ['label' => 'form.password'],
                    'second_options' => ['label' => 'form.password_confirmation'],
                    'invalid_message' => 'fos_user.password.mismatch',
                    'required' => false
                ]
            )
            ->add('profilePictureFile', 'file', ['required' => false])
            ->end()
            ->with('additionalInfo')
            ->add('firstName', null, ['required' => false])
            ->add('lastName', null, ['required' => false])
            ->add('patronymic', null, ['required' => false])
            ->add('phone', null, ['required' => false, 'label' => 'phone'])
            ->add(
                'sendEmail',
                null,
                [
                    'label_attr' => [
                        'style' => 'width: 260px'
                    ],
                    'attr' => [
                        'style' => 'width: 10px'
                    ]
                ]
            )
            ->add(
                'sendSms',
                null,
                [
                    'label_attr' => [
                        'style' => 'width: 260px'
                    ],
                    'attr' => [
                        'style' => 'width: 10px'
                    ]
                ]
            )
            ->end()
            ->end();
    }

    /**
     * @param UserManager $userManager
     *
     * @return $this
     */
    public function setUserManager($userManager)
    {
        $this->userManager = $userManager;

        return $this;
    }

    /**
     * @param mixed $user
     *
     * @return void
     */
    public function preUpdate($user)
    {
        $this->userManager->updatePassword($user);
    }
}
