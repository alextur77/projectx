<?php

namespace AppBundle\Admin;

use Aristek\Bundle\AdminBundle\Admin\Admin;
use AppBundle\DataTransferObject\DTOGroup;
use AppBundle\Entity\Role;
use AppBundle\Entity\User;
use AppBundle\Manager\TopicGroupPermissionManager;
use AppBundle\Service\User\CurrentUserService;

/**
 * Class BaseGroupAdmin
 * @package AppBundle\Admin
 *
 * @method User getCurrentUser()
 * @method DTOGroup getSubject()
 */
class BaseGroupAdmin extends Admin
{
    /**
     * @var CurrentUserService
     */
    protected $currentUserService;

    /**
     * @var TopicGroupPermissionManager
     */
    protected $topicGroupPermissionManager;

    /**
     * @var null|integer[]
     */
    private $datagridResultIds = null;

    /**
     * @var null|integer[]
     */
    private $groupTopicQuantityCache = null;

    /**
     * @return integer[]
     */
    public function getDatagridResultIds()
    {
        if (null === $this->datagridResultIds) {
            /** @var DTOGroup[] $results */
            $results = $this->datagrid->getResults();
            $this->datagridResultIds = [];
            foreach ($results as $group) {
                $this->datagridResultIds[] = $group->getId();
            }
        }

        return $this->datagridResultIds;
    }

    /**
     * @param integer $groupId
     *
     * @return integer
     */
    public function getGroupTopicQuantity($groupId)
    {
        if (null === $this->groupTopicQuantityCache) {
            $user = $this->currentUserService->getCurrentUser();
            $role = $this->currentUserService->getCurrentUserRoleInOrganization();
            if ($role === Role::ROLE_ORGANIZATION_ADMIN) {
                $this->groupTopicQuantityCache = $this->topicGroupPermissionManager->getAllTopicQuantity(
                    $this->getDatagridResultIds()
                );
            } else {
                $this->groupTopicQuantityCache = $this->topicGroupPermissionManager->getTopicQuantityForUser(
                    $this->getDatagridResultIds(),
                    $user
                );
            }
        }

        if (key_exists($groupId, $this->groupTopicQuantityCache)) {
            return $this->groupTopicQuantityCache[$groupId];
        } else {
            return 0;
        }
    }

    /**
     * {@inheritdoc}
     */
    public function getUrlsafeIdentifier($entity)
    {
        return $entity instanceof DTOGroup ?
            $entity->id :
            parent::getUrlsafeIdentifier($entity);
    }

    /**
     * {@inheritdoc}
     */
    public function getNormalizedIdentifier($entity)
    {
        return $entity instanceof DTOGroup ?
            $entity->id :
            parent::getNormalizedIdentifier($entity);
    }

    /**
     * @param TopicGroupPermissionManager $topicGroupPermissionManager
     *
     * @return BaseGroupAdmin
     */
    public function setTopicGroupPermissionManager($topicGroupPermissionManager)
    {
        $this->topicGroupPermissionManager = $topicGroupPermissionManager;

        return $this;
    }

    /**
     * @param mixed $currentUserService
     *
     * @return BaseGroupAdmin
     */
    public function setCurrentUserService($currentUserService)
    {
        $this->currentUserService = $currentUserService;

        return $this;
    }
}
