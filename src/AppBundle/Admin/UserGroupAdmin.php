<?php

namespace AppBundle\Admin;

use Component\Doctrine\ORM\SortFixProxyQuery;
use AppBundle\Repository\GroupRepository;
use AppBundle\Manager\GroupManager;
use Doctrine\ORM\QueryBuilder;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Route\RouteCollection;
use Sonata\AdminBundle\Admin\AdminInterface;
use Knp\Menu\ItemInterface as MenuItemInterface;

/**
 * Class UserGroupAdmin
 * @package AppBundle\Admin
 */
class UserGroupAdmin extends BaseGroupAdmin
{
    private $filterParametersCache = null;

    /**
     * {@inheritdoc}
     */
    protected $baseRouteName = 'user_groups';

    /**
     * {@inheritdoc}
     */
    protected $baseRoutePattern = 'user-groups';

    /**
     * @var null|array
     */
    private $userGroupAdditionalData = null;

    /**
     * @var FindGroupAdmin
     */
    protected $findGroupAdmin;

    /**
     * @var GroupRepository
     */
    protected $groupRepository;

    /**
     * @var GroupManager
     */
    protected $groupManager;

    /**
     * @var UserTopicAdmin
     */
    protected $userTopicAdmin;

    /**
     * {@inheritdoc}
     */
    protected $datagridValues = array(
        '_page' => 1,
        '_per_page' => 32,
        '_sort_order' => 'ASC',
        '_sort_by' => 'name'
    );

    /**
     * {@inheritdoc}
     */
    public function getFilterParameters()
    {
        if (null === $this->filterParametersCache) {
            $this->filterParametersCache = parent::getFilterParameters();
        }

        return $this->filterParametersCache;
    }

    /**
     * {@inheritdoc}
     */
    public function createQuery($context = 'list')
    {
        if ($context === 'list') {
            $user = $this->getCurrentUser();
            $organization = $this->currentUserService->getCurrentOrganization();
            /** @var QueryBuilder $qb */
            $qb = new SortFixProxyQuery(
                $this->groupRepository->createUserQueryBuilder($user, $organization),
                ['group_role']
            );

            $this->groupRepository->correctUserQueryBuilderForUserCount($qb);

            $filterParameters = $this->getFilterParameters();
            if (isset($filterParameters['_sort_by'])) {
                $this->groupRepository->correctUserQueryBuilderSorting(
                    $qb,
                    $filterParameters['_sort_by'],
                    $filterParameters['_sort_order']
                );
            }

            return $qb;
        } else {
            return parent::createQuery($context);
        }
    }

    /**
     * {@inheritdoc}
     */
    protected function configureRoutes(RouteCollection $collection)
    {
        $collection
            ->remove('export')
            ->remove('create')
            ->remove('edit')
            ->remove('show')
            ->remove('delete')
            ->add('quit', $this->getRouterIdParameter() . '/quit');
        // This line eliminates the access to the child admin:
        // ->clearExcept(['list', 'edit', 'create', 'delete', 'batch']);
    }

    /**
     * {@inheritdoc}
     */
    protected function configureDatagridFilters(DatagridMapper $filter)
    {
        $filter
            ->add('name')
            ->add('public');
    }

    /**
     * {@inheritdoc}
     */
    protected function configureListFields(ListMapper $list)
    {
        $list
            ->add('name', null, ['label' => 'group.label.name'])
            ->add('public', null, ['row_align' => 'center', 'label' => 'group.label.public'])
            ->add(
                'group_role',
                null,
                [
                    'label' => 'group.label.role',
                    'template' => '::Admin/Group/list__role.html.twig',
                    'sortable' => true,
                    'sort_field_mapping' => ['fieldName' => 'group_role'],
                    'sort_parent_association_mappings' => []
                ]
            )
            ->add(
                '_action',
                'actions',
                [
                    'actions' => [
                        'topics' => ['template' => '::Admin/Group/list__action_topics.html.twig'],
                        'users' => ['template' => '::Admin/Group/user_list__action_users.html.twig'],
                        'quit' => ['template' => '::Admin/Group/list__action_quit.html.twig']
                    ],
                    'label' => 'group.label.group_actions',
                ]
            );
    }

    /**
     * @inheritdoc
     */
    public function getBatchActions()
    {
        $actions = parent::getBatchActions();
        if ($this->isGranted('LIST')) {
            $actions['quit'] = [
                'label' => $this->trans('group.action.quit'),
                'translation_domain' => 'AppBundle',
                'ask_confirmation' => true
            ];

            $actions['show_topics'] = [
                'label' => $this->trans('group.action.show_topics'),
                'translation_domain' => 'AppBundle',
                'ask_confirmation' => false
            ];
        }

        return $actions;
    }

    /**
     * @return array
     */
    public function getUserGroupAdditionalData()
    {
        if (null === $this->userGroupAdditionalData) {
            $this->userGroupAdditionalData = $this->getDatagridResultIds();
            $user = $this->currentUserService->getCurrentUser();
            $this->groupManager->getUserGroupAdditionalData($user, $this->userGroupAdditionalData);
        }

        return $this->userGroupAdditionalData;
    }

    /**
     * @inheritdoc
     */
    protected function configureTabMenu(MenuItemInterface $menu, $action, AdminInterface $childAdmin = null)
    {
        if ((null !== $childAdmin) or ('list' != $action)) {
            return;
        }
        $admin = $this->isChild() ? $this->getParent() : $this;
        $childMenu = $menu->addChild(
            $admin->trans('group.action.search_group'),
            ['uri' => $this->findGroupAdmin->generateUrl('list')]
        );
        $childMenu->setLinkAttribute('style', 'font-weight: bold; color: green;');
    }

    /**
     * @return null|string
     */
    public function getCurrentUserRoleInOrganization()
    {
        return $this->currentUserService->getCurrentUserRoleInOrganization();
    }

    /**
     * @param GroupRepository $groupRepository
     *
     * @return UserGroupAdmin
     */
    public function setGroupRepository($groupRepository)
    {
        $this->groupRepository = $groupRepository;

        return $this;
    }

    /**
     * @param GroupManager $groupManager
     *
     * @return UserGroupAdmin
     */
    public function setGroupManager($groupManager)
    {
        $this->groupManager = $groupManager;

        return $this;
    }

    /**
     * @param FindGroupAdmin $findGroupAdmin
     *
     * @return UserGroupAdmin
     */
    public function setFindGroupAdmin($findGroupAdmin)
    {
        $this->findGroupAdmin = $findGroupAdmin;

        return $this;
    }

    /**
     * This getter is used in the action template
     *
     * @return mixed
     */
    public function getUserTopicAdmin()
    {
        return $this->userTopicAdmin;
    }

    /**
     * @param mixed $userTopicAdmin
     *
     * @return UserGroupAdmin
     */
    public function setUserTopicAdmin($userTopicAdmin)
    {
        $this->userTopicAdmin = $userTopicAdmin;

        return $this;
    }
}
