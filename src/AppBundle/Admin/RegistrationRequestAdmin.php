<?php

namespace AppBundle\Admin;

use Component\Doctrine\ORM\SortFixProxyQuery;
use Aristek\Bundle\AdminBundle\Admin\Admin;
use Aristek\Bundle\AdminBundle\Admin\DisplayFieldDescription;
use AppBundle\Entity\RegistrationRequest;
use AppBundle\Entity\Role;
use AppBundle\Entity\User;
use AppBundle\Repository\RegistrationRequestRepository;
use AppBundle\Service\DateService;
use AppBundle\Service\User\CurrentUserService;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Route\RouteCollection;
use Sonata\AdminBundle\Show\ShowMapper;

/**
 * Class RegistrationRequestAdmin
 * @package AppBundle\Admin
 *
 * @method User getCurrentUser()
 * @method RegistrationRequest getSubject()
 */
class RegistrationRequestAdmin extends Admin
{
    private $filterParametersCache = null;

    /**
     * @var DateService
     */
    protected $dateService;

    /**
     * @var RegistrationRequestRepository
     */
    protected $registrationRequestRepository;

    /**
     * @var CurrentUserService
     */
    protected $currentUserService;

    /**
     * {@inheritdoc}
     */
    protected $datagridValues = array(
        '_page' => 1,
        '_per_page' => 32,
        'status' => ['value' => RegistrationRequest::STATUS_NEW],
        '_sort_order' => 'DESC',
        '_sort_by' => 'createdAt'
    );

    /**
     * {@inheritdoc}
     */
    protected function configureRoutes(RouteCollection $collection)
    {
        $collection
            ->clearExcept(['list', 'show'])
            ->add('approve', $this->getRouterIdParameter() . '/approve')
            ->add('decline', $this->getRouterIdParameter() . '/decline', [], ['_method' => 'POST']);
    }

    /**
     * {@inheritdoc}
     */
    public function getFilterParameters()
    {
        if (null === $this->filterParametersCache) {
            $this->filterParametersCache = parent::getFilterParameters();
        }

        return $this->filterParametersCache;
    }

    /**
     * {@inheritdoc}
     */
    public function createQuery($context = 'list')
    {
        if ($context === 'list') {
            $user = $this->getCurrentUser();
            $organization = $this->currentUserService->getCurrentOrganization();
            $role = $this->currentUserService->getCurrentUserRoleInOrganization();

            if ($role === Role::ROLE_ORGANIZATION_ADMIN) {
                $qb = new SortFixProxyQuery(
                    $this->registrationRequestRepository->createOrganizationAdminQueryBuilder($user, $organization),
                    ['status', 'user.fullName']
                );
            } elseif (($role === Role::ROLE_ORGANIZATION_USER) or (null === $role)) {
                // $user has Role::ROLE_ORGANIZATION_USER role in $organization or have not any role at all:
                $qb = new SortFixProxyQuery(
                    $this->registrationRequestRepository->createGroupAdminQueryBuilder($user, $organization),
                    ['status', 'user.fullName']
                );
            } else {
                throw new \LogicException(__METHOD__ . ': Unknown organization user role: "' . $role . '""!');
            }

            $filterParameters = $this->getFilterParameters();
            if (isset($filterParameters['_sort_by'])) {
                $this->registrationRequestRepository->correctQueryBuilderSorting(
                    $qb,
                    $filterParameters['_sort_by'],
                    $filterParameters['_sort_order']
                );
            }

            return $qb;
        } else {
            return parent::createQuery($context);
        }
    }

    /**
     * {@inheritdoc}
     */
    protected function configureDatagridFilters(DatagridMapper $filter)
    {
        $filter
            ->add('status', null, [], 'choice', ['choices' => $this->getStatusesList()])
            ->add('group.name')
            ->add(
                'createdAt',
                'doctrine_orm_date_range',
                [],
                null,
                null,
                ['label' => 'registration_request.label.request_datetime']
            );
    }

    /**
     * {@inheritdoc}
     */
    protected function configureListFields(ListMapper $list)
    {
        $list
            ->addIdentifier(
                'user.fullName',
                null,
                [
                    'label' => 'registration_request.label.request_user',
                    'sortable' => true,
                    'sort_field_mapping' => ['fieldName' => 'user.fullName'],
                    'sort_parent_association_mappings' => []
                ]
            )
            ->add('group.name')
            ->add(
                'status',
                'string',
                [
                    'label' => 'registration_request.label.request_status',
                    'template' => '::Admin/RegistrationRequest/list__status.html.twig'
                ]
            )
            ->add(
                'createdAt',
                null,
                [
                    'label' => 'registration_request.label.request_datetime',
                    'template' => '::Admin/RegistrationRequest/list_datetime.html.twig',
                    'format' => $this->dateService->getDateTimeFormat()
                ]
            )
            ->add(
                '_action',
                'actions',
                [
                    'actions' => [
                        'approve' => ['template' => '::Admin/RegistrationRequest/list__action_approve.html.twig'],
                        'decline' => ['template' => '::Admin/RegistrationRequest/list__action_decline.html.twig']
                    ],
                    'label' => 'registration_request.label.request_actions',
                ]
            );
    }

    /**
     * {@inheritdoc}
     */
    protected function configureShowFields(ShowMapper $show)
    {
        $statuses = $this->getStatusesList();
        $registrationRequest = $this->getSubject();
        $status = $statuses[$registrationRequest->getStatus()];

        $show
            ->add(
                new DisplayFieldDescription('user'),
                null,
                [
                    'data' => $registrationRequest->getUser()->getFullName(),
                    'admin_code' => 'app.user_admin',
                    'label' => 'registration_request.label.request_user'
                ]
            )
            ->add('organization.name')
            ->add('group.name')
            ->add(
                new DisplayFieldDescription('status'),
                null,
                ['data' => $this->trans($status), 'label' => 'registration_request.label.request_status']
            )
            ->add(
                new DisplayFieldDescription('createdAt'),
                null,
                [
                    'data' => $this->dateService->getFormattedDateTime($registrationRequest->getCreatedAt()),
                    'label' => 'registration_request.label.request_datetime'
                ]
            );
        if ($registrationRequest->getStatus() == RegistrationRequest::STATUS_DECLINED) {
            $show
                ->add(
                    'comment',
                    null,
                    ['label' => 'registration_request.label.request_comment']
                );
        }
    }

    /**
     * @return array
     */
    public function getStatusesList()
    {
        return RegistrationRequest::getStatusesList();
    }

    /**
     * @param RegistrationRequestRepository $registrationRequestRepository
     *
     * @return RegistrationRequestAdmin
     */
    public function setRegistrationRequestRepository($registrationRequestRepository)
    {
        $this->registrationRequestRepository = $registrationRequestRepository;

        return $this;
    }

    /**
     * @param DateService $dateService
     *
     * @return RegistrationRequestAdmin
     */
    public function setDateService($dateService)
    {
        $this->dateService = $dateService;

        return $this;
    }

    /**
     * @param CurrentUserService $currentUserService
     *
     * @return RegistrationRequestAdmin
     */
    public function setCurrentUserService($currentUserService)
    {
        $this->currentUserService = $currentUserService;

        return $this;
    }
}
