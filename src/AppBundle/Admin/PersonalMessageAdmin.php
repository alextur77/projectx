<?php

namespace AppBundle\Admin;

use AppBundle\Entity\PersonalMessage;
use AppBundle\Repository\PersonalMessageRepository;
use AppBundle\Service\User\CurrentUserService;
use Aristek\Bundle\AdminBundle\Admin\Admin;
use Component\Doctrine\ORM\SortFixProxyQuery;
use Doctrine\ORM\QueryBuilder;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Route\RouteCollection;
use Sonata\AdminBundle\Show\ShowMapper;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;

/**
 * Class PersonalMessageAdmin.
 */
class PersonalMessageAdmin extends Admin
{
    /**
     * @var string
     */
    protected $baseRouteName = 'personal_message';

    /**
     * @var string
     */
    protected $baseRoutePattern = 'personal_message';

    /**
     * @var CurrentUserService
     */
    protected $currentUserService;

    /**
     * @var PersonalMessageRepository
     */
    protected $personalMessageRepository;

    /**
     * {@inheritdoc}
     */
    protected $datagridValues = array(
        '_page' => 1,
        '_per_page' => 32,
        '_sort_order' => 'DESC',
        '_sort_by' => 'id'
    );

    /**
     * {@inheritdoc}
     */
    public function createQuery($context = 'list')
    {
        if ($context === 'list') {

            $organization = $this->currentUserService->getCurrentOrganization();

            $qb = new SortFixProxyQuery(
                $this->personalMessageRepository->createOrganizationAdminQueryBuilder(
                    $organization,
                    $this->getCurrentUser()
                )
            );

            return $qb;
        } else {
            return parent::createQuery($context);
        }
    }

    /**
     * {@inheritdoc}
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->addIdentifier('subject')
            ->add('senderFullName', null, ['admin_code' => 'app.user_admin'])
            ->add(
                'createdAt',
                null,
                [
                    'format' => 'd.m.Y H:i',
                ]
            );
    }

    /**
     * @param string $name
     *
     * @return null|string|void
     */
    public function getTemplate($name)
    {
        switch ($name) {
            case 'outer_list_rows_list':
                return '::Admin\PersonalMessage\outer_list_rows_personal_message.html.twig';
            case 'show':
                return '::Admin\PersonalMessage\show.html.twig';
            default:
                return parent::getTemplate($name);
        }
    }

    /**
     * @return array
     */
    public function getFilterParameters()
    {
        $this->datagridValues = array_merge(
            [
                'id' => [
                    'type' => '',
                    'value' => 'new'
                ]
            ],
            $this->datagridValues

        );

        return parent::getFilterParameters();

    }

    /**
     * {@inheritdoc}
     */
    protected function configureDatagridFilters(DatagridMapper $filter)
    {
        $filter
            ->add(
                'senderFullName',
                'doctrine_orm_callback',
                [
                    'callback' => [$this, 'getFullTextFilter'],
                    'field_type' => 'text'
                ]
            )
            ->add(
                'id',
                'doctrine_orm_callback',
                [
                    'callback' => [$this, 'getReadFilter'],
                    'field_type' => 'choice',
                    'field_options' => [
                        'choices' => [
                            'read' => 'personal_message.read',
                            'new' => 'personal_message.new'
                        ]
                    ]
                ],
                null,
                null,
                ['label' => 'label.read_choose']
            );

    }

    /**
     * @param QueryBuilder $queryBuilder
     * @param string       $alias
     * @param string       $field
     * @param array        $value
     *
     * @return bool|void
     */
    public function getFullTextFilter($queryBuilder, $alias, $field, $value)
    {
        if (!$value['value']) {

            return true;
        }

        $queryBuilder->innerJoin('AppBundle:User', 't', 'WITH', 'e.sender = t.id')
            ->andWhere(
                $queryBuilder->expr()->orX(
                    $queryBuilder->expr()->like(
                        't' . '.firstName',
                        $queryBuilder->expr()->literal('%' . $value['value'] . '%')
                    ),
                    $queryBuilder->expr()->like(
                        't' . '.lastName',
                        $queryBuilder->expr()->literal('%' . $value['value'] . '%')
                    ),
                    $queryBuilder->expr()->like(
                        't' . '.patronymic',
                        $queryBuilder->expr()->literal('%' . $value['value'] . '%')
                    )
                )
            );

        return true;
    }

    /**
     * @param QueryBuilder $queryBuilder
     * @param string       $alias
     * @param string       $field
     * @param array        $value
     *
     * @return bool|void
     */
    public function getReadFilter($queryBuilder, $alias, $field, $value)
    {
        if (!$value['value']) {

            return true;
        }

        if ($value['value'] == 'read') {

            $queryBuilder->leftJoin('AppBundle:PersonalMessageRead', 'r', 'WITH', 'e.id = r.message')
                ->andWhere('r.id IS NOT NULL');

        } else {

            $queryBuilder->leftJoin('AppBundle:PersonalMessageRead', 'r', 'WITH', 'e.id = r.message')
                ->andWhere('r.id IS NULL');
        }

        return true;
    }

    /**
     * {@inheritdoc}
     */
    protected function configureShowFields(ShowMapper $show)
    {
        $show
            ->with('General')
            ->add('subject')
            ->add('body')
            ->add('senderFullName', null, ['admin_code' => 'app.user_admin'])
            ->add(
                'createdAt',
                null,
                [
                    'format' => 'd.m.Y H:i'
                ]
            )
            ->end();
    }

    /**
     * @param FormMapper $form
     *
     * @return void
     */
    protected function configureFormFields(FormMapper $form)
    {
        $form
            ->with('General')
            ->add('subject')
            ->add('body')
            ->add('organization', HiddenType::class, ['disabled' => true], ['admin_code' => 'app.organization_admin'])
            ->add('recipient', null, ['multiple' => true], ['admin_code' => 'app.user_admin'])
            ->end();
    }

    /**
     * @return mixed
     */
    public function getNewInstance()
    {
        $instance = parent::getNewInstance();
        $organization = $this->currentUserService->getCurrentOrganization();
        $instance->setOrganization($organization);

        return $instance;
    }

    /**
     * {@inheritdoc}
     */
    protected function configureRoutes(RouteCollection $collection)
    {
        $collection
            ->clearExcept(['list', 'show', 'delete', 'export', 'batch', 'create']);
    }

    /**
     * @param mixed $currentUserService
     *
     * @return BaseGroupAdmin
     */
    public function setCurrentUserService($currentUserService)
    {
        $this->currentUserService = $currentUserService;

        return $this;
    }

    /**
     * @param PersonalMessage $personalMessageRepository
     *
     * @return EventsAdmin
     */
    public function setPersonalMessageRepository($personalMessageRepository)
    {
        $this->personalMessageRepository = $personalMessageRepository;

        return $this;
    }

    /**
     * @inheritdoc
     *
     * @return array
     */
    public function getBatchActions()
    {
        $actions = parent::getBatchActions();

        if ($this->isGranted('LIST')) {
            $actions['read_message'] = [
                'label' => $this->trans('personal_message.check_message'),
                'translation_domain' => 'AppBundle',
                'ask_confirmation' => false
            ];

        }

        return $actions;
    }
}
