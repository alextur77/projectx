<?php

namespace AppBundle\Admin;

use Aristek\Bundle\AdminBundle\Admin\Admin;
use Component\Doctrine\ORM\SortFixProxyQuery;
use AppBundle\DataTransferObject\DTOGroup;
use AppBundle\Entity\RegistrationInvite;
use AppBundle\Entity\User;
use AppBundle\Entity\Role;
use AppBundle\Repository\RegistrationInviteRepository;
use AppBundle\Repository\GroupRepository;
use AppBundle\Service\User\CurrentUserService;
use AppBundle\Service\DateService;
use AppBundle\Service\RegistrationInvite\RegistrationInviteService;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Route\RouteCollection;
use Sonata\CoreBundle\Validator\ErrorElement;
use Doctrine\ORM\QueryBuilder;

/**
 * Class RegistrationInviteAdmin
 * @package AppBundle\Admin
 * @method User getCurrentUser()
 */
class RegistrationInviteAdmin extends Admin
{
    private $filterParametersCache = null;

    /**
     * @var GroupRepository
     */
    protected $groupRepository;

    /**
     * @var DateService
     */
    protected $dateService;

    /**
     * @var RegistrationInviteRepository
     */
    protected $registrationInviteRepository;

    /**
     * @var RegistrationInviteService
     */
    protected $registrationInviteService;

    /**
     * @var CurrentUserService
     */
    protected $currentUserService;

    /**
     * {@inheritdoc}
     */
    protected $datagridValues = array(
        '_page' => 1,
        '_per_page' => 32,
        '_sort_order' => 'DESC',
        '_sort_by' => 'createdAt'
    );

    /**
     * @param DatagridMapper $filter
     */
    protected function configureDatagridFilters(DatagridMapper $filter)
    {
        $filter
            ->add('status', null, [], 'choice', ['choices' => $this->getStatusesList()])
            ->add('email')
            ->add('createdBy')
            ->add(
                'createdAt',
                'doctrine_orm_date_range',
                [],
                null,
                null,
                ['label' => 'registration_invite.label.invitation_datetime']
            );
    }

    /**
     * {@inheritdoc}
     */
    public function getFilterParameters()
    {
        if (null === $this->filterParametersCache) {
            $this->filterParametersCache = parent::getFilterParameters();
        }

        return $this->filterParametersCache;
    }

    /**
     * {@inheritdoc}
     */
    public function createQuery($context = 'list')
    {
        if ($context === 'list') {
            $user = $this->currentUserService->getCurrentUser();
            $organization = $this->currentUserService->getCurrentOrganization();
            $role = $this->currentUserService->getCurrentUserRoleInOrganization();

            $fix = ['status', 'organization_group_name'];
            if ($role === Role::ROLE_ORGANIZATION_ADMIN) {
                $fix[] = 'organization_group_type';
                $qb = $this->registrationInviteRepository->createOrganizationAdminQueryBuilder($organization);
            } elseif (($role === Role::ROLE_ORGANIZATION_USER) or (null === $role)) {
                // $user has Role::ROLE_ORGANIZATION_USER role in $organization or have not any role at all:
                $qb = $this->registrationInviteRepository->createGroupAdminQueryBuilder($user, $organization);
            } else {
                throw new \LogicException(__METHOD__ . ': Unknown organization user role: "' . $role . '""!');
            }

            /** @var QueryBuilder $qb */
            $qb = new SortFixProxyQuery($qb, $fix);

            $filterParameters = $this->getFilterParameters();
            if (isset($filterParameters['_sort_by'])) {
                $this->registrationInviteRepository->correctQueryBuilderSorting(
                    $qb,
                    $filterParameters['_sort_by'],
                    $filterParameters['_sort_order']
                );
            }

            return $qb;
        } else {
            return parent::createQuery($context);
        }
    }

    /**
     * {@inheritdoc}
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        $role = $this->currentUserService->getCurrentUserRoleInOrganization();
        $isOrgAdmin = $role === Role::ROLE_ORGANIZATION_ADMIN;

        $listMapper
            ->add(
                'status',
                'string',
                [
                    'label' => 'registration_invite.label.invitation_status',
                    'template' => '::Admin/RegistrationInvite/list__status.html.twig'
                ]
            );

        if ($isOrgAdmin) {
            $listMapper->add(
                'organization_group_type',
                null,
                [
                    'label' => 'registration_invite.label.invitation_type',
                    'template' => '::Admin/RegistrationInvite/list__type.html.twig',
                    'sortable' => true,
                    'sort_field_mapping' => ['fieldName' => 'organization_group_type'],
                    'sort_parent_association_mappings' => []
                ]
            );
        }

        $listMapper
            ->add(
                'organization_group_name',
                null,
                [
                    'label' => 'registration_invite.label.invitation_name',
                    'sortable' => true,
                    'sort_field_mapping' => ['fieldName' => 'organization_group_name'],
                    'sort_parent_association_mappings' => []
                ]
            )
            ->add('email')
            ->add('createdBy')
            ->add(
                'createdAt',
                null,
                [
                    'template' => '::Admin/RegistrationInvite/list_datetime.html.twig',
                    'format' => $this->dateService->getDateTimeFormat()
                ]
            );
    }

    /**
     * {@inheritdoc}
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        $groupIds = $this->getRequest()->query->get('groups');
        if (null !== $groupIds) {
            $groupIds = explode(',', $groupIds);
        }

        $user = $this->currentUserService->getCurrentUser();
        $organization = $this->currentUserService->getCurrentOrganization();
        $role = $this->currentUserService->getCurrentUserRoleInOrganization();

        $groupChoices = [];
        if ($role === Role::ROLE_ORGANIZATION_ADMIN) {
            $groupChoices[''] = $organization->getName();
            $groups = $this->groupRepository->findActiveByOrganization($organization);
        } elseif (($role === Role::ROLE_ORGANIZATION_USER) or (null === $role)) {
            // $user has Role::ROLE_ORGANIZATION_USER role in $organization or have not any role at all:
            $groups = $this->groupRepository->findByGroupAdmin($user, $organization);
        } else {
            throw new \LogicException(__METHOD__ . ': Unknown organization user role: "' . $role . '""!');
        }
        /** @var DTOGroup $group */
        foreach ($groups as $group) {
            $groupChoices[$group->id] = $group->getShift() . $group->name;
        }

        $formMapper
            ->with($this->trans('label.organization_groups'))
            ->add(
                'organizationGroups',
                'choice',
                [
                    'choices' => $groupChoices,
                    'required' => true,
                    'multiple' => true,
                    'mapped' => false,
                    'data' => $groupIds,
                    'attr' => ['placeholder' => $this->trans('registration_invite.admin.org_placeholder')]
                ]
            )
            ->end()
            ->with($this->trans('label.emails'))
            ->add(
                'emails',
                'text',
                ['mapped' => false, 'required' => true]
            )
            ->end();
    }

    /**
     * {@inheritdoc}
     */
    protected function configureRoutes(RouteCollection $collection)
    {
        $collection->clearExcept(['create', 'list']);
    }

    /**
     * {@inheritdoc}
     *
     * @param RegistrationInvite $object
     */
    public function create($object)
    {
        //  This function is called from CRUDController::createAction().
        $emails = explode(',', $this->getForm()->get('emails')->getData());

        /** @var array $groupIds */
        $groupIds = $this->getForm()->get('organizationGroups')->getData();
        $groups = $this->groupRepository->findByIds($groupIds);

        $organization = null;
        $key = array_search('', $groupIds);
        if (false !== $key) {
            unset($groupIds[$key]);
            $organization = $this->currentUserService->getCurrentOrganization();
        }

        $this->registrationInviteService->process($emails, $groups, $organization);

        //  This is original return value.
        //      return $object;
        //  The null value must be returned because the $object entity was not persisted to the database.
        //  See how this return value is used in CRUDController::createAction().
        return null;
    }

    /**
     * {@inheritdoc}
     */
    public function validate(ErrorElement $errorElement, $object)
    {
        $emails = explode(',', $this->getForm()->get('emails')->getData());
        $invalidEmails = '';
        foreach ($emails as $email) {
            if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
                $invalidEmails .= ', ' . $email;
            }
        }
        if (!empty($invalidEmails)) {
            $invalidEmails = substr($invalidEmails, 2);
            $errorElement
                ->addViolation(
                    $this->trans('registration_invite.validator.invalid_email', ['%Emails%' => $invalidEmails])
                );
        }
    }

    /**
     * @return array
     */
    public function getTypesList()
    {
        return RegistrationInvite::getTypesList();
    }

    /**
     * @return array
     */
    public function getStatusesList()
    {
        return RegistrationInvite::getStatusesList();
    }

    /**
     * @param DateService $dateService
     *
     * @return RegistrationInviteAdmin
     */
    public function setDateService(DateService $dateService)
    {
        $this->dateService = $dateService;

        return $this;
    }

    /**
     * @param CurrentUserService $currentUserService
     *
     * @return RegistrationInviteAdmin
     */
    public function setCurrentUserService($currentUserService)
    {
        $this->currentUserService = $currentUserService;

        return $this;
    }

    /**
     * @param RegistrationInviteService $registrationInviteService
     *
     * @return RegistrationInviteAdmin
     */
    public function setRegistrationInviteService($registrationInviteService)
    {
        $this->registrationInviteService = $registrationInviteService;

        return $this;
    }

    /**
     * @param RegistrationInviteRepository $registrationInviteRepository
     *
     * @return RegistrationInviteAdmin
     */
    public function setRegistrationInviteRepository($registrationInviteRepository)
    {
        $this->registrationInviteRepository = $registrationInviteRepository;

        return $this;
    }

    /**
     * @param GroupRepository $groupRepository
     *
     * @return RegistrationInviteAdmin
     */
    public function setGroupRepository($groupRepository)
    {
        $this->groupRepository = $groupRepository;

        return $this;
    }
}
