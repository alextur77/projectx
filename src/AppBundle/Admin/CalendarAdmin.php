<?php

namespace AppBundle\Admin;

use Aristek\Bundle\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\ListMapper;

/**
 * Class CalendarAdmin.
 */
class CalendarAdmin extends Admin
{
    /**
     * @var string
     */
    protected $baseRouteName = 'my_calendar';

    /**
     * @var string
     */
    protected $baseRoutePattern = 'my_calendar';

    /**
     * {@inheritdoc}
     */
    public function getTemplate($name)
    {
        switch ($name) {
            case 'list':
                return 'AppBundle:Events:event_calendar_show.html.twig';
            default:
                return parent::getTemplate($name);
        }
    }
}
