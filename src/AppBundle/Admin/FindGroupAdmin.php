<?php

namespace AppBundle\Admin;

use Aristek\Bundle\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Route\RouteCollection;

/**
 * Class FindGroupAdmin
 * @package AppBundle\Admin
 */
class FindGroupAdmin extends Admin
{
    /**
     * The base route name used to generate the routing information
     *
     * @var string
     */
    protected $baseRouteName = 'find-group';

    /**
     * The base route pattern used to generate the routing information
     *
     * @var string
     */
    protected $baseRoutePattern = 'find-group';

    /**
     * @param RouteCollection $collection
     */
    protected function configureRoutes(RouteCollection $collection)
    {
        $collection
            ->clearExcept(['list'])
            ->add('list-groups', 'list-groups/{organization_id}')
            ->add('request-organization', 'request-organization/{organization_id}')
            ->add('request-groups', 'request-groups/{organization_id}');
    }
}
