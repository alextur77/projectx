<?php

namespace AppBundle\Admin;

use Aristek\Bundle\AdminBundle\Admin\Admin;
use Component\Doctrine\ORM\SortFixProxyQuery;
use AppBundle\DataTransferObject\DTOGroup;
use AppBundle\Repository\GroupUserPermissionRepository;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Route\RouteCollection;
use Doctrine\ORM\QueryBuilder;

/**
 * Class UserGroupUserPermissionAdmin
 * @package AppBundle\Admin
 *
 * @method GroupUserPermission getSubject()
 * @method User getCurrentUser()
 */
class UserGroupUserPermissionAdmin extends Admin
{
    private $filterParametersCache = null;

    // WARNING: Changing the base route name and pattern in the child admin
    // eliminates the parent routes to this child admin!
    //    /**
    //     * {@inheritdoc}
    //     */
    //    protected $baseRouteName = 'user_group_user_permissions';
    //
    //    /**
    //     * {@inheritdoc}
    //     */
    //    protected $baseRoutePattern = 'user-group-user-permissions';
    //
    /**
     * @var GroupUserPermissionRepository
     */
    protected $groupUserPermissionRepository;

    /**
     * {@inheritdoc}
     */
    protected $parentAssociationMapping = 'group';

    /**
     * {@inheritdoc}
     */
    protected $datagridValues = array(
        '_page' => 1,
        '_per_page' => 32,
        '_sort_order' => 'ASC',
        '_sort_by' => 'user.fullName'
    );

    /**
     * {@inheritdoc}
     */
    public function getFilterParameters()
    {
        if (null === $this->filterParametersCache) {
            $this->filterParametersCache = parent::getFilterParameters();
        }

        return $this->filterParametersCache;
    }

    /**
     * {@inheritdoc}
     */
    public function createQuery($context = 'list')
    {
        if ('list' === $context) {
            /** @var QueryBuilder $qb */
            $qb = new SortFixProxyQuery($this->groupUserPermissionRepository->createQueryBuilder(), ['user.fullName']);

            $filterParameters = $this->getFilterParameters();
            if (isset($filterParameters['_sort_by'])) {
                $this->groupUserPermissionRepository->correctQueryBuilderSorting(
                    $qb,
                    $filterParameters['_sort_by'],
                    $filterParameters['_sort_order']
                );
            }

            return $qb;
        } else {
            return parent::createQuery($context);
        }
    }

    /**
     * {@inheritdoc}
     */
    protected function configureRoutes(RouteCollection $collection)
    {
        $collection->clearExcept(['list']);
    }

    /**
     * {@inheritdoc}
     */
    protected function configureListFields(ListMapper $list)
    {
        $list
            ->add('user.username', null, ['label' => 'group_user_permission.label.user_username'])
            ->add(
                'user.fullName',
                null,
                [
                    'label' => 'group_user_permission.label.user_full_name',
                    'sortable' => true,
                    'sort_field_mapping' => ['fieldName' => 'user.fullName'],
                    'sort_parent_association_mappings' => []
                ]
            );
    }

    /**
     * {@inheritdoc}
     */
    public function getUrlsafeIdentifier($entity)
    {
        return $entity instanceof DTOGroup ?
            $entity->id :
            parent::getUrlsafeIdentifier($entity);
    }

    /**
     * {@inheritdoc}
     */
    public function getNormalizedIdentifier($entity)
    {
        return $entity instanceof DTOGroup ?
            $entity->id :
            parent::getNormalizedIdentifier($entity);
    }

    /**
     * @param GroupUserPermissionRepository $groupUserPermissionRepository
     *
     * @return UserGroupUserPermissionAdmin
     */
    public function setGroupUserPermissionRepository($groupUserPermissionRepository)
    {
        $this->groupUserPermissionRepository = $groupUserPermissionRepository;

        return $this;
    }
}
