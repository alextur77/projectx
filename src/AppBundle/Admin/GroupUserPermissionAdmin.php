<?php

namespace AppBundle\Admin;

use Aristek\Bundle\AdminBundle\Admin\Admin;
use Component\Doctrine\ORM\SortFixProxyQuery;
use AppBundle\DataTransferObject\DTOGroup;
use AppBundle\Entity\Role;
use AppBundle\Entity\User;
use AppBundle\Entity\GroupUserPermission;
use AppBundle\Service\User\CurrentUserService;
use AppBundle\Repository\RoleRepository;
use AppBundle\Repository\GroupUserPermissionRepository;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Route\RouteCollection;
use Doctrine\ORM\QueryBuilder;

/**
 * Class GroupUserPermissionAdmin
 * @package AppBundle\Admin
 *
 * @method GroupUserPermission getSubject()
 * @method User getCurrentUser()
 */
class GroupUserPermissionAdmin extends Admin
{
    private $filterParametersCache = null;

    /**
     * @var array
     */
    private $groupRoles = null;

    /**
     * @var integer
     */
    private $groupAdminQuantity = null;

    /**
     * @var RoleRepository;
     */
    protected $roleRepository;

    /**
     * @var GroupUserPermissionRepository
     */
    protected $groupUserPermissionRepository;

    /**
     * @var CurrentUserService
     */
    protected $currentUserService;

    /**
     * {@inheritdoc}
     */
    protected $parentAssociationMapping = 'group';

    /**
     * {@inheritdoc}
     */
    protected $datagridValues = array(
        '_page' => 1,
        '_per_page' => 32,
        '_sort_order' => 'ASC',
        '_sort_by' => 'user.username'
    );

    /**
     * {@inheritdoc}
     */
    public function getFilterParameters()
    {
        if (null === $this->filterParametersCache) {
            $this->filterParametersCache = parent::getFilterParameters();
        }

        return $this->filterParametersCache;
    }

    /**
     * {@inheritdoc}
     */
    public function createQuery($context = 'list')
    {
        if ('list' === $context) {
            /** @var QueryBuilder $qb */
            $qb = new SortFixProxyQuery($this->groupUserPermissionRepository->createQueryBuilder(), ['user.fullName']);

            $filterParameters = $this->getFilterParameters();
            if (isset($filterParameters['_sort_by'])) {
                $this->groupUserPermissionRepository->correctQueryBuilderSorting(
                    $qb,
                    $filterParameters['_sort_by'],
                    $filterParameters['_sort_order']
                );
            }

            return $qb;
        } else {
            return parent::createQuery($context);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function getUrlsafeIdentifier($entity)
    {
        return $entity instanceof DTOGroup ?
            $entity->id :
            parent::getUrlsafeIdentifier($entity);
    }

    /**
     * {@inheritdoc}
     */
    public function getNormalizedIdentifier($entity)
    {
        return $entity instanceof DTOGroup ?
            $entity->id :
            parent::getNormalizedIdentifier($entity);
    }

    /**
     * {@inheritdoc}
     */
    protected function configureRoutes(RouteCollection $collection)
    {
        $collection
            ->clearExcept(['list', 'edit', 'create', 'delete', 'batch'])
            ->add('change-role', $this->getRouterIdParameter() . '/change-role');
    }

    /**
     * {@inheritdoc}
     */
    protected function configureListFields(ListMapper $list)
    {
        $list
            ->add('user.username', null, ['label' => 'group_user_permission.label.user_username'])
            ->add(
                'user.fullName',
                null,
                [
                    'label' => 'group_user_permission.label.user_full_name',
                    'sortable' => true,
                    'sort_field_mapping' => ['fieldName' => 'user.fullName'],
                    'sort_parent_association_mappings' => []
                ]
            )
            ->add(
                'role.name',
                null,
                [
                    'editable' => true,
                    'label' => 'group_user_permission.label.role',
                    'template' => '::Admin/GroupUserPermission/list__role.html.twig'
                ]
            )
            ->add(
                '_action',
                'actions',
                [
                    'actions' =>
                        [
                            'delete' => ['template' => '::Admin/GroupUserPermission/list__action_delete.html.twig'],
                            'sendMessage' => ['template' => '::Admin/GroupUserPermission/list__action_send_message.html.twig']
                        ],
                    'label' => 'group_user_permission.label.group_actions',
                ]
            );
    }

    /**
     * @inheritdoc
     */
    public function getBatchActions()
    {
        $actions = parent::getBatchActions();

        // Replace standard delete action attributes:
        if (array_key_exists('delete', $actions)) {
            $actions['delete']['label'] = $this->trans('group_user_permission.label.action_delete');
            $actions['delete']['translation_domain'] = 'AppBundle';
            $actions['delete']['ask_confirmation'] = true;
        }

        return $actions;
    }

    /**
     * @return array
     */
    public function getGroupRoles()
    {
        if (null === $this->groupRoles) {
            $this->groupRoles = [];
            $groupRoles = $this->roleRepository->findByType(Role::TYPE_GROUP);
            foreach ($groupRoles as $groupRole) {
                $this->groupRoles[$groupRole->getId()] = $groupRole->getName();
            }
        }

        return $this->groupRoles;
    }

    /**
     * @return int
     */
    public function getGroupAdminQuantity()
    {
        if (null === $this->groupAdminQuantity) {
            $admin = $this->isChild() ? $this->getParent() : $this;
            $groupId = $admin->getSubject()->getId();
            $this->groupAdminQuantity = $this->groupUserPermissionRepository->getGroupAdminQuantity($groupId, 1);
        }

        return $this->groupAdminQuantity;
    }

    /**
     * @return null|string
     */
    public function getCurrentUserRoleInOrganization()
    {
        return $this->currentUserService->getCurrentUserRoleInOrganization();
    }

    /**
     * @param RoleRepository $roleRepository
     *
     * @return GroupUserPermissionAdmin
     */
    public function setRoleRepository($roleRepository)
    {
        $this->roleRepository = $roleRepository;

        return $this;
    }

    /**
     * @param GroupUserPermissionRepository $groupUserPermissionRepository
     *
     * @return GroupUserPermissionAdmin
     */
    public function setGroupUserPermissionRepository($groupUserPermissionRepository)
    {
        $this->groupUserPermissionRepository = $groupUserPermissionRepository;

        return $this;
    }

    /**
     * @param CurrentUserService $currentUserService
     *
     * @return GroupUserPermissionAdmin
     */
    public function setCurrentUserService($currentUserService)
    {
        $this->currentUserService = $currentUserService;

        return $this;
    }
}
