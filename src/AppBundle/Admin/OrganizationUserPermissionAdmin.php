<?php

namespace AppBundle\Admin;

use AppBundle\Entity\Role;
use AppBundle\Repository\RoleRepository;
use Aristek\Bundle\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Form\FormMapper;

/**
 * Class OrganizationUserPermissionAdmin
 */
class OrganizationUserPermissionAdmin extends Admin
{

    /**
     * @var string
     */
    protected $parentAssociationMapping = 'organization';

    /**
     * @var RoleRepository
     */
    protected $roleRepository;

    /**
     * @param FormMapper $form
     */
    protected function configureFormFields(FormMapper $form)
    {
        $form
            ->add(
                'user',
                null,
                [],
                [
                    'admin_code' => 'app.user_admin'
                ]
            )
            ->add(
                'role',
                null,
                ['query_builder' => $this->roleRepository->createQueryBuilderByType(Role::TYPE_ORGANIZATION)]
            );
    }

    /**
     * @param RoleRepository $roleRepository
     *
     * @return OrganizationUserPermissionAdmin
     */
    public function setRoleRepository($roleRepository)
    {
        $this->roleRepository = $roleRepository;

        return $this;
    }
}
