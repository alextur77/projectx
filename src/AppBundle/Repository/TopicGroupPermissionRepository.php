<?php

namespace AppBundle\Repository;

use AppBundle\DataTransferObject\DTOTopic;
use AppBundle\Entity\Role;
use AppBundle\Entity\Topic;
use AppBundle\Entity\User;
use Aristek\Component\ORM\EntityRepository;
use AppBundle\Entity\TopicGroupPermission;
use Doctrine\ORM\QueryBuilder;
use Doctrine\ORM\Query\Expr\Join;

/**
 * Class TopicGroupPermissionRepository
 * @package AppBundle\Repository
 *
 * @method TopicGroupPermission findOneBy(array $criteria, array $orderBy = null)
 * @method TopicGroupPermission find($id, $lockMode = null, $lockVersion = null)
 * @method TopicGroupPermission[] findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class TopicGroupPermissionRepository extends EntityRepository
{
    const ALIAS = 'TopicGroupPermission';

    const ALIAS_GROUP_CLOSURE = 'GroupClosure';
    const ENTITY_GROUP_CLOSURE = 'AppBundle\Entity\GroupClosure';

    const ALIAS_GROUP_PERMISSION = 'GroupUserPermission';
    const ENTITY_GROUP_PERMISSION = 'AppBundle\Entity\GroupUserPermission';

    /**
     * @param string $alias
     *
     * @return string
     */
    private function resolveAlias($alias)
    {
        return null === $alias ? $this::ALIAS : $alias;
    }

    /**
     * @param string|null $alias
     * @param string|null $indexBy
     *
     * @return QueryBuilder
     */
    public function createQueryBuilder($alias = null, $indexBy = null)
    {
        $alias = $this->resolveAlias($alias);

        return parent::createQueryBuilder($alias, $indexBy);
    }

    public function createDeleteQueryBuilder($alias = null)
    {
        $alias = $this->resolveAlias($alias);
        $qb = $this->_em->createQueryBuilder();
        $qb->delete($this->getClassName(), $alias);

        return $qb;
    }

    /**
     * Returns an array indexed by the topic_ids where each item is true if $user has moderator rights
     * to this topic through the group admin rights or false otherwise.
     *
     * @param User                                                $user
     * @param DTOTopic|Topic|integer|DTOTopic[]|Topic[]|integer[] $topics
     *
     * @return boolean[]
     */
    public function hasUserTopicGroupAdminRights(User $user, $topics)
    {
        if (!is_array($topics)) {
            $topics = [$topics];
        }
        if (reset($topics) instanceof DTOTopic) {
            foreach ($topics as $index => $topic) {
                $topics[$index] = $topic->getId();
            }
        }
        $aliasR = 'roles';
        $qb = $this->createQueryBuilder();
        $qb
            ->select('IDENTITY(' . $this::ALIAS . '.topic) AS topic_id')
            ->innerJoin(
                $this::ENTITY_GROUP_CLOSURE,
                $this::ALIAS_GROUP_CLOSURE,
                Join::WITH,
                $this::ALIAS_GROUP_CLOSURE . '.descendantId = ' . $this::ALIAS . '.group'
            )
            ->innerJoin(
                $this::ENTITY_GROUP_PERMISSION,
                $this::ALIAS_GROUP_PERMISSION,
                Join::WITH,
                $this::ALIAS_GROUP_CLOSURE . '.ancestorId = ' . $this::ALIAS_GROUP_PERMISSION . '.group'
            )
            ->innerJoin($this::ALIAS_GROUP_PERMISSION . '.role', $aliasR)
            ->where($qb->expr()->in($this::ALIAS . '.topic', ':topics'))
            ->andWhere($this::ALIAS_GROUP_PERMISSION . '.user = :user')
            ->andWhere($aliasR . '.code = :code')
            ->groupBy($this::ALIAS . '.topic')
            ->setParameter('user', $user)
            ->setParameter('topics', $topics)
            ->setParameter('code', Role::ROLE_GROUP_ADMIN);
        $result = $qb->getQuery()->getResult();
        $result = array_column($result, 'topic_id');
        $result = array_fill_keys($result, true);

        foreach ($topics as $topic) {
            if ($topic instanceof Topic) {
                $id = $topic->getId();
            } else {
                $id = $topic;
            }
            if (!key_exists($id, $result)) {
                $result[$id] = false;
            }
        }

        return $result;
    }

    /**
     * Returns an array indexed by the topic_ids where each item is Role::ROLE_GROUP_ADMIN if $user has moderator
     * rights to this topic through the group admin rights or Role::ROLE_GROUP_USER if $user has usual user rights
     * to this topic or false if $user has no access to this topic.
     *
     * @param User                                                $user
     * @param DTOTopic|Topic|integer|DTOTopic[]|Topic[]|integer[] $topics
     *
     * @return array
     */
    public function getUserTopicRights(User $user, $topics)
    {
        if (!is_array($topics)) {
            $topics = [$topics];
        }
        if (reset($topics) instanceof DTOTopic) {
            foreach ($topics as $index => $topic) {
                $topics[$index] = $topic->getId();
            }
        }
        $aliasR = 'roles';
        $qb = $this->createQueryBuilder();
        $qb
            ->select('IDENTITY(' . $this::ALIAS . '.topic) AS topic_id')
            ->addSelect(
                'SUM(CASE WHEN ' . $aliasR . '.code = \'' . Role::ROLE_GROUP_ADMIN . '\' THEN 1 ELSE 0 END) AS admin'
            )
            ->innerJoin(
                $this::ENTITY_GROUP_CLOSURE,
                $this::ALIAS_GROUP_CLOSURE,
                Join::WITH,
                $this::ALIAS_GROUP_CLOSURE . '.descendantId = ' . $this::ALIAS . '.group'
            )
            ->innerJoin(
                $this::ENTITY_GROUP_PERMISSION,
                $this::ALIAS_GROUP_PERMISSION,
                Join::WITH,
                $this::ALIAS_GROUP_CLOSURE . '.ancestorId = ' . $this::ALIAS_GROUP_PERMISSION . '.group'
            )
            ->innerJoin($this::ALIAS_GROUP_PERMISSION . '.role', $aliasR)
            ->where($qb->expr()->in($this::ALIAS . '.topic', ':topics'))
            ->andWhere($this::ALIAS_GROUP_PERMISSION . '.user = :user')
            ->groupBy($this::ALIAS . '.topic')
            ->setParameter('user', $user)
            ->setParameter('topics', $topics);
        $result = $qb->getQuery()->getResult();
        $result = array_column($result, 'admin', 'topic_id');
        array_walk(
            $result,
            function (&$item) {
                if ($item > 0) {
                    $item = Role::ROLE_GROUP_ADMIN;
                } else {
                    $item = Role::ROLE_GROUP_USER;
                }
            }
        );
        foreach ($topics as $topic) {
            if ($topic instanceof Topic) {
                $id = $topic->getId();
            } else {
                $id = $topic;
            }
            if (!key_exists($id, $result)) {
                $result[$id] = false;
            }
        }

        return $result;
    }
}
