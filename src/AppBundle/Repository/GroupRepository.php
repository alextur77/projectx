<?php

namespace AppBundle\Repository;

use Aristek\Component\ORM\EntityRepository;
use AppBundle\DataTransferObject\DTOGroup;
use AppBundle\Entity\Group;
use AppBundle\Entity\Organization;
use AppBundle\Entity\RegistrationRequest;
use AppBundle\Entity\User;
use AppBundle\Entity\Role;
use AppBundle\Manager\GroupManager;
use Doctrine\ORM\QueryBuilder;
use Doctrine\ORM\Query\Expr;
use Doctrine\ORM\Query\Expr\Join;
use Sonata\DoctrineORMAdminBundle\Datagrid\ProxyQuery;

/**
 * Class GroupRepository
 * @package AppBundle\Repository
 *
 * @method Group findOneBy(array $criteria, array $orderBy = null)
 * @method Group find($id, $lockMode = null, $lockVersion = null)
 * @method Group[] findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class GroupRepository extends EntityRepository
{
    const ALIAS = 'Groups';

    const ENTITY_USER_PERMISSION = 'AppBundle\Entity\GroupUserPermission';

    /**
     * @var GroupManager
     */
    protected $groupManager;

    /**
     * @var GroupClosureRepository
     */
    protected $groupClosureRepository;

    /**
     * @param string $alias
     *
     * @return string
     */
    private function resolveAlias($alias)
    {
        return null === $alias ? $this::ALIAS : $alias;
    }

    /**
     * @param string|null $alias
     * @param string|null $indexBy
     *
     * @return QueryBuilder
     */
    public function createQueryBuilder($alias = null, $indexBy = null)
    {
        $alias = $this->resolveAlias($alias);

        return parent::createQueryBuilder($alias, $indexBy);
    }

    public function createDeleteQueryBuilder($alias = null)
    {
        $alias = $this->resolveAlias($alias);
        $qb = $this->_em->createQueryBuilder();
        $qb->delete($this->getClassName(), $alias);

        return $qb;
    }

    /**
     * @param QueryBuilder|ProxyQuery $qb
     *
     * @return void
     */
    public function correctUserQueryBuilderForUserCount(&$qb)
    {
        $this->checkQueryBuilder($qb);

        $alias = $qb->getRootAliases()[0];
        $qb
            ->select(
                'NEW AppBundle\DataTransferObject\DTOGroup(' .
                $alias . '.id, ' . $alias . '.name, ' . $alias . '.public, ' .
                $alias . '.active, 0, COUNT(gup.user))'
            )
            ->leftJoin($alias . '.groupUserPermissions', 'gup')
            ->groupBy($alias . '.id');
    }

    /**
     * @param User         $user
     * @param Organization $organization
     *
     * @return QueryBuilder
     */
    public function createUserQueryBuilder(User $user, Organization $organization)
    {
        $qb = $this->createQueryBuilder();
        $alias = $qb->getRootAliases()[0];
        $aliasGUP = 'GroupUserPermissions';

        $qb
            ->innerJoin($alias . '.groupUserPermissions', $aliasGUP)
            ->andWhere($alias . '.organization = :organization')
            ->andWhere($alias . '.active = :active')
            ->andWhere($aliasGUP . '.user = :user')
            ->setParameter('organization', $organization)
            ->setParameter('active', 1)
            ->setParameter('user', $user);

        return $qb;
    }

    /**
     * @param User         $user
     * @param Organization $organization
     *
     * @return QueryBuilder
     */
    public function createOnlyUserAccessQueryBuilder(User $user, Organization $organization)
    {
        // DQL implementation of the following SQL query:
        /*
            SELECT g.id, g.name
            FROM groups g
                INNER JOIN group_user_permissions gup ON gup.group_id = g.id
                INNER JOIN roles r ON r.id = gup.role_id
            WHERE
                g.active = 1
                g.organization = :organization
                AND gup.user_id = :user
                AND r.code = 'ROLE_GROUP_USER'
                AND NOT EXISTS
                (
                    SELECT *
                    FROM group_closure gc
                        INNER JOIN group_user_permissions gupgc ON gupgc.group_id = gc.ancestor_id
                        INNER JOIN roles rgc ON rgc.id = gupgc.role_id
                    WHERE
                        gc.descendant_id = g.id
                        AND rgc.code = 'ROLE_GROUP_ADMIN'
                        AND gupgc.user_id = gup.user_id
                )
            ORDER BY
                g.name
        */
        $qb = $this->createQueryBuilder();
        $subQB = $this->groupClosureRepository->createQueryBuilder();

        $alias = $qb->getRootAliases()[0];
        $aliasGC = $subQB->getRootAliases()[0];
        $aliasGUP = 'GroupUserPermissions';
        $aliasGUPGC = 'GroupUserPermissionsGC';
        $aliasR = 'Roles';
        $aliasRGC = 'RolesGC';

        $subQB
            ->innerJoin(
                $this::ENTITY_USER_PERMISSION,
                $aliasGUPGC,
                Join::WITH,
                $aliasGUPGC . '.group = ' . $aliasGC . '.ancestorId'
            )
            ->innerJoin($aliasGUPGC . '.role', $aliasRGC)
            ->andWhere($aliasGC . '.descendantId = ' . $alias . '.id')
            ->andWhere($aliasGUPGC . '.user = ' . $aliasGUP . '.user')
            ->andWhere($aliasRGC . '.code = :codeGC');

        $qb
            ->innerJoin($alias . '.groupUserPermissions', $aliasGUP)
            ->innerJoin($aliasGUP . '.role', $aliasR)
            ->andWhere($alias . '.active = :active')
            ->andWhere($alias . '.organization = :organization')
            ->andWhere($aliasGUP . '.user = :user')
            ->andWhere($aliasR . '.code = :code')
            ->andWhere($qb->expr()->not($qb->expr()->exists($subQB)))
            ->setParameter('user', $user)
            ->setParameter('active', 1)
            ->setParameter('organization', $organization)
            ->setParameter('code', Role::ROLE_GROUP_USER)
            ->setParameter('codeGC', Role::ROLE_GROUP_ADMIN);

        return $qb;
    }

    /**
     * @param User         $user
     * @param Organization $organization
     *
     * @return Group[]
     */
    public function findByOnlyUserAccess(User $user, Organization $organization)
    {
        $qb = $this->createOnlyUserAccessQueryBuilder($user, $organization);
        $qb->orderBy($this::ALIAS . '.name');

        return $qb->getQuery()->getResult();
    }

    /**
     * Find all groups where $user has the group admin rights
     * that are directly assigned through the group user permissions table.
     *
     * @param User         $user
     * @param Organization $organization
     *
     * @return QueryBuilder
     */
    public function createWithGroupAdminRightsQueryBuilder(User $user, Organization $organization)
    {
        $qb = $this->createQueryBuilder();
        $alias = $qb->getRootAliases()[0];
        $aliasGUP = 'GroupUserPermissions';
        $aliasR = 'Roles';

        $qb
            ->innerJoin($alias . '.groupUserPermissions', $aliasGUP)
            ->innerJoin($aliasGUP . '.role', $aliasR)
            ->andWhere($alias . '.organization = :organization')
            ->andWhere($aliasGUP . '.user = :user')
            ->andWhere($aliasR . '.code = :code')
            ->andWhere($alias . '.active = :active')
            ->setParameter('organization', $organization)
            ->setParameter('user', $user)
            ->setParameter('active', 1)
            ->setParameter('code', Role::ROLE_GROUP_ADMIN);

        return $qb;
    }

    /**
     * Find all groups where $user has the group admin rights
     * that are directly assigned through the group user permissions table.
     *
     * @param User         $user
     * @param Organization $organization
     *
     * @return Group[]
     */
    public function findWithGroupAdminRights(User $user, Organization $organization)
    {
        $qb = $this->createWithGroupAdminRightsQueryBuilder($user, $organization);

        return $qb->getQuery()->getResult();
    }

    /**
     * Find all groups where $user has the group admin rights
     * except ones that are in ancestor<---->descendant relations.
     *
     * @param User         $user
     * @param Organization $organization
     *
     * @return Group[]
     */
    public function findRootsByGroupAdmin(User $user, Organization $organization)
    {
        $groups = $this->findWithGroupAdminRights($user, $organization);
        $groups = $this->groupManager->eliminateAncestorDescendantRelations($groups);

        return $groups;
    }

    /**
     * Group admin cannot see the groups having active field set to false.
     *
     * @param User         $user
     * @param Organization $organization
     *
     * @return QueryBuilder
     */
    public function createGroupAdminQueryBuilder(User $user, Organization $organization)
    {
        $roots = $this->findRootsByGroupAdmin($user, $organization);
        $qb = $this->createTreeQueryBuilder($organization, $roots);
        $alias = $qb->getRootAliases()[0];
        $qb
            ->andWhere($alias . '.active = :active')
            ->setParameter('active', 1);

        return $qb;
    }

    /**
     * @param Organization $organization
     *
     * @return QueryBuilder
     */
    public function createRootsQueryBuilder(Organization $organization)
    {
        // DQL implementation of the following SQL query:
        //
        //    SELECT grp.id, grp.name
        //    FROM groups AS grp
        //    INNER JOIN group_closure AS gc1
        //            ON gc1.ancestor_id = grp.id
        //    LEFT JOIN group_closure AS gc2
        //            ON gc2.descendant_id = gc1.descendant_id AND gc2.ancestor_id <> gc1.ancestor_id
        //    WHERE gc2.ancestor_id IS NULL;
        //
        $qb = $this->createQueryBuilder();
        $alias = $qb->getRootAliases()[0];

        $entity = 'AppBundle\Entity\GroupClosure';
        $qb
            ->innerJoin($entity, 'gc1', Join::WITH, $alias . '.id = gc1.ancestorId')
            ->leftJoin(
                $entity,
                'gc2',
                Join::WITH,
                $qb->expr()->andX('gc1.descendantId = gc2.descendantId', 'gc1.ancestorId <> gc2.ancestorId')
            )
            ->andWhere($qb->expr()->isNull('gc2.ancestorId'))
            ->andWhere($alias . '.organization = :organization')
            ->setParameter('organization', $organization);

        return $qb;
    }

    /**
     * @param Organization $organization
     * @param string       $userID
     *
     * @return QueryBuilder
     */
    public function createRootsAdminQueryBuilder(Organization $organization, $userID)
    {
        // DQL implementation of the following SQL query:
        //
        //    SELECT grp.id, grp.name
        //    FROM groups AS grp
        //    INNER JOIN group_closure AS gc1
        //            ON gc1.ancestor_id = grp.id
        //    LEFT JOIN group_closure AS gc2
        //            ON gc2.descendant_id = gc1.descendant_id AND gc2.ancestor_id <> gc1.ancestor_id
        //    INNER JOIN registration_invites
        //            ON grp.id = registration_invites.group_id
        //    WHERE gc2.ancestor_id IS NULL;
        //
        $qb = $this->createQueryBuilder();
        $alias = $qb->getRootAliases()[0];

        $entity = 'AppBundle\Entity\GroupClosure';
        $entityRegInvite = 'AppBundle\Entity\RegistrationInvite';
        $qb
            ->innerJoin($entity, 'gc1', Join::WITH, $alias . '.id = gc1.ancestorId')
            ->innerJoin($entityRegInvite, 'gc3', Join::WITH, $alias . '.id = gc3.group')
            ->leftJoin(
                $entity,
                'gc2',
                Join::WITH,
                $qb->expr()->andX('gc1.descendantId = gc2.descendantId', 'gc1.ancestorId <> gc2.ancestorId')
            )
            ->andWhere($qb->expr()->isNull('gc2.ancestorId'))
            ->andWhere($alias . '.organization = :organization')
            ->andWhere('gc3' . '.user = :userID')
            ->setParameter('organization', $organization)
            ->setParameter('userID', $userID);

        return $qb;
    }

    /**
     * @param Organization $organization
     *
     * @return Group[]
     */
    public function findRoots(Organization $organization)
    {
        $qb = $this->createRootsQueryBuilder($organization);

        return $qb->getQuery()->getResult();
    }

    /**
     * @param Organization $organization
     * @param string       $userID
     *
     * @return Group[]
     */
    public function findRootsGroup(Organization $organization, $userID)
    {
        $qb = $this->createRootsAdminQueryBuilder($organization, $userID);

        return $qb->getQuery()->getResult();
    }

    /**
     * @param QueryBuilder|ProxyQuery $qb
     * @param string                  $sortBy
     * @param string                  $sortOrder
     *
     * @return void
     */
    public function correctAdminQueryBuilderSorting(&$qb, $sortBy, $sortOrder)
    {
        $this->checkQueryBuilder($qb);

        switch ($sortBy) {
            case 'name': {
                $qb
                    ->addSelect(
                        'GROUP_CONCAT(DISTINCT breadcrump_grp.name, breadcrumb.ancestorId ORDER BY breadcrumb.depth DESC) AS HIDDEN breadcrumbs'
                    )
                    ->orderBy('breadcrumbs', $sortOrder);
                break;
            }
        }
    }

    /**
     * @param QueryBuilder|ProxyQuery $qb
     *
     * @return void
     */
    public function correctTreeQueryBuilderForUserCount(&$qb)
    {
        $this->checkQueryBuilder($qb);

        $alias = $qb->getRootAliases()[0];
        $permissionEntity = 'AppBundle\Entity\GroupUserPermission';
        $qb
            ->select(
                'NEW AppBundle\DataTransferObject\DTOGroup(' .
                $alias . '.id, ' . $alias . '.name, ' . $alias . '.public, ' .
                $alias . '.active, gc1.depth, COUNT(gup.id))'
            )
            ->leftJoin(
                $permissionEntity,
                'gup',
                Join::WITH,
                'breadcrumb.descendantId = gup.group AND breadcrumb.ancestorId = breadcrumb.descendantId'
            );
    }

    /**
     * @param Organization $organization
     * @param Group[]      $parentNodes
     *
     * @return QueryBuilder
     */
    public function createTreeQueryBuilder(Organization $organization, array $parentNodes)
    {
        // DQL implementation of the following SQL query:
        //
        //    SELECT
        //        grp.*,
        //        GROUP_CONCAT(breadcrump_grp.name, breadcrumb.ancestor_id ORDER BY breadcrumb.depth DESC) AS breadcrumbs
        //    FROM groups AS grp
        //        INNER JOIN group_closure AS gc1
        //            ON gc1.descendant_id = grp.id
        //        INNER JOIN group_closure AS breadcrumb
        //            ON gc1.descendant_id = breadcrumb.descendant_id
        //        INNER JOIN groups AS breadcrump_grp
        //            ON breadcrumb.ancestor_id = breadcrump_grp.id
        //    WHERE gc1.ancestor_id IN ($parentNodes) AND grp.organization_id = $organization
        //    GROUP BY gc1.descendant_id
        //    ORDER BY breadcrumbs;
        //
        // public function correctAdminQueryBuilderSorting() - completes this DQL query when the sorting by name
        //                                                     is necessary.
        //
        $qb = $this->createQueryBuilder();
        $alias = $qb->getRootAliases()[0];

        $closureEntity = 'AppBundle\Entity\GroupClosure';
        $qb
            ->select(
                'NEW AppBundle\DataTransferObject\DTOGroup(' .
                $alias . '.id, ' . $alias . '.name, ' . $alias . '.public, ' . $alias . '.active, gc1.depth)'
            )
            ->innerJoin($closureEntity, 'gc1', Join::WITH, $alias . '.id = gc1.descendantId')
            ->innerJoin($closureEntity, 'breadcrumb', Join::WITH, 'gc1.descendantId = breadcrumb.descendantId')
            ->innerJoin($this->_entityName, 'breadcrump_grp', Join::WITH, 'breadcrumb.ancestorId = breadcrump_grp.id')
            ->andWhere($alias . '.organization = :organization')
            ->andWhere($qb->expr()->in('gc1.ancestorId', ':ancestors'))
            ->groupBy('gc1.descendantId')
            ->setParameter('organization', $organization)
            ->setParameter('ancestors', $parentNodes);

        return $qb;
    }

    /**
     * @param Organization $organization
     *
     * @return QueryBuilder
     */
    public function createOrganizationAdminQueryBuilder(Organization $organization)
    {
        $roots = $this->findRoots($organization);

        $qb = $this->createTreeQueryBuilder($organization, $roots);

        return $qb;
    }

    /**
     * @param Organization $organization
     * @param string       $userID
     *
     * @return QueryBuilder
     */
    public function createGroupAdminIDQueryBuilder(Organization $organization, $userID)
    {
        $roots = $this->findRootsGroup($organization, $userID);

        $qb = $this->createTreeQueryBuilder($organization, $roots);

        return $qb;
    }

    /**
     * @param QueryBuilder|ProxyQuery $qb
     * @param string                  $sortBy
     * @param string                  $sortOrder
     *
     * @return void
     */
    public function correctUserQueryBuilderSorting(&$qb, $sortBy, $sortOrder)
    {
        $this->checkQueryBuilder($qb);

        $alias = $qb->getRootAliases()[0];
        switch ($sortBy) {
            case 'group_role': {
                $aliasGUP = 'GroupUserPermissions';
                $aliasR = 'Roles';

                $needJoin = true;
                $dqlPart = $qb->getDQLPart('join');
                if (array_key_exists($alias, $dqlPart)) {
                    $dqlPart = $dqlPart[$alias];

                    /** @var Expr\Join $join */
                    foreach ($dqlPart as $join) {
                        if ($join->getJoin() === $alias . '.groupUserPermissions') {
                            if ($join->getJoinType() === 'INNER') {
                                $aliasGUP = $join->getAlias();
                                $needJoin = false;
                                break;
                            }
                        }
                    }
                }

                if ($needJoin) {
                    $qb->innerJoin($alias . '.groupUserPermissions', $aliasGUP);
                }
                $qb
                    ->addSelect($aliasR . '.name AS HIDDEN group_role_order')
                    ->innerJoin($aliasGUP . '.role', $aliasR)
                    ->addOrderBy('group_role_order', $sortOrder)
                    ->addOrderBy($alias . '.name', 'ASC');
                break;
            }
        }
    }

    /**
     * @param integer[] $ids
     *
     * @return Group[]
     */
    public function findByIds($ids)
    {
        $ids = (array) $ids;

        return $this->findBy(['id' => $ids]);
    }

    /**
     * @param Organization $organization
     *
     * @return DTOGroup[]
     */
    public function findByOrganization(Organization $organization)
    {
        $qb = $this->createOrganizationAdminQueryBuilder($organization);
        $this->correctAdminQueryBuilderSorting($qb, 'name', 'ASC');

        return $qb->getQuery()->getResult();
    }

    /**
     * @param Organization $organization
     *
     * @return DTOGroup[]
     */
    public function findActiveByOrganization(Organization $organization)
    {
        $qb = $this->createOrganizationAdminQueryBuilder($organization);
        $alias = $qb->getRootAliases()[0];
        $qb
            ->andWhere($alias . '.active = :active')
            ->setParameter('active', 1);
        $this->correctAdminQueryBuilderSorting($qb, 'name', 'ASC');

        return $qb->getQuery()->getResult();
    }

    /**
     * @param User         $user
     * @param Organization $organization
     *
     * @return Group[]
     */
    public function findByGroupAdmin(User $user, Organization $organization)
    {
        $qb = $this->createGroupAdminQueryBuilder($user, $organization);
        $this->correctAdminQueryBuilderSorting($qb, 'name', 'ASC');

        return $qb->getQuery()->getResult();
    }

    /**
     * @param User         $user
     * @param Organization $organization
     *
     * @return Group[]
     */
    public function findUserGroupsByOrganization(User $user, Organization $organization)
    {
        $qb = $this->createQueryBuilder();

        $qb
            ->innerJoin('Groups.groupUserPermissions', 'GroupUserPermission')
            ->andWhere('Groups.organization = :organization')
            ->andWhere('GroupUserPermission.user = :user')
            ->setParameter('organization', $organization)
            ->setParameter('user', $user);

        return $qb->getQuery()->getResult();
    }

    /**
     * @param string       $search_group  - If this string is empty then select all groups for $organization
     * @param User         $user
     * @param Organization $organization
     * @param integer[]    $ids           - Identifiers of the chosen groups. They are selected independently of $search_group value
     * @param boolean      $sortDirection
     *
     * @return array
     */
    public function findSearchable($search_group, User $user, Organization $organization, $ids, $sortDirection)
    {
        $alias = 'Groups';
        $qb = $this->_em->createQueryBuilder();

        // Selected groups must be at the top of the list.
        if (count($ids) > 0) {
            $qb
                ->addSelect(
                    '(CASE WHEN ' . $alias . '.id IN (' . implode(',', $ids) . ') THEN 0 ELSE 1 END) AS HIDDEN selected'
                )
                ->addOrderBy('selected');
        }

        $qb
            ->addSelect($alias . '.id')
            ->addSelect($alias . '.name')
            ->from($this->_entityName, $alias)
            ->andWhere($alias . '.public = :public')
            ->andWhere($alias . '.active = :active')
            ->andWhere($alias . '.organization = :organization')
            ->addOrderBy($alias . '.name', $sortDirection ? 'ASC' : 'DESC')
            ->setParameter('organization', $organization)
            ->setParameter('public', 1)
            ->setParameter('active', 1);

        if ($search_group) {
            $or = $qb->expr()->orX();
            $or->add($qb->expr()->like($alias . '.name', ':name'));
            $qb->setParameter('name', '%' . $search_group . '%');
            if (count($ids) > 0) {
                $or->add($qb->expr()->in($alias . '.id', $ids));
            }
            $qb->andWhere($or);
        }

        $aliasJoinP = 'Permissions';
        $qb
            ->addSelect('(CASE WHEN ' . $aliasJoinP . '.user IS NULL THEN 0 ELSE 1 END) AS registered')
            ->leftJoin(
                $alias . '.groupUserPermissions',
                $aliasJoinP,
                Expr\Join::WITH,
                $qb->expr()->eq($aliasJoinP . '.user', ':user')
            )
            ->setParameter('user', $user);

        $aliasJoinR = 'Requests';
        $and = $qb->expr()->andX();
        $and->add($qb->expr()->eq($aliasJoinR . '.user', ':userR'));
        $and->add($qb->expr()->eq($aliasJoinR . '.status', ':statusR'));
        $qb
            ->addSelect('(CASE WHEN ' . $aliasJoinR . '.user IS NULL THEN 0 ELSE 1 END) AS has_request')
            ->leftJoin($alias . '.registrationRequests', $aliasJoinR, Expr\Join::WITH, $and)
            ->setParameter('userR', $user)
            ->setParameter('statusR', RegistrationRequest::STATUS_NEW);

        return $qb->getQuery()->getArrayResult();
    }

    /**
     * @param $qb
     */
    public function checkQueryBuilder($qb)
    {
        if (!($qb instanceof QueryBuilder) && !($qb instanceof ProxyQuery)) {
            throw new \InvalidArgumentException(
                __METHOD__ . ': The qb parameter must be an instance of QueryBuilder or ProxyQuery!'
            );
        }
    }

    /**
     * @param GroupManager $groupManager
     *
     * @return GroupRepository
     */
    public function setGroupManager($groupManager)
    {
        $this->groupManager = $groupManager;

        return $this;
    }

    /**
     * @param GroupClosureRepository $groupClosureRepository
     *
     * @return GroupRepository
     */
    public function setGroupClosureRepository($groupClosureRepository)
    {
        $this->groupClosureRepository = $groupClosureRepository;

        return $this;
    }
}
