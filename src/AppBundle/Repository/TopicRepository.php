<?php

namespace AppBundle\Repository;

use Aristek\Component\ORM\EntityRepository;
use AppBundle\DataTransferObject\DTOGroup;
use AppBundle\Entity\Group;
use AppBundle\Entity\Organization;
use AppBundle\Entity\Role;
use AppBundle\Entity\Topic;
use AppBundle\Entity\User;
use Doctrine\ORM\Query\Expr;
use Doctrine\ORM\Query\Expr\Join;
use Doctrine\ORM\QueryBuilder;
use Sonata\DoctrineORMAdminBundle\Datagrid\ProxyQuery;

/**
 * Class TopicRepository
 * @package AppBundle\Repository
 *
 * @method Topic findOneBy(array $criteria, array $orderBy = null)
 * @method Topic find($id, $lockMode = null, $lockVersion = null)
 * @method Topic[] findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class TopicRepository extends EntityRepository
{
    const ALIAS = 'Topic';
    const ALIAS_POST = 'post';
    const CLOSURE_NAME = 'AppBundle\Entity\GroupClosure';
    const PERMISSION_NAME = 'AppBundle\Entity\GroupUserPermission';

    /**
     * @param string $alias
     *
     * @return string
     */
    private function resolveAlias($alias)
    {
        return null === $alias ? $this::ALIAS : $alias;
    }

    /**
     * @param string|null $alias
     * @param string|null $indexBy
     *
     * @return QueryBuilder
     */
    public function createQueryBuilder($alias = null, $indexBy = null)
    {
        $alias = $this->resolveAlias($alias);

        return parent::createQueryBuilder($alias, $indexBy);
    }

    public function createDeleteQueryBuilder($alias = null)
    {
        $alias = $this->resolveAlias($alias);
        $qb = $this->_em->createQueryBuilder();
        $qb->delete($this->getClassName(), $alias);

        return $qb;
    }

    /**
     * @param Organization $organization
     * @param boolean      $returnDTOs
     *
     * @return QueryBuilder
     */
    public function createQueryBuilderForOrganizationAdmin(Organization $organization, $returnDTOs)
    {
        $qb = $this->createQueryBuilder();
        $alias = $qb->getRootAliases()[0];
        if ($returnDTOs) {
            $qb
                ->select(
                    'NEW AppBundle\DataTransferObject\DTOTopic(' .
                    $alias . '.id, ' . $alias . '.name, ' . $alias . '.description, ' . $alias . '.createdAt, ' .
                    $alias . '.opened, ' . $alias . '.active, ' .
                    $qb->expr()->countDistinct($this::ALIAS_POST . '.id') . ', ' .
                    $qb->expr()->max($this::ALIAS_POST . '.dateTime') . ')'
                )
                ->leftJoin($alias . '.posts', $this::ALIAS_POST)
                ->groupBy($alias . '.id');
        }
        $qb
            ->where($alias . '.organization = :organization')
            ->setParameter('organization', $organization);

        return $qb;
    }

    /**
     * @param Organization $organization
     * @param User         $user
     * @param boolean      $returnDTOs
     * @param boolean      $onlyAdminAccess - true if selecting only topics witch group
     *                                      participants the $user is admin for.
     *                                      false if selecting all topics witch group
     *                                      participants the $user is admin for or is usual
     *                                      member of.
     *
     * @return QueryBuilder
     */
    public function createQueryBuilderForGroupAdmin(
        Organization $organization,
        User $user,
        $returnDTOs,
        $onlyAdminAccess)
    {
        // DQL implementation of the following SQL query:
        /*
            SELECT
                t.id,
                t.name,
                t.opened,
                t.active,
                COUNT(DISTINCT p.id),
                MAX(p.date_time)
            FROM
                topics t
                LEFT JOIN posts p ON p.topic_id = t.id
                INNER JOIN topic_group_permissions tgp ON t.id = tgp.topic_id
                INNER JOIN groups g ON tgp.group_id = g.id
                INNER JOIN group_closure gc ON (gc.descendant_id = tgp.group_id)
                INNER JOIN group_user_permissions gup ON (gup.group_id = gc.ancestor_id)
                INNER JOIN groups g_parent ON gup.group_id = g_parent.id
                INNER JOIN roles r ON gup.role_id = r.id
            WHERE
                t.organization_id = :organization AND
                g.organization_id = :organization AND
                g.active = 1 AND
                g_parent.active = 1 AND
                gup.user_id = :user AND
                (
                    r.code = 'ROLE_GROUP_ADMIN' OR
                    (gc.ancestor_id = gc.descendant_id AND t.active = 1)
                )
            GROUP BY t.id
         */

        $qb = $this->createQueryBuilder();
        $alias = $qb->getRootAliases()[0];
        $aliasTGP = 'topicGroupPermission';
        $aliasG = 'groups';
        $aliasParentG = 'parent_groups';
        $aliasGC = 'groupClosure';
        $aliasGUP = 'groupUserPermission';
        $aliasR = 'roles';
        if ($returnDTOs) {
            $qb
                ->select(
                    'NEW AppBundle\DataTransferObject\DTOTopic(' .
                    $alias . '.id, ' . $alias . '.name, ' . $alias . '.description, ' . $alias . '.createdAt, ' .
                    $alias . '.opened, ' . $alias . '.active, ' .
                    $qb->expr()->countDistinct($this::ALIAS_POST . '.id') . ', ' .
                    $qb->expr()->max($this::ALIAS_POST . '.dateTime') . ')'
                )
                ->leftJoin($alias . '.posts', $this::ALIAS_POST);
        }
        $qb
            ->innerJoin($alias . '.topicGroupPermissions', $aliasTGP)
            ->innerJoin($aliasTGP . '.group', $aliasG)
            ->innerJoin(
                $this::CLOSURE_NAME,
                $aliasGC,
                Join::WITH,
                $aliasGC . '.descendantId = ' . $aliasTGP . '.group'
            )
            ->innerJoin(
                $this::PERMISSION_NAME,
                $aliasGUP,
                Join::WITH,
                $aliasGUP . ".group = " . $aliasGC . '.ancestorId'
            )
            ->innerJoin($aliasGUP . '.group', $aliasParentG)
            ->innerJoin($aliasGUP . '.role', $aliasR)
            ->where($alias . '.organization = :organization')
            ->andWhere($aliasG . '.organization = :organization')
            ->andWhere($aliasG . '.active = :group_active')
            ->andWhere($aliasParentG . '.active = :parent_group_active')
            ->andWhere($aliasGUP . '.user = :user');

        if ($onlyAdminAccess) {
            $qb
                ->andWhere($aliasR . '.code = :code');
        } else {
            $qb
                ->andWhere(
                    $qb->expr()->orX(
                        $aliasR . '.code = :code',
                        $qb->expr()->andX(
                            $aliasGC . '.ancestorId = ' . $aliasGC . '.descendantId',
                            $alias . '.active = :active'
                        )
                    )
                )
                ->setParameter('active', 1);
        }

        $qb
            ->groupBy($alias . '.id')
            ->setParameter('group_active', 1)
            ->setParameter('parent_group_active', 1)
            ->setParameter('organization', $organization)
            ->setParameter('code', Role::ROLE_GROUP_ADMIN)
            ->setParameter('user', $user);

        return $qb;
    }

    /**
     * @param QueryBuilder                                        $qb
     * @param DTOGroup|Group|integer|DTOGroup[]|Group[]|integer[] $groups
     */
    public function correctQueryBuilderGroupsFilter(&$qb, $groups)
    {
        $this->checkQueryBuilder($qb);

        $alias = $qb->getRootAliases()[0];
        $aliasTGP = 'TopicGroupPermissions';

        $needJoin = true;
        $dqlPart = $qb->getDQLPart('join');
        if (array_key_exists($alias, $dqlPart)) {
            $dqlPart = $dqlPart[$alias];

            /** @var Expr\Join $join */
            foreach ($dqlPart as $join) {
                if ($join->getJoin() === $alias . '.topicGroupPermissions') {
                    if ($join->getJoinType() === 'INNER') {
                        $aliasTGP = $join->getAlias();
                        $needJoin = false;
                        break;
                    }
                }
            }
        }
        if ($needJoin) {
            $qb->innerJoin($alias . '.topicGroupPermissions', $aliasTGP);
        }
        if (!is_array($groups)) {
            $groups = [$groups];
        }
        if (reset($groups) instanceof DTOGroup) {
            foreach ($groups as $index => $group) {
                $groups[$index] = $group->getId();
            }
        }
        $qb
            ->andWhere($qb->expr()->in($aliasTGP . '.group', ':groups'))
            ->setParameter('groups', $groups);
    }

    /**
     * @param QueryBuilder|ProxyQuery $qb
     * @param string                  $sortBy
     * @param string                  $sortOrder
     *
     * @return void
     */
    public function correctQueryBuilderSorting(&$qb, $sortBy, $sortOrder)
    {
        $this->checkQueryBuilder($qb);

        $alias = $qb->getRootAliases()[0];
        switch ($sortBy) {
            case 'post_quantity': {
                $qb
                    ->addSelect(
                        $qb->expr()->countDistinct($this::ALIAS_POST . '.id') . ' AS HIDDEN post_quantity_order'
                    )
                    ->addOrderBy('post_quantity_order', $sortOrder)
                    ->addOrderBy($alias . '.name', 'ASC');
                break;
            }

            case 'last_post': {
                $qb
                    ->addSelect(
                        $qb->expr()->max($this::ALIAS_POST . '.dateTime') . ' AS HIDDEN last_post_order'
                    )
                    ->addOrderBy('last_post_order', $sortOrder)
                    ->addOrderBy($alias . '.name', 'ASC');
                break;
            }
        }
    }

    /**
     * @param $qb
     */
    public function checkQueryBuilder($qb)
    {
        if (!($qb instanceof QueryBuilder) && !($qb instanceof ProxyQuery)) {
            throw new \InvalidArgumentException(
                __METHOD__ . ': The qb parameter must be an instance of QueryBuilder or ProxyQuery!'
            );
        }
    }
}
