<?php

namespace AppBundle\Repository;

use Aristek\Component\ORM\EntityRepository;
use AppBundle\Entity\Group;
use AppBundle\Entity\Organization;
use AppBundle\Entity\RegistrationRequest;
use AppBundle\Entity\Role;
use AppBundle\Entity\User;
use Doctrine\ORM\QueryBuilder;
use Doctrine\ORM\NoResultException;
use Doctrine\ORM\NonUniqueResultException;
use Sonata\DoctrineORMAdminBundle\Datagrid\ProxyQuery;

/**
 * Class RegistrationRequestRepository
 * @package AppBundle\Repository
 *
 * @method RegistrationRequest findOneBy(array $criteria, array $orderBy = null)
 * @method RegistrationRequest find($id, $lockMode = null, $lockVersion = null)
 */
class RegistrationRequestRepository extends EntityRepository
{
    /**
     * @param string $alias
     * @param string $indexBy
     *
     * @return QueryBuilder
     */
    public function createQueryBuilder($alias = 'RegistrationRequest', $indexBy = null)
    {
        return parent::createQueryBuilder($alias, $indexBy);
    }

    /**
     * @param User $user
     *
     * @return QueryBuilder
     */
    public function createUserQueryBuilder($user)
    {
        $qb = $this->createQueryBuilder('RegistrationRequest');
        $qb
            ->andWhere('RegistrationRequest.user = :user_id')
            ->setParameter('user_id', $user->getId());

        return $qb;
    }

    /**
     * @param User         $user
     * @param Organization $organization
     *
     * @return QueryBuilder
     */
    public function createGroupAdminQueryBuilder(User $user, Organization $organization)
    {
        $qb = $this->createQueryBuilder();
        $alias = $qb->getRootAliases()[0];
        $aliasG = 'Groups';
        $aliasGUP = 'GroupUserPermission';
        $aliasR = 'Role';

        $qb
            ->innerJoin($alias . '.group', $aliasG)
            ->innerJoin($aliasG . '.groupUserPermissions', $aliasGUP)
            ->innerJoin($aliasGUP . '.role', $aliasR)
            ->andWhere($aliasG . '.organization = :organization')
            ->andWhere($aliasGUP . '.user = :user')
            ->andWhere($aliasR . '.code = :code')
            ->setParameter('organization', $organization)
            ->setParameter('user', $user)
            ->setParameter('code', Role::ROLE_GROUP_ADMIN);

        return $qb;
    }

    /**
     * @param User         $user
     * @param Organization $organization
     *
     * @return QueryBuilder
     */
    public function createOrganizationAdminQueryBuilder(User $user, Organization $organization)
    {
        $qb = $this->createQueryBuilder();
        $alias = $qb->getRootAliases()[0];
        $aliasO = 'Organization';
        $aliasOUP = 'OrganizationUserPermission';
        $aliasR = 'Role';

        $qb
            ->innerJoin($alias . '.organization', $aliasO)
            ->innerJoin($aliasO . '.organizationUserPermissions', $aliasOUP)
            ->innerJoin($aliasOUP . '.role', $aliasR)
            ->andWhere($alias . '.organization = :organization')
            ->andWhere($aliasOUP . '.user = :user')
            ->andWhere($aliasR . '.code = :code')
            ->setParameter('organization', $organization)
            ->setParameter('user', $user)
            ->setParameter('code', Role::ROLE_ORGANIZATION_ADMIN);

        return $qb;
    }

    /**
     * @param QueryBuilder|ProxyQuery $qb
     * @param                         $sortBy
     * @param                         $sortOrder
     *
     * @return void
     */
    public function correctQueryBuilderSorting(&$qb, $sortBy, $sortOrder)
    {
        if (!($qb instanceof QueryBuilder) && !($qb instanceof ProxyQuery)) {
            throw new \InvalidArgumentException(
                __METHOD__ . ': The qb parameter must be an instance of QueryBuilder or ProxyQuery!'
            );
        }
        $alias = $qb->getRootAliases()[0];
        switch ($sortBy) {
            case 'status': {
                $qb
                    ->addSelect(
                        '(CASE ' . $alias . '.status ' .
                        'WHEN \'' . RegistrationRequest::STATUS_NEW . '\' THEN 1 ' .
                        'WHEN \'' . RegistrationRequest::STATUS_APPROVED . '\' THEN 2 ' .
                        'WHEN \'' . RegistrationRequest::STATUS_DECLINED . '\' THEN 3 ' .
                        'ELSE 4 END) AS HIDDEN status_order'
                    )
                    ->addOrderBy('status_order', $sortOrder)
                    ->addOrderBy($alias . '.createdAt', 'DESC');
                break;
            }

            case 'user.fullName': {
                $aliasU = 'user';
                $qb
                    ->addSelect(
                        'CONCAT(' . $aliasU . '.firstName, ' .
                        'CONCAT(\' \', ' . $aliasU . '.lastName)) AS HIDDEN full_name_order'
                    )
                    ->innerJoin($alias . '.user', $aliasU)
                    ->addOrderBy('full_name_order', $sortOrder)
                    ->addOrderBy($alias . '.createdAt', 'DESC');
                break;
            }
        }
    }

    /**
     * @param User $user
     *
     * @return QueryBuilder
     */
    public function createAdminQueryBuilder($user)
    {
        $qb = $this->createQueryBuilder('RegistrationRequest');
        $qb
            ->innerJoin('RegistrationRequest.organization', 'Organization')
            ->innerJoin('Organization.organizationUserPermissions', 'OrganizationUserPermission')
            ->innerJoin('OrganizationUserPermission.role', 'Role')
            ->leftJoin('RegistrationRequest.group', 'Groups')
            ->andWhere(
                $qb->expr()->orX(
                    'OrganizationUserPermission.user = :user_id AND Role.code = :orgCode',
                    'Groups.id in (:groups)',
                    'RegistrationRequest.user = :user_id'
                )
            )
            ->setParameter('user_id', $user->getId())
            ->setParameter('groups', $user->getAdminGroups())
            ->setParameter('orgCode', Role::ROLE_ORGANIZATION_ADMIN)
            ->addOrderBy('RegistrationRequest.createdAt', 'ASC');

        return $qb;
    }

    /**
     * @param RegistrationRequest $registrationRequest
     * @param User                $user
     *
     * @return bool
     */
    public function checkAccess(RegistrationRequest $registrationRequest, User $user)
    {
        $qb = $this->createAdminQueryBuilder($user);

        $qb
            ->andWhere('RegistrationRequest.id = :id')
            ->setParameter('id', $registrationRequest->getId());

        return count($qb->getQuery()->getScalarResult()) ? true : false;
    }

    /**
     * @param User         $user
     * @param Organization $organization
     * @param Group        $group
     *
     * If $group is set then this function looks for the group registration request existence.
     * If $organization is set then this function looks for the organization registration request existence and
     * ignores the $group parameter.
     * If no $group, nor $organization are set then this function looks for any registration request existence.
     *
     * @return bool
     */
    public function checkExists($user, Organization $organization = null, Group $group = null)
    {
        $qb = $this->createQueryBuilder();
        $alias = $qb->getRootAliases()[0];
        $qb
            ->select($alias . '.id')
            ->andWhere($alias . '.user = :user')
            ->andWhere($alias . '.status = :status')
            ->setParameter('status', RegistrationRequest::STATUS_NEW)
            ->setParameter('user', $user)
            ->setMaxResults(1);

        if (null !== $group) {
            $qb
                ->andWhere($alias . '.group = :group')
                ->setParameter('group', $group);
        } else {
            if (null !== $organization) {
                $and = $qb->expr()->andX();
                $and->add($alias . '.organization = :organization');
                $and->add($qb->expr()->isNull($alias . '.group'));
                $qb
                    ->andWhere($and)
                    ->setParameter('organization', $organization);
            }
        }

        $result = null;
        try {
            $result = $qb->getQuery()->getSingleScalarResult();
        } catch (NoResultException $e) {
            $result = null;
        } catch (NonUniqueResultException $e) {
            $result = 0;
        }

        return null !== $result;
    }

    /**
     * @param User         $user
     * @param Organization $organization
     * @param Group        $group
     * @param string       $status
     *
     * @return QueryBuilder
     */
    public function createUpdateQueryBuilder($status, User $user, Organization $organization, Group $group = null)
    {
        $alias = 'RegistrationRequest';
        $qb = $this->_em->createQueryBuilder();
        $qb
            ->update($this->getEntityName(), $alias)
            ->set($alias . '.status', "'" . $status . "'")
            ->andWhere($qb->expr()->eq($alias . '.status', ':status'))
            ->andWhere($qb->expr()->eq($alias . '.user', ':user'))
            ->setParameter('status', RegistrationRequest::STATUS_NEW)
            ->setParameter('user', $user);

        if (null === $group) {
            if (null === $organization) {
                throw new \InvalidArgumentException(
                    __METHOD__ . ': The organization or the group must be specified!'
                );
            }
            $qb
                ->andWhere($qb->expr()->eq($alias . '.organization', ':organization'))
                ->andWhere($qb->expr()->isNull($alias . '.group'))
                ->setParameter('organization', $organization);
        } else {
            $qb
                ->set($alias . '.organization', $group->getOrganization()->getId())
                ->andWhere($qb->expr()->eq($alias . '.group', ':group'))
                ->setParameter('group', $group);
        }

        return $qb;
    }
}
