<?php

namespace AppBundle\Repository;

use AppBundle\Entity\Organization;
use Aristek\Component\ORM\EntityRepository;
use AppBundle\Entity\Event;
use Doctrine\ORM\QueryBuilder;
use Doctrine\ORM\Query\Expr\Join;

/**
 * Class EventRepository
 * @package AppBundle\Repository
 *
 * @method Event findOneBy(array $criteria, array $orderBy = null)
 * @method Event find($id, $lockMode = null, $lockVersion = null)
 */
class EventRepository extends EntityRepository
{
    /**
     * @param string $alias
     * @param string $indexBy
     *
     * @return QueryBuilder
     */
    public function createQueryBuilder($alias = 'Event', $indexBy = null)
    {
        return parent::createQueryBuilder($alias, $indexBy);
    }

    /**
     * @param int $eventId
     *
     * @return \Doctrine\ORM\QueryBuilder
     */
    public function createOrganizationQueryBuilder($eventId)
    {
        $query = $this->createQueryBuilder();
        $query
            ->innerJoin('Event.eventRelations', 'r')
            ->innerJoin('r.event', 'e')
            ->andWhere('e.id = :id');
        $query->setParameter('id', $eventId);

        return $query;
    }

    /**
     * @param \DateTime    $periodStart
     * @param \DateTime    $periodFinish
     * @param Organization $organization
     * @param array        $massGroup
     *
     * @return \Doctrine\ORM\QueryBuilder
     */
    public function findEventsPeriodList($periodStart, $periodFinish, $organization, $massGroup)
    {
        $query = $this->createQueryBuilder('e');
        $query
            ->innerJoin('e.eventRelations', 'r')
            ->where('e.timeStart BETWEEN :period_start AND :period_end')
            ->andWhere('r.organization = :organization')
            ->andwhere('r.group IN (:grouparray) OR r.group IS NULL')
            ->setParameter('grouparray', $massGroup)
            ->setParameter('period_start', $periodStart)
            ->setParameter('period_end', $periodFinish)
            ->setParameter('organization', $organization);

        return $query->getQuery()->getArrayResult();
    }

    /**
     * @param \DateTime    $periodStart
     * @param \DateTime    $periodFinish
     * @param Organization $organization
     *
     * @return \Doctrine\ORM\QueryBuilder
     */
    public function findEventsPeriodListOrgAdmin($periodStart, $periodFinish, $organization)
    {
        $query = $this->createQueryBuilder('e');
        $query
            ->innerJoin('e.eventRelations', 'r')
            ->where('e.timeStart BETWEEN :period_start AND :period_end')
            ->andWhere('r.organization = :organization')
            ->setParameter('period_start', $periodStart)
            ->setParameter('period_end', $periodFinish)
            ->setParameter('organization', $organization);

        return $query->getQuery()->getArrayResult();
    }

    /**
     * @param Organization $organization
     *
     * @return QueryBuilder
     */
    public function createOrganizationAdminQueryBuilder(Organization $organization)
    {
        $roots = $this->findRoots($organization);

        return $roots;
    }

    /**
     * @param Organization $organization
     *
     * @return Event[]
     */
    public function findRoots(Organization $organization)
    {
        $qb = $this->createRootsQueryBuilder($organization);

        return $qb;
    }

    /**
     * @param Organization $organization
     *
     * @return QueryBuilder
     */
    public function createRootsQueryBuilder(Organization $organization)
    {
        $entity = 'AppBundle\Entity\EventRelation';

        $qb = $this->createQueryBuilder();
        $alias = $qb->getRootAliases()[0];

        $qb
            ->innerJoin($entity, 'gc1', Join::WITH, $alias . '.id = gc1.event')
            ->Where('gc1.organization = :organization')
            ->setParameter('organization', $organization);

        return $qb;
    }
}
