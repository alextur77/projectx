<?php

namespace AppBundle\Repository;

use Aristek\Component\ORM\EntityRepository;
use AppBundle\Entity\EventRelation;

/**
 * Class EventRelationRepository
 * @package AppBundle\Repository
 *
 * @method EventRelation findOneBy(array $criteria, array $orderBy = null)
 * @method EventRelation find($event, $lockMode = null, $lockVersion = null)
 */
class EventRelationRepository extends EntityRepository
{

}
