<?php

namespace AppBundle\Repository;

use Aristek\Component\ORM\EntityRepository;
use AppBundle\Entity\Post;
use Doctrine\ORM\QueryBuilder;

/**
 * Class PostRepository
 * @package AppBundle\Repository
 *
 * @method Post findOneBy(array $criteria, array $orderBy = null)
 * @method Post find($id, $lockMode = null, $lockVersion = null)
 * @method Post[] findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PostRepository extends EntityRepository
{
    const ALIAS = 'Post';

    /**
     * @param string $alias
     *
     * @return string
     */
    private function resolveAlias($alias)
    {
        return null === $alias ? $this::ALIAS : $alias;
    }

    /**
     * @param string|null $alias
     * @param string|null $indexBy
     *
     * @return QueryBuilder
     */
    public function createQueryBuilder($alias = null, $indexBy = null)
    {
        $alias = $this->resolveAlias($alias);

        return parent::createQueryBuilder($alias, $indexBy);
    }

    /**
     * @return array
     */
    public function findPostPeriodList()
    {
        $date = date('Y-m-d h:i:s');
        $query = $this->createQueryBuilder('e');
        $query
            ->select('e', 'r')
            ->innerJoin('e.topic', 'r')
            ->where('e.dateTime >= :datetime')
            ->setParameter('datetime', $date);

        return $query->getQuery()->getArrayResult();
    }

    /**
     * @param string $topicIds
     *
     * @return array
     */
    public function getTopicFromNotReadPost($topicIds)
    {
        $query = $this->createQueryBuilder('e');

        $query
            ->andWhere('e.topic IN (' . $topicIds . ')');

        return $query->getQuery()->getArrayResult();
    }
}
