<?php

namespace AppBundle\Repository;

use Aristek\Component\ORM\EntityRepository;
use AppBundle\DataTransferObject\DTOGroup;
use AppBundle\Entity\Group;
use AppBundle\Entity\GroupClosure;
use Doctrine\ORM\AbstractQuery;
use Doctrine\ORM\QueryBuilder;

/**
 * Class GroupClosureRepository
 * @package AppBundle\Repository
 *
 * @method GroupClosure findOneBy(array $criteria, array $orderBy = null)
 */
class GroupClosureRepository extends EntityRepository
{
    const ALIAS = 'GroupClosures';

    /**
     * @param string $alias
     *
     * @return string
     */
    private function resolveAlias($alias)
    {
        return null === $alias ? $this::ALIAS : $alias;
    }

    /**
     * @param string|null $alias
     * @param string|null $indexBy
     *
     * @return QueryBuilder
     */
    public function createQueryBuilder($alias = null, $indexBy = null)
    {
        $alias = $this->resolveAlias($alias);

        return parent::createQueryBuilder($alias, $indexBy);
    }

    /**
     * @param integer|Group|DTOGroup $groupId - Group or groupId witch parent is being looked for
     *
     * @return QueryBuilder
     */
    public function createParentQueryBuilder($groupId)
    {
        if (($groupId instanceof Group) or ($groupId instanceof DTOGroup)) {
            $groupId = $groupId->getId();
        }

        $qb = $this->createQueryBuilder();
        $alias = $qb->getRootAliases()[0];

        $qb
            ->andWhere($alias . '.descendantId = :groupId')
            ->andWhere($alias . '.depth = 1')
            ->setParameter('groupId', $groupId);

        return $qb;
    }

    /**
     * @param integer|Group|DTOGroup $groupId - Group or groupId witch parent is being looked for
     *
     * @return GroupClosure|null
     */
    public function findParent($groupId)
    {
        $qb = $this->createParentQueryBuilder($groupId);

        return $qb->getQuery()->getOneOrNullResult(AbstractQuery::HYDRATE_OBJECT);
    }

    /**
     * @param integer|Group|DTOGroup $groupId - Group or groupId witch parent is being looked for
     *
     * @return integer|null
     */
    public function findParentId($groupId)
    {
        $qb = $this->createParentQueryBuilder($groupId);
        $alias = $qb->getRootAliases()[0];
        $qb->select($alias . '.ancestorId');
        $result = $qb->getQuery()->getOneOrNullResult(AbstractQuery::HYDRATE_SINGLE_SCALAR);

        return $result;
    }

    /**
     * @param integer|Group|DTOGroup $groupId - Group or groupId witch ancestors are being looked for
     * @param bool                   $addSelf
     *
     * @return QueryBuilder
     */
    public function createAncestorQueryBuilder($groupId, $addSelf = false)
    {
        if (($groupId instanceof Group) or ($groupId instanceof DTOGroup)) {
            $groupId = $groupId->getId();
        }

        $qb = $this->createQueryBuilder();
        $alias = $qb->getRootAliases()[0];

        $qb
            ->andWhere($alias . '.descendantId = :groupId')
            ->setParameter('groupId', $groupId);
        if (!$addSelf) {
            $qb->andWhere($alias . '.depth > 0');
        }

        return $qb;
    }

    /**
     * @param integer|Group|DTOGroup $groupId - Group or groupId witch ancestors are being looked for
     * @param bool                   $addSelf
     *
     * @return integer[] - ancestor ids
     */
    public function findAncestorIds($groupId, $addSelf = false)
    {
        $qb = $this->createAncestorQueryBuilder($groupId, $addSelf);
        $alias = $qb->getRootAliases()[0];
        $qb
            ->select($alias . '.ancestorId')
            ->orderBy($alias . '.depth', 'DESC');
        $result = $qb->getQuery()->getScalarResult();
        $result = array_column($result, 'ancestorId');

        return $result;
    }

    /**
     * @param integer|Group|DTOGroup $groupId - Group or groupId witch ancestors are being looked for
     * @param bool                   $addSelf
     *
     * @return GroupClosure[]
     */
    public function findAncestors($groupId, $addSelf = false)
    {
        $qb = $this->createAncestorQueryBuilder($groupId, $addSelf);

        return $qb->getQuery()->getResult();
    }

    /**
     * @param integer|Group|DTOGroup $groupId - Group or groupId witch descendants are being looked for
     * @param bool                   $addSelf
     *
     * @return QueryBuilder
     */
    public function createDescendantQueryBuilder($groupId, $addSelf = false)
    {
        if (($groupId instanceof Group) or ($groupId instanceof DTOGroup)) {
            $groupId = $groupId->getId();
        }

        $qb = $this->createQueryBuilder();
        $alias = $qb->getRootAliases()[0];

        $qb
            ->andWhere($alias . '.ancestorId = :groupId')
            ->setParameter('groupId', $groupId);
        if (!$addSelf) {
            $qb->andWhere($alias . '.depth > 0');
        }

        return $qb;
    }

    /**
     * @param integer|Group|DTOGroup $groupId - Group or groupId witch descendants are being looked for
     * @param bool                   $addSelf
     *
     * @return integer[] - descendant ids
     */
    public function findDescendantIds($groupId, $addSelf = false)
    {
        $qb = $this->createDescendantQueryBuilder($groupId, $addSelf);
        $alias = $qb->getRootAliases()[0];
        $qb
            ->select($alias . '.descendantId')
            ->orderBy($alias . '.depth', 'ASC');

        $result = $qb->getQuery()->getScalarResult();
        $result = array_column($result, 'descendantId');

        return $result;
    }

    /**
     * @param integer|Group|DTOGroup $groupId - Group or groupId witch ancestors are being looked for
     * @param bool                   $addSelf
     *
     * @return GroupClosure[]
     */
    public function findDescendants($groupId, $addSelf = false)
    {
        $qb = $this->createDescendantQueryBuilder($groupId, $addSelf);

        return $qb->getQuery()->getResult();
    }
}
