<?php

namespace AppBundle\Repository;

use Aristek\Component\ORM\EntityRepository;
use AppBundle\Entity\Group;
use AppBundle\Entity\Organization;
use AppBundle\Entity\User;
use AppBundle\Entity\Role;
use AppBundle\Entity\GroupUserPermission;
use Doctrine\ORM\QueryBuilder;
use Doctrine\ORM\NoResultException;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\Query\Expr\Join;
use Sonata\DoctrineORMAdminBundle\Datagrid\ProxyQuery;

/**
 * Class GroupUserPermissionRepository
 * @package AppBundle\Repository
 *
 * @method GroupUserPermission findOneBy(array $criteria, array $orderBy = null)
 * @method GroupUserPermission find($id, $lockMode = null, $lockVersion = null)
 */
class GroupUserPermissionRepository extends EntityRepository
{
    const ALIAS = 'GroupUserPermission';
    const CLOSURE_NAME = 'AppBundle\Entity\GroupClosure';

    /**
     * @var OrganizationUserPermissionRepository
     */
    protected $organizationUserPermissionRepository;

    /**
     * @param string $alias
     *
     * @return string
     */
    private function resolveAlias($alias)
    {
        return null === $alias ? $this::ALIAS : $alias;
    }

    /**
     * @param string|null $alias
     * @param string|null $indexBy
     *
     * @return QueryBuilder
     */
    public function createQueryBuilder($alias = null, $indexBy = null)
    {
        $alias = $this->resolveAlias($alias);

        return parent::createQueryBuilder($alias, $indexBy);
    }

    public function createDeleteQueryBuilder($alias = null)
    {
        $alias = $this->resolveAlias($alias);
        $qb = $this->_em->createQueryBuilder();
        $qb->delete($this->getClassName(), $alias);

        return $qb;
    }

    /**
     * @param QueryBuilder|ProxyQuery $qb
     * @param                         $sortBy
     * @param                         $sortOrder
     *
     * @return void
     */
    public function correctQueryBuilderSorting(&$qb, $sortBy, $sortOrder)
    {
        if (!($qb instanceof QueryBuilder) && !($qb instanceof ProxyQuery)) {
            throw new \InvalidArgumentException(
                __METHOD__ . ': The qb parameter must be an instance of QueryBuilder or ProxyQuery!'
            );
        }
        $alias = $qb->getRootAliases()[0];
        switch ($sortBy) {
            case 'user.fullName': {
                $aliasU = 'user';
                $qb
                    ->addSelect(
                        'CONCAT(' . $aliasU . '.firstName, ' .
                        'CONCAT(\' \', ' . $aliasU . '.lastName)) AS HIDDEN full_name_order'
                    )
                    ->innerJoin($alias . '.user', $aliasU)
                    ->addOrderBy('full_name_order', $sortOrder);
                break;
            }
        }
    }

    /**
     * @param integer|integer[]|Group|Group[] $groups - ids or Groups to find users registered in them.
     *
     * @return QueryBuilder
     */
    public function createQueryBuilderByGroups($groups)
    {
        if (!is_array($groups)) {
            $groups = [$groups];
        }
        $aliasU = 'Users';
        $qb = $this->_em->createQueryBuilder();
        $qb
            ->select($aliasU . '.id')
            ->from($this->_entityName, $this::ALIAS)
            ->innerJoin($this::ALIAS . '.user', $aliasU)
            ->where($qb->expr()->in($this::ALIAS . '.group', ':groups'))
            ->groupBy($aliasU . '.id')
            ->setParameter('groups', $groups);

        return $qb;
    }

    /**
     * @param integer|integer[]|Group|Group[] $groups - ids or Groups to find users registered in them.
     *
     * @return array
     */
    public function getUserIdsByGroups($groups)
    {
        $qb = $this->createQueryBuilderByGroups($groups);
        $result = $qb->getQuery()->getResult();
        $result = array_column($result, 'id', 'id');

        return $result;
    }

    /**
     * @param integer|integer[]|Group|Group[] $groups - ids or Groups to find users registered in them.
     *
     * @return array
     */
    public function getUserFullNameByGroups($groups)
    {
        $aliasU = 'Users';
        $qb = $this->createQueryBuilderByGroups($groups);
        $qb
            ->addSelect(
                $qb->expr()->concat(
                    $qb->expr()->concat(
                        $qb->expr()->concat(
                            $aliasU . '.username',
                            '\' (\''
                        ),
                        $qb->expr()->concat(
                            $qb->expr()->concat($aliasU . '.firstName', '\' \''),
                            $aliasU . '.lastName'
                        )
                    ),
                    '\')\''
                ) . 'AS text' // The field must have name 'text' for the select2 dropdown correct usage.
            )
            ->orderBy('text', 'ASC');
        $result = $qb->getQuery()->getResult();

        return $result;
    }

    /**
     * @param integer|integer[]|Group|Group[] $groups - ids or Groups to find users registered in them.
     *
     * @return array
     */
    public function getUserListByGroups($groups)
    {
        $aliasU = 'Users';
        $qb = $this->createQueryBuilderByGroups($groups);
        $qb
            ->addSelect($aliasU . '.username')
            ->addSelect(
                $qb->expr()->concat(
                    $qb->expr()->concat($aliasU . '.firstName', '\' \''),
                    $aliasU . '.lastName'
                ) . 'AS fullName'
            )
            ->orderBy('fullName', 'ASC');
        $result = $qb->getQuery()->getResult();

        return $result;
    }

    /**
     * Calculates the specified user role in the specified group taking into account the group hierarchy.
     *
     * @param Group $group
     * @param User  $user
     *
     * @return string|null - the user role in the specified group or
     *                       null if the specified user has no access to that group
     */
    public function getUserGroupRole(Group $group, User $user)
    {
        $organization = $group->getOrganization();
        $role = $this->organizationUserPermissionRepository->getUserRoleInOrganization($user, $organization);
        if ($role === Role::ROLE_ORGANIZATION_ADMIN) {
            $role = Role::ROLE_GROUP_ADMIN;
        } else {
            if (($role === Role::ROLE_ORGANIZATION_USER) or (null === $role)) {
                // $user has Role::ROLE_ORGANIZATION_USER role in $organization or have not any role at all:
                $qb = $this->createQueryBuilder();
                $alias = $qb->getRootAliases()[0];
                $aliasTree = 'Closure';
                $aliasR = 'Roles';
                $aliasG = 'Groups';

                $qb
                    ->select($aliasR . '.code')
                    ->innerJoin(
                        $this::CLOSURE_NAME,
                        $aliasTree,
                        Join::WITH,
                        $alias . '.group = ' . $aliasTree . '.ancestorId'
                    )
                    ->innerJoin($alias . '.role', $aliasR)
                    ->innerJoin($alias . '.group', $aliasG)
                    ->andWhere($aliasTree . '.descendantId = :group')
                    ->andWhere($alias . '.user = :user')
                    ->andWhere($aliasR . '.code = :code')
                    ->andWhere($aliasG . '.active = :active')
                    ->setParameter('user', $user)
                    ->setParameter('group', $group)
                    ->setParameter('active', 1)
                    ->setParameter('code', Role::ROLE_GROUP_ADMIN)
                    ->setMaxResults(1);

                $role = null;
                try {
                    $role = $qb->getQuery()->getSingleScalarResult();
                } catch (NoResultException $e) {
                    $role = null;
                }
            }
        }

        return $role;
    }

    /**
     * @param Group $group
     * @param User  $user
     *
     * @return boolean
     */
    public function hasUserGroupAccess(Group $group, User $user)
    {
        return null !== $this->getUserGroupRole($group, $user);
    }

    /**
     * @param Group $group
     * @param User  $user
     *
     * @return boolean
     */
    public function hasUserGroupAdminAccess(Group $group, User $user)
    {
        return Role::ROLE_GROUP_ADMIN === $this->getUserGroupRole($group, $user);
    }

    /**
     * @param User         $user
     * @param Organization $organization
     *
     * @return boolean
     */
    public function hasUserAnyGroupAdminAccess(User $user, Organization $organization)
    {
        $qb = $this->createQueryBuilder();
        $alias = $qb->getRootAliases()[0];
        $aliasR = 'roles';
        $aliasG = 'groups';
        $qb
            ->innerJoin($alias . '.role', $aliasR)
            ->innerJoin($alias . '.group', $aliasG)
            ->where($alias . '.user = :user')
            ->andWhere($aliasR . '.code = :code')
            ->andWhere($aliasG . '.organization = :organization')
            ->setParameter('user', $user)
            ->setParameter('organization', $organization)
            ->setParameter('code', Role::ROLE_GROUP_ADMIN)
            ->setMaxResults(1);

        $result = null;
        try {
            $result = $qb->getQuery()->getSingleScalarResult();
        } catch (NoResultException $e) {
            $result = null;
        } catch (NonUniqueResultException $e) {
            $result = 0;
        }

        return null !== $result;
    }

    /**
     * @param Group $group
     * @param User  $user
     *
     * @return bool
     */
    public function checkExists(Group $group, User $user)
    {
        $qb = $this->createQueryBuilder();
        $alias = $qb->getRootAliases()[0];
        $qb
            ->select($alias . '.id')
            ->andWhere($alias . '.user = :user')
            ->andWhere($alias . '.group = :group')
            ->setParameter('user', $user)
            ->setParameter('group', $group)
            ->setMaxResults(1);

        $result = null;
        try {
            $result = $qb->getQuery()->getSingleScalarResult();
        } catch (NoResultException $e) {
            $result = null;
        } catch (NonUniqueResultException $e) {
            $result = 0;
        }

        return null !== $result;
    }

    /**
     * @param integer|Group $group  - id of the group or the Group object
     * @param bool|null     $active - calculate the group admin quantity in active(true)/passive(false)/all(null) groups
     *
     * @return integer - The total user quantity having the group admin rights in the specified group
     *                   taking into account the group hierarchy.
     */
    public function getGroupAdminQuantity($group, $active = null)
    {
        if ($group instanceof Group) {
            $group = $group->getId();
        }

        $aliasR = 'Roles';
        $aliasTree = 'Closure';
        $qb = $this->_em->createQueryBuilder();
        $qb
            ->addSelect($qb->expr()->count($this::ALIAS . '.id'))
            ->from($this->_entityName, $this::ALIAS)
            ->innerJoin(
                $this::CLOSURE_NAME,
                $aliasTree,
                Join::WITH,
                $this::ALIAS . '.group = ' . $aliasTree . '.ancestorId'
            )
            ->innerJoin($this::ALIAS . '.role', $aliasR)
            ->andWhere($aliasR . '.code = :code')
            ->andWhere($aliasTree . '.descendantId = :group')
            ->setParameter('group', $group)
            ->setParameter('code', Role::ROLE_GROUP_ADMIN);

        if ($active !== null) {
            $aliasG = 'Groups';
            $qb
                ->innerJoin($this::ALIAS . '.group', $aliasG)
                ->andWhere($aliasG . '.active = :active')
                ->setParameter('active', $active ? 1 : 0);
        }

        return $qb->getQuery()->getSingleScalarResult();
    }

    /**
     * @param User         $user
     * @param Organization $organization
     */
    public function removeUserFromOrganization($user, $organization)
    {
        $qb = $this->createQueryBuilder();

        $exists = '
            SELECT 1
              FROM AppBundle:Group AS Groups
             WHERE Groups.organization = :organization
               AND Groups.id = GroupUserPermission.group
        ';

        $qb
            ->delete()
            ->andWhere($qb->expr()->exists($exists))
            ->andWhere('GroupUserPermission.user = :user')
            ->setParameter('user', $user)
            ->setParameter('organization', $organization);

        $qb->getQuery()->execute();
    }

    /**
     * @param OrganizationUserPermissionRepository $organizationUserPermissionRepository
     *
     * @return GroupUserPermissionRepository
     */
    public function setOrganizationUserPermissionRepository($organizationUserPermissionRepository)
    {
        $this->organizationUserPermissionRepository = $organizationUserPermissionRepository;

        return $this;
    }
}
