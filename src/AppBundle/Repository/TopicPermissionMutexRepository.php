<?php

namespace AppBundle\Repository;

use Aristek\Component\ORM\EntityRepository;
use AppBundle\DataTransferObject\DTOTopicPermission;
use AppBundle\DataTransferObject\DTOTopic;
use AppBundle\Entity\Organization;
use AppBundle\Entity\Role;
use AppBundle\Entity\Topic;
use AppBundle\Entity\User;
use Doctrine\ORM\QueryBuilder;
use Doctrine\ORM\Query\Expr;
use Doctrine\ORM\Query\Expr\Join;
use Sonata\DoctrineORMAdminBundle\Datagrid\ProxyQuery;

/**
 * Class TopicPermissionMutexRepository
 * @package AppBundle\Repository
 */
class TopicPermissionMutexRepository extends EntityRepository
{
    const ENTITY_GROUP = 'AppBundle\Entity\TopicGroupPermission';
    const ENTITY_MODERATOR = 'AppBundle\Entity\TopicModerator';

    const ALIAS_GROUPS = 'Groups';
    const ALIAS_USERS = 'Users';
    const ALIAS_TOPIC_GROUP_PERMISSIONS = 'TopicGroupPermissions';
    const ALIAS_TOPIC_MODERATORS = 'TopicModerators';

    /**
     * @var TopicModeratorRepository
     */
    protected $topicModeratorRepository;

    /**
     * @var TopicGroupPermissionRepository
     */
    protected $topicGroupPermissionRepository;

    /**
     * @param string $alias
     * @param string $indexBy
     *
     * @return QueryBuilder
     */
    public function createQueryBuilder($alias = 'TopicPermissionMutex', $indexBy = null)
    {
        return parent::createQueryBuilder($alias, $indexBy);
    }

    /**
     * @param Organization  $organization
     * @param Topic|integer $topic
     *
     * @return QueryBuilder
     */
    public function createAdminQueryBuilder(Organization $organization, $topic)
    {
        $qb = $this->createQueryBuilder();
        $alias = $qb->getRootAliases()[0];

        $qb
            ->select(
                'NEW AppBundle\DataTransferObject\DTOTopicPermission(' .
                'COALESCE(' . $this::ALIAS_TOPIC_GROUP_PERMISSIONS . '.id, 0), ' .
                'COALESCE(' . $this::ALIAS_TOPIC_MODERATORS . '.id, 0), ' .
                'COALESCE(' . $this::ALIAS_GROUPS . '.id, 0), ' .
                'COALESCE(' . $this::ALIAS_USERS . '.id, 0), ' .
                'COALESCE(' . $this::ALIAS_GROUPS . '.name, ' .
                $qb->expr()->concat(
                    $this::ALIAS_USERS . '.firstName',
                    $qb->expr()->concat('\' \'', $this::ALIAS_USERS . '.lastName')
                ) . '), ' .
                $this::ALIAS_USERS . '.username, ' .
                'CASE WHEN ' . $this::ALIAS_GROUPS . '.id is null ' .
                '    THEN \'' . DTOTopicPermission::TYPE_MODERATOR . '\' ' .
                '    ELSE \'' . DTOTopicPermission::TYPE_GROUP . '\' ' .
                'END)'
            )
            ->leftJoin(
                $this::ENTITY_GROUP,
                $this::ALIAS_TOPIC_GROUP_PERMISSIONS,
                Join::WITH,
                $alias . '.topicGroupPermissionId = 0'
            )
            ->leftJoin(
                $this::ENTITY_MODERATOR,
                $this::ALIAS_TOPIC_MODERATORS,
                Join::WITH,
                $alias . '.topicGroupPermissionId = 1'
            )
            ->leftJoin($this::ALIAS_TOPIC_GROUP_PERMISSIONS . '.group', $this::ALIAS_GROUPS)
            ->leftJoin($this::ALIAS_TOPIC_MODERATORS . '.user', $this::ALIAS_USERS)
            ->where(
                $qb->expr()->orX(
                    $qb->expr()->isNotNull($this::ALIAS_TOPIC_GROUP_PERMISSIONS . '.id'),
                    $qb->expr()->isNotNull($this::ALIAS_TOPIC_MODERATORS . '.id')
                )
            )
            ->andWhere(
                $qb->expr()->orX(
                    $this::ALIAS_GROUPS . '.organization = :organization',
                    $qb->expr()->isNull($this::ALIAS_GROUPS . '.organization')
                )
            )
            ->andWhere(
                $qb->expr()->orX(
                    $this::ALIAS_TOPIC_GROUP_PERMISSIONS . '.topic = :topic',
                    $qb->expr()->isNull($this::ALIAS_TOPIC_GROUP_PERMISSIONS . '.topic')
                )
            )
            ->andWhere(
                $qb->expr()->orX(
                    $this::ALIAS_TOPIC_MODERATORS . '.topic = :topic',
                    $qb->expr()->isNull($this::ALIAS_TOPIC_MODERATORS . '.topic')
                )
            )
            ->setParameter('topic', $topic)
            ->setParameter('organization', $organization);

        return $qb;
    }

    /**
     * @param QueryBuilder|ProxyQuery $qb
     */
    private function correctAdminQueryBuilderSortingByName(&$qb)
    {
        $expr = $qb->expr();
        $qb->addSelect(
            'COALESCE(' . $this::ALIAS_GROUPS . '.name, ' .
            $expr->concat(
                $this::ALIAS_USERS . '.firstName',
                $expr->concat('\' \'', $this::ALIAS_USERS . '.lastName')
            ) . ') AS HIDDEN name_order'
        );
    }

    /**
     * @param QueryBuilder|ProxyQuery $qb
     * @param string                  $sortBy
     * @param string                  $sortOrder
     *
     * @return void
     */
    public function correctAdminQueryBuilderSorting(&$qb, $sortBy, $sortOrder)
    {
        $this->checkQueryBuilder($qb);

        switch ($sortBy) {
            case 'login': {
                $qb
                    ->addSelect(
                        $this::ALIAS_USERS . '.username AS HIDDEN login_order'
                    )
                    ->orderBy('login_order', $sortOrder);
                break;
            }

            case 'name': {
                $this->correctAdminQueryBuilderSortingByName($qb);
                $qb->orderBy('name_order', $sortOrder);
                break;
            }

            case 'type': {
                $qb->addSelect(
                    'CASE WHEN ' . $this::ALIAS_GROUPS . '.id is null THEN 0 ELSE 1 END AS HIDDEN type_order'
                );
                $this->correctAdminQueryBuilderSortingByName($qb);
                $qb
                    ->orderBy('type_order', $sortOrder)
                    ->addOrderBy('name_order', 'ASC');
                break;
            }
        }
    }

    /**
     * @param $qb
     */
    public function checkQueryBuilder($qb)
    {
        if (!($qb instanceof QueryBuilder) && !($qb instanceof ProxyQuery)) {
            throw new \InvalidArgumentException(
                __METHOD__ . ': The qb parameter must be an instance of QueryBuilder or ProxyQuery!'
            );
        }
    }

    /**
     * Returns an array indexed by the topic_ids where each item is true if $user has moderator rights
     * to this topic or false otherwise.
     *
     * @param User                                                $user
     * @param DTOTopic|Topic|integer|DTOTopic[]|Topic[]|integer[] $topics
     *
     * @return boolean[]
     */
    public function hasUserTopicModeratorRights(User $user, $topics)
    {
        $rights = $this->topicModeratorRepository->hasUserTopicModeratorRights($user, $topics);
        $noRights = array_filter(
            $rights,
            function ($item) {
                return !$item;
            }
        );
        $rights = array_diff($rights, $noRights);
        if (!empty($noRights)) {
            $noRights = array_keys($noRights);
            $groupRights = $this->topicGroupPermissionRepository->hasUserTopicGroupAdminRights($user, $noRights);
            $rights = $rights + $groupRights;
        }

        return $rights;
    }

    /**
     * Returns an array indexed by the topic_ids where each item is
     * - true if $user has direct moderator rights to this topic or
     * - Role::ROLE_GROUP_ADMIN if $user has moderator rights to this topic through the group admin rights or
     * - Role::ROLE_GROUP_USER if $user has usual user rights to this topic or
     * - false if $user has no access to this topic.
     *
     * @param User                                                $user
     * @param DTOTopic|Topic|integer|DTOTopic[]|Topic[]|integer[] $topics
     *
     * @return array
     */
    public function getUserRights(User $user, $topics)
    {
        $rights = $this->topicGroupPermissionRepository->getUserTopicRights($user, $topics);
        $userRights = array_filter(
            $rights,
            function ($item) {
                return $item === Role::ROLE_GROUP_USER;
            }
        );
        $rights = array_diff($rights, $userRights);

        if (!empty($userRights)) {
            $userRights = array_keys($userRights);
            $userRights = $this->topicModeratorRepository->hasUserTopicModeratorRights($user, $userRights);
            array_walk(
                $userRights,
                function (&$item) {
                    if (false === $item) {
                        $item = Role::ROLE_GROUP_USER;
                    }
                }
            );
            $rights = $rights + $userRights;
        }

        return $rights;
    }

    /**
     * @param TopicModeratorRepository $topicModeratorRepository
     *
     * @return TopicPermissionMutexRepository
     */
    public function setTopicModeratorRepository($topicModeratorRepository)
    {
        $this->topicModeratorRepository = $topicModeratorRepository;

        return $this;
    }

    /**
     * @param TopicGroupPermissionRepository $topicGroupPermissionRepository
     *
     * @return TopicPermissionMutexRepository
     */
    public function setTopicGroupPermissionRepository($topicGroupPermissionRepository)
    {
        $this->topicGroupPermissionRepository = $topicGroupPermissionRepository;

        return $this;
    }
}
