<?php

namespace AppBundle\Repository;

use Aristek\Component\ORM\EntityRepository;
use AppBundle\Entity\RegistrationInvite;
use AppBundle\Entity\User;
use AppBundle\Entity\Organization;
use AppBundle\Entity\Role;
use Doctrine\ORM\QueryBuilder;
use Doctrine\ORM\Query;
use Doctrine\ORM\Query\Expr;
use Doctrine\ORM\Query\Expr\Join;
use Doctrine\ORM\NoResultException;
use Doctrine\ORM\NonUniqueResultException;
use Sonata\DoctrineORMAdminBundle\Datagrid\ProxyQuery;

/**
 * Class RegistrationInviteRepository
 * @package AppBundle\Repository
 *
 * @method RegistrationInvite findOneBy(array $criteria, array $orderBy = null)
 * @method RegistrationInvite find($id, $lockMode = null, $lockVersion = null)
 */
class RegistrationInviteRepository extends EntityRepository
{
    const CLOSURE_NAME = 'AppBundle\Entity\GroupClosure';

    /**
     * @param string $alias
     * @param string $indexBy
     *
     * @return QueryBuilder
     */
    public function createQueryBuilder($alias = 'RegistrationInvite', $indexBy = null)
    {
        return parent::createQueryBuilder($alias, $indexBy);
    }

    /**
     * @param Organization $organization
     *
     * @return QueryBuilder
     */
    public function createOrganizationAdminQueryBuilder(Organization $organization)
    {
        $qb = $this->createQueryBuilder();
        $alias = $qb->getRootAliases()[0];
        $aliasG = 'Groups';

        $qb
            ->leftJoin($alias . '.group', $aliasG)
            ->andWhere(
                $qb->expr()->orX(
                    $qb->expr()->andX(
                        $alias . '.organization = :organization1',
                        $qb->expr()->isNull($alias . '.group')
                    ),
                    $qb->expr()->andX(
                        $qb->expr()->isNull($alias . '.organization'),
                        $aliasG . '.organization = :organization2'
                    )
                )
            )
            ->setParameter('organization1', $organization)
            ->setParameter('organization2', $organization);

        return $qb;
    }

    /**
     * @param User         $user
     * @param Organization $organization
     *
     * @return QueryBuilder
     */
    public function createGroupAdminQueryBuilder(User $user, Organization $organization)
    {
        // DQL implementation of the following SQL query:
        //
        //    SELECT
        //        gc.descendant_id, gDescendant.name, r.id, r.`status`, r.email, r.created_by, r.created_at
        //    FROM registration_invites r
        //        INNER JOIN group_closure gc ON r.group_id = gc.descendant_id
        //        INNER JOIN groups gAncestor ON gc.ancestor_id = gAncestor.id
        //        INNER JOIN group_user_permissions gup ON gup.group_id = gAncestor.id
        //        INNER JOIN roles rol ON gup.role_id = rol.id
        //        INNER JOIN groups gDescendant ON gDescendant.id = r.group_id
        //    WHERE
        //        gAncestor.organization_id = :organization AND
        //        gAncestor.active = 1 AND
        //        gup.user_id = :user AND
        //        gDescendant.active = 1 AND
        //        gDescendant.organization_id = :organization AND
        //        rol.code = 'ROLE_GROUP_ADMIN'
        //    GROUP BY
        //        gc.descendant_id, r.id
        //

        $qb = $this->createQueryBuilder();
        $alias = $qb->getRootAliases()[0];
        $aliasTree = 'Closure';
        $aliasGAncestor = 'GAncestor';
        $aliasGDescendant = 'GDescendant';
        $aliasGUP = 'GroupUserPermission';
        $aliasR = 'Role';

        $qb
            ->innerJoin(
                $this::CLOSURE_NAME,
                $aliasTree,
                Join::WITH,
                $alias . '.group = ' . $aliasTree . '.descendantId'
            )
            ->innerJoin(
                'AppBundle\Entity\Group',
                $aliasGAncestor,
                Join::WITH,
                $aliasGAncestor . '.id = ' . $aliasTree . '.ancestorId'
            )
            ->innerJoin($aliasGAncestor . '.groupUserPermissions', $aliasGUP)
            ->innerJoin($aliasGUP . '.role', $aliasR)
            ->innerJoin(
                'AppBundle\Entity\Group',
                $aliasGDescendant,
                Join::WITH,
                $aliasGDescendant . '.id = ' . $aliasTree . '.descendantId'
            )
            ->andWhere($aliasGAncestor . '.organization = :a_organization')
            ->andWhere($aliasGAncestor . '.active = :a_active')
            ->andWhere($aliasGUP . '.user = :user')
            ->andWhere($aliasR . '.code = :code')
            ->andWhere($aliasGDescendant . '.organization = :d_organization')
            ->andWhere($aliasGDescendant . '.active = :d_active')
            ->groupBy($aliasTree . '.descendantId')
            ->addGroupBy($alias . '.id')
            ->setParameter('a_organization', $organization)
            ->setParameter('a_active', 1)
            ->setParameter('user', $user)
            ->setParameter('code', Role::ROLE_GROUP_ADMIN)
            ->setParameter('d_organization', $organization)
            ->setParameter('d_active', 1);

        return $qb;
    }

    /**
     * @param User $user
     *
     * @return QueryBuilder
     */
    public function createSelectByUserQueryBuilder(User $user)
    {
        $qb = $this->createQueryBuilder();
        $alias = $qb->getRootAliases()[0];
        $and = $qb->expr()->andX();
        $and->add($qb->expr()->isNull($alias . '.user'));
        $and->add($qb->expr()->eq($alias . '.email', ':email'));
        $qb
            ->where($qb->expr()->eq($alias . '.user', ':user'))
            ->orWhere($and)
            ->setParameter('user', $user)
            ->setParameter('email', $user->getEmailCanonical());

        return $qb;
    }

    /**
     * @param QueryBuilder|ProxyQuery $qb
     * @param                         $sortBy
     * @param                         $sortOrder
     *
     * @return void
     */
    public function correctQueryBuilderSorting(&$qb, $sortBy, $sortOrder)
    {
        if (!($qb instanceof QueryBuilder) && !($qb instanceof ProxyQuery)) {
            throw new \InvalidArgumentException(
                __METHOD__ . ': The qb parameter must be an instance of QueryBuilder or ProxyQuery!'
            );
        }
        $alias = $qb->getRootAliases()[0];
        switch ($sortBy) {
            case 'status': {
                $qb
                    ->addSelect(
                        '(CASE ' . $alias . '.status ' .
                        'WHEN \'' . RegistrationInvite::STATUS_NEW . '\' THEN 1 ' .
                        'WHEN \'' . RegistrationInvite::STATUS_ACCEPTED . '\' THEN 2 ' .
                        'WHEN \'' . RegistrationInvite::STATUS_DECLINED . '\' THEN 3 ' .
                        'ELSE 4 END) AS HIDDEN status_order'
                    )
                    ->addOrderBy('status_order', $sortOrder)
                    ->addOrderBy($alias . '.createdAt', 'DESC');
                break;
            }

            case 'organization_group_type': {
                $qb
                    ->addSelect(
                        '(CASE WHEN ' . $alias . '.group IS NULL THEN \'' . RegistrationInvite::TYPE_GROUP . '\' ' .
                        'ELSE \'' . RegistrationInvite::TYPE_ORGANIZATION . '\' END) AS HIDDEN type_order'
                    )
                    ->addSelect(
                        "COALESCE(g.name, o.name) AS HIDDEN name_order"
                    )
                    ->leftJoin($alias . '.group', 'g')
                    ->leftJoin($alias . '.organization', 'o')
                    ->addOrderBy('type_order', $sortOrder)
                    ->addOrderBy($alias . '.createdAt', 'DESC')
                    ->addOrderBy('name_order');
                break;
            }

            case 'organization_group_name': {
                $qb
                    ->addSelect(
                        "COALESCE(g.name, o.name) AS HIDDEN name_order"
                    )
                    ->leftJoin($alias . '.group', 'g')
                    ->leftJoin($alias . '.organization', 'o')
                    ->addOrderBy('name_order', $sortOrder)
                    ->addOrderBy($alias . '.createdAt', 'DESC');
                break;
            }
        }
    }

    /**
     * @param string $token
     *
     * @return RegistrationInvite[]
     */
    public function findByToken($token)
    {
        $qb = $this->createQueryBuilder();
        $qb
            ->andWhere('RegistrationInvite.token = :token')
            ->setParameter('token', $token);

        return $qb->getQuery()->getResult();
    }

    public function findByTokenAsSortedArray($token)
    {
        $qb = $this->_em->createQueryBuilder();
        $qb
            ->select('r.id')
            ->addSelect('r.status')
            ->addSelect(
                '(CASE r.status ' .
                'WHEN \'' . RegistrationInvite::STATUS_NEW . '\' THEN 1 ' .
                'WHEN \'' . RegistrationInvite::STATUS_ACCEPTED . '\' THEN 2 ' .
                'WHEN \'' . RegistrationInvite::STATUS_DECLINED . '\' THEN 3 ' .
                'ELSE 4 END) AS HIDDEN status_order'
            )
            ->addSelect('COALESCE(g.name, o.name) AS name')
            ->addSelect(
                '(CASE WHEN g.id IS NULL THEN \'' . RegistrationInvite::TYPE_ORGANIZATION . '\' ' .
                'ELSE \'' . RegistrationInvite::TYPE_GROUP . '\' END) AS organization_group_type'
            )
            ->from($this->getEntityName(), 'r')
            ->leftJoin('r.group', 'g')
            ->leftJoin('r.organization', 'o')
            ->andWhere('r.token = :token')
            ->orderBy('status_order')
            ->addOrderBy('organization_group_type', 'DESC')
            ->addOrderBy('name')
            ->setParameter('token', $token);

        return $qb->getQuery()->getArrayResult();
    }

    /**
     * @param string $token
     *
     * @return RegistrationInvite
     *
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function findOneByToken($token)
    {
        return $this->findOneBy(['token' => $token]);
    }

    /**
     * @param string $token - The token value must be correct and must exist
     *
     * @return bool
     */
    public function isConfirmed($token)
    {
        $qb = $this->createQueryBuilder();
        $qb
            ->select('RegistrationInvite.id')
            ->andWhere('RegistrationInvite.token = :token')
            ->andWhere('RegistrationInvite.status = :status')
            ->setParameter('token', $token)
            ->setParameter('status', RegistrationInvite::STATUS_NEW)
            ->setMaxResults(1);

        $result = null;
        try {
            $result = $qb->getQuery()->getSingleScalarResult();
        } catch (NoResultException $e) {
            $result = null;
        } catch (NonUniqueResultException $e) {
            $result = 0;
        }

        return null === $result;
    }

    /**
     * @param User        $user
     * @param string|null $status
     *
     * @return integer
     *
     * Function returns the registration invites record quantity for given $user with given $status.
     * If $status is null then the total registration invites record quantity for given $user is returned.
     */
    public function getUserInvitesCount(User $user, $status = null)
    {
        $alias = 'RegistrationInvite';
        $qb = $this->_em->createQueryBuilder();

        $and = $qb->expr()->andX();
        $and->add($qb->expr()->isNull($alias . '.user'));
        $and->add($qb->expr()->eq($alias . '.email', ':email'));

        $or = $qb->expr()->orX();
        $or->add($qb->expr()->eq($alias . '.user', ':user'));
        $or->add($and);

        $qb
            ->select('COUNT(' . $alias . ')')
            ->from($this->_entityName, $alias, null)
            ->where($or)
            ->setParameter('user', $user)
            ->setParameter('email', $user->getEmailCanonical());

        if (null !== $status) {
            $qb
                ->andWhere($qb->expr()->eq($alias . '.status', ':status'))
                ->setParameter('status', $status);
        }

        return $qb->getQuery()->getSingleScalarResult();
    }

    /**
     * @param User   $user
     * @param string $status
     *
     * @return QueryBuilder
     */
    public function createUpdateQueryBuilder(User $user, $status)
    {
        $qb = $this->_em->createQueryBuilder();
        $and = $qb->expr()->andX();
        $and->add($qb->expr()->eq('r.email', ':email'));
        $and->add($qb->expr()->isNull('r.user'));
        $or = $qb->expr()->orX();
        $or->add($qb->expr()->eq('r.user', ':user'));
        $or->add($and);
        $qb
            ->update($this->getEntityName(), 'r')
            ->set('r.status', "'" . $status . "'")
            ->set('r.user', $user->getId())
            ->where($or)
            ->andWhere($qb->expr()->eq('r.status', ':status'))
            ->setParameter('status', RegistrationInvite::STATUS_NEW)
            ->setParameter('user', $user)
            ->setParameter('email', $user->getEmailCanonical());

        return $qb;
    }
}
