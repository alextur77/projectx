<?php

namespace AppBundle\Repository;

use AppBundle\Entity\Organization;
use AppBundle\Entity\User;
use Aristek\Bundle\AdminBundle\Util\FilterUtil;
use Aristek\Component\ORM\EntityRepository;
use Doctrine\ORM\QueryBuilder;
use FOS\UserBundle\Util\Canonicalizer;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class UserRepository
 * @package AppBundle\Repository
 *
 * @method User findOneBy(array $criteria, array $orderBy = null)
 * @method User find($id, $lockMode = null, $lockVersion = null)
 * @method User[] findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class UserRepository extends EntityRepository
{
    const ALIAS = 'User';

    /**
     * @var Canonicalizer
     */
    protected $usernameCanonicalizer;

    /**
     * @param string $alias
     *
     * @return string
     */
    private function resolveAlias($alias)
    {
        return null === $alias ? $this::ALIAS : $alias;
    }

    /**
     * @param string|null $alias
     * @param string|null $indexBy
     *
     * @return QueryBuilder
     */
    public function createQueryBuilder($alias = null, $indexBy = null)
    {
        $alias = $this->resolveAlias($alias);

        return parent::createQueryBuilder($alias, $indexBy);
    }

    public function createDeleteQueryBuilder($alias = null)
    {
        $alias = $this->resolveAlias($alias);
        $qb = $this->_em->createQueryBuilder();
        $qb->delete($this->getClassName(), $alias);

        return $qb;
    }

    /**
     * @param Organization $organization
     * @param array        $filters
     *
     * @return QueryBuilder
     */
    public function createOrganizationUsersQueryBuilder($organization, $filters = [])
    {
        $resolver = new OptionsResolver();
        $resolver->setDefaults(['group' => []]);
        $filters = $resolver->resolve($filters);

        $qb = $this->createQueryBuilder();
        $exp = $qb->expr();

        $organizationQueryBuilder = $this->em()->createQueryBuilder()
            ->select('1')
            ->from('AppBundle:OrganizationUserPermission', 'OrganizationUserPermission')
            ->andWhere('OrganizationUserPermission.organization = :organization')
            ->andWhere('OrganizationUserPermission.user = User');

        $groupQueryBuilder = $this->em()->createQueryBuilder()
            ->select('1')
            ->from('AppBundle:Group', 'Groups')
            ->innerJoin('Groups.groupUserPermissions', 'GroupUserPermission')
            ->andWhere('Groups.organization = :organization')
            ->andWhere('GroupUserPermission.user = User');

        $exists = $exp->orX(
            $exp->exists($organizationQueryBuilder),
            $exp->exists($groupQueryBuilder)
        );

        if ($filters['group']) {
            list($cond, $value) = FilterUtil::process($filters['group']);

            $groupQueryBuilder->andWhere('Groups.name ' . $cond . ' :group');
            $qb->setParameter('group', $value);

            $exists = $exp->exists($groupQueryBuilder);
        }

        $qb
            ->andWhere($exists)
            ->setParameter('organization', $organization);

        return $qb;
    }

    /**
     * @param string $username
     *
     * @return User
     */
    public function findUserWithRoles($username)
    {
        $qb = $this->createQueryBuilder();

        $qb
            ->select(
                'User, OrganizationUserPermission, OrganizationUserRole, GroupUserPermissions, GroupUserPermissionsRole'
            )
            ->leftJoin('User.organizationUserPermissions', 'OrganizationUserPermission')
            ->leftJoin('OrganizationUserPermission.role', 'OrganizationUserRole')
            ->leftJoin('User.groupUserPermissions', 'GroupUserPermissions')
            ->leftJoin('GroupUserPermissions.role', 'GroupUserPermissionsRole')
            ->andWhere('User.usernameCanonical = :username')
            ->setParameter('username', $this->usernameCanonicalizer->canonicalize($username));

        return $qb->getQuery()->getSingleResult();
    }

    /**
     * @param string $canonicalEmail
     *
     * @return User
     */
    public function findUserByCanonicalEmail($canonicalEmail)
    {
        return $this->findOneBy(['emailCanonical' => $canonicalEmail]);
    }

    /**
     * @param string $email
     *
     * @return User
     */
    public function findUserByEmail($email)
    {
        return $this->findOneBy(['email' => strtolower($email)]);
    }

    /**
     * @param Canonicalizer $usernameCanonicalizer
     *
     * @return UserRepository
     */
    public function setUsernameCanonicalizer($usernameCanonicalizer)
    {
        $this->usernameCanonicalizer = $usernameCanonicalizer;

        return $this;
    }
}

