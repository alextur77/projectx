<?php

namespace AppBundle\Repository;

use Aristek\Component\ORM\EntityRepository;
use AppBundle\Entity\PersonalMessageRead;

/**
 * Class PersonalMessageReadRepository
 * @package AppBundle\Repository
 *
 * @method PersonalMessageRead findOneBy(array $criteria, array $orderBy = null)
 * @method PersonalMessageRead find($event, $lockMode = null, $lockVersion = null)
 */
class PersonalMessageReadRepository extends EntityRepository
{

}
