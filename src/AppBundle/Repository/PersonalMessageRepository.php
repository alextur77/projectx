<?php

namespace AppBundle\Repository;

use AppBundle\Entity\Organization;
use AppBundle\Entity\User;
use Aristek\Component\ORM\EntityRepository;
use AppBundle\Entity\PersonalMessage;
use Doctrine\ORM\QueryBuilder;

/**
 * Class PersonalMessageRepository
 * @package AppBundle\Repository
 *
 * @method PersonalMessage findOneBy(array $criteria, array $orderBy = null)
 * @method PersonalMessage find($event, $lockMode = null, $lockVersion = null)
 */
class PersonalMessageRepository extends EntityRepository
{
    /**
     * @param string $alias
     * @param string $indexBy
     *
     * @return QueryBuilder
     */
    public function createQueryBuilder($alias = 'PersonalMessage', $indexBy = null)
    {
        return parent::createQueryBuilder($alias, $indexBy);
    }

    /**
     * @param Organization $organization
     * @param User         $user
     *
     * @return QueryBuilder
     */
    public function createOrganizationAdminQueryBuilder(Organization $organization, User $user)
    {
        $roots = $this->findRoots($organization, $user);

        return $roots;
    }

    /**
     * @param Organization $organization
     * @param User         $user
     *
     * @return PersonalMessage[]
     */
    public function findRoots(Organization $organization, User $user)
    {
        $qb = $this->createRootsQueryBuilder($organization, $user);

        return $qb;
    }

    /**
     * @param Organization $organization
     * @param User         $user
     *
     * @return QueryBuilder
     */
    public function createRootsQueryBuilder(Organization $organization, User $user)
    {
        $qb = $this->createQueryBuilder('e');

        $qb
            ->leftJoin('AppBundle:PersonalMessageRead', 'u', 'WITH', 'e.id = u.message')
            ->andWhere('e.organization = :organization')
            ->andWhere('e.recipient = :user')
            ->setParameter('organization', $organization)
            ->setParameter('user', $user);

        return $qb;
    }

}
