<?php

namespace AppBundle\Repository;

use AppBundle\Entity\Organization;
use AppBundle\Entity\OrganizationUserPermission;
use AppBundle\Entity\User;
use Aristek\Component\ORM\EntityRepository;
use Doctrine\ORM\QueryBuilder;
use Doctrine\ORM\NoResultException;
use Doctrine\ORM\NonUniqueResultException;

/**
 * Class OrganizationUserPermissionRepository
 * @package AppBundle\Repository
 *
 * @method OrganizationUserPermission findOneBy(array $criteria, array $orderBy = null)
 * @method OrganizationUserPermission find($id, $lockMode = null, $lockVersion = null)
 */
class OrganizationUserPermissionRepository extends EntityRepository
{
    /**
     * @param string $alias
     * @param string $indexBy
     *
     * @return QueryBuilder
     */
    public function createQueryBuilder($alias = 'OrganizationUserPermission', $indexBy = null)
    {
        return parent::createQueryBuilder($alias, $indexBy);
    }

    /**
     * @param User $user
     *
     * @return QueryBuilder
     */
    public function createUserQueryBuilder(User $user)
    {
        $qb = $this->createQueryBuilder();
        $qb
            ->andWhere('OrganizationUserPermission.user = :user')
            ->setParameter('user', $user);

        return $qb;
    }

    /**
     * @param User         $user
     * @param Organization $organization
     */
    public function removeUser(User $user, Organization $organization)
    {
        $qb = $this->createQueryBuilder();

        $qb
            ->delete()
            ->andWhere('OrganizationUserPermission.user = :user')
            ->andWhere('OrganizationUserPermission.organization = :organization')
            ->setParameter('user', $user)
            ->setParameter('organization', $organization);

        $qb->getQuery()->execute();
    }

    /**
     * @param Organization $organization
     * @param User         $user
     *
     * @return bool
     */
    public function checkExists(Organization $organization, User $user)
    {
        $qb = $this->createQueryBuilder();
        $alias = $qb->getRootAliases()[0];
        $qb
            ->select($alias . '.id')
            ->andWhere($alias . '.user = :user')
            ->andWhere($alias . '.organization = :organization')
            ->setParameter('user', $user)
            ->setParameter('organization', $organization)
            ->setMaxResults(1);

        $result = null;
        try {
            $result = $qb->getQuery()->getSingleScalarResult();
        } catch (NoResultException $e) {
            $result = null;
        } catch (NonUniqueResultException $e) {
            $result = 0;
        }

        return null !== $result;
    }

    /**
     * @param User         $user
     * @param Organization $organization
     *
     * @return string|null - The user role code in $organization
     */
    public function getUserRoleInOrganization(User $user, Organization $organization)
    {
        $alias = 'OrganizationUserPermission';
        $aliasR = 'Role';
        $qb = $this->_em->createQueryBuilder();
        $qb
            ->select($aliasR . '.code')
            ->from($this->_entityName, $alias)
            ->innerJoin($alias . '.role', $aliasR)
            ->andWhere($alias . '.user = :user')
            ->andWhere($alias . '.organization = :organization')
            ->setParameter('user', $user)
            ->setParameter('organization', $organization);

        $result = null;
        try {
            $result = $qb->getQuery()->getSingleScalarResult();
        } catch (NoResultException $e) {
            $result = null;
        }

        return $result;
    }
}
