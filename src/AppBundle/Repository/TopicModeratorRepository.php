<?php

namespace AppBundle\Repository;

use Aristek\Component\ORM\EntityRepository;
use AppBundle\DataTransferObject\DTOTopic;
use AppBundle\Entity\Topic;
use AppBundle\Entity\User;
use AppBundle\Entity\TopicModerator;
use Doctrine\ORM\Query\Expr\Join;
use Doctrine\ORM\QueryBuilder;

/**
 * Class TopicModeratorRepository
 * @package AppBundle\Repository
 *
 * @method TopicModerator findOneBy(array $criteria, array $orderBy = null)
 * @method TopicModerator find($id, $lockMode = null, $lockVersion = null)
 * @method TopicModerator[] findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class TopicModeratorRepository extends EntityRepository
{
    const ALIAS = 'TopicModerator';

    const ALIAS_GROUP_PERMISSION = 'TopicGroupPermission';
    const ENTITY_GROUP_PERMISSION = 'AppBundle\Entity\TopicGroupPermission';

    /**
     * @var GroupUserPermissionRepository
     */
    protected $groupUserPermissionRepository;

    /**
     * @param string $alias
     *
     * @return string
     */
    private function resolveAlias($alias)
    {
        return null === $alias ? $this::ALIAS : $alias;
    }

    /**
     * @param string|null $alias
     * @param string|null $indexBy
     *
     * @return QueryBuilder
     */
    public function createQueryBuilder($alias = null, $indexBy = null)
    {
        $alias = $this->resolveAlias($alias);

        return parent::createQueryBuilder($alias, $indexBy);
    }

    public function createDeleteQueryBuilder($alias = null)
    {
        $alias = $this->resolveAlias($alias);
        $qb = $this->_em->createQueryBuilder();
        $qb->delete($this->getClassName(), $alias);

        return $qb;
    }

    /**
     * @param Topic|integer $topic - Topic object or the topic id
     *
     * @return QueryBuilder
     *
     * Creates the query builder to find the moderators witch cannot have access to this $topic
     * because there are no any groups having access to this $topic witch these moderators belong to.
     */
    public function createProhibitedQueryBuilder($topic)
    {
        $subQB = $this->groupUserPermissionRepository->createQueryBuilder();
        $aliasG = $subQB->getRootAliases()[0];
        $subQB
            ->innerJoin(
                $this::ENTITY_GROUP_PERMISSION,
                $this::ALIAS_GROUP_PERMISSION,
                Join::WITH,
                $aliasG . '.group = ' . $this::ALIAS_GROUP_PERMISSION . '.group'
            )
            ->where($this::ALIAS_GROUP_PERMISSION . '.topic = :topic')
            ->andWhere($aliasG . '.user = ' . $this::ALIAS . '.user')
            ->setParameter('topic', $topic);

        $qb = $this->createQueryBuilder();
        $qb
            ->where($this::ALIAS . '.topic = :topic')
            ->andWhere($qb->expr()->not($qb->expr()->exists($subQB)))
            ->setParameter('topic', $topic);

        return $qb;
    }

    /**
     * Returns an array indexed by the topic_ids where each item is true if $user has direct moderator rights
     * to this topic or false otherwise.
     *
     * @param User                                                $user
     * @param DTOTopic|Topic|integer|DTOTopic[]|Topic[]|integer[] $topics
     *
     * @return boolean[]
     */
    public function hasUserTopicModeratorRights(User $user, $topics)
    {
        if (!is_array($topics)) {
            $topics = [$topics];
        }
        if (reset($topics) instanceof DTOTopic) {
            foreach ($topics as $index => $topic) {
                $topics[$index] = $topic->getId();
            }
        }
        $qb = $this->createQueryBuilder();
        $qb
            ->select('IDENTITY(' . $this::ALIAS . '.topic) AS topic_id')
            ->where($this::ALIAS . '.user = :user')
            ->andWhere($qb->expr()->in($this::ALIAS . '.topic', ':topics'))
            ->groupBy($this::ALIAS . '.topic')
            ->setParameter('user', $user)
            ->setParameter('topics', $topics);
        $result = $qb->getQuery()->getResult();
        $result = array_column($result, 'topic_id');
        $result = array_fill_keys($result, true);

        foreach ($topics as $topic) {
            if ($topic instanceof Topic) {
                $id = $topic->getId();
            } else {
                $id = $topic;
            }
            if (!key_exists($id, $result)) {
                $result[$id] = false;
            }
        }

        return $result;
    }

    /**
     * @param GroupUserPermissionRepository $groupUserPermissionRepository
     *
     * @return TopicModeratorRepository
     */
    public function setGroupUserPermissionRepository($groupUserPermissionRepository)
    {
        $this->groupUserPermissionRepository = $groupUserPermissionRepository;

        return $this;
    }
}
