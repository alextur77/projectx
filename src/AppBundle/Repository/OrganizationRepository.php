<?php

namespace AppBundle\Repository;

use Aristek\Component\ORM\EntityRepository;
use AppBundle\Entity\Organization;
use AppBundle\Entity\User;
use AppBundle\Entity\RegistrationRequest;
use AppBundle\Repository\Organization\OrganizationsWithPermissionsFilter;
use Doctrine\ORM\QueryBuilder;
use Doctrine\ORM\Query\Expr;

/**
 * Class OrganizationRepository
 * @package AppBundle\Repository
 *
 * @method Organization findOneBy(array $criteria, array $orderBy = null)
 * @method Organization find($id, $lockMode = null, $lockVersion = null)
 * @method Organization[] findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class OrganizationRepository extends EntityRepository
{
    /**
     * @param string $alias
     * @param string $indexBy
     *
     * @return QueryBuilder
     */
    public function createQueryBuilder($alias = 'Organization', $indexBy = null)
    {
        return parent::createQueryBuilder($alias, $indexBy);
    }

    /**
     * @param User $user
     *
     * @return OrganizationsWithPermissionsFilter
     */
    public function getOrganizationsByUserFilter(User $user)
    {
        $qb = new OrganizationsWithPermissionsFilter($this->createQueryBuilder());

        $qb->setUser($user);

        return $qb;
    }

    /**
     * @param string  $search
     * @param User    $user
     * @param boolean $sortDirection
     *
     * @return array
     */
    public function findSearchableOrganizations($search, User $user, $sortDirection)
    {
        $alias = 'Organization';
        $qb = $this->_em->createQueryBuilder();
        $like = $qb->expr()->like($alias . '.name', ':name');
        $qb
            ->select($alias . '.id')
            ->distinct()
            ->addSelect($alias . '.name')
            ->addSelect($alias . '.allowSingUp')
            ->from($this->_entityName, $alias)
            ->andWhere($alias . '.public = :public')
            ->andWhere($alias . '.active = :active')
            ->andWhere($like)
            ->addOrderBy($alias . '.name', $sortDirection ? 'ASC' : 'DESC')
            ->setParameter('name', '%' . $search . '%')
            ->setParameter('public', 1)
            ->setParameter('active', 1);

        $aliasJoinP = 'Permissions';
        $qb
            ->addSelect('(CASE WHEN ' . $aliasJoinP . '.user IS NULL THEN 0 ELSE 1 END) AS registered')
            ->leftJoin(
                $alias . '.organizationUserPermissions',
                $aliasJoinP,
                Expr\Join::WITH,
                $qb->expr()->eq($aliasJoinP . '.user', ':user')
            )
            ->setParameter('user', $user);

        $aliasJoinR = 'Requests';
        $and = $qb->expr()->andX();
        $and->add($qb->expr()->eq($aliasJoinR . '.user', ':userR'));
        $and->add($qb->expr()->eq($aliasJoinR . '.status', ':statusR'));
        $and->add($qb->expr()->isNull($aliasJoinR . '.group'));
        $qb
            ->addSelect('(CASE WHEN ' . $aliasJoinR . '.user IS NULL THEN 0 ELSE 1 END) AS has_request')
            ->leftJoin($alias . '.registrationRequests', $aliasJoinR, Expr\Join::WITH, $and)
            ->setParameter('userR', $user)
            ->setParameter('statusR', RegistrationRequest::STATUS_NEW);

        return $qb->getQuery()->getArrayResult();
    }
}
