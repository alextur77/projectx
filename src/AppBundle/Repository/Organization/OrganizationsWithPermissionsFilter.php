<?php

namespace AppBundle\Repository\Organization;

use AppBundle\Entity\Role;
use AppBundle\Entity\User;
use Aristek\Component\ORM\AbstractEntityFilter;
use Doctrine\ORM\QueryBuilder;

/**
 * Class OrganizationsWithPermissionsFilter
 */
class OrganizationsWithPermissionsFilter extends AbstractEntityFilter
{
    /**
     * @param QueryBuilder $queryBuilder
     */
    public function __construct(QueryBuilder $queryBuilder)
    {
        parent::__construct($queryBuilder);

        $this->queryBuilder->innerJoin('Organization.organizationUserPermissions', 'OrganizationUserPermission');
    }

    /**
     * @param User $user
     *
     * @return $this
     */
    public function setUser($user)
    {
        $this->queryBuilder
        ->andWhere('OrganizationUserPermission.user = :user')
        ->setParameter('user', $user->getId());

        return $this;
    }

    /**
     * Return only organizations where user is organization admin
     *
     * @return $this
     */
    public function getAsAdmin()
    {
        $this->queryBuilder
            ->innerJoin('OrganizationUserPermission.role', 'Role')
            ->andWhere('Role.type = :type')
            ->andWhere('Role.code = :code')
            ->setParameter('type', Role::TYPE_ORGANIZATION)
            ->setParameter('code', Role::ROLE_ORGANIZATION_ADMIN);

        return $this;
    }

    /**
     * Return only organizations where user is not admin user
     *
     * @return $this
     */
    public function getAsUser()
    {
        $this->queryBuilder
            ->innerJoin('OrganizationUserPermission.role', 'Role')
            ->andWhere('Role.type = :type')
            ->andWhere('Role.code = :code')
            ->setParameter('type', Role::TYPE_ORGANIZATION)
            ->setParameter('code', Role::ROLE_ORGANIZATION_USER);

        return $this;
    }
}
