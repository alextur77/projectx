<?php

namespace AppBundle\Repository;

use Aristek\Component\ORM\EntityRepository;
use AppBundle\Entity\Role;
use Doctrine\ORM\QueryBuilder;

/**
 * Class RoleRepository
 * @package AppBundle\Repository
 *
 * @method Role findOneBy(array $criteria, array $orderBy = null)
 * @method Role find($id, $lockMode = null, $lockVersion = null)
 * @method Role[] findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class RoleRepository extends EntityRepository
{
    const ALIAS = 'Role';

    /**
     * @param string $alias
     *
     * @return string
     */
    private function resolveAlias($alias)
    {
        return null === $alias ? $this::ALIAS : $alias;
    }

    /**
     * @param string|null $alias
     * @param string|null $indexBy
     *
     * @return QueryBuilder
     */
    public function createQueryBuilder($alias = null, $indexBy = null)
    {
        $alias = $this->resolveAlias($alias);

        return parent::createQueryBuilder($alias, $indexBy);
    }

    /**
     * @param string $type
     *
     * @return QueryBuilder
     */
    public function createQueryBuilderByType($type)
    {
        $qb = $this->createQueryBuilder();
        $alias = $qb->getRootAliases()[0];
        $qb
            ->andWhere($alias . '.type = :type')
            ->setParameter('type', $type);

        return $qb;
    }

    /**
     * @param string $type
     * @param string $code
     *
     * @return Role
     */
    public function findByTypeAndCode($type, $code)
    {
        return $this->findOneBy(['type' => $type, 'code' => $code]);
    }

    /**
     * @param string $type
     *
     * @return Role[]
     */
    public function findByType($type)
    {
        $qb = $this->createQueryBuilderByType($type);

        return $qb->getQuery()->getResult();
    }
}
