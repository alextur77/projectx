<?php

namespace AppBundle\Repository;

use AppBundle\Entity\User;
use Aristek\Component\ORM\EntityRepository;
use AppBundle\Entity\TopicMessageRead;

/**
 * Class TopicMessageReadRepository
 * @package AppBundle\Repository
 *
 * @method TopicMessageRead findOneBy(array $criteria, array $orderBy = null)
 * @method TopicMessageRead find($event, $lockMode = null, $lockVersion = null)
 */
class TopicMessageReadRepository extends EntityRepository
{
    /**
     * @param string $topicIds
     * @param User   $recipient
     *
     * @return array
     */
    public function getTopicReadMessage($topicIds, $recipient)
    {
        $query = $this->createQueryBuilder('e');

        $query
            ->andWhere('e.topic IN (' . $topicIds . ')')
            ->andWhere('e.recipient = :recipient')
            ->setParameter('recipient', $recipient);

        return $query->getQuery()->getArrayResult();
    }

}
