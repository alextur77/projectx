<?php

namespace AppBundle\Service\RegistrationInvite;

use AppBundle\Admin\UserRegistrationInviteAdmin;
use AppBundle\Entity\Organization;
use AppBundle\Entity\Group;
use AppBundle\Repository\GroupUserPermissionRepository;
use AppBundle\Repository\OrganizationUserPermissionRepository;
use AppBundle\Repository\UserRepository;
use AppBundle\Manager\RegistrationInviteManager;
use Symfony\Bundle\TwigBundle\TwigEngine;
use Symfony\Component\Translation\DataCollectorTranslator;
use FOS\UserBundle\Util\Canonicalizer;

/**
 * Class RegistrationInviteService
 * @package AppBundle\Service\RegistrationInvite
 */
class RegistrationInviteService
{
    /**
     * @var int - The quantity of the emails sent last time.
     */
    private $sentEmailsQuantity = 0;

    /**
     * @var RegistrationInviteManager
     */
    protected $registrationInviteManager;

    /**
     * @var TwigEngine
     */
    protected $twigEngine;

    /**
     * @var \Swift_Mailer
     */
    protected $mailer;

    /**
     * @var Canonicalizer
     */
    protected $canonicalizer;

    /**
     * @var UserRepository
     */
    protected $userRepository;

    /**
     * @var OrganizationUserPermissionRepository
     */
    protected $organizationUserPermissionRepository;

    /**
     * @var GroupUserPermissionRepository
     */
    protected $groupUserPermissionRepository;

    /** @var DataCollectorTranslator */
    protected $translator;

    /**
     * @var string
     */
    protected $fromEmail;

    /**
     * @var UserRegistrationInviteAdmin
     */
    protected $userRegistrationInviteAdmin;

    /**
     * RegistrationInviteService constructor.
     *
     * @param string $fromEmail
     */
    public function __construct($fromEmail)
    {
        $this->fromEmail = $fromEmail;
    }

    /**
     * Stores in the database records for each pair of $email/$group|$organization.
     * Sends emails for each email in $emailArray.
     *
     * @param string[]     $emailArray
     * @param Group[]      $groups
     * @param Organization $organization
     *
     * @return int - The sent email messages quantity
     *
     * @throws \InvalidArgumentException
     */
    public function process(array $emailArray, array $groups, Organization $organization = null)
    {
        $this->sentEmailsQuantity = 0;

        if (empty($emailArray)) {
            throw new \InvalidArgumentException(__METHOD__ . ': The emailArray parameter must contain a value!');
        }
        if ((null !== $organization) && !($organization instanceof Organization)) {
            throw new \InvalidArgumentException(
                __METHOD__ . ': The organization parameter must be an AppBundle\\Entity\\Organization!'
            );
        }
        if ((null === $organization) && empty($groups)) {
            throw new \InvalidArgumentException(
                __METHOD__ . ': The organization or at least one group must be specified!'
            );
        }
        foreach ($groups as $group) {
            if (!($group instanceof Group)) {
                throw new \InvalidArgumentException(
                    __METHOD__ . ': The group must be an AppBundle\\Entity\\Organization!'
                );
            }
        }

        // Canonicalize $emailArray and remove duplicates from it:
        foreach ($emailArray as &$email) {
            $email = $this->canonicalizer->canonicalize($email);
        }
        // According to http://php.net/manual/en/function.array-unique.php
        // the function array_flip() is much more faster then the function array_unique().
        $emailArray = array_flip($emailArray); // Exchanges all keys with their associated values in an array.

        foreach ($emailArray as $email => $dummy) {
            $this->sendInviteEmail($email, $groups, $organization);
        }

        return $this->sentEmailsQuantity;
    }

    /**
     * @param string       $canonicalEmail
     * @param Group[]      $groups
     * @param Organization $organization
     *
     * @return void
     *
     * @throws \Twig_Error
     */
    private function sendInviteEmail($canonicalEmail, array $groups, Organization $organization = null)
    {
        $token = $this->generateToken($canonicalEmail);
        $parameters = ['token' => $token, 'admin' => $this->userRegistrationInviteAdmin, 'groups' => []];
        $user = $this->userRepository->findUserByCanonicalEmail($canonicalEmail);
        if (null == $user) {
            $parameters['fullName'] = $canonicalEmail;
        } else {
            $parameters['fullName'] = $user->getFullName();
        }

        if (null !== $organization) {
            if (null === $user ||
                ((null !== $user) && !$this->organizationUserPermissionRepository->checkExists($organization, $user))
            ) {
                $this->registrationInviteManager->addNew($canonicalEmail, $token, null, $organization);
                $parameters['organization'] = $organization->getName();
            }
        }

        foreach ($groups as $group) {
            if (null === $user ||
                ((null !== $user) && !$this->groupUserPermissionRepository->checkExists($group, $user))
            ) {
                $this->registrationInviteManager->addNew($canonicalEmail, $token, $group, null);
                $parameters['groups'][] = $group->getName();
            }
        }

        if (array_key_exists('organization', $parameters) || !empty($parameters['groups'])) {
            $message = \Swift_Message::newInstance()
                ->setSubject($this->translator->trans('registration_invite.email.subject'))
                ->setFrom($this->fromEmail)
                ->setTo($canonicalEmail)
                ->setBody(
                    $this->twigEngine->render('Emails/RegistrationInvite.html.twig', $parameters),
                    'text/html'
                )
                ->addPart(
                    $this->twigEngine->render('Emails/RegistrationInvite.txt.twig', $parameters),
                    'text/plain'
                );

            if ($this->mailer->send($message)) {
                $this->sentEmailsQuantity++;
            }
        }
    }

    /**
     * @param RegistrationInviteManager $registrationInviteManager
     *
     * @return RegistrationInviteService
     */
    public function setRegistrationInviteManager(RegistrationInviteManager $registrationInviteManager)
    {
        $this->registrationInviteManager = $registrationInviteManager;

        return $this;
    }

    /**
     * @param TwigEngine $twigEngine
     *
     * @return RegistrationInviteService
     */
    public function setTwigEngine(TwigEngine $twigEngine)
    {
        $this->twigEngine = $twigEngine;

        return $this;
    }

    /**
     * @param \Swift_Mailer $mailer
     *
     * @return RegistrationInviteService
     */
    public function setMailer(\Swift_Mailer $mailer)
    {
        $this->mailer = $mailer;

        return $this;
    }

    /**
     * @param string $salt
     *
     * @return string
     */
    public function generateToken($salt)
    {
        return md5(strrev(dechex(time())) . $salt);
    }

    /**
     * @param Canonicalizer $canonicalizer
     *
     * @return RegistrationInviteService
     */
    public function setCanonicalizer($canonicalizer)
    {
        $this->canonicalizer = $canonicalizer;

        return $this;
    }

    /**
     * @return int
     */
    public function getSentEmailsQuantity()
    {
        return $this->sentEmailsQuantity;
    }

    /**
     * @param UserRepository $userRepository
     *
     * @return RegistrationInviteService
     */
    public function setUserRepository($userRepository)
    {
        $this->userRepository = $userRepository;

        return $this;
    }

    /**
     * @param OrganizationUserPermissionRepository $organizationUserPermissionRepository
     *
     * @return RegistrationInviteService
     */
    public function setOrganizationUserPermissionRepository($organizationUserPermissionRepository)
    {
        $this->organizationUserPermissionRepository = $organizationUserPermissionRepository;

        return $this;
    }

    /**
     * @param GroupUserPermissionRepository $groupUserPermissionRepository
     *
     * @return RegistrationInviteService
     */
    public function setGroupUserPermissionRepository($groupUserPermissionRepository)
    {
        $this->groupUserPermissionRepository = $groupUserPermissionRepository;

        return $this;
    }

    /**
     * @param DataCollectorTranslator $translator
     *
     * @return RegistrationInviteService
     */
    public function setTranslator($translator)
    {
        $this->translator = $translator;

        return $this;
    }

    /**
     * @param UserRegistrationInviteAdmin $userRegistrationInviteAdmin
     *
     * @return RegistrationInviteService
     */
    public function setUserRegistrationInviteAdmin($userRegistrationInviteAdmin)
    {
        $this->userRegistrationInviteAdmin = $userRegistrationInviteAdmin;

        return $this;
    }
}
