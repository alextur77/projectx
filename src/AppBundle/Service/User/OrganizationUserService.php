<?php

namespace AppBundle\Service\User;

use AppBundle\Entity\Group;
use AppBundle\Entity\Organization;
use AppBundle\Entity\User;
use AppBundle\Repository\GroupRepository;
use AppBundle\Repository\GroupUserPermissionRepository;
use AppBundle\Repository\OrganizationUserPermissionRepository;

/**
 * Class OrganizationUserService
 */
class OrganizationUserService
{
    /**
     * @var CurrentUserService
     */
    protected $currentUserService;

    /**
     * @var GroupRepository
     */
    protected $groupRepository;

    /**
     * @var GroupUserPermissionRepository
     */
    protected $groupUserPermissionRepository;

    /**
     * @var OrganizationUserPermissionRepository
     */
    protected $organizationUserPermissionRepository;

    /**
     * @param User|null         $user
     * @param Organization|null $organization
     *
     * @return Group[]
     */
    public function getGroups($user = null, $organization = null)
    {
        $organization = $organization ?: $this->currentUserService->getCurrentOrganization();
        $user = $user ?: $this->currentUserService->getCurrentUser();

        return $this->groupRepository->findUserGroupsByOrganization($user, $organization);
    }

    /**
     * @param User              $user
     * @param Organization|null $organization
     */
    public function removeUserFromOrganization($user, $organization = null)
    {
        $organization = $organization ?: $this->currentUserService->getCurrentOrganization();

        $em = $this->organizationUserPermissionRepository->em();
        $em->beginTransaction();

        $this->organizationUserPermissionRepository->removeUser($user, $organization);
        $this->groupUserPermissionRepository->removeUserFromOrganization($user, $organization);

        $em->commit();
    }

    /**
     * @param CurrentUserService $currentUserService
     *
     * @return OrganizationUserService
     */
    public function setCurrentUserService($currentUserService)
    {
        $this->currentUserService = $currentUserService;

        return $this;
    }

    /**
     * @param GroupRepository $groupRepository
     *
     * @return OrganizationUserService
     */
    public function setGroupRepository($groupRepository)
    {
        $this->groupRepository = $groupRepository;

        return $this;
    }

    /**
     * @param GroupUserPermissionRepository $groupUserPermissionRepository
     *
     * @return OrganizationUserService
     */
    public function setGroupUserPermissionRepository($groupUserPermissionRepository)
    {
        $this->groupUserPermissionRepository = $groupUserPermissionRepository;

        return $this;
    }

    /**
     * @param OrganizationUserPermissionRepository $organizationUserPermissionRepository
     *
     * @return OrganizationUserService
     */
    public function setOrganizationUserPermissionRepository($organizationUserPermissionRepository)
    {
        $this->organizationUserPermissionRepository = $organizationUserPermissionRepository;

        return $this;
    }
}
