<?php

namespace AppBundle\Service\User;

use AppBundle\Entity\Group;
use AppBundle\Entity\Organization;
use AppBundle\Entity\RegistrationInvite;
use AppBundle\Entity\Role;
use AppBundle\Entity\User;
use AppBundle\Repository\GroupRepository;
use AppBundle\Repository\GroupUserPermissionRepository;
use AppBundle\Repository\OrganizationRepository;
use AppBundle\Repository\OrganizationUserPermissionRepository;
use AppBundle\Repository\RegistrationInviteRepository;
use AppBundle\Repository\UserRepository;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

/**
 * Class CurrentUserService
 */
class CurrentUserService
{
    /**
     * @var string|null - The current user role code in $organization
     */
    private $currentUserRoleInOrganization = null;

    /**
     * @var string|null - The current user role code in $organization
     */
    private $currentUserRoleInGroup = null;

    /**
     * @var boolean|null - Has the current user any group admin access?
     */
    private $groupAdminAccess = null;

    /**
     * @var OrganizationUserPermissionRepository
     */
    protected $organizationUserPermissionRepository;

    /**
     * @var GroupUserPermissionRepository
     */
    protected $groupUserPermissionRepository;

    /**
     * @var OrganizationRepository
     */
    protected $organizationRepository;

    /**
     * @var UserRepository
     */
    protected $userRepository;

    /**
     * @var GroupRepository
     */
    protected $groupRepository;

    /**
     * @var RegistrationInviteRepository
     */
    protected $registrationInviteRepository;

    /**
     * @var TokenStorageInterface
     */
    protected $tokenStorage;

    /**
     * @return Organization
     */
    public function getCurrentOrganization()
    {
        $result = $this->organizationRepository->findOneBy(
            ['name' => $this->getCurrentUser()->getUserCurrentOrganization()]
        );
        if (!empty($result)) {

            $organizationName = $result->getName();

            if (empty($organizationName)) {

                /** @var Group $group */
                $group = $this->registrationInviteRepository->findOneBy(['user' => $this->getCurrentUser()]);

                if (!empty($group)) {

                    $organizations = $this->groupRepository->findOneBy(['id' => $group->getGroup()]);

                    $result = $this->organizationRepository->findOneBy(['id' => $organizations->getOrganization()]);
                } else {
                    $result = null;
                }
            }
        }

        return $result;
    }

    /**
     * @return User
     */
    public function getCurrentUser()
    {
        return $this->tokenStorage->getToken()->getUser();
    }

    /**
     * @return string|null - The current user role code in the current organization
     */
    public function getCurrentUserRoleInOrganization()
    {
        if (null === $this->currentUserRoleInOrganization) {
            $user = $this->getCurrentUser();
            $organization = $this->getCurrentOrganization();
            $this->currentUserRoleInOrganization = $this->organizationUserPermissionRepository
                ->getUserRoleInOrganization(
                    $user,
                    $organization
                );
        }

        return $this->currentUserRoleInOrganization;
    }

    /**
     * @return string|null - The current user role code in the current group
     */
    public function getCurrentUserRoleInGroup()
    {
        if (null === $this->currentUserRoleInGroup) {
            $user = $this->getCurrentUser();
            /** @var RegistrationInvite $groupID */
            $groupID = $this->registrationInviteRepository->findOneBy(['user' => $user]);
            if (!empty($groupID)) {
                $group = $this->groupRepository->find(['id' => $groupID->getGroup()]);

                $this->currentUserRoleInGroup = $this->groupUserPermissionRepository
                    ->getUserGroupRole(
                        $group,
                        $user
                    );
            }
        }

        return $this->currentUserRoleInGroup;
    }

    /**
     * @return boolean - Has the current user any group admin access?
     */
    public function hasCurrentUserAnyGroupAdminAccess()
    {
        if (null === $this->groupAdminAccess) {
            $organization = $this->getCurrentOrganization();
            $user = $this->getCurrentUser();
            $role = $this->getCurrentUserRoleInOrganization();

            $this->groupAdminAccess = Role::ROLE_ORGANIZATION_ADMIN === $role;
            if (!$this->groupAdminAccess) {
                if (($role === Role::ROLE_ORGANIZATION_USER) or (null === $role)) {
                    // $user has Role::ROLE_ORGANIZATION_USER role in $organization or have not any role at all:
                    $this->groupAdminAccess = $this->groupUserPermissionRepository->hasUserAnyGroupAdminAccess(
                        $user,
                        $organization
                    );
                } else {
                    throw new \LogicException(__METHOD__ . ': Unknown organization user role: "' . $role . '""!');
                }
            }
        }

        return $this->groupAdminAccess;
    }

    /**
     * @param OrganizationRepository $organizationRepository
     *
     * @return CurrentUserService
     */
    public function setOrganizationRepository($organizationRepository)
    {
        $this->organizationRepository = $organizationRepository;

        return $this;
    }

    /**
     * @param UserRepository $userRepository
     *
     * @return $this
     */
    public function setUserRepository($userRepository)
    {
        $this->userRepository = $userRepository;

        return $this;
    }

    /**
     * @param GroupRepository $groupRepository
     *
     * @return CurrentUserService
     */
    public function setGroupRepository($groupRepository)
    {
        $this->groupRepository = $groupRepository;

        return $this;
    }

    /**
     * @param RegistrationInviteRepository $registrationInviteRepository
     *
     * @return CurrentUserService
     */
    public function setRegistrationInviteRepository($registrationInviteRepository)
    {
        $this->registrationInviteRepository = $registrationInviteRepository;

        return $this;
    }

    /**
     * @param TokenStorageInterface $tokenStorage
     *
     * @return CurrentUserService
     */
    public function setTokenStorage($tokenStorage)
    {
        $this->tokenStorage = $tokenStorage;

        return $this;
    }

    /**
     * @param OrganizationUserPermissionRepository $organizationUserPermissionRepository
     *
     * @return CurrentUserService
     */
    public function setOrganizationUserPermissionRepository($organizationUserPermissionRepository)
    {
        $this->organizationUserPermissionRepository = $organizationUserPermissionRepository;

        return $this;
    }

    /**
     * @param GroupUserPermissionRepository $groupUserPermissionRepository
     *
     * @return CurrentUserService
     */
    public function setGroupUserPermissionRepository($groupUserPermissionRepository)
    {
        $this->groupUserPermissionRepository = $groupUserPermissionRepository;

        return $this;
    }
}
