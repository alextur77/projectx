<?php

namespace AppBundle\Service;

use AppBundle\Repository\PostRepository;
use QueueBundle\Manager\QueueManager;

/**
 * Class PrepareQueueService
 *
 * AppBundle\Service
 */
class PrepareQueueService
{
    /**
     * @var PostRepository;
     */
    protected $postRepository;

    /**
     * @var QueueManager
     */
    protected $queueManager;

    /**
     * @return bool
     */
    public function prepareQueue()
    {
        $topic = $this->postRepository->findPostPeriodList();

        foreach ($topic as $key => $value) {

            $recipient = 'text@aaa.ru';

            $this->queueManager->createQueue($recipient, $value['topic']['description'], $value['topic']['name'], 'email');
        }

        return true;
    }

    /**
     * @param PostRepository $postRepository
     *
     * @return $this
     */
    public function setPostRepository($postRepository)
    {
        $this->postRepository = $postRepository;

        return $this;
    }

    /**
     * @param QueueManager $queueManager
     *
     * @return $this
     */
    public function setQueueManager($queueManager)
    {
        $this->queueManager = $queueManager;

        return $this;
    }
}