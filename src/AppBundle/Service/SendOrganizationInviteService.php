<?php

namespace AppBundle\Service;

use AppBundle\Service\RegistrationInvite\RegistrationInviteService;
use Symfony\Component\Translation\DataCollectorTranslator;

/**
 * Class SendOrganizationInviteService.
 */
class SendOrganizationInviteService
{
    /**
     * EmailSend constructor.
     *
     * @param string $fromEmailAddress
     */
    public function __construct($fromEmailAddress)
    {
        $this->fromEmailAddress = $fromEmailAddress;
    }

    /**
     * @var \Swift_Mailer
     */
    protected $swiftEmail;

    /** @var DataCollectorTranslator */
    protected $translator;

    /**
     * @var
     */
    protected $fromEmailAddress;

    /**
     * @param \Swift_Mailer $swiftEmail
     *
     * @return $this
     */
    public function setSwiftMailer($swiftEmail)
    {
        $this->swiftEmail = $swiftEmail;

        return $this;
    }

    /**
     * @param string $recipient
     * @param string $body
     *
     * @return void
     */
    public function sendEmailInviteAction($recipient, $body)
    {
        $subject = $this->translator->trans('registration_invite.email.organization_invite');

        $message = \Swift_Message::newInstance()
            ->setSubject($subject)
            ->setFrom($this->fromEmailAddress)
            ->setTo($recipient)
            ->setBody($body);

        $this->swiftEmail->send($message);
    }

    /**
     * @param DataCollectorTranslator $translator
     *
     * @return RegistrationInviteService
     */
    public function setTranslator($translator)
    {
        $this->translator = $translator;

        return $this;
    }
}