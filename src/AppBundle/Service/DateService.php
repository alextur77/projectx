<?php

namespace AppBundle\Service;

/**
 * Class DateService
 */
/**
 * Class DateService
 * @package AppBundle\Service
 */
class DateService
{
    /**
     * @param \DateTime $date
     *
     * @return string
     */
    public function getFormattedDate(\DateTime $date)
    {
        return $date->format($this->getDateFormat());
    }

    /**
     * @param \DateTime $date
     *
     * @return string
     */
    public function getFormattedDateTime(\DateTime $date)
    {
        return $date->format($this->getDateTimeFormat());
    }

    /**
     * @return string
     */
    public function getDateTimeFormat()
    {
        return 'm.d.Y H:i';
    }

    /**
     * @return string
     */
    public function getDateFormat()
    {
        return 'm.d.Y';
    }

    /**
     * @return string
     */
    public function getHourMinuteFormat()
    {
        return 'H:i';
    }

    /**
     * @return string
     */
    public function getDateFormatUnix()
    {
        return 'd.m.Y';
    }

    /**
     * @return string
     */
    public function getDateYearMonthDayFullFormat()
    {
        return 'Y-m-d H:i:s';
    }

    /**
     * @return string
     */
    public function getDateYearMonthDayFormat()
    {
        return 'Y-m-d';
    }

    /**
     * @param string $date
     *
     * @return bool|string
     */
    public function getFormattedDateFromUnix($date)
    {
        return date($this->getDateFormatUnix(), $date);
    }

    /**
     * @param \DateTime $date
     *
     * @return string
     */
    public function getFormattedDayMonthYearDate(\DateTime $date)
    {
        return $date->format($this->getDateFormatUnix());
    }

    /**
     * @param \DateTime $date
     *
     * @return string
     */
    public function getFormattedHourMinuteDate(\DateTime $date)
    {
        return $date->format($this->getHourMinuteFormat());
    }

    /**
     * @param \DateTime $date
     *
     * @return string
     */
    public function getFormattedYearMonthDatFullDate(\DateTime $date)
    {
        return $date->format($this->getDateYearMonthDayFullFormat());
    }
}