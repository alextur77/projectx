<?php

namespace AppBundle\Controller\API;

use Aristek\Bundle\ExtraBundle\Exception\AppException;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class SecurityController
 *
 * @Route("/api")
 */
class SecurityController extends APIController
{
    /**
     * @return JsonResponse
     *
     * @Route("/login")
     * @Method("PUT")
     */
    public function loginAction()
    {
        $options = new OptionsResolver();
        $options->setRequired(['user', 'password']);

        $data = $options->resolve($this->getData());
        $translator = $this->get('translator');

        return $this->process(
            function () use ($data, $translator) {
                $user = $this->get('app.user_provider')->loadUserByUsername($data['user']);

                if (null === $user) {
                    throw new AppException($translator->trans('authentication_failed'));
                }

                $valid = $this->get('security.password_encoder')->isPasswordValid($user, $data['password']);

                if (!$valid) {
                    throw new AppException($translator->trans('authentication_failed'));
                }

                $this->get('fos_user.security.login_manager')->loginUser('main', $user);
            }
        );
    }
}
