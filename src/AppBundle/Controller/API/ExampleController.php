<?php

namespace AppBundle\Controller\API;

use Aristek\Bundle\ExtraBundle\Exception\AppException;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class ExampleController
 *
 * @Route("/api/examples")
 */
class ExampleController extends APIController
{
    /**
     * Returns list of examples
     *
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     *
     * @Route("/")
     * @Method("GET")
     */
    public function exampleListAction()
    {
        return $this->process(
            function () {
                return [
                    ['id' => 1, 'text' => 'test1'],
                    ['id' => 2, 'text' => 'test2'],
                    ['id' => 3, 'text' => 'test3'],
                ];
            }
        );
    }

    /**
     * Returns list of examples
     *
     * @param int $id
     *
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     *
     * @Route("/{id}")
     * @Method("GET")
     */
    public function exampleItemAction($id)
    {
        return $this->process(
            function () use ($id) {
                $data = [
                    ['id' => 1, 'text' => 'test1'],
                    ['id' => 2, 'text' => 'test2'],
                    ['id' => 3, 'text' => 'test3'],
                ];

                foreach ($data as $row) {
                    if ($row['id'] == $id) {
                        return $row;
                    }
                }

                return [];
            }
        );
    }

    /**
     * Returns list of examples
     *
     * @param int $id
     *
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     *
     * @Route("/{id}")
     * @Method("PUT")
     */
    public function exampleUpdateItemAction($id)
    {
        $options = new OptionsResolver();
        $options->setRequired(['id' , 'text']);
        $data = $options->resolve($this->getData());
        $translator = $this->get('translator');

        return $this->process(
            function () use ($id, $data, $translator) {
                $arr = [
                    ['id' => 1, 'text' => 'test1'],
                    ['id' => 2, 'text' => 'test2'],
                    ['id' => 3, 'text' => 'test3'],
                ];

                foreach ($arr as $row) {
                    if ($row['id'] == $id) {
                        return $data;
                    }
                }

                throw new AppException($translator->trans('id_not_found'));
            }
        );
    }

    /**
     * Returns list of examples
     *
     * @param int $id
     *
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     *
     * @Route("/{id}")
     * @Method("DELETE")
     */
    public function exampleDeleteItemAction($id)
    {
        $translator = $this->get('translator');

        return $this->process(
            function () use ($id, $translator) {
                $data = [
                    ['id' => 1, 'text' => 'test1'],
                    ['id' => 2, 'text' => 'test2'],
                    ['id' => 3, 'text' => 'test3'],
                ];

                foreach ($data as $row) {
                    if ($row['id'] == $id) {
                        return $row;
                    }
                }

                throw new AppException($translator->trans('id_not_found'));
            }
        );
    }
}
