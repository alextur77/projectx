<?php

namespace AppBundle\Controller\API;

use Aristek\Bundle\ExtraBundle\Controller\BaseController;
use Aristek\Bundle\ExtraBundle\Exception\AppException;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * Class APIController
 */
class APIController extends BaseController
{
    /**
     * @return mixed
     */
    protected function getData()
    {
        $request = $this->get('request_stack')->getCurrentRequest();

        $data = json_decode($request->getContent(), true);

        return $data;
    }

    /**
     * @param callable $callback
     *
     * @return JsonResponse
     */
    protected function process($callback)
    {
        try {
            $response = call_user_func($callback);
        } catch (AppException $e) {
            return $this->getErrorResponse($e->getMessage());
        } catch (\Exception $e) {
            return $this->getErrorResponse('Error!');
        }

        return $this->getSuccessResponse($response);
    }

    /**
     * @param $data
     *
     * @return JsonResponse
     */
    protected function getSuccessResponse($data = null)
    {
        $response = [
            'result' => 'success'
        ];

        if (null !== $data) {
            $response['data'] = $data;
        }

        return new JsonResponse($response);
    }

    /**
     * @param $data
     *
     * @return JsonResponse
     */
    protected function getErrorResponse($data = null)
    {
        $response = [
            'result' => 'error'
        ];

        if (null !== $data) {
            $response['data'] = $data;
        }

        return new JsonResponse($response);
    }
}
