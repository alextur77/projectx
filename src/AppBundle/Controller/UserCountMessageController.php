<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Role;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

/**
 * Class UserCountMessageController
 */
class UserCountMessageController extends Controller
{
    /**
     * @param Request $request
     *
     * @return Response
     *
     * @Route("/count-messages")
     */
    public function getNumberOfMessagesAction(Request $request)
    {
        $organizationFromRequest = $request->query->get('organization');

        $organization = $this->get('app.organization_repository')->findOneBy(['name' => $organizationFromRequest]);

        $messages = $this->get('app.personal_message_repository')->findBy(
            ['organization' => $organization, 'recipient' => $this->getUser()]
        );

        $countMessage = 0;

        foreach ($messages as $value) {

            $message = $this->get('app.personal_message_read_repository')->findBy(['message' => $value->getID()]);

            if (empty($message)) {
                $countMessage++;
            }
        }

        $response = new JsonResponse();

        $responsePeriodMass = [];

        $responsePeriodMass['count'] = $countMessage;

        $response->setData($responsePeriodMass);

        return $response->send();
    }

    /**
     * @param Request $request
     *
     * @return Response
     *
     * @Route("/count-topic-message")
     */
    public function getNumberOfMessagesFromTopicAction(Request $request)
    {
        $organizationFromRequest = $request->query->get('organization');

        $organization = $this->get('app.organization_repository')->findOneBy(['name' => $organizationFromRequest]);

        $topic = $this->get('app.topic_repository')->findBy(['organization' => $organization]);

        $roleInOrganization = $this->get('app.organization_user_permission_repository')->getUserRoleInOrganization(
            $this->getUser(),
            $organization
        );

        if ($roleInOrganization == Role::ROLE_ORGANIZATION_ADMIN) {

            $groups = $this->get('app.group_repository')->findActiveByOrganization($organization);
        } else {

            $groups = $this->get('app.group_repository')->findUserGroupsByOrganization($this->getUser(), $organization);
        }

        $topicMass = [];

        foreach ($topic as $item) {
            foreach ($groups as $value) {
                $topicID = $this->get('app.topic_group_permission_repository')->findOneBy(
                    ['topic' => $item->getId(), 'group' => $value->getId()]
                );
                if (!empty($topicID)) {
                    $topicMass[] = $item->getId();
                }
            }
        }

        $numberOfMessage = 0;

        foreach (array_unique($topicMass) as $value) {

            $topic = $this->get('app.topic_repository')->findOneBy(['id' => $value]);

            $post = $this->get('app.post_repository')->findBy(['topic' => $topic]);

            foreach ($post as $item) {

                $readMessage = $this->get('app.topic_message_read_repository')->findOneBy(
                    ['topic' => $topic, 'post' => $item, 'recipient' => $this->getUser()]
                );

                if (empty($readMessage)) {
                    $numberOfMessage++;
                }
            }
        }

        unset($topicMass);

        $response = new JsonResponse();

        $responsePeriodMass = [];

        $responsePeriodMass['count'] = $numberOfMessage;

        $response->setData($responsePeriodMass);

        return $response->send();
    }
}