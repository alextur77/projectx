<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Role;
use Aristek\Bundle\W2uiBundle\Form\Type\CheckboxFormType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class OrganizationController
 */
class OrganizationController extends Controller
{
    /**
     * @param Request $request
     *
     * @return Response
     *
     * @Route("/organization/{user}")
     */
    public function newAction(Request $request, $user)
    {
        $form = $this->createFormBuilder()
            ->add('orgname', TextType::class, ['required' => true])
            ->add('orgaddress', TextType::class, ['required' => true])
            ->add('orgcity', TextType::class, ['required' => true])
            ->add('state', TextType::class, ['required' => true])
            ->add('zip', TextType::class, ['required' => true])
            ->add('active', CheckboxFormType::class, ['required' => false])
            ->add('public', CheckboxFormType::class, ['required' => false])
            ->add('allowsignup', CheckboxFormType::class, ['required' => false])
            ->add('email', TextType::class)
            ->add('phone', TextType::class)
            ->add('website', TextType::class)
            ->getForm();

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $task = $form->getData();

            $this
                ->get('app.organization_manager')
                ->add(
                    $task['orgname'],
                    $task['orgaddress'],
                    $task['orgcity'],
                    $task['state'],
                    $task['zip'],
                    $task['active'],
                    $task['public'],
                    $task['allowsignup'],
                    $task['email'],
                    $task['phone'],
                    $task['website']
                );

            $currentUser = $this->get('app.user_repository')->findOneBy(['username' => $user]);

            $organization = $this->get('app.organization_repository')->findOneBy(['name' => $task['orgname']]);

            $this->get('app.organization_user_permission_manager')->registerUser(
                $organization,
                $currentUser,
                Role::ROLE_ORGANIZATION_ADMIN
            );

            return $this->redirectToRoute(
                'app_grouporganization_new',
                ['org' => $task['orgname']]
            );
        }

        return $this->render(
            '@App/organization.create.form.html.twig',
            array(
                'form' => $form->createView(),
            )
        );
    }
}