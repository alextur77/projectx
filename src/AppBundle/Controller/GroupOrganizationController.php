<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Group;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class GroupOrganizationController
 */
class GroupOrganizationController extends Controller
{
    /**
     * @param Request $request
     * @param string  $org
     *
     * @return Response
     *
     * @Route("/group/{org}")
     */
    public function newAction(Request $request, $org)
    {
        if ($request->query->get('form') == 'new') {

            $organization = $this->get('app.organization_repository')->findOneBy(['name' => $org]);

            foreach ($request->query->get('massive') as $massItem) {

                $massElement = explode('_', $massItem);

                if ($massElement[0] == 'class') {

                    $nameClass = $massElement[1] . $massElement[2];

                    $this
                        ->get('app.group_manager')
                        ->add($nameClass, $organization, 1, 1);

                    /** @var Group $groupID */
                    $groupID = $this->get('app.group_repository')->findOneBy(
                        ['name' => $nameClass, 'organization' => $organization]
                    );

                    $this->get('app.group_closure_manager')->add($groupID->getId());
                }
            }
        } else {

            $abc = [];

            foreach (range(chr(0xC0), chr(0xDF)) as $b) {
                $abc[] = iconv('CP1251', 'UTF-8', $b);
            }

            return $this->render(
                '@App/group.create.form.html.twig',
                [
                    'alpha' => $abc,
                    'class' => Group::TYPE_CLASS_NUMBER,
                    'org' => $org
                ]
            );
        }
    }
}