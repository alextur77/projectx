<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class CsrfController
 * @package AppBundle\Controller
 */
class CsrfController extends Controller
{
    /**
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function deleteTokenAction()
    {
        return new Response($this->get('security.csrf.token_manager')->getToken('sonata.delete')->getValue());
    }
}
