<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class GroupInviteController
 */
class GroupInviteController extends Controller
{
    /**
     * @param Request $request
     * @param string  $org
     *
     * @return Response
     *
     * @Route("/invite-group/{org}")
     */
    public function inviteAction(Request $request, $org)
    {
        $organization = $this->get('app.organization_repository')->findOneBy(['name' => $org]);

        $group = $this->get('app.group_repository')->findBy(['organization' => $organization->getId()]);

        if ($request->query->get('form') == 'email') {

            foreach ($group as $key => $value) {

                $name = 'class_' . $value->getName();

                $groupNew = $this->get('app.group_repository')->findBy(
                    ['name' => $value->getName(), 'organization' => $organization]
                );

                $email = $request->query->get($name);

                $emailPreg = explode(',', $email);

                foreach ($emailPreg as $item) {

                    $pregMatch = preg_match(
                        '/^([a-z0-9_-]+\.)*[a-z0-9_-]+@[a-z0-9_-]+(\.[a-z0-9_-]+)*\.[a-z]{2,6}$/',
                        $item
                    );

                    if ($pregMatch == 0) {

                        return new Response(json_encode(1));
                        break;

                    } else {

                        $emailSend[] = $item;

                        $this->get('app.registration_invite_service')->process($emailSend, $groupNew);

                        $invite = $this->get('app.registration_invite_repository')->findOneBy(['email' => $item]);

                        $this->get('app.registration_invite_manager')->setRegistrationInvite($invite->getId());

                        unset($emailSend);

                    }
                }
            }

            return $this->redirect('/login');

        } else {

            $groupName = [];

            foreach ($group as $key => $value) {

                $groupName[] = $value->getName();
            }

            return $this->render(
                '@App/group.invite.form.html.twig',
                [
                    'group' => $groupName,
                    'org' => $org
                ]
            );
        }
    }
}