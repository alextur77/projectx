<?php

namespace AppBundle\Controller\Admin;

use AppBundle\Admin\TopicAdmin;
use AppBundle\Entity\Role;
use AppBundle\Entity\Topic;
use Sonata\AdminBundle\Controller\CRUDController;
use Sonata\AdminBundle\Datagrid\ProxyQueryInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class TopicController
 * @package AppBundle\Controller\Admin
 */
class TopicController extends CRUDController
{
    /**
     * @param Request $request
     *
     * @return Response
     * @throws \LogicException
     */
    public function updateModeratorsAction(Request $request)
    {
        if ($this->isXmlHttpRequest($request)) {
            /** @var integer[] $selectedGroupIds */
            $selectedGroupIds = $request->request->get('groupIds', []);
            // This action is called only when creating a new topic.
            // So there are no any group permissions nor assigned moderators for this new topic.
            // So it is unnecessary to call something like this:
            // list(, $moderators) =
            //     $this->get('app.topic_permission_mutex_manager')->getParticipantChoices(null, $selectedGroupIds);
            $moderators = $this->get('app.group_user_permission_repository')
                ->getUserFullNameByGroups($selectedGroupIds);

            return new Response(json_encode($moderators));
        } else {
            throw new \LogicException(__METHOD__ . ': Not AJAX call!');
        }
    }

    /**
     * @param integer $topicId - the id of the Topic witch participants are to be displayed
     *
     * @return RedirectResponse
     */
    public function participantsAction($topicId)
    {
        $admin = $this->get('app.topic_permission_admin');
        $parameters['topicId'] = $topicId;

        return new RedirectResponse($admin->generateUrl('list', $parameters));
    }

    /**
     * @inheritdoc
     */
    public function batchActionDelete(ProxyQueryInterface $query, Request $request = null)
    {
        $this->admin->checkAccess('batchDelete');

        $topics = $query->execute();
        $manager = $this->get('app.topic_manager');
        $deleted = true;
        foreach ($topics as $topic) {
            if (!$manager->delete($topic)) {
                $deleted = false;
            }
        }
        if ($deleted) {
            $this->sendMessage('sonata_flash_success', 'topic.batch.delete_success');
        } else {
            $this->sendMessage('sonata_flash_info', 'topic.batch.delete_warning');
        }

        return new RedirectResponse(
            $this->admin->generateUrl('list', ['filter' => $this->admin->getFilterParameters()])
        );
    }

    /**
     * @inheritdoc
     */
    public function deleteAction($id, Request $request = null)
    {
        if ($this->CheckAccess($id)) {
            return parent::deleteAction($id, $request);
        } else {
            return $this->AccessDenied();
        }
    }

    /**
     * @inheritdoc
     */
    public function editAction($id = null, Request $request = null)
    {
        if ($this->CheckAccess($id)) {
            return parent::editAction($id, $request);
        } else {
            return $this->AccessDenied();
        }
    }

    /**
     * @return Response
     */
    private function AccessDenied()
    {
        /** @var TopicAdmin $admin */
        $admin = $this->admin;

        return $this->render(
            ':Admin:access_denied.html.twig',
            ['text' => $admin->trans('topic.access_denied_text')]
        );
    }

    /**
     * @param integer|Topic|null $topic
     *
     * @return boolean
     */
    private function CheckAccess($topic = null)
    {
        $ok = null != $topic;
        if ($ok) {
            if ($topic instanceof Topic) {
                $id = $topic->getId();
            } else {
                $id = $topic;
            }
            $currentUserService = $this->get('app.current_user_service');
            $ok = $currentUserService->getCurrentUserRoleInOrganization() === Role::ROLE_ORGANIZATION_ADMIN;
            if (!$ok) {
                $user = $currentUserService->getCurrentUser();
                $rights = $this->get('app.topic_group_permission_repository')->hasUserTopicGroupAdminRights(
                    $user,
                    $topic
                );
                $ok = $rights[$id];
            }
        }

        return $ok;
    }

    /**
     * Adds a flash message for type.
     *
     * @param string $type
     * @param string $message
     * @param array  $parameters
     */
    protected function sendMessage($type, $message, $parameters = [])
    {
        parent::addFlash($type, $this->admin->trans($message, $parameters));
    }
}
