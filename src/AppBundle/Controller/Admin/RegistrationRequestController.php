<?php

namespace AppBundle\Controller\Admin;

use Sonata\AdminBundle\Controller\CRUDController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class RegistrationRequestController
 * @package AppBundle\Controller\Admin
 */
class RegistrationRequestController extends CRUDController
{
    /**
     * @param int $id
     *
     * @return RedirectResponse
     */
    public function approveAction($id)
    {
        $this->get('app.registration_request_manager')->approveById($id);

        return new RedirectResponse($this->admin->generateUrl('list', $this->admin->getFilterParameters()));
    }

    /**
     * @param Request $request
     * @param int     $id
     *
     * @return RedirectResponse
     */
    public function declineAction(Request $request, $id)
    {
        $this->get('app.registration_request_manager')->declineById($id, $request->get('decline-comment'));

        return new RedirectResponse($this->admin->generateUrl('list', $this->admin->getFilterParameters()));
    }
}
