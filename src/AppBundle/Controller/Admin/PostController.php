<?php

namespace AppBundle\Controller\Admin;

use AppBundle\Admin\PostAdmin;
use AppBundle\Entity\Role;
use AppBundle\Entity\Topic;
use Sonata\AdminBundle\Controller\CRUDController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class PostController
 * @package AppBundle\Controller\Admin
 */
class PostController extends CRUDController
{
    /**
     * @param integer $childId
     *
     * @return RedirectResponse|Response
     */
    public function quoteAction($childId)
    {
        if ($this->CheckAccess()) {
            return new RedirectResponse($this->admin->generateUrl('create', ['parent_post_id' => $childId]));
        } else {
            return $this->AccessDenied();
        }
    }

    /**
     * @param integer $postId
     * @param boolean $active
     * @param boolean $withChildren
     *
     * @return RedirectResponse
     */
    private function changeActive($postId, $active, $withChildren)
    {
        $post = $this->get('app.post_repository')->find($postId);
        $this->get('app.post_manager')->changeActive($post, $active, $withChildren);

        return new RedirectResponse(
            $this->admin->generateUrl('list', ['filter' => $this->admin->getFilterParameters()])
        );
    }

    /**
     * @param integer $childId
     *
     * @return RedirectResponse|Response
     */
    public function inactivateAction($childId)
    {
        if ($this->CheckAccess()) {
            return $this->changeActive($childId, false, false);
        } else {
            return $this->AccessDenied();
        }
    }

    /**
     * @param integer $childId
     *
     * @return RedirectResponse|Response
     */
    public function inactivateAllAction($childId)
    {
        if ($this->CheckAccess()) {
            return $this->changeActive($childId, false, true);
        } else {
            return $this->AccessDenied();
        }
    }

    /**
     * @param integer $childId
     *
     * @return RedirectResponse|Response
     */
    public function activateAction($childId)
    {
        if ($this->CheckAccess()) {
            return $this->changeActive($childId, true, false);
        } else {
            return $this->AccessDenied();
        }
    }

    /**
     * @param integer $childId
     *
     * @return RedirectResponse|Response
     */
    public function activateAllAction($childId)
    {
        if ($this->CheckAccess()) {
            return $this->changeActive($childId, true, true);
        } else {
            return $this->AccessDenied();
        }
    }

    /**
     * @inheritdoc
     */
    public function editAction($id = null, Request $request = null)
    {
        if ($this->CheckAccess()) {
            return parent::editAction($id, $request);
        } else {
            return $this->AccessDenied();
        }
    }

    /**
     * @inheritdoc
     */
    public function createAction(Request $request = null)
    {
        if ($this->CheckAccess()) {
            return parent::createAction($request);
        } else {
            return $this->AccessDenied();
        }
    }

    /**
     * @inheritdoc
     */
    public function listAction(Request $request = null)
    {
        if ($this->CheckAccess()) {
            $topic = $this->get('app.topic_repository')->findOneBy(['id' => $request->attributes->get('id')]);

            $post = $this->get('app.post_repository')->findBy(['topic' => $topic]);

            foreach ($post as $value) {
                $readPost = $this->get('app.topic_message_read_repository')->findBy(
                    ['topic' => $topic, 'post' => $value, 'recipient' => $this->getUser()]
                );
                if (empty($readPost)) {

                    $this->get('app.topic_message_read_manager')->add($topic, $value, $this->getUser());

                }
            }

            return parent::listAction($request);
        } else {
            return $this->AccessDenied();
        }
    }

    /**
     * @inheritdoc
     */
    public function deleteAction($id, Request $request = null)
    {
        throw new \AccessDeniedException(__METHOD__ . ': The Post entity cannot be deleted!');
    }

    /**
     * @return boolean
     */
    private function CheckAccess()
    {
        /** @var Topic $topic */
        $topic = $this->admin->getParent()->getSubject();
        $ok = null != $topic;
        if ($ok) {
            $currentUserService = $this->get('app.current_user_service');
            $ok = $currentUserService->getCurrentUserRoleInOrganization() === Role::ROLE_ORGANIZATION_ADMIN;
            if (!$ok) {
                $user = $currentUserService->getCurrentUser();
                $rights = $this->get('app.topic_group_permission_repository')->getUserTopicRights($user, $topic);
                $id = $topic->getId();
                $ok = false !== $rights[$id];
                if ($ok) {
                    $ok = $topic->getActive() || ($rights[$id] == Role::ROLE_GROUP_ADMIN);
                }
            }
        }

        return $ok;
    }

    /**
     * @return Response
     */
    private function AccessDenied()
    {
        /** @var PostAdmin $admin */
        $admin = $this->admin;

        return $this->render(
            ':Admin:access_denied.html.twig',
            ['text' => $admin->trans('post.access_denied_text')]
        );
    }
}
