<?php

namespace AppBundle\Controller\Admin;

use AppBundle\DataTransferObject\DTOGroup;
use Sonata\AdminBundle\Controller\CRUDController;
use Sonata\AdminBundle\Datagrid\ProxyQueryInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;

/**
 * Class BaseGroupController
 * @package AppBundle\Controller\Admin
 */
class BaseGroupController extends CRUDController
{
    /**
     * @param ProxyQueryInterface $selectedModelQuery
     * @param Request             $request
     *
     * @return RedirectResponse
     */
    public function batchActionShowTopics(ProxyQueryInterface $selectedModelQuery, Request $request = null)
    {
        if (!$this->admin->isGranted('LIST')) {
            throw new AccessDeniedException();
        }

        /** @var DTOGroup[] $groups */
        $groups = $selectedModelQuery->execute();
        $groupIds = [];
        foreach ($groups as $group) {
            if ($group->active) {
                $groupIds[] = $group->id;
            }
        }

        if (empty($groupIds)) {
            $this->sendMessage('sonata_flash_info', 'group.show_topics_warning');

            return new RedirectResponse(
                $this->admin->generateUrl('list', ['filter' => $this->admin->getFilterParameters()])
            );
        } else {
            return new RedirectResponse(
                $this->get('app.user_topic_admin')->generateListURLFilteredByGroups($request, $groupIds)
            );
        }
    }

    /**
     * Adds a flash message for type.
     *
     * @param string $type
     * @param string $message
     * @param array  $parameters
     */
    protected function sendMessage($type, $message, $parameters = [])
    {
        parent::addFlash($type, $this->admin->trans($message, $parameters));
    }
}
