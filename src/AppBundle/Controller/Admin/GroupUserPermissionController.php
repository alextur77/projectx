<?php

namespace AppBundle\Controller\Admin;

use Sonata\AdminBundle\Controller\CRUDController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;

/**
 * Class GroupUserPermissionController
 * @package AppBundle\Controller\Admin
 */
class GroupUserPermissionController extends CRUDController
{
    /**
     * @param Request $request
     * @param integer $childId - the $id of the GroupUserPermission witch role is to be changed
     *
     * @return Response
     */
    public function changeRoleAction(Request $request, $childId)
    {
        if (!$this->admin->isGranted('EDIT')) {
            throw new AccessDeniedException();
        }

        $role = $request->request->get('value');
        $role = $this->get('app.role_repository')->find($role);
        $result['status'] = $this->get('app.group_user_permission_manager')->changeUserRole($childId, $role);
        if ($result['status']) {
            $result['role'] = $role->getCode();
            $result['name'] = $request->request->get('name');
            $result['pk'] = $request->request->get('pk');

            $groupPermission = $this->get('app.group_user_permission_repository')->find($childId);
            $user = $this->get('app.current_user_service')->getCurrentUser();
            $access = $this->get('app.group_user_permission_repository')->hasUserGroupAdminAccess(
                $groupPermission->getGroup(),
                $user
            );
            $result['access'] = $access ? 1 : 0;
        }

        return $this->renderJson($result);
    }

    /**
     * @inheritdoc
     */
    public function listAction(Request $request = null)
    {
        $group = $this->admin->getParent()->getSubject();
        $user = $this->get('app.current_user_service')->getCurrentUser();
        if ($this->get('app.group_user_permission_repository')->hasUserGroupAdminAccess($group, $user)) {
            return parent::listAction($request);
        } else {
            return $this->render(
                ':Admin:access_denied.html.twig',
                ['text' => $this->admin->trans('group_user_permission.access_denied_text')]
            );
        }
    }

    /**
     * @inheritdoc
     */
    public function deleteAction($id, Request $request = null)
    {
        $group = $this->admin->getParent()->getSubject();
        $user = $this->get('app.current_user_service')->getCurrentUser();
        if ($this->get('app.group_user_permission_repository')->hasUserGroupAdminAccess($group, $user)) {
            return parent::deleteAction($id, $request);
        } else {
            return $this->render(
                ':Admin:access_denied.html.twig',
                ['text' => $this->admin->trans('group_user_permission.access_denied_text')]
            );
        }
    }
}
