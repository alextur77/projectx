<?php

namespace AppBundle\Controller\Admin;

use AppBundle\DataTransferObject\DTOGroup;
use Sonata\AdminBundle\Datagrid\ProxyQueryInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;

/**
 * Class GroupController
 * @package AppBundle\Controller\Admin
 */
class GroupController extends BaseGroupController
{
    /**
     * @param ProxyQueryInterface $selectedModelQuery
     *
     * @return RedirectResponse
     */
    public function batchActionSendInvite(ProxyQueryInterface $selectedModelQuery)
    {
        if (!$this->admin->isGranted('LIST')) {
            throw new AccessDeniedException();
        }

        /** @var DTOGroup[] $groups */
        $groups = $selectedModelQuery->execute();

        $groupIds = [];
        foreach ($groups as $group) {
            if ($group->active) {
                $groupIds[] = $group->id;
            }
        }

        if (empty($groupIds)) {
            $this->sendMessage('sonata_flash_info', 'group.send_invites_warning');

            return new RedirectResponse(
                $this->admin->generateUrl('list', ['filter' => $this->admin->getFilterParameters()])
            );
        } else {
            return new RedirectResponse(
                $this->get('app.registration_invite_admin')->generateUrl(
                    'create',
                    ['groups' => implode(',', $groupIds)]
                )
            );
        }
    }

    /**
     * @inheritdoc
     */
    public function batchActionDeleteSubtree(ProxyQueryInterface $query)
    {
        $this->admin->checkAccess('batchDelete');

        $manager = $this->get('app.group_manager');
        $groups = $query->execute();
        $groups = $manager->eliminateAncestorDescendantRelations($groups);
        foreach ($groups as $group) {
            $manager->deleteSubtree($group);
        }
        $this->addFlash('sonata_flash_success', 'flash_batch_delete_success');

        return new RedirectResponse(
            $this->admin->generateUrl('list', ['filter' => $this->admin->getFilterParameters()])
        );
    }

    /**
     * @inheritdoc
     */
    public function batchActionDelete(ProxyQueryInterface $query, Request $request = null)
    {
        $this->admin->checkAccess('batchDelete');

        $groups = $query->execute();
        $manager = $this->get('app.group_manager');
        foreach ($groups as $group) {
            $manager->delete($group);
        }
        $this->addFlash('sonata_flash_success', 'flash_batch_delete_success');

        return new RedirectResponse(
            $this->admin->generateUrl('list', ['filter' => $this->admin->getFilterParameters()])
        );
    }

    /**
     * @param Request $request
     *
     * @return array
     */
    private function changeBooleanSubtree(Request $request)
    {
        if (!$this->admin->isGranted('EDIT')) {
            throw new AccessDeniedException();
        }

        $value = explode('-', $request->request->get('value'));
        if (
            (count($value) !== 2) or
            (($value[0] != '0') and ($value[0] != '1')) or
            (($value[1] != '0') and ($value[1] != '1'))
        ) {
            throw new \InvalidArgumentException(__METHOD__ . ': The boolean subtree field has incorrect value!');
        }

        $result['pk'] = $request->request->get('pk');
        $result['value'] = $value[0] == 1;
        $result['withDescendants'] = $value[1] == 1;

        return $result;
    }

    /**
     * @param Request $request
     * @param integer $id - the $id of the Group witch active status is to be changed
     *
     * @return Response
     */
    public function changeActiveAction(Request $request, $id)
    {
        $result = $this->changeBooleanSubtree($request);
        $result['ids'] = $this->get('app.group_manager')->changeActive(
            $id,
            $result['value'],
            $result['withDescendants']
        );
        $result['fieldName'] = 'active';

        return $this->renderJson($result);
    }

    /**
     * @param Request $request
     * @param integer $id - the $id of the Group witch active status is to be changed
     *
     * @return Response
     */
    public function changePublicAction(Request $request, $id)
    {
        $result = $this->changeBooleanSubtree($request);
        $result['ids'] = $this->get('app.group_manager')->changePublic(
            $id,
            $result['value'],
            $result['withDescendants']
        );
        $result['fieldName'] = 'public';

        return $this->renderJson($result);
    }
}
