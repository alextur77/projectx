<?php

namespace AppBundle\Controller\Admin;

use AppBundle\DataTransferObject\DTOGroup;
use Sonata\AdminBundle\Datagrid\ProxyQueryInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;

/**
 * Class UserGroupController
 * @package AppBundle\Controller\Admin
 */
class UserGroupController extends BaseGroupController
{
    /**
     * @param integer $id - the $id of the Group witch the user quits
     *
     * @return RedirectResponse
     */
    public function quitAction($id)
    {
        if (!$this->admin->isGranted('LIST')) {
            throw new AccessDeniedException();
        }

        $group = $this->get('app.group_repository')->find($id);
        $user = $this->get('app.current_user_service')->getCurrentUser();
        $this->get('app.group_user_permission_manager')->removeUser($group, $user);

        return new RedirectResponse($this->admin->generateUrl('list', $this->admin->getFilterParameters()));
    }

    /**
     * @param ProxyQueryInterface $selectedModelQuery
     *
     * @return RedirectResponse
     */
    public function batchActionQuit(ProxyQueryInterface $selectedModelQuery)
    {
        if (!$this->admin->isGranted('LIST')) {
            throw new AccessDeniedException();
        }

        /** @var DTOGroup[] $groups */
        $groups = $selectedModelQuery->execute();
        /** @var integer[] $groupIds */
        $groupIds = [];
        foreach ($groups as $group) {
            $groupIds[] = $group->getId();
        }
        $user = $this->get('app.current_user_service')->getCurrentUser();
        $this->get('app.group_user_permission_manager')->removeUser($groupIds, $user);

        return new RedirectResponse($this->admin->generateUrl('list', $this->admin->getFilterParameters()));
    }
}
