<?php

namespace AppBundle\Controller\Admin;

use Sonata\AdminBundle\Controller\CRUDController;
use Symfony\Component\HttpFoundation\RedirectResponse;

/**
 * Class OrganizationUserController
 */
class OrganizationUserController extends CRUDController
{
    /**
     * @param int $id
     *
     * @return RedirectResponse
     */
    public function removeAction($id)
    {
        $this->admin->checkAccess('list');

        $this->tryCatch(
            function () use ($id) {
                $user = $this->get('app.user_repository')->find($id);
                $this->get('app.organization_user_service')->removeUserFromOrganization($user);
            }
        );

        return $this->redirect($this->admin->generateUrl('list'));
    }

    /**
     * @param callable $function
     *
     * @throws \Exception
     */
    protected function tryCatch($function)
    {
        try {
            $function();
        } catch (\Exception $e) {
            if ('prod' != $this->get('kernel')->getEnvironment()) {
                throw $e;
            } else {
                $this->getLogger()->error($e->getMessage());
            }
        }
    }
}
