<?php

namespace AppBundle\Controller\Admin;

use Sonata\AdminBundle\Controller\CRUDController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;

/**
 * Class ProfileUserEditController
 */
class ProfileUserEditController extends CRUDController
{
    /**
     * List action.
     *
     * @param Request $request
     *
     * @return Response
     *
     * @throws AccessDeniedException If access is not granted
     */
    public function listAction(Request $request = null)
    {
        $id = $this->get('app.profile_user_edit_admin')->getCurrentUser()->getId();
        $request->request->set('id', $id);

        return parent::editAction($id, $request);
    }
}
