<?php

namespace AppBundle\Controller\Admin;

use AppBundle\Exception\UserLevelException;
use Sonata\AdminBundle\Controller\CRUDController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class FindGroupController
 * @package AppBundle\Controller\Admin
 */
class FindGroupController extends CRUDController
{
    /**
     * @param Request $request
     *
     * @return Response
     */
    public function listAction(Request $request = null)
    {
        $this->admin->checkAccess('list');
        $ids = $search = $search_group = $sort = $sort_group = null;
        $this->extractParameters($request, $ids, $search, $search_group, $sort, $sort_group);

        $organizations = [];
        if ($search) {
            $organizations = $this->get('app.organization_repository')->findSearchableOrganizations(
                $search,
                $this->getUser(),
                (boolean) $sort
            );
        }

        if ($this->isXmlHttpRequest($request)) {
            return new Response(json_encode($organizations));
        } else {
            return $this->render(
                ':Admin/FindGroup:step1.html.twig',
                ['organizations' => $organizations, 'search' => $search, 'sort' => $sort]
            );
        }
    }

    /**
     * @param Request $request
     * @param int     $organization_id
     *
     * @return Response
     */
    public function listGroupsAction(Request $request, $organization_id)
    {
        $this->admin->checkAccess('list');
        $ids = $search = $search_group = $sort = $sort_group = null;
        $this->extractParameters($request, $ids, $search, $search_group, $sort, $sort_group);

        $organization = $this->get('app.organization_repository')->find($organization_id);
        $groups = $this->get('app.group_repository')->findSearchable(
            $search_group,
            $this->getUser(),
            $organization,
            $ids,
            (boolean) $sort_group
        );

        foreach ($groups as &$group) {
            $group['selected'] = in_array($group['id'], $ids);
        }

        if ($this->isXmlHttpRequest($request)) {
            return new Response(json_encode($groups));
        } else {
            return $this->render(
                ':Admin/FindGroup:step2.html.twig',
                [
                    'organization' => $organization,
                    'groups' => $groups,
                    'search' => $search,
                    'search_group' => $search_group,
                    'sort' => $sort,
                    'sort_group' => $sort_group
                ]
            );
        }
    }

    /**
     * @param Request $request
     * @param int     $organization_id
     *
     * @return Response|RedirectResponse
     */
    public function requestOrganizationAction(Request $request, $organization_id)
    {
        $this->admin->checkAccess('list');
        $ids = $search = $search_group = $sort = $sort_group = null;
        $this->extractParameters($request, $ids, $search, $search_group, $sort, $sort_group);

        $organization = $this->get('app.organization_repository')->find($organization_id);

        try {
            $this->get('app.registration_request_manager')->createOrganizationRequest($this->getUser(), $organization);
        } catch (UserLevelException $e) {
            $this->get('session')->getFlashBag()->add('sonata_flash_error', $e->getMessage());

            return new RedirectResponse($this->admin->generateUrl('list', ['search' => $search, 'sort' => $sort]));
        }

        return $this->render(
            ':Admin/FindGroup:step3_organization.html.twig',
            ['organization' => $organization, 'search' => $search, 'sort' => $sort]
        );
    }

    /**
     * @param Request $request
     * @param integer $organization_id
     *
     * @return Response
     */
    public function requestGroupsAction(Request $request, $organization_id)
    {
        $this->admin->checkAccess('list');
        $ids = $search = $search_group = $sort = $sort_group = null;
        $this->extractParameters($request, $ids, $search, $search_group, $sort, $sort_group);

        $organization = $this->get('app.organization_repository')->find($organization_id);
        $groups = $this->get('app.group_repository')->findByIds($ids);

        try {
            $this->get('app.registration_request_manager')->createGroupsRequest($this->getUser(), $groups);
        } catch (UserLevelException $e) {
            $this->get('session')->getFlashBag()->add('sonata_flash_error', $e->getMessage());
            $url = $this->admin->generateUrl(
                'list-groups',
                [
                    'organization_id' => $organization_id,
                    'search' => $search,
                    'search_group' => $search_group,
                    'sort' => $sort,
                    'sort_group' => $sort_group,
                    'ids' => $ids
                ]
            );

            return new RedirectResponse($url);
        }

        return $this->render(
            ':Admin/FindGroup:step3_groups.html.twig',
            [
                'organization' => $organization,
                'groups' => $groups,
                'search' => $search,
                'search_group' => $search_group,
                'sort' => $sort,
                'sort_group' => $sort_group
            ]
        );
    }

    /**
     * @param Request   $request
     * @param integer[] $ids
     * @param string    $search
     * @param string    $search_group
     * @param integer   $sort       - 1 = asc, 0 = desc
     * @param integer   $sort_group - 1 = asc, 0 = desc
     */
    private function extractParameters(Request &$request, &$ids, &$search, &$search_group, &$sort, &$sort_group)
    {
        if (null === $request) {
            $request = $this->getRequest();
        }
        if ('POST' === $request->getMethod()) {
            $ids = $request->request->get('ids', []);
            $search = $request->request->get('search', '');
            $search_group = $request->request->get('search_group', '');
            $sort = $request->request->get('sort', 1);
            $sort_group = $request->request->get('sort_group', 1);
        } else {
            $ids = $request->query->get('ids', []);
            $search = $request->query->get('search', '');
            $search_group = $request->query->get('search_group', '');
            $sort = $request->query->get('sort', 1);
            $sort_group = $request->query->get('sort_group', 1);
        }
    }
}
