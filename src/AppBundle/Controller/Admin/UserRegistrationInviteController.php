<?php

namespace AppBundle\Controller\Admin;

use AppBundle\Entity\RegistrationInvite;
use Sonata\AdminBundle\Controller\CRUDController;
use Sonata\AdminBundle\Datagrid\ProxyQueryInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Cookie;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;

/**
 * Class UserRegistrationInviteController
 * @package AppBundle\Controller\Admin
 */
class UserRegistrationInviteController extends CRUDController
{
    /**
     * @param string $token
     *
     * @return RedirectResponse
     */
    public function inviteAction($token)
    {
        $redirectRoute = null;
        $currUser = $this->get('app.user_registration_invite_admin')->getCurrentUser();

        if (is_object($currUser)) {
            // There is a user logged in the application.
            $redirectRoute = 'sonata_admin_dashboard';
        } else {
            // There is no user logged in the application.
            $redirectRoute = 'fos_user_security_login';
            $user = $this->get('app.registration_invite_manager')->getUserByToken($token);
            if (null === $user) {
                $email = $this->get('app.registration_invite_manager')->getCanonicalEmailByToken($token);
                if (null !== $email) {
                    // The correct registration invite token is specified.
                    $user = $this->get('app.user_repository')->findUserByCanonicalEmail($email);
                    if (null === $user) {
                        // The registration invite was sent to an unknown user.
                        $redirectRoute = 'fos_user_registration_register';
                    }
                }
            }
        }

        $response = $this->redirectToRoute($redirectRoute);
        $response->headers->setCookie(new Cookie('token', $token, 0, '/', null, false, false));

        return $response;
    }

    /**
     * @param Request $request
     * @param int     $id
     *
     * @return RedirectResponse|Response
     */
    public function acceptAction(Request $request, $id)
    {
        $this->get('app.registration_invite_manager')->acceptById($id);
        if ($this->isXmlHttpRequest($request)) {
            $statusMsgID = RegistrationInvite::getStatusesList()[RegistrationInvite::STATUS_ACCEPTED];
            $result = [
                'id' => $id,
                'status_text' => $this->admin->trans($statusMsgID),
                'status_code' => RegistrationInvite::STATUS_ACCEPTED
            ];

            return new Response(json_encode($result));
        } else {
            return new RedirectResponse($this->admin->generateUrl('list', $this->admin->getFilterParameters()));
        }
    }

    /**
     * @param Request $request
     * @param int     $id
     *
     * @return RedirectResponse|Response
     */
    public function declineAction(Request $request, $id)
    {
        $this->get('app.registration_invite_manager')->declineById($id);
        if ($this->isXmlHttpRequest($request)) {
            $statusMsgID = RegistrationInvite::getStatusesList()[RegistrationInvite::STATUS_DECLINED];
            $result = [
                'id' => $id,
                'status_text' => $this->admin->trans($statusMsgID),
                'status_code' => RegistrationInvite::STATUS_DECLINED
            ];

            return new Response(json_encode($result));
        } else {
            return new RedirectResponse($this->admin->generateUrl('list', $this->admin->getFilterParameters()));
        }
    }

    /**
     * @param string $token
     *
     * @return RedirectResponse
     *
     * @throws AccessDeniedException
     */
    public function declineAllAction($token)
    {
        $currUser = $this->get('app.user_registration_invite_admin')->getCurrentUser();
        if (!is_object($currUser)) {
            throw new AccessDeniedException();
        }

        $invites = $this->get('app.registration_invite_repository')->findByToken($token);
        $this->get('app.registration_invite_manager')->decline($invites);
        $this->sendMessage('sonata_flash_success', 'registration_invite.token.declined_successfully');

        $response = $this->redirectToRoute('sonata_admin_dashboard');
        $response->headers->clearCookie('token');

        return $response;
    }

    /**
     * @param string $token
     *
     * @return RedirectResponse
     *
     * @throws AccessDeniedException
     */
    public function acceptAllAction($token)
    {
        $currUser = $this->get('app.user_registration_invite_admin')->getCurrentUser();
        if (!is_object($currUser)) {
            throw new AccessDeniedException();
        }

        $invites = $this->get('app.registration_invite_repository')->findByToken($token);
        if ($this->get('app.registration_invite_manager')->accept($invites)) {
            $this->sendMessage('sonata_flash_success', 'registration_invite.token.accepted_successfully');
        } else {
            $this->sendMessage('sonata_flash_info', 'registration_invite.token.already_registered');
        }

        $response = $this->redirectToRoute('sonata_admin_dashboard');
        $response->headers->clearCookie('token');

        return $response;
    }

    /**
     * @param ProxyQueryInterface $selectedModelQuery
     *
     * @return RedirectResponse
     */
    public function batchActionAccept(ProxyQueryInterface $selectedModelQuery)
    {
        if (!$this->admin->isGranted('LIST')) {
            throw new AccessDeniedException();
        }

        /** @var RegistrationInvite[] $invites */
        $invites = $selectedModelQuery->execute();
        $this->get('app.registration_invite_manager')->accept($invites);
        $this->sendMessage('sonata_flash_success', 'registration_invite.batch.accepted_successfully');

        return new RedirectResponse($this->admin->generateUrl('list', $this->admin->getFilterParameters()));
    }

    /**
     * @param ProxyQueryInterface $selectedModelQuery
     *
     * @return RedirectResponse
     */
    public function batchActionDecline(ProxyQueryInterface $selectedModelQuery)
    {
        if (!$this->admin->isGranted('LIST')) {
            throw new AccessDeniedException();
        }

        /** @var RegistrationInvite[] $invites */
        $invites = $selectedModelQuery->execute();
        $this->get('app.registration_invite_manager')->decline($invites);
        $this->sendMessage('sonata_flash_success', 'registration_invite.batch.declined_successfully');

        return new RedirectResponse($this->admin->generateUrl('list', $this->admin->getFilterParameters()));
    }

    /**
     * Adds a flash message for type.
     *
     * @param string $type
     * @param string $message
     * @param array  $parameters
     */
    protected function sendMessage($type, $message, $parameters = [])
    {
        parent::addFlash($type, $this->admin->trans($message, $parameters));
    }
}
