<?php

namespace AppBundle\Controller\Admin;

use AppBundle\Admin\TopicPermissionAdmin;
use AppBundle\DataTransferObject\DTOTopicPermission;
use Sonata\AdminBundle\Controller\CRUDController;
use Sonata\AdminBundle\Datagrid\ProxyQueryInterface;
use Sonata\DoctrineORMAdminBundle\Model\ModelManager;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class TopicPermissionController
 * @package AppBundle\Controller\Admin
 */
class TopicPermissionController extends CRUDController
{
    /**
     * @param Request $request
     *
     * @return Response
     * @throws \LogicException
     */

    /**
     * @param Request $request
     * @param integer $groupId
     *
     * @return Response
     */
    public function groupMembersAction($groupId, Request $request)
    {
        if ($this->isXmlHttpRequest($request)) {
            $users = $this->get('app.group_user_permission_repository')->getUserListByGroups($groupId);

            return new Response(json_encode($users));
        } else {
            throw new \LogicException(__METHOD__ . ': Not AJAX call!');
        }
    }

    /**
     * @param Request $request
     *
     * @return Response
     * @throws \LogicException
     */
    public function updateModeratorsAction(Request $request)
    {
        if ($this->isXmlHttpRequest($request)) {
            /** @var integer[] $selectedGroupIds */
            $selectedGroupIds = $request->request->get('groupIds', []);
            $topicPermissionAdmin = $this->get('app.topic_permission_admin');
            if (!$topicPermissionAdmin->hasRequest()) {
                $topicPermissionAdmin->setRequest($request);
            }
            list(, $moderators) = $this->get('app.topic_permission_mutex_manager')->getParticipantChoices(
                $topicPermissionAdmin->getTopic(),
                $selectedGroupIds
            );

            $moderatorChoices = [];
            foreach ($moderators as $id => $text) {
                $moderatorChoices[] = ['id' => $id, 'text' => $text];
            }

            return new Response(json_encode($moderatorChoices));
        } else {
            throw new \LogicException(__METHOD__ . ': Not AJAX call!');
        }
    }

    /**
     * @param integer $topicId
     * @param string  $type
     * @param integer $id
     *
     * @return RedirectResponse
     */
    public function excludeAction($topicId, $type, $id)
    {
        $admin = $this->admin;
        $admin->checkAccess('delete');

        switch ($type) {
            case DTOTopicPermission::TYPE_GROUP:
                $this->get('app.topic_group_permission_manager')->delete($id);
                break;
            case DTOTopicPermission::TYPE_MODERATOR:
                $this->get('app.topic_moderator_manager')->delete($id);
                break;
            default:
                throw new \InvalidArgumentException(__METHOD__ . ': The parameter "type" value is invalid!');
        }

        $this->get('app.topic_moderator_manager')->deleteProhibited($topicId);

        $parameters['filter'] = $admin->getFilterParameters();
        $parameters['topicId'] = $topicId;

        return new RedirectResponse($admin->generateUrl('list', $parameters));
    }

    /**
     * @param ProxyQueryInterface $query
     * @param Request             $request
     *
     * @return RedirectResponse
     * @throws \InvalidArgumentException
     */
    public function batchActionExclude(
        /** @noinspection PhpUnusedParameterInspection */
        ProxyQueryInterface $query,
        Request $request)
    {
        $admin = $this->admin;
        $admin->checkAccess('batchDelete');

        $topicId = $request->query->get('topicId');
        if (null === $topicId) {
            throw new \InvalidArgumentException(__METHOD__ . ': The parameter "topicId" is absent!');
        }
        if (!$request->request->has('all_elements')) {
            throw new \InvalidArgumentException(__METHOD__ . ': The parameter "all_elements" is absent!');
        }

        $ok = true;
        $topicModeratorManager = $this->get('app.topic_moderator_manager');
        if (false === $request->request->get('all_elements')) {
            if (!$request->request->has('idx')) {
                throw new \InvalidArgumentException(__METHOD__ . ': The parameter "idx" is absent!');
            }
            $idx = $request->request->get('idx');
            $groupPermissionIds = [];
            $moderatorIds = [];
            foreach ($idx as $id) {
                $ids = explode(ModelManager::ID_SEPARATOR, $id);
                if ($ids[0] != 0) {
                    $groupPermissionIds[] = $ids[0];
                }
                if ($ids[1] != 0) {
                    $moderatorIds[] = $ids[1];
                }
            }
            if (!empty($groupPermissionIds)) {
                $topicGroupPermissionManager = $this->get('app.topic_group_permission_manager');
                $totalGroupPermissionsQuantity = $topicGroupPermissionManager->getGroupQuantity($topicId);
                if ($totalGroupPermissionsQuantity == count($groupPermissionIds)) {
                    array_shift($groupPermissionIds);
                    $this->sendMessage('sonata_flash_info', 'topic_permission.batch.delete_warning');
                    $ok = false;
                }
                $topicGroupPermissionManager->delete($groupPermissionIds);
            }
            if (!empty($moderatorIds)) {
                $topicModeratorManager->delete($moderatorIds);
            }
        } else {
            $topicGroupPermissions = $this->get('app.topic_group_permission_repository')->findBy(['topic' => $topicId]);
            if (!empty($topicGroupPermissions)) {
                array_shift($topicGroupPermissions);
                $this->sendMessage('sonata_flash_info', 'topic_permission.batch.delete_warning');
                $ok = false;
                if (!empty($topicGroupPermissions)) {
                    $this->get('app.topic_group_permission_manager')->delete($topicGroupPermissions);
                }
            }
            $topicModeratorManager->deleteByTopic($topicId);
        }

        $topicModeratorManager->deleteProhibited($topicId);
        if ($ok) {
            $this->sendMessage('sonata_flash_success', 'topic_permission.batch.delete_success');
        }

        $parameters['filter'] = $admin->getFilterParameters();
        $parameters['topicId'] = $topicId;

        return new RedirectResponse($admin->generateUrl('list', $parameters));
    }

    /**
     * @inheritdoc
     */
    public function listAction(Request $request = null)
    {
        if ($this->CheckAccess()) {
            return parent::listAction($request);
        } else {
            return $this->AccessDenied();
        }
    }

    /**
     * @inheritdoc
     */
    public function deleteAction($id, Request $request = null)
    {
        if ($this->CheckAccess()) {
            return parent::deleteAction($id, $request);
        } else {
            return $this->AccessDenied();
        }
    }

    /**
     * @return boolean
     */
    private function CheckAccess()
    {
        /** @var TopicPermissionAdmin $admin */
        $admin = $this->admin;

        return $admin->isCurrentUserOrganizationAdmin() || $admin->getOwnGroupPermissionsQuantity() > 0;
    }

    /**
     * @return Response
     */
    private function AccessDenied()
    {
        /** @var TopicPermissionAdmin $admin */
        $admin = $this->admin;

        return $this->render(
            ':Admin:access_denied.html.twig',
            ['text' => $admin->trans('topic_permission.access_denied_text')]
        );
    }

    /**
     * Adds a flash message for type.
     *
     * @param string $type
     * @param string $message
     * @param array  $parameters
     */
    protected function sendMessage($type, $message, $parameters = [])
    {
        parent::addFlash($type, $this->admin->trans($message, $parameters));
    }
}
