<?php

namespace AppBundle\Controller\Admin;

use Sonata\AdminBundle\Controller\CRUDController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Cookie;

/**
 * Class EventOrganizationsController
 */
class EventOrganizationsController extends CRUDController
{
    /**
     * @param Request|null $request
     *
     * @return null|Response
     */
    public function listAction(Request $request = null)
    {
        $preResponse = $this->preList($request);
        if ($preResponse !== null) {
            return $preResponse;
        }

        if ($listMode = $request->get('_list_mode')) {
            $this->admin->setListMode($listMode);
        }

        $datagrid = $this->admin->getDatagrid();
        $formView = $datagrid->getForm()->createView();

        // set the theme for the current Admin Form
        $this->get('twig')->getExtension('form')->renderer->setTheme($formView, $this->admin->getFilterTheme());

        $response = $this->render(
            $this->admin->getTemplate('inline_list'),
            array(
                'action' => 'list',
                'form' => $formView,
                'datagrid' => $datagrid,
                'csrf_token' => $this->getCsrfToken('sonata.batch'),
            ),
            null,
            $request
        );

        $response->headers->setCookie(new Cookie('event_id', $request->cookies->get('event_id')));

        return $response;
    }

}
