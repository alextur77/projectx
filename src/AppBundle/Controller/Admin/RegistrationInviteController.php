<?php

namespace AppBundle\Controller\Admin;

use Sonata\AdminBundle\Controller\CRUDController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\Flash\FlashBagInterface;

/**
 * Class RegistrationInviteController
 * @package AppBundle\Controller\Admin
 */
class RegistrationInviteController extends CRUDController
{
    /**
     * {@inheritdoc}
     */
    public function createAction(Request $request = null)
    {
        $response = parent::createAction($request);

        /** @var FlashBagInterface $flashBag */
        $flashBag = $this->get('session')->getFlashBag();
        if ($flashBag->has('sonata_flash_success')) {
            $inviteService = $this->get('app.registration_invite_service');
            $quantity = $inviteService->getSentEmailsQuantity();
            // Don't use parent::addFlash()! The success flash must be replaced!
            $flashBag->set(
                'sonata_flash_success',
                $this->admin->trans('registration_invite.token.email_sent', ['%Quantity%' => $quantity])
            );
        }

        return $response;
    }
}
