<?php

namespace AppBundle\Controller\Admin;

use Sonata\AdminBundle\Controller\CRUDController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Class UserOrganizationsController
 * @package AppBundle\Controller\Admin
 */
class UserOrganizationsController extends CRUDController
{
    /**
     * @param \AppBundle\Entity\OrganizationUserPermission $object
     *
     * @param                                              $right
     */
    protected function objectValidation($object, $right)
    {
        if (!$object) {
            throw new NotFoundHttpException(sprintf('unable to find the object'));
        }

        if (false === $this->admin->isGranted($right, $object)) {
            throw new AccessDeniedException();
        }

        if ($object->getUser() !== $this->getUser()) {
            throw new AccessDeniedException();
        }
    }

    /**
     * Show action.
     *
     * @param int|string|null $id
     * @param Request         $request
     *
     * @return \Symfony\Component\HttpFoundation\Response
     *
     * @throws NotFoundHttpException If the object does not exist
     * @throws AccessDeniedException If access is not granted
     */
    public function showAction($id = null, Request $request = null)
    {
        $id = $request->get($this->admin->getIdParameter());

        $object = $this->admin->getObject($id);

        $this->objectValidation($object, 'VIEW');

        return $this->render(
            $this->admin->getTemplate('show'),
            array(
                'action' => 'show',
                'object' => $object,
                'elements' => $this->admin->getShow(),
                'organization_name' => $object->getOrganization()->getName()
            ),
            null,
            $request
        );
    }
}
