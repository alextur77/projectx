<?php

namespace AppBundle\Controller\Admin;

use Sonata\AdminBundle\Controller\CRUDController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Symfony\Component\HttpFoundation\Response;
use AppBundle\Entity\Role;

/**
 * Class EventsController
 */
class EventsController extends CRUDController
{
    /**
     * @param \AppBundle\Entity\Event $object
     */
    protected function objectValidation($object)
    {
        if (!$object) {
            throw new NotFoundHttpException(sprintf('unable to find the object'));
        }

        if ($object->getUserPermission($this->getUser()) === null) {
            throw new AccessDeniedException();
        }
    }

    /**
     * @param Request|null $request
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|Response
     *
     * @throws \Exception
     */
    public function goAction(Request $request = null)
    {
        $id = $request->get($this->admin->getIdParameter());
        $event = $this->admin->getObject($id);
        $this->objectValidation($event);
        $user = $this->getUser();
        $userPermission = $event->getUserPermission($user);
        switch ($userPermission) {
            case ROLE::ROLE_EVENT_USER:
                return $this->showAction($id, $request);
            case ROLE::ROLE_EVENT_OWNER:
            case ROLE::ROLE_EVENT_ADMIN:
                return $this->editAction($id, $request);
        }

        throw new \Exception(sprintf('User role %s not found', $userPermission));
    }
}
