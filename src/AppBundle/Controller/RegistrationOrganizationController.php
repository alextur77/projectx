<?php

namespace AppBundle\Controller;

use AppBundle\Form\Type\RegistrationFormType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class RegistrationOrganizationController
 *
 */
class RegistrationOrganizationController extends Controller
{
    /**
     * @param Request $request
     *
     * @return Response
     *
     * @Route("/organization-invite")
     */
    public function showAction(Request $request)
    {
        $hash = $this->get('app.organization_invite')->findOneBy(['hash' => $request->query->get('hash')]);

        $this->get('app.organization_invite_manager')->updateOrganizationInvite($hash->getId());

        $user = $this->get('app.user_repository')->findOneBy(['email' => $hash->getEmail()]);

        if (!empty($user)) {

            $login = $user->getUsername();
            $emailHash = '';
        } else {

            $emailHash = $hash->getEmail();
            $login = '';
        }

        $form = $this->createForm(RegistrationFormType::class);

        return $this->render(
            '@App/organization.sign.form.html.twig',
            [
                'login' => $login,
                'email_hash' => $emailHash,
                'form' => $form->createView(),
                'check' => 'true',
                'check_pass' => 'true'
            ]
        );
    }

    /**
     * @param Request $request
     *
     * @return Response
     *
     * @Route("/organization-check-login")
     */
    public function checkLoginAction(Request $request)
    {
        $form = $this->createForm(RegistrationFormType::class);

        $login = $request->request->get('_username');
        $password = $request->request->get('_password');

        $encoder = $this->get('security.password_encoder');

        $user = $this->get('app.user_repository')->findOneBy(['username' => $login]);

        $pass = $encoder->encodePassword($user, $password);

        if ($pass == $user->getPassword()) {

            return $this->redirectToRoute('app_organization_new', ['user' => $user]);
        } else {

            return $this->render(
                '@App/organization.sign.form.html.twig',
                ['check' => 'false', 'login' => $login, 'form' => $form->createView(), 'check_pass' => 'true']
            );
        }
    }

    /**
     * @param Request $request
     *
     * @return Response
     *
     * @Route("/register-user")
     */
    public function registerAction(Request $request)
    {
        $form = $this->createForm(RegistrationFormType::class);

        $encoder = $this->get('security.password_encoder');
        $formData = $request->request->get('app_user_registration');

        $file = $request->files->get('app_user_registration');
        $fileObject = $file['profilePictureFile'];

        if ($formData['plainPassword']['first'] != $formData['plainPassword']['second']) {
            return $this->render(
                '@App/organization.sign.form.html.twig',
                [
                    'check_pass' => 'false',
                    'login' => $formData['username'],
                    'form' => $form->createView(),
                    'check' => 'true'
                ]
            );
        } else {

            if (isset($formData['sendSms'])) {
                $sendSms = 1;
            } else {
                $sendSms = 0;
            }

            if (isset($formData['sendEmail'])) {
                $sendEmail = 1;
            } else {
                $sendEmail = 0;
            }

            $this->get('app.user_manager')->add(
                $formData['username'],
                $formData['email'],
                $formData['plainPassword']['first'],
                $formData['firstName'],
                $formData['lastName'],
                $formData['phone'],
                $sendEmail,
                $sendSms,
                $formData['patronymic'],
                $formData['_token'],
                $fileObject
            );

            $user = $this->get('app.user_repository')->findOneBy(['username' => $formData['username']]);
            $pass = $encoder->encodePassword($user, $formData['plainPassword']['first']);

            $this->get('app.user_manager')->updateUserPassword($user, $pass);

            return $this->redirectToRoute('app_organization_new', ['user' => $user]);
        }
    }
}