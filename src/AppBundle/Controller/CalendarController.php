<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Event;
use AppBundle\Entity\Role;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class CalendarController
 */
class CalendarController extends Controller
{
    /**
     * @param Request $request
     *
     * @return \Symfony\Component\HttpFoundation\Response
     * @throws \Exception
     *
     * @Route("/calendar")
     */
    public function getCalendarEventAction(Request $request)
    {
        $startCalendarPeriod = new \DateTime(
            date($this->get('app.date_service')->getDateYearMonthDayFullFormat(), $request->query->get('start'))
        );
        $endCalendarPeriod = new \DateTime(
            date($this->get('app.date_service')->getDateYearMonthDayFullFormat(), $request->query->get('end'))
        );

        $organization = $this->get('app.organization_repository')->findOneBy(
            ['name' => $request->query->get('organization')]
        );

        $userRole = $this->get('app.organization_user_permission_repository')->getUserRoleInOrganization(
            $this->getUser(),
            $organization
        );

        $groupList = $this->get('app.group_repository')->findUserGroupsByOrganization($this->getUser(), $organization);

        $eventRepository = $this->get('app.event_repository');

        if ($userRole == Role::ROLE_ORGANIZATION_ADMIN) {

            $eventsPeriodMass = $eventRepository->findEventsPeriodListOrgAdmin(
                $startCalendarPeriod,
                $endCalendarPeriod,
                $organization
            );
        } else {
            $massGroup = [];

            foreach ($groupList as $value) {

                $massGroup[] = $value->getId();
            }

            $eventsPeriodMass = $eventRepository->findEventsPeriodList(
                $startCalendarPeriod,
                $endCalendarPeriod,
                $organization,
                $massGroup
            );
        }

        $response = new JsonResponse();

        $responsePeriodMass = [];

        foreach ($eventsPeriodMass as $key => $value) {

            /** @var \DateTime $timeStart */
            $timeStart = $value['timeStart'];

            /** @var \DateTime $timeEnd */
            $timeEnd = $value['timeEnd'];

            $getTimeStart = explode(' ', $this->get('app.date_service')->getFormattedYearMonthDatFullDate($timeStart));
            $getTimeEnd = explode(' ', $this->get('app.date_service')->getFormattedYearMonthDatFullDate($timeEnd));

            if ($getTimeStart[1] == '00:00:00' && $getTimeEnd[1] == '23:59:59') {
                $responsePeriodMass[$key]['allDay'] = 1;
            } else {
                $responsePeriodMass[$key]['allDay'] = 0;
            }

            $responsePeriodMass[$key]['title'] = $value['name'];
            $responsePeriodMass[$key]['description'] = $value['description'];
            $responsePeriodMass[$key]['start'] = $this->get('app.date_service')->getFormattedYearMonthDatFullDate(
                $timeStart
            );
            $responsePeriodMass[$key]['end'] = $this->get('app.date_service')->getFormattedYearMonthDatFullDate(
                $timeEnd
            );
            $type = $value['type'];

            if ($type == Event::TYPE_SPORT_EVENT) {
                $responsePeriodMass[$key]['color'] = '#' . $this->getParameter('color_sport');
            } elseif ($type == Event::TYPE_SCHOOL_EVENT) {
                $responsePeriodMass[$key]['color'] = $this->getParameter('color_school');
            } elseif ($type == Event::TYPE_CLASS_EVENT) {
                $responsePeriodMass[$key]['color'] = $this->getParameter('color_class');
            }
        }

        $response->setData($responsePeriodMass);

        return $response->send();
    }

    /**
     * @param Request $request
     *
     * @return Response
     *
     * @Route("/calendar-new")
     */
    public function newAction(Request $request)
    {
        $name = $request->query->get('title');
        $description = $request->query->get('description');
        $type = $request->query->get('type');
        $change = $request->query->get('change');

        $groupMass = $request->query->get('group');

        $organization = $this->get('app.organization_repository')->findOneBy(
            ['name' => $request->query->get('organization')]
        );

        $dateStartConcat = date(
                $this->get('app.date_service')->getDateYearMonthDayFormat(),
                strtotime($request->query->get('start'))
            ) . ' ' . $request->query->get(
                'timeStart'
            ) . ':00';
        $dateEndConcat = date(
                $this->get('app.date_service')->getDateYearMonthDayFormat(),
                strtotime($request->query->get('end'))
            ) . ' ' . $request->query->get(
                'timeEnd'
            ) . ':00';

        if ($request->query->get('fullDay') == 1) {

            $dateStart = new \DateTime(date('Y-m-d 00:00:00', strtotime($request->query->get('start'))));
            $dateEnd = new \DateTime(date('Y-m-d 23:59:59', strtotime($request->query->get('end'))));
        } else {
            $dateStart = new \DateTime(
                date($this->get('app.date_service')->getDateYearMonthDayFullFormat(), strtotime($dateStartConcat))
            );
            $dateEnd = new \DateTime(
                date($this->get('app.date_service')->getDateYearMonthDayFullFormat(), strtotime($dateEndConcat))
            );
        }
        if ($request->query->get('change') == null) {

            foreach ($groupMass as $value) {

                $group = $this->get('app.group_repository')->findOneBy(
                    ['name' => $value, 'organization' => $organization]
                );

                $nameEvent = $name . ' - ' . $group->getName();

                $this->get('app.event_manager')->add(
                    $nameEvent,
                    $this->getUser(),
                    $description,
                    $dateStart,
                    $dateEnd,
                    $type
                );

                $event = $this->get('app.event_repository')->findOneBy(['name' => $nameEvent]);

                unset($nameEvent);

                if (empty($group)) {

                    $relationType = 'O';
                } else {
                    $relationType = 'G';
                }

                $this->get('app.event_relation_manager')->add($event, $organization, $relationType, $group);
            }

        } else {

            $event = $this->get('app.event_repository')->findOneBy(['name' => $change]);

            $eventGroup = $this->get('app.event_relation_repository')->findOneBy(['event' => $event]);

            foreach ($groupMass as $value) {

                $group = $this->get('app.group_repository')->findOneBy(
                    ['name' => $value, 'organization' => $organization]
                );

                $groupEvent = $this->get('app.group_repository')->findOneBy(['id' => $eventGroup->getGroup()]);

                if ($group->getId() == $groupEvent->getId()) {

                    $this->get('app.event_manager')->update(
                        $event->getId(),
                        $name,
                        $this->getUser(),
                        $description,
                        $dateStart,
                        $dateEnd,
                        $type
                    );

                    if (empty($group)) {

                        $relationType = 'O';
                    } else {
                        $relationType = 'G';
                    }

                    $this->get('app.event_relation_manager')->update($event, $relationType, $group);

                } else {

                    $nameEvent = $name . '-' . $group->getName();

                    $this->get('app.event_manager')->add(
                        $nameEvent,
                        $this->getUser(),
                        $description,
                        $dateStart,
                        $dateEnd,
                        $type
                    );

                    $eventAdd = $this->get('app.event_repository')->findOneBy(['name' => $nameEvent]);

                    unset($nameEvent);

                    if (empty($group)) {

                        $relationType = 'O';
                    } else {
                        $relationType = 'G';
                    }

                    $this->get('app.event_relation_manager')->add($eventAdd, $organization, $relationType, $group);
                }
            }
        }

        return new Response();
    }

    /**
     * @return Response
     *
     * @Route("/calendar-type-list")
     */
    public function getEventType()
    {
        $response = new JsonResponse();
        $response->setData($this->get('app.event_manager')->getEventListConst());

        return $response->send();
    }

    /**
     * @param Request $request
     *
     * @return Response
     *
     * @Route("/calendar-group-list")
     */
    public function getUserGroupList(Request $request)
    {
        $organization = $this->get('app.organization_repository')->findOneBy(
            ['name' => $request->query->get('organization')]
        );
        $user = $this->getUser();

        $userRole = $this->get('app.organization_user_permission_repository')->getUserRoleInOrganization(
            $this->getUser(),
            $organization
        );

        $responsePeriodMass = [];

        if ($userRole == Role::ROLE_ORGANIZATION_ADMIN) {

            $groupListDTO = $this->get('app.group_repository')->findActiveByOrganization($organization);

            foreach ($groupListDTO as $value) {

                $group = $this->get('app.group_repository')->findOneBy(['id' => $value->getId()]);

                $userHasAccess = $this->get('app.group_user_permission_repository')->hasUserGroupAdminAccess(
                    $group,
                    $this->getUser()
                );

                if ($userHasAccess == true) {

                    $responsePeriodMass['name_' . $value->getId()] = $value->getName();
                }
            }
        } else {
            $groupList = $this->get('app.group_repository')->findUserGroupsByOrganization($user, $organization);
            foreach ($groupList as $value) {

                $userHasAccess = $this->get('app.group_user_permission_repository')->hasUserGroupAdminAccess(
                    $value,
                    $this->getUser()
                );

                if ($userHasAccess == true) {

                    $responsePeriodMass['name_' . $value->getId()] = $value->getName();
                }
            }
        }

        $response = new JsonResponse();
        $response->setData($responsePeriodMass);

        return $response->send();
    }

    /**
     * @param Request $request
     *
     * @return Response
     *
     * @Route("/calendar-edit")
     */
    public function editAction(Request $request)
    {
        $access = 0;
        $event = $this->get('app.event_repository')->findOneBy(['name' => $request->query->get('title')]);

        $groupID = $this->get('app.event_relation_repository')->findOneBy(['event' => $event]);

        $groupDB = $this->get('app.group_repository')->findOneBy(['id' => $groupID->getGroup()]);

        $organization = $this->get('app.organization_repository')->findOneBy(
            ['name' => $request->query->get('organization')]
        );

        $userRole = $this->get('app.organization_user_permission_repository')->getUserRoleInOrganization(
            $this->getUser(),
            $organization
        );

        if ($userRole == Role::ROLE_ORGANIZATION_ADMIN) {
            $access = 1;
        }

        if (!empty($groupDB)) {

            $userHasAccess = $this->get('app.group_user_permission_repository')->hasUserGroupAdminAccess(
                $groupDB,
                $this->getUser()
            );

            if ($userHasAccess == true) {

                $access = 1;
            }
        }

        if (!empty($groupDB)) {

            $group = $groupDB->getName();
        } else {
            $group = '';
        }

        $response = new JsonResponse();

        $responsePeriodMass = [];

        /** @var \DateTime $timeStart */
        $timeStart = $event->getTimeStart();

        /** @var \DateTime $timeEnd */
        $timeEnd = $event->getTimeEnd();

        $type = $event->getType();

        $getTimeStart = explode(' ', $this->get('app.date_service')->getFormattedYearMonthDatFullDate($timeStart));
        $getTimeEnd = explode(' ', $this->get('app.date_service')->getFormattedYearMonthDatFullDate($timeEnd));

        if ($getTimeStart[1] == '00:00:00' && $getTimeEnd[1] == '23:59:59') {
            $responsePeriodMass['allDay'] = 1;
        } else {
            $responsePeriodMass['allDay'] = 0;
        }

        if ($type == Event::TYPE_SPORT_EVENT) {
            $this->get('translator')->trans('accept');
            $responsePeriodMass['type'] = $this->get('translator')->trans('Sport');
        } elseif ($type == Event::TYPE_SCHOOL_EVENT) {
            $responsePeriodMass['type'] = $this->get('translator')->trans('School');
        } elseif ($type == Event::TYPE_CLASS_EVENT) {
            $responsePeriodMass['type'] = $this->get('translator')->trans('Class');
        }

        $responsePeriodMass['title'] = $event->getName();
        $responsePeriodMass['description'] = $event->getDescription();
        $responsePeriodMass['start'] = $this->get('app.date_service')->getFormattedDayMonthYearDate($timeStart);
        $responsePeriodMass['timeStart'] = $this->get('app.date_service')->getFormattedHourMinuteDate($timeStart);
        $responsePeriodMass['end'] = $this->get('app.date_service')->getFormattedDayMonthYearDate($timeEnd);
        $responsePeriodMass['timeEnd'] = $this->get('app.date_service')->getFormattedHourMinuteDate($timeEnd);
        $responsePeriodMass['typeID'] = $event->getType();
        $responsePeriodMass['group'] = $group;
        $responsePeriodMass['access'] = $access;

        $response->setData($responsePeriodMass);

        return $response->send();
    }

    /**
     * @param Request $request
     *
     * @return Response
     *
     * @Route("/calendar-form-date")
     */
    public function getFormattedDate(Request $request)
    {
        $date = $request->query->get('start');

        $formattedDate = $this->get('app.date_service')->getFormattedDateFromUnix($date);

        $response = new JsonResponse();

        $responsePeriodMass = [];

        $responsePeriodMass['date'] = $formattedDate;

        $response->setData($responsePeriodMass);

        return $response->send();
    }
}