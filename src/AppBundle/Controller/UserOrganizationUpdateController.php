<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class UserOrganizationUpdateController
 */
class UserOrganizationUpdateController extends Controller
{
    /**
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     *
     * @Route("/organization")
     */
    public function updateUserOrganizationAction(Request $request)
    {
        $organization = $request->query->get('organization');

        $this->get('app.user_manager')->updateUserOrganization($this->getUser()->getID(), $organization);

        return new Response();
    }
}