<?php

namespace AppBundle\Form\Type;

use Aristek\Bundle\W2uiBundle\Form\Type\CheckboxFormType;
use FOS\UserBundle\Form\Type\RegistrationFormType as BaseRegistrationFormType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;

/**
 * Class RegistrationFormType
 */
class RegistrationFormType extends BaseRegistrationFormType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        parent::buildForm($builder, $options);

        $builder
            ->add('firstName')
            ->add('lastName')
            ->add('patronymic')
            ->add('phone', TextType::class)
            ->add(
                'sendEmail',
                CheckboxFormType::class,
                [
                    'data' => true,
                    'required' => false
                ]
            )
            ->add(
                'sendSms',
                CheckboxFormType::class,
                [
                    'required' => false
                ]
            )
            ->add('profilePictureFile');
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'app_user_registration';
    }
}
