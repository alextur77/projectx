<?php

namespace AppBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

/**
 * Class DateTimePickerType
 */
class DateTimePickerType extends AbstractType
{
    /**
     * {@inheritDoc}
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(
            [
                'attr' => ['class' => 'datetimepicker', 'style' => 'width: 250px', 'date-format' => 'yyyy-mm-dd h:i:s'],
                'widget' => 'single_text',
                'date_widget' => 'text'
            ]
        );
    }

    /**
     * {@inheritDoc}
     */
    public function getName()
    {
        return 'datetimepicker';
    }

    /**
     * {@inheritDoc}
     */
    public function getParent()
    {
        return 'datetime';
    }
}