<?php

namespace AppBundle\EventListener\Security;

use AppBundle\Entity\RegistrationRequest;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;

/**
 * Class RegistrationRequestListener
 */
class RegistrationRequestListener
{
    /**
     * @var ContainerInterface
     */
    protected $container;

    /**
     * @param RegistrationRequest $registrationRequest
     */
    public function preUpdate(RegistrationRequest $registrationRequest)
    {
        $this->checkSecurity($registrationRequest);
    }

    /**
     * @param RegistrationRequest $registrationRequest
     */
    public function preRemove(RegistrationRequest $registrationRequest)
    {
        $this->checkSecurity($registrationRequest);
    }

    /**
     * @param RegistrationRequest $registrationRequest
     */
    protected function checkSecurity(RegistrationRequest $registrationRequest)
    {
        $user = $this->container->get('security.token_storage')->getToken()->getUser();
        $registrationRequestRepository = $this->container->get('app.registration_request_repository');

        if (!$registrationRequestRepository->checkAccess($registrationRequest, $user)) {
            throw new AccessDeniedException();
        }
    }

    /**
     * @param ContainerInterface $container
     *
     * @return RegistrationRequestListener
     */
    public function setContainer($container)
    {
        $this->container = $container;

        return $this;
    }
}
