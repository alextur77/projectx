<?php

namespace AppBundle\Command;

use AppBundle\Entity\Role;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Class InstallCommand
 */
class InstallCommand extends ContainerAwareCommand
{
    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this
            ->setName('app:install')
            ->setDescription('Install user roles')
            ->addArgument('role', InputArgument::REQUIRED);
    }

    /**
     * {@inheritdoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $role = $input->getArgument('role');

        if (strtolower($role) == strtolower(Role::TYPE_ORGANIZATION)) {
            if (empty($this->getContainer()->get('app.role_manager')->checkIfNotExistRole(
                Role::ROLE_ORGANIZATION_ADMIN
            ))
            ) {
                $this->getContainer()->get('app.role_manager')->add(
                    'admin',
                    Role::TYPE_ORGANIZATION,
                    Role::ROLE_ORGANIZATION_ADMIN
                );
            }
            if (empty($this->getContainer()->get('app.role_manager')->checkIfNotExistRole(
                Role::ROLE_ORGANIZATION_USER
            ))
            ) {
                $this->getContainer()->get('app.role_manager')->add(
                    'user',
                    Role::TYPE_ORGANIZATION,
                    Role::ROLE_ORGANIZATION_USER
                );
            }
        } elseif (strtolower($role) == strtolower(Role::TYPE_GROUP)) {
            if (empty($this->getContainer()->get('app.role_manager')->checkIfNotExistRole(Role::ROLE_GROUP_ADMIN))) {
                $this->getContainer()->get('app.role_manager')->add('admin', Role::TYPE_GROUP, Role::ROLE_GROUP_ADMIN);
            }
            if (empty($this->getContainer()->get('app.role_manager')->checkIfNotExistRole(Role::ROLE_GROUP_USER))) {
                $this->getContainer()->get('app.role_manager')->add('user', Role::TYPE_GROUP, Role::ROLE_GROUP_USER);
            }
        } elseif (strtolower($role) == strtolower(Role::TYPE_EVENT)) {
            if (empty($this->getContainer()->get('app.role_manager')->checkIfNotExistRole(Role::ROLE_EVENT_ADMIN))) {
                $this->getContainer()->get('app.role_manager')->add('admin', Role::TYPE_EVENT, Role::ROLE_EVENT_ADMIN);
            }
            if (empty($this->getContainer()->get('app.role_manager')->checkIfNotExistRole(Role::ROLE_EVENT_USER))) {
                $this->getContainer()->get('app.role_manager')->add('user', Role::TYPE_EVENT, Role::ROLE_EVENT_USER);
            }
            if (empty($this->getContainer()->get('app.role_manager')->checkIfNotExistRole(Role::ROLE_EVENT_OWNER))) {
                $this->getContainer()->get('app.role_manager')->add('owner', Role::TYPE_EVENT, Role::ROLE_EVENT_OWNER);
            }
        } elseif (strtolower($role) == strtolower(Role::TYPE_TOPIC)) {
            if (empty($this->getContainer()->get('app.role_manager')->checkIfNotExistRole(Role::ROLE_TOPIC_ADMIN))) {
                $this->getContainer()->get('app.role_manager')->add('admin', Role::TYPE_TOPIC, Role::ROLE_TOPIC_ADMIN);
            }
            if (empty($this->getContainer()->get('app.role_manager')->checkIfNotExistRole(Role::ROLE_TOPIC_USER))) {
                $this->getContainer()->get('app.role_manager')->add('user', Role::TYPE_TOPIC, Role::ROLE_TOPIC_USER);
            }
        }
    }
}
