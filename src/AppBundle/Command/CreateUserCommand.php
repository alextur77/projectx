<?php

namespace AppBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Class CreateUserCommand
 */
class CreateUserCommand extends ContainerAwareCommand
{
    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this
            ->setName('app:create-user')
            ->setDescription('Create a new user')
            ->addArgument('username', InputArgument::REQUIRED, 'User name')
            ->addArgument('password', InputArgument::REQUIRED)
            ->addArgument('email', InputArgument::REQUIRED)
            ->addArgument('first-name', InputArgument::REQUIRED)
            ->addArgument('last-name', InputArgument::REQUIRED)
            ->addArgument('phone', InputArgument::REQUIRED)
            ->addArgument('patronymic', InputArgument::REQUIRED);
    }

    /**
     * {@inheritdoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $userName = $input->getArgument('username');
        $userPassword = $input->getArgument('password');
        $email = $input->getArgument('email');
        $firstName = $input->getArgument('first-name');
        $lastName = $input->getArgument('last-name');
        $phone = $input->getArgument('phone');
        $patronymic = $input->getArgument('patronymic');
        $role[] = 'ROLE_SYSTEM_ADMIN';

        $this->getContainer()
            ->get('app.user_manager')
            ->add(
                $userName,
                $email,
                $userPassword,
                $firstName,
                $lastName,
                $phone,
                0,
                0,
                $patronymic,
                null,
                null,
                $role
            );

        $user = $this->getContainer()->get('app.user_repository')->findOneBy(['username' => $userName]);

        $encoder = $this->getContainer()->get('security.password_encoder');

        $this->getContainer()->get('app.user_manager')->updateUserPassword(
            $user,
            $encoder->encodePassword(
                $user,
                $userPassword
            )
        );
    }
}