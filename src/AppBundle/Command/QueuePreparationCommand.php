<?php

namespace AppBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Input\InputArgument;

/**
 * Class QueuePreparationCommand
 *
 * AppBundle\Command
 */
class QueuePreparationCommand extends ContainerAwareCommand
{
    /**
     * @return void
     */
    protected function configure()
    {
        $this
            ->setName('app:prepare-queue')
            ->setDescription('Prepare queue')
            ->setHelp('This command allows you to prepare queue');
    }

    /**
     * @param InputInterface  $input
     * @param OutputInterface $output
     *
     * @return void
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $prepareParams = $this->getContainer()->get('post.prepare_queue_service');

        $prepareParams->prepareQueue();
    }
}