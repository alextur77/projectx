var global = (function (obj) {
    obj.showConfirm = showConfirm;

    return obj;

    function showConfirm(title, body, action) {
        var $global = $('#global-modal');
        $global.find('.modal-title').html(title);
        $global.find('.modal-body p').html(body);
        $global.modal('show');

        var callBack = null;
        if (typeof action == 'function') {
            callBack = action;
        } else {
            callBack = function() {
                document.location = action;
            }
        }

        $global.find('.btn-yes').off('click').click(callBack);
    }

})(global || {});
